package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 1/28/2016.
 */
public class ArticlesModel {

    private String id,title,description,author_id,created_at,picture,like_count,comment_count,share_count,category_id,author_fname,author_lname,author_photo,author_group_id;

    public ArticlesModel(String id, String title, String description, String author_id,
                         String created_at, String picture, String like_count,
                         String comment_count, String share_count, String category_id,
                         String author_fname, String author_lname, String author_photo,
                         String author_group_id) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author_id = author_id;
        this.created_at = created_at;
        this.picture = picture;
        this.like_count = like_count;
        this.comment_count = comment_count;
        this.share_count = share_count;
        this.category_id = category_id;
        this.author_fname = author_fname;
        this.author_lname = author_lname;
        this.author_photo = author_photo;
        this.author_group_id = author_group_id;
    }

    public ArticlesModel(String id, String title, String description, String author_id, String created_at, String picture, String like_count, String comment_count, String share_count, String category_id) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author_id = author_id;
        this.created_at = created_at;
        this.picture = picture;
        this.like_count = like_count;
        this.comment_count = comment_count;
        this.share_count = share_count;
        this.category_id = category_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getShare_count() {
        return share_count;
    }

    public void setShare_count(String share_count) {
        this.share_count = share_count;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getAuthor_fname() {
        return author_fname;
    }

    public void setAuthor_fname(String author_fname) {
        this.author_fname = author_fname;
    }

    public String getAuthor_lname() {
        return author_lname;
    }

    public void setAuthor_lname(String author_lname) {
        this.author_lname = author_lname;
    }

    public String getAuthor_photo() {
        return author_photo;
    }

    public void setAuthor_photo(String author_photo) {
        this.author_photo = author_photo;
    }

    public String getAuthor_group_id() {
        return author_group_id;
    }

    public void setAuthor_group_id(String author_group_id) {
        this.author_group_id = author_group_id;
    }
}
