package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 1/25/2016.
 */
public class PrescribedDrugModel {
    private String drug_name,power,dosage,noOfDays;

    public PrescribedDrugModel(String drug_name, String power, String dosage, String noOfDays) {
        this.drug_name = drug_name;
        this.power = power;
        this.dosage = dosage;
        this.noOfDays = noOfDays;
    }

    public String getDrug_name() {
        return drug_name;
    }

    public void setDrug_name(String drug_name) {
        this.drug_name = drug_name;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }
}
