package com.theuserhub.jolpie.Models;

/**
 * Created by tumpa on 2/24/2016.
 */
public class Album {
    String id;
    String album_name;
    String user_id;
    String picture;

    public Album(String id, String album_name, String user_id, String picture) {
        this.id = id;
        this.album_name = album_name;
        this.user_id = user_id;
        this.picture = picture;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }
}
