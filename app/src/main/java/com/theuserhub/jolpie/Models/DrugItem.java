package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 11/18/2015.
 */
public class DrugItem {
    public DrugItem(String drug_id, String generic_id, String brand_name, String description, String packet_image, String packet_title, String packet_description, String drug_image, String overview, String side_effect, String dosage, String user_id, String drugs_profile, String generic_name, String first_name) {
        this.drug_id = drug_id;
        this.generic_id = generic_id;
        this.brand_name = brand_name;
        this.description = description;
        this.packet_image = packet_image;
        this.packet_title = packet_title;
        this.packet_description = packet_description;
        this.drug_image = drug_image;
        this.overview = overview;
        this.side_effect = side_effect;
        this.dosage = dosage;
        this.user_id = user_id;
        this.drugs_profile = drugs_profile;
        this.generic_name = generic_name;
        this.first_name = first_name;
    }

    public String getDrug_id() {
        return drug_id;
    }

    public void setDrug_id(String drug_id) {
        this.drug_id = drug_id;
    }

    public String getGeneric_id() {
        return generic_id;
    }

    public void setGeneric_id(String generic_id) {
        this.generic_id = generic_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPacket_image() {
        return packet_image;
    }

    public void setPacket_image(String packet_image) {
        this.packet_image = packet_image;
    }

    public String getPacket_title() {
        return packet_title;
    }

    public void setPacket_title(String packet_title) {
        this.packet_title = packet_title;
    }

    public String getPacket_description() {
        return packet_description;
    }

    public void setPacket_description(String packet_description) {
        this.packet_description = packet_description;
    }

    public String getDrug_image() {
        return drug_image;
    }

    public void setDrug_image(String drug_image) {
        this.drug_image = drug_image;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getSide_effect() {
        return side_effect;
    }

    public void setSide_effect(String side_effect) {
        this.side_effect = side_effect;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDrugs_profile() {
        return drugs_profile;
    }

    public void setDrugs_profile(String drugs_profile) {
        this.drugs_profile = drugs_profile;
    }

    public String getGeneric_name() {
        return generic_name;
    }

    public void setGeneric_name(String generic_name) {
        this.generic_name = generic_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    String drug_id, generic_id, brand_name, description, packet_image,packet_title,packet_description,drug_image,overview,side_effect,dosage,user_id,drugs_profile,generic_name,first_name;


}
