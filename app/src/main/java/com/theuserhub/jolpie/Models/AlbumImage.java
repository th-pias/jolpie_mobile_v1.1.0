package com.theuserhub.jolpie.Models;

/**
 * Created by tumpa on 2/24/2016.
 */
public class AlbumImage {
    String id;
    String picture;
    String picture_caption;
    String title;
    String description;
    String album_id;
    String created_at;
    String user_id;

    public AlbumImage(String id, String user_id, String created_at, String album_id, String description, String title, String picture_caption, String picture) {
        this.id = id;
        this.user_id = user_id;
        this.created_at = created_at;
        this.album_id = album_id;
        this.description = description;
        this.title = title;
        this.picture_caption = picture_caption;
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPicture_caption() {
        return picture_caption;
    }

    public void setPicture_caption(String picture_caption) {
        this.picture_caption = picture_caption;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
