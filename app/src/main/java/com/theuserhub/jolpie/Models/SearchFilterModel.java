package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 5/11/2016.
 */
public class SearchFilterModel {
    private String name;
    private int count;
    private boolean iscked;

    public SearchFilterModel(String name, int count, boolean iscked) {
        this.name = name;
        this.count = count;
        this.iscked = iscked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean iscked() {
        return iscked;
    }

    public void setIscked(boolean iscked) {
        this.iscked = iscked;
    }
}
