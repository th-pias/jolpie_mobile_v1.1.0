package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 4/11/2016.
 */
public class CabinBedModel {
    private String id,service_id,service_name,service_des,service_price,user_id;

    public CabinBedModel(String id, String service_id, String service_name, String service_des, String service_price, String user_id) {
        this.id = id;
        this.service_id = service_id;
        this.service_name = service_name;
        this.service_des = service_des;
        this.service_price = service_price;
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_des() {
        return service_des;
    }

    public void setService_des(String service_des) {
        this.service_des = service_des;
    }

    public String getService_price() {
        return service_price;
    }

    public void setService_price(String service_price) {
        this.service_price = service_price;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
