package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 11/17/2015.
 */
public class CommentModel {
    private String commentId;
    private String commentdetails;
    private String userIdOfComment;
    private String userFname;
    private String userLname;
    private String userPhoto;
    private String userGroupId;
    private String commentDate;
    private String postId;

    public CommentModel(String commentId, String commentdetails, String userIdOfComment,
                        String userFname, String userLname, String userPhoto,
                        String userGroupId, String commentDate, String postId) {
        this.commentId = commentId;
        this.commentdetails = commentdetails;
        this.userIdOfComment = userIdOfComment;
        this.userFname = userFname;
        this.userLname = userLname;
        this.userPhoto = userPhoto;
        this.userGroupId = userGroupId;
        this.commentDate = commentDate;
        this.postId = postId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCommentdetails() {
        return commentdetails;
    }

    public void setCommentdetails(String commentdetails) {
        this.commentdetails = commentdetails;
    }

    public String getUserIdOfComment() {
        return userIdOfComment;
    }

    public void setUserIdOfComment(String userIdOfComment) {
        this.userIdOfComment = userIdOfComment;
    }

    public String getUserFname() {
        return userFname;
    }

    public void setUserFname(String userFname) {
        this.userFname = userFname;
    }

    public String getUserLname() {
        return userLname;
    }

    public void setUserLname(String userLname) {
        this.userLname = userLname;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}
