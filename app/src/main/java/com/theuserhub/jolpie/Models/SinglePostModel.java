package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 10/25/2015.
 */
public class SinglePostModel {
    private int dbpost_id;
    private String post_id,post_title,post_description,post_feeling,post_likes,post_answers,post_follows,post_userId,post_user_fname,post_user_lname,post_user_photo,
            post_userGroupId,post_created,post_updated,post_imgUrl,post_vdoUrl,post_liked;

    public SinglePostModel(String post_id, String post_title, String post_description, String post_feeling, String post_likes, String post_answers, String post_follows, String post_userId, String post_user_fname, String post_user_lname, String post_user_photo, String post_userGroupId, String post_created, String post_updated, String post_imgUrl, String post_vdoUrl, String post_liked) {
        this.post_id = post_id;
        this.post_title = post_title;
        this.post_description = post_description;
        this.post_feeling = post_feeling;
        this.post_likes = post_likes;
        this.post_answers = post_answers;
        this.post_follows = post_follows;
        this.post_userId = post_userId;
        this.post_user_fname = post_user_fname;
        this.post_user_lname = post_user_lname;
        this.post_user_photo = post_user_photo;
        this.post_userGroupId = post_userGroupId;
        this.post_created = post_created;
        this.post_updated = post_updated;
        this.post_imgUrl = post_imgUrl;
        this.post_vdoUrl = post_vdoUrl;
        this.post_liked = post_liked;
    }

    public SinglePostModel(int dbpost_id, String post_id, String post_title, String post_description, String post_feeling, String post_likes, String post_answers, String post_follows, String post_userId, String post_user_fname, String post_user_lname, String post_user_photo, String post_userGroupId, String post_created, String post_updated, String post_imgUrl, String post_vdoUrl) {
        this.dbpost_id = dbpost_id;
        this.post_id = post_id;
        this.post_title = post_title;
        this.post_description = post_description;
        this.post_feeling = post_feeling;
        this.post_likes = post_likes;
        this.post_answers = post_answers;
        this.post_follows = post_follows;
        this.post_userId = post_userId;
        this.post_user_fname = post_user_fname;
        this.post_user_lname = post_user_lname;
        this.post_user_photo = post_user_photo;
        this.post_userGroupId = post_userGroupId;
        this.post_created = post_created;
        this.post_updated = post_updated;
        this.post_imgUrl = post_imgUrl;
        this.post_vdoUrl = post_vdoUrl;
    }

    public int getDbpost_id() {
        return dbpost_id;
    }

    public void setDbpost_id(int dbpost_id) {
        this.dbpost_id = dbpost_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_description() {
        return post_description;
    }

    public void setPost_description(String post_description) {
        this.post_description = post_description;
    }

    public String getPost_feeling() {
        return post_feeling;
    }

    public void setPost_feeling(String post_feeling) {
        this.post_feeling = post_feeling;
    }

    public String getPost_likes() {
        return post_likes;
    }

    public void setPost_likes(String post_likes) {
        this.post_likes = post_likes;
    }

    public String getPost_answers() {
        return post_answers;
    }

    public void setPost_answers(String post_answers) {
        this.post_answers = post_answers;
    }

    public String getPost_follows() {
        return post_follows;
    }

    public void setPost_follows(String post_follows) {
        this.post_follows = post_follows;
    }

    public String getPost_userId() {
        return post_userId;
    }

    public void setPost_userId(String post_userId) {
        this.post_userId = post_userId;
    }

    public String getPost_user_fname() {
        return post_user_fname;
    }

    public void setPost_user_fname(String post_user_fname) {
        this.post_user_fname = post_user_fname;
    }

    public String getPost_user_lname() {
        return post_user_lname;
    }

    public void setPost_user_lname(String post_user_lname) {
        this.post_user_lname = post_user_lname;
    }

    public String getPost_user_photo() {
        return post_user_photo;
    }

    public void setPost_user_photo(String post_user_photo) {
        this.post_user_photo = post_user_photo;
    }

    public String getPost_userGroupId() {
        return post_userGroupId;
    }

    public void setPost_userGroupId(String post_userGroupId) {
        this.post_userGroupId = post_userGroupId;
    }

    public String getPost_created() {
        return post_created;
    }

    public void setPost_created(String post_created) {
        this.post_created = post_created;
    }

    public String getPost_updated() {
        return post_updated;
    }

    public void setPost_updated(String post_updated) {
        this.post_updated = post_updated;
    }

    public String getPost_imgUrl() {
        return post_imgUrl;
    }

    public void setPost_imgUrl(String post_imgUrl) {
        this.post_imgUrl = post_imgUrl;
    }

    public String getPost_vdoUrl() {
        return post_vdoUrl;
    }

    public void setPost_vdoUrl(String post_vdoUrl) {
        this.post_vdoUrl = post_vdoUrl;
    }

    public String getPost_liked() {
        return post_liked;
    }

    public void setPost_liked(String post_liked) {
        this.post_liked = post_liked;
    }
}
