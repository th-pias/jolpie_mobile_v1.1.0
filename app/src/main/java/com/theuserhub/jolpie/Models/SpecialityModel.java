package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 1/31/2016.
 */
public class SpecialityModel {
    private String id,name;

    public SpecialityModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
