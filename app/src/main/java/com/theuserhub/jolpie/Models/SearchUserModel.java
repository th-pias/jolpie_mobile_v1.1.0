package com.theuserhub.jolpie.Models;

import java.util.List;

/**
 * Created by Admin on 5/18/2016.
 */
public class SearchUserModel {
    private String id, firstName,lastName,photo,banner, user_group_id, degree, type, district, review_count, review_star, isSaved, isConsult, isReviewed, itemPrice, isSubscribed, isAppointment;
    private List<SpecialityModel> specialityModels;
    private List<ChamberExModel>eachChambers;

    public SearchUserModel(String id, String firstName, String lastName, String photo, String user_group_id, String degree, String type, String district, String review_count, String review_star, List<SpecialityModel> specialityModels, String isSaved, String isConsult, String itemPrice, String isSubscribed, String isAppointment, List<ChamberExModel>eachChambers) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo = photo;
        this.user_group_id = user_group_id;
        this.degree = degree;
        this.type = type;
        this.district = district;
        this.review_count = review_count;
        this.review_star = review_star;
        this.specialityModels = specialityModels;
        this.isSaved = isSaved;
        this.isConsult = isConsult;
        this.itemPrice = itemPrice;
        this.isSubscribed = isSubscribed;
        this.isAppointment = isAppointment;
        this.eachChambers = eachChambers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getUser_group_id() {
        return user_group_id;
    }

    public void setUser_group_id(String user_group_id) {
        this.user_group_id = user_group_id;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getReview_count() {
        return review_count;
    }

    public void setReview_count(String review_count) {
        this.review_count = review_count;
    }

    public String getReview_star() {
        return review_star;
    }

    public void setReview_star(String review_star) {
        this.review_star = review_star;
    }

    public String getIsSaved() {
        return isSaved;
    }

    public void setIsSaved(String isSaved) {
        this.isSaved = isSaved;
    }

    public String getIsConsult() {
        return isConsult;
    }

    public void setIsConsult(String isConsult) {
        this.isConsult = isConsult;
    }

    public String getIsReviewed() {
        return isReviewed;
    }

    public void setIsReviewed(String isReviewed) {
        this.isReviewed = isReviewed;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getIsSubscribed() {
        return isSubscribed;
    }

    public void setIsSubscribed(String isSubscribed) {
        this.isSubscribed = isSubscribed;
    }

    public String getIsAppointment() {
        return isAppointment;
    }

    public void setIsAppointment(String isAppointment) {
        this.isAppointment = isAppointment;
    }

    public List<SpecialityModel> getSpecialityModels() {
        return specialityModels;
    }

    public void setSpecialityModels(List<SpecialityModel> specialityModels) {
        this.specialityModels = specialityModels;
    }

    public List<ChamberExModel> getEachChambers() {
        return eachChambers;
    }

    public void setEachChambers(List<ChamberExModel> eachChambers) {
        this.eachChambers = eachChambers;
    }
}
