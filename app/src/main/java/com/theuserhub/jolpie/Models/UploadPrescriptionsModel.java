package com.theuserhub.jolpie.Models;

/**
 * Created by tumpa on 3/13/2016.
 */
public class UploadPrescriptionsModel {
    String id;
    String user_id;
    String prescription_image;
    String created_at;

    public UploadPrescriptionsModel(String id, String user_id, String prescription_image, String created_at) {
        this.id = id;
        this.user_id = user_id;
        this.prescription_image = prescription_image;
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPrescription_image() {
        return prescription_image;
    }

    public void setPrescription_image(String prescription_image) {
        this.prescription_image = prescription_image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
