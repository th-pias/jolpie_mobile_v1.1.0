package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 1/17/2016.
 */
public class GroupMemberModel {
    private String id,user_id,first_name,last_name,image,user_group_id;

    public GroupMemberModel(String id, String user_id, String first_name, String last_name, String image, String user_group_id) {
        this.id = id;
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.image = image;
        this.user_group_id = user_group_id;
    }

    public GroupMemberModel(String user_id, String first_name, String last_name,
                            String image, String user_group_id) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.image = image;
        this.user_group_id = user_group_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUser_group_id() {
        return user_group_id;
    }

    public void setUser_group_id(String user_group_id) {
        this.user_group_id = user_group_id;
    }
}
