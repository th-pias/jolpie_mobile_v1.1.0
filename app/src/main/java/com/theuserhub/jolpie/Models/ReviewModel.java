package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 1/11/2016.
 */
public class ReviewModel {
    private String review_id,review_by,review_title,review_description,review_created_at,review_star_count,review_fname,review_lname,review_by_photo,review_group_id;

    public ReviewModel(String review_id, String review_by, String review_title, String review_description,
                       String review_created_at, String review_star_count, String review_fname, String review_lname,
                       String review_by_photo, String review_group_id) {
        this.review_id = review_id;
        this.review_by = review_by;
        this.review_title = review_title;
        this.review_description = review_description;
        this.review_created_at = review_created_at;
        this.review_star_count = review_star_count;
        this.review_fname = review_fname;
        this.review_lname = review_lname;
        this.review_by_photo = review_by_photo;
        this.review_group_id = review_group_id;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getReview_by() {
        return review_by;
    }

    public void setReview_by(String review_by) {
        this.review_by = review_by;
    }

    public String getReview_title() {
        return review_title;
    }

    public void setReview_title(String review_title) {
        this.review_title = review_title;
    }

    public String getReview_description() {
        return review_description;
    }

    public void setReview_description(String review_description) {
        this.review_description = review_description;
    }

    public String getReview_created_at() {
        return review_created_at;
    }

    public void setReview_created_at(String review_created_at) {
        this.review_created_at = review_created_at;
    }

    public String getReview_star_count() {
        return review_star_count;
    }

    public void setReview_star_count(String review_star_count) {
        this.review_star_count = review_star_count;
    }

    public String getReview_fname() {
        return review_fname;
    }

    public void setReview_fname(String review_fname) {
        this.review_fname = review_fname;
    }

    public String getReview_lname() {
        return review_lname;
    }

    public void setReview_lname(String review_lname) {
        this.review_lname = review_lname;
    }

    public String getReview_by_photo() {
        return review_by_photo;
    }

    public void setReview_by_photo(String review_by_photo) {
        this.review_by_photo = review_by_photo;
    }

    public String getReview_group_id() {
        return review_group_id;
    }

    public void setReview_group_id(String review_group_id) {
        this.review_group_id = review_group_id;
    }
}
