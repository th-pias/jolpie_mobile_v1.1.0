package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 12/15/2015.
 */
public class AutoCompleteModel {
    private String autoID, autoTitle, autoType, autoCount, autoImage,autoGroupId,autoProfile;

    public AutoCompleteModel(String autoID, String autoTitle, String autoType, String autoCount, String autoImage, String autoGroupId, String autoProfile) {
        this.autoID = autoID;
        this.autoTitle = autoTitle;
        this.autoType = autoType;
        this.autoCount = autoCount;
        this.autoImage = autoImage;
        this.autoGroupId = autoGroupId;
        this.autoProfile = autoProfile;
    }

    public String getAutoID() {
        return autoID;
    }

    public void setAutoID(String autoID) {
        this.autoID = autoID;
    }

    public String getAutoTitle() {
        return autoTitle;
    }

    public void setAutoTitle(String autoTitle) {
        this.autoTitle = autoTitle;
    }

    public String getAutoType() {
        return autoType;
    }

    public void setAutoType(String autoType) {
        this.autoType = autoType;
    }

    public String getAutoCount() {
        return autoCount;
    }

    public void setAutoCount(String autoCount) {
        this.autoCount = autoCount;
    }

    public String getAutoImage() {
        return autoImage;
    }

    public void setAutoImage(String autoImage) {
        this.autoImage = autoImage;
    }

    public String getAutoGroupId() {
        return autoGroupId;
    }

    public void setAutoGroupId(String autoGroupId) {
        this.autoGroupId = autoGroupId;
    }

    public String getAutoProfile() {
        return autoProfile;
    }

    public void setAutoProfile(String autoProfile) {
        this.autoProfile = autoProfile;
    }
}
