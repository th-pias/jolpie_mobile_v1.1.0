package com.theuserhub.jolpie.Models;

/**
 * Created by tumpa on 3/13/2016.
 */
public class GivenPrescriptionModel {
    String prescription_id;
    String conditions;
    String advice;
    String   patient_id;
    String   doctor_id;
    String  appointment_id;
    String   updated_at;
    String user_group_id;
    String   created_at;
    String   first_name;
    String   last_name;
    String  photo;

    public GivenPrescriptionModel(String prescription_id, String conditions, String advice, String patient_id, String doctor_id, String appointment_id, String updated_at, String created_at, String first_name, String last_name, String photo, String user_group_id) {
        this.prescription_id = prescription_id;
        this.conditions = conditions;
        this.advice = advice;
        this.patient_id = patient_id;
        this.doctor_id = doctor_id;
        this.appointment_id = appointment_id;
        this.updated_at = updated_at;
        this.created_at = created_at;
        this.first_name = first_name;
        this.last_name = last_name;
        this.photo = photo;
        this.user_group_id = user_group_id;
    }

    public String getPrescription_id() {
        return prescription_id;
    }

    public void setPrescription_id(String prescription_id) {
        this.prescription_id = prescription_id;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getAppointment_id() {
        return appointment_id;
    }

    public void setAppointment_id(String appointment_id) {
        this.appointment_id = appointment_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUser_group_id() {
        return user_group_id;
    }

    public void setUser_group_id(String user_group_id) {
        this.user_group_id = user_group_id;
    }
}
