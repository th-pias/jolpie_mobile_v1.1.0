package com.theuserhub.jolpie.Models;

/**
 * Created by tumpa on 3/2/2016.
 */
public class ServiceModel {
    String id ;
    String category_id ;
    String service_id ;
    String service_price ;
    String user_id ;
    String category_name ;
    String service_name ;

    public ServiceModel(String id, String category_id, String service_id, String service_price, String user_id, String category_name, String service_name) {
        this.id = id;
        this.category_id = category_id;
        this.service_id = service_id;
        this.service_price = service_price;
        this.user_id = user_id;
        this.category_name = category_name;
        this.service_name = service_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_price() {
        return service_price;
    }

    public void setService_price(String service_price) {
        this.service_price = service_price;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }


}
