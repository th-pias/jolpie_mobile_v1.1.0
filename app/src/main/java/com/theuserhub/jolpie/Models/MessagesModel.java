package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 1/4/2016.
 */
public class MessagesModel {
    private String msg_id,from_user_id,to_user_id,msg_text,msg_created_at,to_first_name,to_last_name,to_photo,to_group_id;

    //for messages data
    public MessagesModel(String msg_id, String from_user_id, String to_user_id, String msg_text, String msg_created_at,
                         String to_first_name, String to_last_name, String to_photo, String to_group_id) {
        this.msg_id = msg_id;
        this.from_user_id = from_user_id;
        this.to_user_id = to_user_id;
        this.msg_text = msg_text;
        this.msg_created_at = msg_created_at;
        this.to_first_name = to_first_name;
        this.to_last_name = to_last_name;
        this.to_photo = to_photo;
        this.to_group_id = to_group_id;
    }

    //for conversation data
    public MessagesModel(String msg_id, String from_user_id, String to_user_id, String msg_text, String msg_created_at) {
        this.msg_id = msg_id;
        this.from_user_id = from_user_id;
        this.to_user_id = to_user_id;
        this.msg_text = msg_text;
        this.msg_created_at = msg_created_at;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getMsg_text() {
        return msg_text;
    }

    public void setMsg_text(String msg_text) {
        this.msg_text = msg_text;
    }

    public String getMsg_created_at() {
        return msg_created_at;
    }

    public void setMsg_created_at(String msg_created_at) {
        this.msg_created_at = msg_created_at;
    }

    public String getTo_first_name() {
        return to_first_name;
    }

    public void setTo_first_name(String to_first_name) {
        this.to_first_name = to_first_name;
    }

    public String getTo_last_name() {
        return to_last_name;
    }

    public void setTo_last_name(String to_last_name) {
        this.to_last_name = to_last_name;
    }

    public String getTo_photo() {
        return to_photo;
    }

    public void setTo_photo(String to_photo) {
        this.to_photo = to_photo;
    }

    public String getTo_group_id() {
        return to_group_id;
    }

    public void setTo_group_id(String to_group_id) {
        this.to_group_id = to_group_id;
    }
}
