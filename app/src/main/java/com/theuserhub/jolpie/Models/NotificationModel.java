package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 2/24/2016.
 */
public class NotificationModel {
    private String id, section, section_id, activity, activity_by_id , activity_by_fname, activity_by_lname, activity_by_photo, activity_by_group_id, seen, created_at;

    public NotificationModel(String id, String section, String section_id, String activity, String activity_by_id, String activity_by_fname, String activity_by_lname, String activity_by_photo, String activity_by_group_id, String seen, String created_at) {
        this.id = id;
        this.section = section;
        this.section_id = section_id;
        this.activity = activity;
        this.activity_by_id = activity_by_id;
        this.activity_by_fname = activity_by_fname;
        this.activity_by_lname = activity_by_lname;
        this.activity_by_photo = activity_by_photo;
        this.activity_by_group_id = activity_by_group_id;
        this.seen = seen;
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivity_by_id() {
        return activity_by_id;
    }

    public void setActivity_by_id(String activity_by_id) {
        this.activity_by_id = activity_by_id;
    }

    public String getActivity_by_fname() {
        return activity_by_fname;
    }

    public void setActivity_by_fname(String activity_by_fname) {
        this.activity_by_fname = activity_by_fname;
    }

    public String getActivity_by_lname() {
        return activity_by_lname;
    }

    public void setActivity_by_lname(String activity_by_lname) {
        this.activity_by_lname = activity_by_lname;
    }

    public String getActivity_by_photo() {
        return activity_by_photo;
    }

    public void setActivity_by_photo(String activity_by_photo) {
        this.activity_by_photo = activity_by_photo;
    }

    public String getActivity_by_group_id() {
        return activity_by_group_id;
    }

    public void setActivity_by_group_id(String activity_by_group_id) {
        this.activity_by_group_id = activity_by_group_id;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
