package com.theuserhub.jolpie.Models;

import java.util.List;

/**
 * Created by Admin on 1/28/2016.
 */
public class MagazinesModel {

    private String category_id,category;
    private List<ArticlesModel>articlesModels;

    public MagazinesModel(String category_id, String category, List<ArticlesModel> articlesModels) {
        this.category_id = category_id;
        this.category = category;
        this.articlesModels = articlesModels;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<ArticlesModel> getArticlesModels() {
        return articlesModels;
    }

    public void setArticlesModels(List<ArticlesModel> articlesModels) {
        this.articlesModels = articlesModels;
    }
}
