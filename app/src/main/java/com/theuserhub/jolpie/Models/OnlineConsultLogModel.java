package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 4/4/2016.
 */
public class OnlineConsultLogModel {
    private String consultancy_id,package_name,audio,video,text,consultancy_type,duration,price,created_at,doctor_id,patient_id,tran_date,first_name,last_name,photo,degree,user_group_id;

    public OnlineConsultLogModel(String consultancy_id, String package_name, String audio, String video, String text, String consultancy_type, String duration, String price, String created_at, String doctor_id, String patient_id, String tran_date, String first_name, String last_name, String photo, String degree, String user_group_id) {
        this.consultancy_id = consultancy_id;
        this.package_name = package_name;
        this.audio = audio;
        this.video = video;
        this.text = text;
        this.consultancy_type = consultancy_type;
        this.duration = duration;
        this.price = price;
        this.created_at = created_at;
        this.doctor_id = doctor_id;
        this.patient_id = patient_id;
        this.tran_date = tran_date;
        this.first_name = first_name;
        this.last_name = last_name;
        this.photo = photo;
        this.degree = degree;
        this.user_group_id = user_group_id;
    }

    public String getConsultancy_id() {
        return consultancy_id;
    }

    public void setConsultancy_id(String consultancy_id) {
        this.consultancy_id = consultancy_id;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getConsultancy_type() {
        return consultancy_type;
    }

    public void setConsultancy_type(String consultancy_type) {
        this.consultancy_type = consultancy_type;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getTran_date() {
        return tran_date;
    }

    public void setTran_date(String tran_date) {
        this.tran_date = tran_date;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getUser_group_id() {
        return user_group_id;
    }

    public void setUser_group_id(String user_group_id) {
        this.user_group_id = user_group_id;
    }
}
