package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 2/3/2016.
 */
public class PostInfoModel {
    private String id,title,description,feeling,photo,video,created_at,updated_at,by_id,by_fname,by_lname,by_photo,by_user_group_id,to_id,to_fname,to_lname,to_photo,to_user_group_id,like_count,answer_count,share_count,star_count,liked_by_me,shared_by_me;

    public PostInfoModel(String id, String title, String description, String feeling, String photo, String video, String created_at, String updated_at, String by_id, String by_fname, String by_lname, String by_photo, String by_user_group_id, String to_id, String to_fname, String to_lname, String to_photo, String to_user_group_id, String like_count, String answer_count, String share_count, String star_count, String liked_by_me, String shared_by_me) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.feeling = feeling;
        this.photo = photo;
        this.video = video;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.by_id = by_id;
        this.by_fname = by_fname;
        this.by_lname = by_lname;
        this.by_photo = by_photo;
        this.by_user_group_id = by_user_group_id;
        this.to_id = to_id;
        this.to_fname = to_fname;
        this.to_lname = to_lname;
        this.to_photo = to_photo;
        this.to_user_group_id = to_user_group_id;
        this.like_count = like_count;
        this.answer_count = answer_count;
        this.share_count = share_count;
        this.star_count = star_count;
        this.liked_by_me = liked_by_me;
        this.shared_by_me = shared_by_me;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFeeling() {
        return feeling;
    }

    public void setFeeling(String feeling) {
        this.feeling = feeling;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBy_id() {
        return by_id;
    }

    public void setBy_id(String by_id) {
        this.by_id = by_id;
    }

    public String getBy_fname() {
        return by_fname;
    }

    public void setBy_fname(String by_fname) {
        this.by_fname = by_fname;
    }

    public String getBy_lname() {
        return by_lname;
    }

    public void setBy_lname(String by_lname) {
        this.by_lname = by_lname;
    }

    public String getBy_photo() {
        return by_photo;
    }

    public void setBy_photo(String by_photo) {
        this.by_photo = by_photo;
    }

    public String getBy_user_group_id() {
        return by_user_group_id;
    }

    public void setBy_user_group_id(String by_user_group_id) {
        this.by_user_group_id = by_user_group_id;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public String getTo_fname() {
        return to_fname;
    }

    public void setTo_fname(String to_fname) {
        this.to_fname = to_fname;
    }

    public String getTo_lname() {
        return to_lname;
    }

    public void setTo_lname(String to_lname) {
        this.to_lname = to_lname;
    }

    public String getTo_photo() {
        return to_photo;
    }

    public void setTo_photo(String to_photo) {
        this.to_photo = to_photo;
    }

    public String getTo_user_group_id() {
        return to_user_group_id;
    }

    public void setTo_user_group_id(String to_user_group_id) {
        this.to_user_group_id = to_user_group_id;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getAnswer_count() {
        return answer_count;
    }

    public void setAnswer_count(String answer_count) {
        this.answer_count = answer_count;
    }

    public String getShare_count() {
        return share_count;
    }

    public void setShare_count(String share_count) {
        this.share_count = share_count;
    }

    public String getStar_count() {
        return star_count;
    }

    public void setStar_count(String star_count) {
        this.star_count = star_count;
    }

    public String getLiked_by_me() {
        return liked_by_me;
    }

    public void setLiked_by_me(String liked_by_me) {
        this.liked_by_me = liked_by_me;
    }

    public String getShared_by_me() {
        return shared_by_me;
    }

    public void setShared_by_me(String shared_by_me) {
        this.shared_by_me = shared_by_me;
    }
}
