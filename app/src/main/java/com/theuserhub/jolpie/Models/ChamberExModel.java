package com.theuserhub.jolpie.Models;

import java.util.List;

/**
 * Created by Admin on 3/30/2016.
 */
public class ChamberExModel {
    private String chamber_id,organisation_id,chamber_first_name,chamber_last_name,chamber_photo,chamber_mobile,chamber_user_group_id,chamber_street_address,chamber_address,cut_of_time,chamber_fee,each_patient_time;
    private List<ScheduleModel> scheduleModelList;

    public ChamberExModel(String chamber_id, String organisation_id, String chamber_first_name,
                        String chamber_last_name, String chamber_photo, String chamber_mobile,
                        String chamber_user_group_id, String chamber_street_address,
                        String chamber_address, String cut_of_time, String chamber_fee,
                        String each_patient_time, List<ScheduleModel>scheduleModelList) {
        this.chamber_id = chamber_id;
        this.organisation_id = organisation_id;
        this.chamber_first_name = chamber_first_name;
        this.chamber_last_name = chamber_last_name;
        this.chamber_photo = chamber_photo;
        this.chamber_mobile = chamber_mobile;
        this.chamber_user_group_id = chamber_user_group_id;
        this.chamber_street_address = chamber_street_address;
        this.chamber_address = chamber_address;
        this.cut_of_time = cut_of_time;
        this.chamber_fee = chamber_fee;
        this.each_patient_time = each_patient_time;
        this.scheduleModelList = scheduleModelList;
    }

    public String getChamber_id() {
        return chamber_id;
    }

    public void setChamber_id(String chamber_id) {
        this.chamber_id = chamber_id;
    }

    public String getOrganisation_id() {
        return organisation_id;
    }

    public void setOrganisation_id(String organisation_id) {
        this.organisation_id = organisation_id;
    }

    public String getChamber_first_name() {
        return chamber_first_name;
    }

    public void setChamber_first_name(String chamber_first_name) {
        this.chamber_first_name = chamber_first_name;
    }

    public String getChamber_last_name() {
        return chamber_last_name;
    }

    public void setChamber_last_name(String chamber_last_name) {
        this.chamber_last_name = chamber_last_name;
    }

    public String getChamber_photo() {
        return chamber_photo;
    }

    public void setChamber_photo(String chamber_photo) {
        this.chamber_photo = chamber_photo;
    }

    public String getChamber_mobile() {
        return chamber_mobile;
    }

    public void setChamber_mobile(String chamber_mobile) {
        this.chamber_mobile = chamber_mobile;
    }

    public String getChamber_user_group_id() {
        return chamber_user_group_id;
    }

    public void setChamber_user_group_id(String chamber_user_group_id) {
        this.chamber_user_group_id = chamber_user_group_id;
    }

    public String getChamber_street_address() {
        return chamber_street_address;
    }

    public void setChamber_street_address(String chamber_street_address) {
        this.chamber_street_address = chamber_street_address;
    }

    public String getChamber_address() {
        return chamber_address;
    }

    public void setChamber_address(String chamber_address) {
        this.chamber_address = chamber_address;
    }

    public String getCut_of_time() {
        return cut_of_time;
    }

    public void setCut_of_time(String cut_of_time) {
        this.cut_of_time = cut_of_time;
    }

    public String getChamber_fee() {
        return chamber_fee;
    }

    public void setChamber_fee(String chamber_fee) {
        this.chamber_fee = chamber_fee;
    }

    public String getEach_patient_time() {
        return each_patient_time;
    }

    public void setEach_patient_time(String each_patient_time) {
        this.each_patient_time = each_patient_time;
    }

    public List<ScheduleModel> getScheduleModelList() {
        return scheduleModelList;
    }

    public void setScheduleModelList(List<ScheduleModel> scheduleModelList) {
        this.scheduleModelList = scheduleModelList;
    }
}
