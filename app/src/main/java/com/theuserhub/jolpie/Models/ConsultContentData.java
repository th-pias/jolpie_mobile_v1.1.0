package com.theuserhub.jolpie.Models;

/**
 * Created by tumpa on 2/24/2016.
 */
public class ConsultContentData {
    String consultancy_id;
    String package_name;
    String audio;
    String video;
    String text;
    String duration;
    String price;
    String user_id;

    public ConsultContentData(String consultancy_id, String package_name, String video, String audio, String text, String duration, String price, String user_id) {
        this.consultancy_id = consultancy_id;
        this.package_name = package_name;
        this.video = video;
        this.audio = audio;
        this.text = text;
        this.duration = duration;
        this.price = price;
        this.user_id = user_id;
    }

    public String getConsultancy_id() {
        return consultancy_id;
    }

    public void setConsultancy_id(String consultancy_id) {
        this.consultancy_id = consultancy_id;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
