package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 11/8/2015.
 */
public class GroupItem {
    String postgroup_id,postgroup_name,postgroup_description,postgroup_type,postgroup_image,postgroup_member_count,postgroup_discussion_count,postgroup_created_at,postgroup_updated_at,postgroup_created_by;

    public GroupItem(String postgroup_id, String postgroup_name, String postgroup_description, String postgroup_type,
                     String postgroup_image, String postgroup_member_count, String postgroup_discussion_count,
                     String postgroup_created_at, String postgroup_updated_at, String postgroup_created_by) {
        this.postgroup_id = postgroup_id;
        this.postgroup_name = postgroup_name;
        this.postgroup_description = postgroup_description;
        this.postgroup_type = postgroup_type;
        this.postgroup_image = postgroup_image;
        this.postgroup_member_count = postgroup_member_count;
        this.postgroup_discussion_count = postgroup_discussion_count;
        this.postgroup_created_at = postgroup_created_at;
        this.postgroup_updated_at = postgroup_updated_at;
        this.postgroup_created_by = postgroup_created_by;
    }

    public String getPostgroup_id() {
        return postgroup_id;
    }

    public void setPostgroup_id(String postgroup_id) {
        this.postgroup_id = postgroup_id;
    }

    public String getPostgroup_name() {
        return postgroup_name;
    }

    public void setPostgroup_name(String postgroup_name) {
        this.postgroup_name = postgroup_name;
    }

    public String getPostgroup_description() {
        return postgroup_description;
    }

    public void setPostgroup_description(String postgroup_description) {
        this.postgroup_description = postgroup_description;
    }

    public String getPostgroup_type() {
        return postgroup_type;
    }

    public void setPostgroup_type(String postgroup_type) {
        this.postgroup_type = postgroup_type;
    }

    public String getPostgroup_image() {
        return postgroup_image;
    }

    public void setPostgroup_image(String postgroup_image) {
        this.postgroup_image = postgroup_image;
    }

    public String getPostgroup_member_count() {
        return postgroup_member_count;
    }

    public void setPostgroup_member_count(String postgroup_member_count) {
        this.postgroup_member_count = postgroup_member_count;
    }

    public String getPostgroup_discussion_count() {
        return postgroup_discussion_count;
    }

    public void setPostgroup_discussion_count(String postgroup_discussion_count) {
        this.postgroup_discussion_count = postgroup_discussion_count;
    }

    public String getPostgroup_created_at() {
        return postgroup_created_at;
    }

    public void setPostgroup_created_at(String postgroup_created_at) {
        this.postgroup_created_at = postgroup_created_at;
    }

    public String getPostgroup_updated_at() {
        return postgroup_updated_at;
    }

    public void setPostgroup_updated_at(String postgroup_updated_at) {
        this.postgroup_updated_at = postgroup_updated_at;
    }

    public String getPostgroup_created_by() {
        return postgroup_created_by;
    }

    public void setPostgroup_created_by(String postgroup_created_by) {
        this.postgroup_created_by = postgroup_created_by;
    }
}
