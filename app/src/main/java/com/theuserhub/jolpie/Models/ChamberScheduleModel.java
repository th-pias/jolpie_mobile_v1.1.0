package com.theuserhub.jolpie.Models;

import java.util.Date;

/**
 * Created by Admin on 2/14/2016.
 */
public class ChamberScheduleModel {
    private String schedule_id,user_id,chamber_id,day,start_time_str,end_time_str;

    private Date start_time,end_time;
    public ChamberScheduleModel(String schedule_id, String user_id, String chamber_id, Date start_time, Date end_time, String day, String start_time_str, String end_time_str) {
        this.schedule_id = schedule_id;
        this.user_id = user_id;
        this.chamber_id = chamber_id;
        this.start_time = start_time;
        this.end_time = end_time;
        this.day = day;
        this.start_time_str = start_time_str;
        this.end_time_str = end_time_str;
    }

    public String getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(String schedule_id) {
        this.schedule_id = schedule_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getChamber_id() {
        return chamber_id;
    }

    public void setChamber_id(String chamber_id) {
        this.chamber_id = chamber_id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public String getEnd_time_str() {
        return end_time_str;
    }

    public void setEnd_time_str(String end_time_str) {
        this.end_time_str = end_time_str;
    }

    public String getStart_time_str() {
        return start_time_str;
    }

    public void setStart_time_str(String start_time_str) {
        this.start_time_str = start_time_str;
    }
}
