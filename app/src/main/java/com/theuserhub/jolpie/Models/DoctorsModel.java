package com.theuserhub.jolpie.Models;

import java.util.List;

/**
 * Created by Admin on 4/11/2016.
 */
public class DoctorsModel {
    private String user_id,first_name,last_name,image,user_group_id,degree;
    private List<SpecialityModel> specialityModels;

    public DoctorsModel(String user_id, String first_name, String last_name, String image, String user_group_id, String degree, List<SpecialityModel> specialityModels) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.image = image;
        this.user_group_id = user_group_id;
        this.degree = degree;
        this.specialityModels = specialityModels;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUser_group_id() {
        return user_group_id;
    }

    public void setUser_group_id(String user_group_id) {
        this.user_group_id = user_group_id;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public List<SpecialityModel> getSpecialityModels() {
        return specialityModels;
    }

    public void setSpecialityModels(List<SpecialityModel> specialityModels) {
        this.specialityModels = specialityModels;
    }
}
