package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 3/30/2016.
 */
public class ScheduleModel {
    private String id,user_id,chamber_id,start_time,end_time,day;

    public ScheduleModel(String id, String user_id, String chamber_id, String start_time, String end_time, String day) {
        this.id = id;
        this.user_id = user_id;
        this.chamber_id = chamber_id;
        this.start_time = start_time;
        this.end_time = end_time;
        this.day = day;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getChamber_id() {
        return chamber_id;
    }

    public void setChamber_id(String chamber_id) {
        this.chamber_id = chamber_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
