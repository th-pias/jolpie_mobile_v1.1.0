package com.theuserhub.jolpie.Models;

/**
 * Created by tumpa on 3/2/2016.
 */
public class ConditionModel {

    public ConditionModel(String condition_id, String condition_name, String bangla_condition_name) {
        this.condition_id = condition_id;
        this.condition_name = condition_name;
        this.bangla_condition_name = bangla_condition_name;
    }

    public String getCondition_id() {
        return condition_id;
    }

    public void setCondition_id(String condition_id) {
        this.condition_id = condition_id;
    }

    public String getCondition_name() {
        return condition_name;
    }

    public void setCondition_name(String condition_name) {
        this.condition_name = condition_name;
    }

    public String getBangla_condition_name() {
        return bangla_condition_name;
    }

    public void setBangla_condition_name(String bangla_condition_name) {
        this.bangla_condition_name = bangla_condition_name;
    }

    String condition_id, condition_name, bangla_condition_name;
}
