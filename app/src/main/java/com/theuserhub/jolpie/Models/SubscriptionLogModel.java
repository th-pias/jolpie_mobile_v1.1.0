package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 4/3/2016.
 */
public class SubscriptionLogModel {
    private String confirm_id, user_id, tran_id, amount, val_id, card_type,
            bank_tran_id, tran_date, card_issuer, card_brand, card_issuer_country,
            store_id, verify_sign, duration;

    public SubscriptionLogModel(String confirm_id, String user_id, String tran_id, String amount, String val_id, String card_type, String bank_tran_id, String tran_date, String card_issuer, String card_brand, String card_issuer_country, String store_id, String verify_sign, String duration) {
        this.confirm_id = confirm_id;
        this.user_id = user_id;
        this.tran_id = tran_id;
        this.amount = amount;
        this.val_id = val_id;
        this.card_type = card_type;
        this.bank_tran_id = bank_tran_id;
        this.tran_date = tran_date;
        this.card_issuer = card_issuer;
        this.card_brand = card_brand;
        this.card_issuer_country = card_issuer_country;
        this.store_id = store_id;
        this.verify_sign = verify_sign;
        this.duration = duration;
    }

    public String getConfirm_id() {
        return confirm_id;
    }

    public void setConfirm_id(String confirm_id) {
        this.confirm_id = confirm_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTran_id() {
        return tran_id;
    }

    public void setTran_id(String tran_id) {
        this.tran_id = tran_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getVal_id() {
        return val_id;
    }

    public void setVal_id(String val_id) {
        this.val_id = val_id;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getBank_tran_id() {
        return bank_tran_id;
    }

    public void setBank_tran_id(String bank_tran_id) {
        this.bank_tran_id = bank_tran_id;
    }

    public String getTran_date() {
        return tran_date;
    }

    public void setTran_date(String tran_date) {
        this.tran_date = tran_date;
    }

    public String getCard_issuer() {
        return card_issuer;
    }

    public void setCard_issuer(String card_issuer) {
        this.card_issuer = card_issuer;
    }

    public String getCard_brand() {
        return card_brand;
    }

    public void setCard_brand(String card_brand) {
        this.card_brand = card_brand;
    }

    public String getCard_issuer_country() {
        return card_issuer_country;
    }

    public void setCard_issuer_country(String card_issuer_country) {
        this.card_issuer_country = card_issuer_country;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getVerify_sign() {
        return verify_sign;
    }

    public void setVerify_sign(String verify_sign) {
        this.verify_sign = verify_sign;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
