package com.theuserhub.jolpie.Models;

import java.util.List;

/**
 * Created by Admin on 2/3/2016.
 */
public class NewsfeedModel {
    private String id,section,section_id,activity,activity_at,by_user_id,by_user_fname,by_user_lname,by_user_photo,by_user_group_id;
    private PostInfoModel postInfoModel;

    public NewsfeedModel(String id, String section, String section_id, String activity, String activity_at, String by_user_id, String by_user_fname, String by_user_lname, String by_user_photo, String by_user_group_id, PostInfoModel postInfoModel) {
        this.id = id;
        this.section = section;
        this.section_id = section_id;
        this.activity = activity;
        this.activity_at = activity_at;
        this.by_user_id = by_user_id;
        this.by_user_fname = by_user_fname;
        this.by_user_lname = by_user_lname;
        this.by_user_photo = by_user_photo;
        this.by_user_group_id = by_user_group_id;
        this.postInfoModel = postInfoModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivity_at() {
        return activity_at;
    }

    public void setActivity_at(String activity_at) {
        this.activity_at = activity_at;
    }

    public String getBy_user_id() {
        return by_user_id;
    }

    public void setBy_user_id(String by_user_id) {
        this.by_user_id = by_user_id;
    }

    public String getBy_user_fname() {
        return by_user_fname;
    }

    public void setBy_user_fname(String by_user_fname) {
        this.by_user_fname = by_user_fname;
    }

    public String getBy_user_lname() {
        return by_user_lname;
    }

    public void setBy_user_lname(String by_user_lname) {
        this.by_user_lname = by_user_lname;
    }

    public String getBy_user_photo() {
        return by_user_photo;
    }

    public void setBy_user_photo(String by_user_photo) {
        this.by_user_photo = by_user_photo;
    }

    public String getBy_user_group_id() {
        return by_user_group_id;
    }

    public void setBy_user_group_id(String by_user_group_id) {
        this.by_user_group_id = by_user_group_id;
    }

    public PostInfoModel getPostInfoModel() {
        return postInfoModel;
    }

    public void setPostInfoModel(PostInfoModel postInfoModel) {
        this.postInfoModel = postInfoModel;
    }
}
