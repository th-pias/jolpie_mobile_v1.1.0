package com.theuserhub.jolpie.Models;

import java.util.List;

/**
 * Created by Admin on 2/8/2016.
 */
public class CompareModel {
    private String user_id,first_name,last_name,photo,gender,user_group_id,contact_no_1,contact_no_2,degree,type,bed_capacity,address,birth_date,
            company_name,company_position,edu_name,edu_year,edu_institute,reg_number,
            trade_license,tin_number,description,packet_title,packet_des,overview,side_effect,
            dosage;
    private List<SpecialityModel> specialityModels;

    public CompareModel(String user_id, String first_name, String last_name, String photo, String gender, String user_group_id, String contact_no_1, String contact_no_2, String degree, String type, String bed_capacity, String address, String birth_date, String company_name, String company_position, String edu_name, String edu_year, String edu_institute, String reg_number, String trade_license, String tin_number, String description, String packet_title, String packet_des, String overview, String side_effect, String dosage, List<SpecialityModel> specialityModels) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.photo = photo;
        this.gender = gender;
        this.user_group_id = user_group_id;
        this.contact_no_1 = contact_no_1;
        this.contact_no_2 = contact_no_2;
        this.degree = degree;
        this.type = type;
        this.bed_capacity = bed_capacity;
        this.address = address;
        this.birth_date = birth_date;
        this.company_name = company_name;
        this.company_position = company_position;
        this.edu_name = edu_name;
        this.edu_year = edu_year;
        this.edu_institute = edu_institute;
        this.reg_number = reg_number;
        this.trade_license = trade_license;
        this.tin_number = tin_number;
        this.description = description;
        this.packet_title = packet_title;
        this.packet_des = packet_des;
        this.overview = overview;
        this.side_effect = side_effect;
        this.dosage = dosage;
        this.specialityModels = specialityModels;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUser_group_id() {
        return user_group_id;
    }

    public void setUser_group_id(String user_group_id) {
        this.user_group_id = user_group_id;
    }

    public String getContact_no_1() {
        return contact_no_1;
    }

    public void setContact_no_1(String contact_no_1) {
        this.contact_no_1 = contact_no_1;
    }

    public String getContact_no_2() {
        return contact_no_2;
    }

    public void setContact_no_2(String contact_no_2) {
        this.contact_no_2 = contact_no_2;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBed_capacity() {
        return bed_capacity;
    }

    public void setBed_capacity(String bed_capacity) {
        this.bed_capacity = bed_capacity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_position() {
        return company_position;
    }

    public void setCompany_position(String company_position) {
        this.company_position = company_position;
    }

    public String getEdu_name() {
        return edu_name;
    }

    public void setEdu_name(String edu_name) {
        this.edu_name = edu_name;
    }

    public String getEdu_year() {
        return edu_year;
    }

    public void setEdu_year(String edu_year) {
        this.edu_year = edu_year;
    }

    public String getEdu_institute() {
        return edu_institute;
    }

    public void setEdu_institute(String edu_institute) {
        this.edu_institute = edu_institute;
    }

    public String getReg_number() {
        return reg_number;
    }

    public void setReg_number(String reg_number) {
        this.reg_number = reg_number;
    }

    public String getTrade_license() {
        return trade_license;
    }

    public void setTrade_license(String trade_license) {
        this.trade_license = trade_license;
    }

    public String getTin_number() {
        return tin_number;
    }

    public void setTin_number(String tin_number) {
        this.tin_number = tin_number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPacket_title() {
        return packet_title;
    }

    public void setPacket_title(String packet_title) {
        this.packet_title = packet_title;
    }

    public String getPacket_des() {
        return packet_des;
    }

    public void setPacket_des(String packet_des) {
        this.packet_des = packet_des;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getSide_effect() {
        return side_effect;
    }

    public void setSide_effect(String side_effect) {
        this.side_effect = side_effect;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public List<SpecialityModel> getSpecialityModels() {
        return specialityModels;
    }

    public void setSpecialityModels(List<SpecialityModel> specialityModels) {
        this.specialityModels = specialityModels;
    }
}
