package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 4/10/2016.
 */
public class WorkModel {
    private String id,org_name,position,from,till;

    public WorkModel(String id, String org_name, String position, String from, String till) {
        this.id = id;
        this.org_name = org_name;
        this.position = position;
        this.from = from;
        this.till = till;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTill() {
        return till;
    }

    public void setTill(String till) {
        this.till = till;
    }
}
