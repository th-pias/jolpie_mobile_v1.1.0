package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 1/26/2016.
 */
public class MyConditionsModel {
    String myId,conditionId,conditionName_en,conditionName_bn;

    public MyConditionsModel(String myId, String conditionId, String conditionName_en, String conditionName_bn) {
        this.myId = myId;
        this.conditionId = conditionId;
        this.conditionName_en = conditionName_en;
        this.conditionName_bn = conditionName_bn;
    }

    public MyConditionsModel(String conditionId, String conditionName_en, String conditionName_bn) {
        this.conditionId = conditionId;
        this.conditionName_en = conditionName_en;
        this.conditionName_bn = conditionName_bn;
    }

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

    public String getConditionId() {
        return conditionId;
    }

    public void setConditionId(String conditionId) {
        this.conditionId = conditionId;
    }

    public String getConditionName_en() {
        return conditionName_en;
    }

    public void setConditionName_en(String conditionName_en) {
        this.conditionName_en = conditionName_en;
    }

    public String getConditionName_bn() {
        return conditionName_bn;
    }

    public void setConditionName_bn(String conditionName_bn) {
        this.conditionName_bn = conditionName_bn;
    }
}
