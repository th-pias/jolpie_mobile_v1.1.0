package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 3/9/2016.
 */
public class HomeCareAreaModel {
    private String id,title;

    public HomeCareAreaModel(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
