package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 4/10/2016.
 */
public class EducationModel {
    private String id,degree,ins_name,year;

    public EducationModel(String id, String degree, String ins_name, String year) {
        this.id = id;
        this.degree = degree;
        this.ins_name = ins_name;
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getIns_name() {
        return ins_name;
    }

    public void setIns_name(String ins_name) {
        this.ins_name = ins_name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
