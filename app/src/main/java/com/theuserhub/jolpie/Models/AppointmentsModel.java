package com.theuserhub.jolpie.Models;

/**
 * Created by Admin on 2/22/2016.
 */
public class AppointmentsModel {
    private String id, user_id, chamber_id, appointment_time, delete_message, first_name, last_name, photo, user_group_id;

    public AppointmentsModel(String id, String user_id, String chamber_id, String appointment_time, String delete_message, String first_name, String last_name, String photo, String user_group_id) {
        this.id = id;
        this.user_id = user_id;
        this.chamber_id = chamber_id;
        this.appointment_time = appointment_time;
        this.delete_message = delete_message;
        this.first_name = first_name;
        this.last_name = last_name;
        this.photo = photo;
        this.user_group_id = user_group_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getChamber_id() {
        return chamber_id;
    }

    public void setChamber_id(String chamber_id) {
        this.chamber_id = chamber_id;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getDelete_message() {
        return delete_message;
    }

    public void setDelete_message(String delete_message) {
        this.delete_message = delete_message;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getUser_group_id() {
        return user_group_id;
    }

    public void setUser_group_id(String user_group_id) {
        this.user_group_id = user_group_id;
    }
}
