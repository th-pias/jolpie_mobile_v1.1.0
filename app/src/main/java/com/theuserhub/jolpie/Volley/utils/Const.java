package com.theuserhub.jolpie.Volley.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

import com.theuserhub.jolpie.R;


public class Const {
	public static final String URL_JSON_OBJECT = "http://api.androidhive.info/volley/person_object.json";
	public static final String URL_JSON_ARRAY = "http://api.androidhive.info/volley/person_array.json";
	public static final String URL_STRING_REQ = "http://api.androidhive.info/volley/string_response.html";
	public static final String URL_IMAGE = "http://api.androidhive.info/volley/volley-image.jpg";

	public static final String URL_BASE = "http://techmorich.com/";
	public static final String URL_DIR = "moapi/";

	public static final String URL_MENU = "http://techmorich.com/moapi/wp_get_menu.php";
	public static final String URL_ = URL_BASE+URL_DIR+"wp_get_menu.php";

	public static void showNoInternetAlert(final Context mContext)
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
		alert.setIcon(R.drawable.ic_no_internet);

		alert.setTitle("No Internet!");
		alert.setMessage("Please connect to the Internet.");

		alert.setPositiveButton("Settings", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				Intent intent = new Intent(
						Settings.ACTION_WIRELESS_SETTINGS);
				mContext.startActivity(intent);

			}
		});
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

				dialog.dismiss();


			}
		});

		alert.show();

	}
}
