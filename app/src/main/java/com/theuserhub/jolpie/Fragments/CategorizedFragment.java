package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.CompareActivity;
import com.theuserhub.jolpie.Adapters.CategorizedAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.CategoryItem;
import com.theuserhub.jolpie.Models.SearchUserModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 11/8/2015.
 */
public class CategorizedFragment extends Fragment {
    private Context context;
    private String TAG = "CategoryFragment";
    private String tag_string_req = "string_req";

    private ListView catItemListView;
    private List<SearchUserModel> eachItems;
    private List<String> compareIds;
    private CategorizedAdapter categorizedAdapter;
    private String tab_name,user_group_id;
    private Button compareButton;
    private boolean isEnd;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_categorized, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Bundle bundle = this.getArguments();
        int position = bundle.getInt("position", 0);
        tab_name = bundle.getString("tabname");

        isEnd = false;

        compareIds = new ArrayList<String>();

        compareButton = (Button) rootView.findViewById(R.id.compare_button);



        if (!AppFunctions.isNetworkConnected(context)) {
            Toast.makeText(context, "No network found \n fatched from db", Toast.LENGTH_LONG).show();
            //fatchFeedDb();
        } else {
            String url = null;
            if (tab_name.equals("Doctors")) {
                user_group_id = "2";
                url = context.getResources().getString(R.string.MAIN_URL) + "doctors";
            } else if (tab_name.equals("Hospitals")) {
                user_group_id = "4";
                url = context.getResources().getString(R.string.MAIN_URL) + "hospitals";
            } else if (tab_name.equals("Labs")) {
                user_group_id = "5";
                url = context.getResources().getString(R.string.MAIN_URL) + "labs";
            }
            /* else {
                url = context.getResources().getString(R.string.MAIN_URL) + "doctors";
            }*/

            catItemListView = (ListView) rootView.findViewById(R.id.categoryItem_lv);
            eachItems = new ArrayList<SearchUserModel>();
            //categorizedAdapter = new CategorizedAdapter(context, eachItems,compareIds,compareButton,appUserId,user_group_id,isEnd);
            //catItemListView.setAdapter(categorizedAdapter);

            getUsersRequest();
        }

        /*catItemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context, ProfileDetailActivity.class);
                startActivity(intent);
            }
        });*/
        /*if (user_group_id.equals("2")) {
            compareButton.setVisibility(View.VISIBLE);
        }
        else {
            compareButton.setVisibility(View.GONE);
        }*/

        compareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (compareIds.size() > 1) {
                    ArrayList<String> sendIds = new ArrayList<String>();
                    for (int c = 0; c < compareIds.size(); ++c) {
                        Log.e("ID", compareIds.get(c));
                        sendIds.add(compareIds.get(c));
                    }
                    Intent intent = new Intent(context, CompareActivity.class);
                    intent.putStringArrayListExtra("compare_ids", sendIds);
                    intent.putExtra("user_group_id", user_group_id);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "Please select 2 doctors to compare", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    public void enableCompareButton() {
        compareButton.setVisibility(View.VISIBLE);
    }

    public void disableCompareButton() {
        compareButton.setVisibility(View.GONE);
    }

    private void getUsersRequest () {
        String url = context.getResources().getString(R.string.MAIN_URL)+"getUsers";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG,"getUsers: "+response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()<8)
                    {
                        isEnd =true;
                    }
                    for (int i=0;i<jsonArray.length();++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);


                        List<SpecialityModel>specialityList = new ArrayList<SpecialityModel>();
                        String id, firstName,lastName,photo, user_group_id, degree, type,
                                district, review_count, review_star, isSaved, isConsult;
                        id = jsonObject.getString("id");
                        firstName = jsonObject.getString("first_name");
                        lastName = jsonObject.getString("last_name");
                        photo = jsonObject.getString("photo");
                        user_group_id = jsonObject.getString("user_group_id");
                        degree = jsonObject.getString("degree");
                        type = jsonObject.getString("type");//////
                        district = jsonObject.getString("district");
                        isConsult = jsonObject.getString("isConsult");

                        if (user_group_id.equals("2")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("speciality");
                            for (int j=0; j<jsonArray1.length();++j) {
                                JSONObject jsonObject1 = (JSONObject) jsonArray1.get(j);
                                String speciality_id = jsonObject1.getString("specialization_id");
                                String speciality_name = jsonObject1.getString("name");

                                SpecialityModel specialityModel = new SpecialityModel(speciality_id,speciality_name);
                                specialityList.add(specialityModel);
                            }
                        }
                        else {

                        }

                        JSONArray jsonArray2 = jsonObject.getJSONArray("review");
                        JSONObject jsonObject2 = (JSONObject) jsonArray2.get(0);

                        review_count = jsonObject2.getString("count");
                        review_star = jsonObject2.getString("rating");

                        isSaved = jsonObject.getString("isSaved");

                        CategoryItem categoryItem = new CategoryItem(id, firstName,lastName,photo, user_group_id, degree, type, district, review_count, review_star,specialityList,isSaved,isConsult,"", "", "");

                        //eachItems.add(categoryItem);
                    }
                    //categorizedAdapter.notifyDataSetChanged();
                    categorizedAdapter = new CategorizedAdapter(context, eachItems,compareIds,compareButton,appUserId,appUserGroupId,user_group_id,isEnd,"0","","","");
                    catItemListView.setAdapter(categorizedAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_group_id", user_group_id);
                params.put("user_id", appUserId);
                params.put("start_index", "0");
                params.put("take_total", "8");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}
