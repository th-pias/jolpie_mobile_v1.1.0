package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.ReviewListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.ReviewModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 2/28/2016.
 */
public class ReceivedReviewsFragment extends Fragment{

    private Context context;
    private String TAG = "ReceivedReviewsFragment", tag_string_req = "rec_rev_str_req";

    private ListView reviewsListView;
    private List<ReviewModel> eachItems;
    private ReviewListAdapter reviewListAdapter;
    private TextView emptyTextView;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_received_reviews, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        emptyTextView = (TextView) rootView.findViewById(R.id.empty_tv);
        reviewsListView = (ListView) rootView.findViewById(R.id.received_reviews_lv);
        eachItems = new ArrayList<ReviewModel>();
        reviewListAdapter = new ReviewListAdapter(context, eachItems, appUserId, getActivity().getSupportFragmentManager());
        reviewsListView.setAdapter(reviewListAdapter);

        getReceivedReviews();

        return rootView;
    }

    private void getReceivedReviews() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "myRecievedReviews";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                eachItems.clear();
                try {
                    JSONArray allReview = new JSONArray(response);
                    if (allReview.length()<=0) {
                        emptyTextView.setVisibility(View.VISIBLE);
                    }
                    else {
                        emptyTextView.setVisibility(View.GONE);
                        for (int i = 0; i < allReview.length(); ++i) {
                            JSONObject singleReview = (JSONObject) allReview.get(i);
                            String review_id, review_by, review_title, review_description, review_created_at, review_star_count, review_fname, review_lname, review_by_photo, review_group_id;
                            review_id = singleReview.getString("review_id");
                            review_by = singleReview.getString("user_id");
                            review_title = singleReview.getString("review_title");
                            review_description = singleReview.getString("review_description");
                            review_created_at = singleReview.getString("created_at");
                            review_star_count = singleReview.getString("star_count");
                            review_fname = singleReview.getString("first_name");
                            review_lname = singleReview.getString("last_name");
                            review_by_photo = singleReview.getString("photo");
                            review_group_id = singleReview.getString("user_group_id");

                            ReviewModel reviewModel = new ReviewModel(review_id, review_by, review_title, review_description, review_created_at, review_star_count, review_fname, review_lname, review_by_photo, review_group_id);

                            eachItems.add(reviewModel);
                        }
                        reviewListAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}
