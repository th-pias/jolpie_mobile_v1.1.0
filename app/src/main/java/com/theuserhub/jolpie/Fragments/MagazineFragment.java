package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.MagazineActivity;
import com.theuserhub.jolpie.Activity.MagazineDetailActivity;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Adapters.MagazineListAdapter;
import com.theuserhub.jolpie.Models.ArticlesModel;
import com.theuserhub.jolpie.Models.MagazinesModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 1/28/2016.
 */
public class MagazineFragment extends Fragment{

    private Context context;
    private String TAG = "MagazineFragment", tag_string_req = "magazine_str_req";

    private ListView magazineListView;
    private List<MagazinesModel>magazineList;
    private List<ArticlesModel>articleList;
    private MagazineListAdapter magazineListAdapter;

    private RelativeLayout progressLayout;
    private ImageView latestArticleImageView;
    private NetworkImageView latestArticleNImageView;
    private TextView latestCatTextView,latestTitleTextView,latestDesTextView,
            authorTextView,likeCountTextView,commentCountTextView,likeImageView,
            commentImageView,emptyTextView;
    private ArticlesModel latestArticleModel;
    private String latest_category,latest_category_id;

    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_magazine, container, false);

        progressLayout = (RelativeLayout) rootView.findViewById(R.id.progress_layout);
        emptyTextView = (TextView) rootView.findViewById(R.id.empty_tv);

        magazineListView = (ListView) rootView.findViewById(R.id.magazine_lv);
        magazineList = new ArrayList<MagazinesModel>();
        magazineListAdapter = new MagazineListAdapter(context,magazineList);
        magazineListView.setAdapter(magazineListAdapter);

        final LayoutInflater header = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup latestArticle = (ViewGroup) header.inflate(R.layout.header_magazine_list, null);

        latestCatTextView = (TextView) latestArticle.findViewById(R.id.magazine_latest_post_cat_tv);
        latestArticleImageView = (ImageView) latestArticle.findViewById(R.id.magazine_latest_post_iv);
        latestArticleNImageView = (NetworkImageView) latestArticle.findViewById(R.id.magazine_latest_post_niv);
        latestTitleTextView = (TextView) latestArticle.findViewById(R.id.magazine_latest_post_title_tv);
        latestDesTextView = (TextView) latestArticle.findViewById(R.id.magazine_latest_post_des_tv);
        authorTextView = (TextView) latestArticle.findViewById(R.id.magazine_latest_post_author_tv);
        likeImageView = (TextView) latestArticle.findViewById(R.id.magazine_like_iv);
        likeCountTextView = (TextView) latestArticle.findViewById(R.id.magazine_like_count_tv);
        commentImageView = (TextView) latestArticle.findViewById(R.id.magazine_comment_iv);
        commentCountTextView = (TextView) latestArticle.findViewById(R.id.magazine_comment_count_tv);

        magazineListView.addHeaderView(latestArticle);
        magazineListView.setHeaderDividersEnabled(false);

        Typeface articleFont = Typeface.createFromAsset( context.getAssets(), "fontawesome-webfont.ttf" );
        likeImageView.setTypeface(articleFont);
        commentImageView.setTypeface(articleFont);

        /*latestArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        latestArticleNImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoArticleDetail();
            }
        });

        latestArticleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoArticleDetail();
            }
        });

        latestTitleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoArticleDetail();
            }
        });

        latestDesTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoArticleDetail();
            }
        });

        latestCatTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MagazineActivity.class);
                intent.putExtra("category_id",latest_category_id);
                intent.putExtra("category_name",latest_category);
                startActivity(intent);
            }
        });

        authorTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ProfileDetailNewActivity.class);
                intent.putExtra("user_id",latestArticleModel.getAuthor_id());
                intent.putExtra("user_fname",latestArticleModel.getAuthor_fname());
                intent.putExtra("user_lname",latestArticleModel.getAuthor_lname());
                intent.putExtra("user_email","");
                intent.putExtra("group_id",latestArticleModel.getAuthor_group_id());
                intent.putExtra("user_image_url",latestArticleModel.getAuthor_photo());
                context.startActivity(intent);
            }
        });

        getLatestArticle();

        return rootView;
    }

    private void getLatestArticle() {
        progressLayout.setVisibility(View.VISIBLE);
        String url = getActivity().getResources().getString(R.string.MAIN_URL) + "lastArticle";
        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Latest: "+response.toString());
                progressLayout.setVisibility(View.GONE);
                try {
                    JSONArray articleInfo = new JSONArray(response);
                    for (int i=0; i<articleInfo.length();++i) {
                        JSONObject articleObject = (JSONObject) articleInfo.get(i);
                        String magazine_id = articleObject.getString("magazine_id");
                        String title = articleObject.getString("title");
                        String description = Html.fromHtml(articleObject.getString("description")).toString();
                        String author_id = articleObject.getString("author_id");
                        String created_at = articleObject.getString("created_at");
                        String picture = articleObject.getString("picture");
                        String like_count = articleObject.getString("good_count");
                        String comment_count = articleObject.getString("answer_count");
                        String share_count = articleObject.getString("share_count");
                        latest_category_id = articleObject.getString("category_id");
                        String first_name = articleObject.getString("first_name");
                        String last_name = articleObject.getString("last_name");
                        String photo = articleObject.getString("photo");
                        String user_group_id = articleObject.getString("user_group_id");
                        latest_category = articleObject.getString("magazine_category");

                        latestArticleModel = new ArticlesModel(magazine_id,title,description,author_id,created_at,picture,
                                like_count,comment_count,share_count,latest_category_id,first_name,last_name,photo,user_group_id);

                        latestCatTextView.setText(latest_category);
                        latestTitleTextView.setText(latestArticleModel.getTitle());
                        if (latestArticleModel.getDescription().length() > 100) {
                            String tempString = latestArticleModel.getDescription().substring(0, 99) + "...Continue Reading";

                            Spannable wordtoSpan = new SpannableString(tempString);

                            wordtoSpan.setSpan(new ForegroundColorSpan(Color.GRAY), 102, 118, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            latestDesTextView.setText(wordtoSpan);
                        } else {
                            latestDesTextView.setText(latestArticleModel.getDescription());
                        }

                        authorTextView.setText(latestArticleModel.getAuthor_fname()+" "+latestArticleModel.getAuthor_lname());
                        likeCountTextView.setText(latestArticleModel.getLike_count());
                        commentCountTextView.setText(latestArticleModel.getComment_count());

                        if (picture.length()>1) {
                            if (imageLoader == null)
                                imageLoader = AppController.getInstance().getImageLoader();

                            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + picture;
                            latestArticleNImageView.setImageUrl(meta_url, imageLoader);
                        }
                        else {
                            latestArticleNImageView.setVisibility(View.GONE);
                            latestArticleImageView.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                getCategorizedArticles();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load data\nTap to retry.");
                //magazineListView.setVisibility(View.GONE);
                emptyTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emptyTextView.setVisibility(View.GONE);
                        getLatestArticle();
                    }
                });
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req );
    }

    private void getCategorizedArticles() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "categorizedArticles";
        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d(TAG, "Ordered: "+response.toString());
                try {
                    JSONArray allMagazines = new JSONArray(response);
                    for (int i=0;i<allMagazines.length();++i) {
                        JSONObject oneMagazine = (JSONObject) allMagazines.get(i);
                        String magazine_category_id = oneMagazine.getString("magazine_category_id");
                        String magazine_category = oneMagazine.getString("magazine_category");
                        JSONArray allPosts = (JSONArray) oneMagazine.get("posts");
                        articleList = new ArrayList<ArticlesModel>();
                        for (int j=0;j<allPosts.length();++j) {
                            JSONObject onePost = (JSONObject) allPosts.get(j);
                            String magazine_id = onePost.getString("magazine_id");
                            String title = onePost.getString("title");
                            String description = onePost.getString("description");
                            String author_id = onePost.getString("author_id");
                            String created_at = onePost.getString("created_at");
                            String picture = onePost.getString("picture");
                            String like_count = onePost.getString("good_count");
                            String comment_count = onePost.getString("answer_count");
                            String share_count = onePost.getString("share_count");
                            String category_id = onePost.getString("category_id");

                            articleList.add(new ArticlesModel(magazine_id,title,description,author_id,created_at,picture,like_count,comment_count,share_count,category_id));
                        }

                        magazineList.add(new MagazinesModel(magazine_category_id,magazine_category,articleList));
                    }
                    magazineListAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req );
    }

    private void gotoArticleDetail() {
        Intent intent = new Intent(context, MagazineDetailActivity.class);
        intent.putExtra("magazine_id", latestArticleModel.getId());
        intent.putExtra("title", latestArticleModel.getTitle());
        intent.putExtra("description", latestArticleModel.getDescription());
        intent.putExtra("author_id", latestArticleModel.getAuthor_id());
        intent.putExtra("created_at", latestArticleModel.getCreated_at());
        intent.putExtra("picture", latestArticleModel.getPicture());
        intent.putExtra("like_count", latestArticleModel.getLike_count());
        intent.putExtra("comment_count", latestArticleModel.getComment_count());
        intent.putExtra("share_count", latestArticleModel.getShare_count());
        intent.putExtra("category_id", latestArticleModel.getCategory_id());
        intent.putExtra("first_name", latestArticleModel.getAuthor_fname());
        intent.putExtra("last_name", latestArticleModel.getAuthor_lname());
        intent.putExtra("photo", latestArticleModel.getAuthor_photo());
        intent.putExtra("user_group_id", latestArticleModel.getAuthor_group_id());
        intent.putExtra("category_name", latest_category);
        startActivity(intent);
    }
}
