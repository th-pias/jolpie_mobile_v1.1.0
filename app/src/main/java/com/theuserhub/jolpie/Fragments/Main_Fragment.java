package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.theuserhub.jolpie.Adapters.AppointmentTabAdapter;
import com.theuserhub.jolpie.Adapters.MainActivityTabAdapter;
import com.theuserhub.jolpie.Adapters.ScrolledTab_Adapter;
import com.theuserhub.jolpie.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 10/5/2015.
 */
public class Main_Fragment extends Fragment {
    private String TAG = "Main_Fragment";
    private Context context;
    private List<String> tabList;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    //private String[] tabs_jolpie = {"Home", "Doctors", "Hospitals", "Labs", "Drugs", "Groups", "Magazine", "Care At Home"};
    private String[] tabs_jolpie = {"Home","Groups", "Magazine", "Care At Home"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        // Initialize the ViewPager and set an adapter
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorHeight(8);
        //tabLayout.

        tabList = new ArrayList<>();
        for (int i = 0; i < tabs_jolpie.length; i++) {
            tabList.add(tabs_jolpie[i]);
        }

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        /*pager.setAdapter(new ScrolledTab_Adapter(context, getActivity().getSupportFragmentManager(), tabList));
        pager.setCurrentItem(tab_position);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        tabs.setViewPager(pager);
        tabs.setIndicatorHeight(8);
        tabs.setIndicatorColorResource(R.color.colorPrimary);
        //tabs.set
        tabs.setTextColor(0xFFFFFFFF);
        tabs.setAllCaps(false);

        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //Toast.makeText(context,"position: "+position,Toast.LENGTH_LONG).show();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/

        return rootView;
    }

    private void setupViewPager(ViewPager viewPager) {
        MainActivityTabAdapter adapter = new MainActivityTabAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "Home");
        adapter.addFragment(new GroupsFragment(), "Groups");
        adapter.addFragment(new MagazineFragment(), "Magazine");
        adapter.addFragment(new CareAtHomeFragment(), "Care At Home");
        viewPager.setAdapter(adapter);
    }

}
