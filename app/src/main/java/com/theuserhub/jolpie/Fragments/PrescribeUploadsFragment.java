package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.AddPrescriptionImageActivity;
import com.theuserhub.jolpie.Activity.AlbumCreateActivity;
import com.theuserhub.jolpie.Adapters.UploadPrescriptionAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.UploadPrescriptionsModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 3/10/2016.
 */
public class PrescribeUploadsFragment extends Fragment{
    private Context context;
    private String TAG = "PrescribeUploadsFragment", tag_string_req = "uploads_pres_str_req";
    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    private List<UploadPrescriptionsModel> uploadPrescriptionsList;
    private GridView gridView;
    private UploadPrescriptionAdapter uploadPrescriptionAdapter;
    private TextView noData;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_prescribe_uploads, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,AddPrescriptionImageActivity.class);
                startActivity(intent);
            }
        });

        noData = (TextView) rootView.findViewById(R.id.no_image_tv);

        gridView = (GridView) rootView.findViewById(R.id.gridview_prescription_gallery);
        uploadPrescriptionsList = new ArrayList<UploadPrescriptionsModel>();

        uploadPrescriptionAdapter = new UploadPrescriptionAdapter(context,uploadPrescriptionsList);
        gridView.setAdapter(uploadPrescriptionAdapter);

        uploadePresRequest();
        return rootView;
    }

    private void uploadePresRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "uploadPrescriptions";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "upload: " + response.toString());
                uploadPrescriptionsList.clear();
                JSONArray allPosts = null;
                try {
                    allPosts = new JSONArray(response);
                    if(allPosts.length()<=0){
                        noData.setVisibility(View.VISIBLE);
                    }
                    for (int i = 0; i < allPosts.length(); ++i) {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        String id = singlePost.getString("id");
                        String user_id = singlePost.getString("user_id");
                        String prescription_image = singlePost.getString("prescription_image");
                        String created_at = singlePost.getString("created_at");

                        UploadPrescriptionsModel uploadPrescriptionsModel = new UploadPrescriptionsModel( id,  user_id,  prescription_image,  created_at);
                        uploadPrescriptionsList.add(uploadPrescriptionsModel);
                    }
                    uploadPrescriptionAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //Toast.makeText(context, "Prescription saved", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onResume() {
        super.onResume();
        uploadePresRequest();
    }
}
