package com.theuserhub.jolpie.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Activity.ReviewListActivity;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 1/11/2016.
 */
public class ReviewDialogFragment extends DialogFragment{

    private String tag_string_req = "review_string_req";
    String user_id,reviewer_id,review_title,review_description,review_star,flag;
    private Dialog dialog;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_review_layout, null);

        review_star = "5.0";
        review_title = "";
        review_description = "";

        Bundle bundle = getArguments();
        flag = bundle.getString("flag");
        user_id=bundle.getString("user_id");
        reviewer_id=bundle.getString("reviewer_id");
        Log.d("Bundle",user_id+":"+reviewer_id);

        RatingBar ratingBar = (RatingBar) rootView.findViewById(R.id.review_rating_star);
        final TextView ratingText = (TextView) rootView.findViewById(R.id.review_rating_text);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                //Log.d("Rating","Rating: "+rating);
                if (rating == 5.0) {
                    ratingText.setText("Excellent");
                    review_star = "5.0";
                } else if (rating == 4.0) {
                    ratingText.setText("Good");
                    review_star = "4.0";
                } else if (rating == 3.0) {
                    ratingText.setText("It's OK");
                    review_star = "3.0";
                } else if (rating == 2.0) {
                    ratingText.setText("Not Bad");
                    review_star = "2.0";
                } else if (rating == 1.0) {
                    ratingText.setText("Bad");
                    review_star = "1.0";
                }
            }
        });

        final TextView reviewTitle = (TextView) rootView.findViewById(R.id.review_title_tv);
        final TextView reviewDes = (TextView) rootView.findViewById(R.id.review_description_tv);
        TextView reviewSubmit = (TextView) rootView.findViewById(R.id.review_submit_tv);
        reviewSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                review_title = reviewTitle.getText().toString();
                review_description = reviewDes.getText().toString();
                sendReviewRequest();
            }
        });
        builder.setView(rootView);
        dialog = builder.create();
        return dialog;
    }

    private void sendReviewRequest() {
        String url = getActivity().getResources().getString(R.string.MAIN_URL) + "sendReview";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("ReviewSend", response.toString());
                if (flag.equals("revlist")) {
                    ((ReviewListActivity)getActivity()).setUpdateDataReviewList();
                    ((ReviewListActivity)getActivity()).disableReviewListFAB();
                }
                else if (flag.equals("prodtl")) {
                    ((ProfileDetailNewActivity)getActivity()).updateReviewProfile();
                }
                dialog.dismiss();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("ReviewSend", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("reviewer_id", reviewer_id);
                params.put("review_title", review_title);
                params.put("review_description", review_description);
                params.put("star_count",review_star);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req );
    }
}
