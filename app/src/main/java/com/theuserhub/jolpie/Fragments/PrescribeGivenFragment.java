package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.PrescriptionViewActivity;
import com.theuserhub.jolpie.Adapters.PrescriptionAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.GivenPrescriptionModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 3/10/2016.
 */
public class PrescribeGivenFragment extends Fragment{
    private Context context;
    private String TAG = "PrescribeGivenFragment", tag_string_req = "given_pres_str_req";
    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    private PrescriptionAdapter prescriptionAdapter;
    private List<GivenPrescriptionModel> givenPrescriptionModelList;
    private ListView listView;
    private TextView noData;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_prescribe_given, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        listView = (ListView) rootView.findViewById(R.id.given_pres_lv);
        noData = (TextView) rootView.findViewById(R.id.no_data);
        givenPrescriptionModelList = new ArrayList<GivenPrescriptionModel>();

        prescriptionAdapter = new PrescriptionAdapter(context,givenPrescriptionModelList);
        listView.setAdapter(prescriptionAdapter);
        givenPresRequest();
        
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(context, PrescriptionViewActivity.class);
                i.putExtra("getPatient_id",givenPrescriptionModelList.get(position).getPatient_id());
                i.putExtra("getAppointment_id",givenPrescriptionModelList.get(position).getAppointment_id());
                i.putExtra("flag","patient");
                context.startActivity(i);
            }
        });

        return rootView;
    }

    private void givenPresRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "givenPrescriptions";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "given: " + response.toString());
                JSONArray allPosts = null;
                try {
                    allPosts = new JSONArray(response);
                    if(allPosts.length()<=0){
                        noData.setVisibility(View.VISIBLE);
                    }
                    for (int i = 0; i < allPosts.length(); ++i) {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        final String prescription_id = singlePost.getString("prescription_id");
                        final String conditions = singlePost.getString("conditions");
                        final String advice = singlePost.getString("advice");
                        final String patient_id = singlePost.getString("patient_id");
                        final String doctor_id = singlePost.getString("doctor_id");
                        final String appointment_id = singlePost.getString("appointment_id");
                        final String updated_at = singlePost.getString("updated_at");
                        final String created_at = singlePost.getString("created_at");
                        final String first_name = singlePost.getString("first_name");
                        final String last_name = singlePost.getString("last_name");
                        final String photo = singlePost.getString("photo");
                        final String user_group_id = singlePost.getString("user_group_id");

                        GivenPrescriptionModel givenPrescriptionModel = new GivenPrescriptionModel(prescription_id,conditions,advice,patient_id,doctor_id,appointment_id,updated_at,created_at,first_name,last_name,photo,user_group_id);
                        givenPrescriptionModelList.add(givenPrescriptionModel);
                    }
                    prescriptionAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //Toast.makeText(context, "Prescription saved", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
