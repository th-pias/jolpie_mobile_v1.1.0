package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theuserhub.jolpie.R;

/**
 * Created by Admin on 11/9/2015.
 */
public class InfoFragment extends Fragment{
    private Context context;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_info, container, false);
        return rootView;
    }
}
