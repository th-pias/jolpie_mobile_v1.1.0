package com.theuserhub.jolpie.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.theuserhub.jolpie.R;

/**
 * Created by Admin on 1/23/2016.
 */
public class FeelingsDialogFragment extends DialogFragment {
    private ArrayAdapter adapter;
    private ListView feelingListView;
    private Dialog dialog;
    String[] planets = new String[]{"Apple", "Banana", "Mango", "Grapes"};

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_feeling_layout, null);

        feelingListView = (ListView) rootView.findViewById(R.id.feeling_lv);
        adapter = new ArrayAdapter(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, android.R.id.text1, planets);

        return dialog;
    }
}
