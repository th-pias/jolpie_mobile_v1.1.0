package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.theuserhub.jolpie.Activity.PostActivity;
import com.theuserhub.jolpie.Adapters.FeedListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.NewsfeedModel;
import com.theuserhub.jolpie.Models.PostInfoModel;
import com.theuserhub.jolpie.Models.SinglePostModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 11/8/2015.
 */
public class HomeFragment extends Fragment{
    private Context context;
    private String TAG = "HomeFragment";
    private String tag_string_req = "feed_str_req";

    private MaterialRefreshLayout materialRefreshLayout;
    private ListView feedListView;
    private List<SinglePostModel> feedList;
    private FeedListAdapter adapter;
    private boolean isEnd;
    private TextView emptyTextView;
    private RelativeLayout progressLayout;

    private List<NewsfeedModel>eachNewsfeed;
    private List<PostInfoModel>eachPost;
    private Typeface homeFont;

    //private HomeDatabaseHandler handler;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName,appUserLName,appUserEmail,appUserId,appUserGroupId,appUserImgUrl;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl=user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent intent = new Intent(context, PostActivity.class);
                intent.putExtra("group_id","0");
                startActivity(intent);
            }
        });

        homeFont = Typeface.createFromAsset( context.getAssets(), "fontawesome-webfont.ttf" );
        isEnd = false;

        emptyTextView = (TextView) rootView.findViewById(R.id.empty_tv);
        progressLayout = (RelativeLayout) rootView.findViewById(R.id.progress_layout);
        feedListView= (ListView) rootView.findViewById(R.id.feedlist_lv);
        feedList = new ArrayList<SinglePostModel>();
        eachNewsfeed = new ArrayList<NewsfeedModel>();
        //adapter = new FeedListAdapter(context, eachNewsfeed, appUserId);
        //feedListView.setAdapter(adapter);

        //handler=new HomeDatabaseHandler(context);

        //if (!AppFunctions.isNetworkConnected(context)) {
            //Toast.makeText(context, "No network found \n fatched from db", Toast.LENGTH_LONG).show();
            //fatchFeedDb();
        //}
       // else {
            String url=context.getResources().getString(R.string.MAIN_URL)+"allPosts";
            //fatchFeed(url);
            //getNewsfeeds();
        //}


        materialRefreshLayout = (MaterialRefreshLayout) rootView.findViewById(R.id.home_refresh);

        //materialRefreshLayout.setLoadMore(true);
        //materialRefreshLayout.finishRefreshLoadMore();
        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                materialRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ////request from here..............................
                        //if (!AppFunctions.isNetworkConnected(context)) {
                            //Toast.makeText(context, "No network found \n fatched from db", Toast.LENGTH_LONG).show();
                            //fatchFeedDb();
                        //}
                        //else {
                            String url=context.getResources().getString(R.string.MAIN_URL)+"allPosts";
                            //fatchFeed(url);
                            getNewsfeeds();
                        //}
                        materialRefreshLayout.finishRefresh();
                    }
                }, 3000);

            }

            @Override
            public void onfinish() {
                //Toast.makeText(context, "finish", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRefreshLoadMore(final MaterialRefreshLayout materialRefreshLayout) {
                //Toast.makeText(context, "load more", Toast.LENGTH_LONG).show();

                ////load more request from here.....................
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //materialRefreshLayout.autoRefresh();
            }
        },3000);

        return rootView;
    }

    /*private void fatchFeedDb() {
        List<SinglePostModel> posts = handler.getAllContacts();

        for (SinglePostModel pst : posts)
        {
            feedList.add(pst);
        }
        adapter.notifyDataSetChanged();
    }*/

    private void getNewsfeeds() {

        progressLayout.setVisibility(View.VISIBLE);
        String url = context.getResources().getString(R.string.MAIN_URL)+"getNewsfeeds";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                progressLayout.setVisibility(View.GONE);
                eachNewsfeed.clear();
                String id,section,section_id,activity,activity_at,by_user_id,by_user_fname,by_user_lname,by_user_photo,by_user_group_id;
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()<=0) {
                        emptyTextView.setVisibility(View.VISIBLE);
                        emptyTextView.setText("You have no Updates.");
                        feedListView.setVisibility(View.GONE);
                    }
                    else {
                        emptyTextView.setVisibility(View.GONE);
                        feedListView.setVisibility(View.VISIBLE);
                        if (jsonArray.length() < 5) {
                            isEnd = true;
                        }
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            //eac
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            id = jsonObject.getString("newsfeed_id");
                            section = jsonObject.getString("section");
                            section_id = jsonObject.getString("section_id");
                            activity = jsonObject.getString("activity");
                            activity_at = jsonObject.getString("activity_at");
                            by_user_id = jsonObject.getString("by_user_id");
                            by_user_fname = jsonObject.getString("by_user_fname");
                            by_user_lname = jsonObject.getString("by_user_lname");
                            by_user_photo = jsonObject.getString("by_user_photo");
                            by_user_group_id = jsonObject.getString("by_user_group_id");

                            JSONObject infoJsonObject = jsonObject.getJSONObject("info");

                            String post_id, title, description, feeling, photo, video, created_at,
                                    updated_at, by_id, by_fname, by_lname, by_photo, by_group_id,
                                    to_id, to_fname, to_lname, to_photo, to_user_group_id,
                                    like_count, answer_count, share_count, star_count, liked_by_me, shared_by_me;
                            post_id = infoJsonObject.getString("section_post_id");
                            title = infoJsonObject.getString("section_title");
                            description = Html.fromHtml(infoJsonObject.getString("section_description")).toString();
                            feeling = infoJsonObject.getString("section_feeling").trim();
                            photo = infoJsonObject.getString("section_photo");
                            video = infoJsonObject.getString("section_video");
                            created_at = infoJsonObject.getString("section_at");
                            updated_at = infoJsonObject.getString("section_updated_at");
                            by_id = infoJsonObject.getString("section_by");
                            by_fname = infoJsonObject.getString("section_by_fname");
                            by_lname = infoJsonObject.getString("section_by_lname");
                            by_photo = infoJsonObject.getString("section_by_photo");
                            by_group_id = infoJsonObject.getString("section_by_user_group_id");
                            to_id = infoJsonObject.getString("section_to");
                            to_fname = infoJsonObject.getString("section_to_fname");
                            to_lname = infoJsonObject.getString("section_to_lname");
                            to_photo = infoJsonObject.getString("section_to_photo");
                            to_user_group_id = infoJsonObject.getString("section_to_user_group_id");
                            like_count = infoJsonObject.getString("good_count");
                            answer_count = infoJsonObject.getString("answer_count");
                            share_count = infoJsonObject.getString("share_count");
                            star_count = infoJsonObject.getString("star_count");
                            liked_by_me = infoJsonObject.getString("liked_by_me");
                            shared_by_me = infoJsonObject.getString("shared_by_me");

                            PostInfoModel infoModel = new PostInfoModel(post_id, title, description, feeling, photo, video, created_at,
                                    updated_at, by_id, by_fname, by_lname, by_photo, by_group_id,
                                    to_id, to_fname, to_lname, to_photo, to_user_group_id,
                                    like_count, answer_count, share_count, star_count, liked_by_me, shared_by_me);

                            NewsfeedModel newsfeedModel = new NewsfeedModel(id, section, section_id, activity, activity_at, by_user_id, by_user_fname, by_user_lname, by_user_photo, by_user_group_id, infoModel);

                            eachNewsfeed.add(newsfeedModel);
                        }
                        //adapter.notifyDataSetChanged();
                        adapter = new FeedListAdapter(context, eachNewsfeed, appUserId, appUserGroupId, isEnd);
                        feedListView.setAdapter(adapter);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                progressLayout.setVisibility(View.GONE);
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load Updates.\nTap to retry.");

                emptyTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emptyTextView.setVisibility(View.GONE);
                        getNewsfeeds();
                    }
                });
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("user_group_id", appUserGroupId);
                params.put("start_index", "0");
                params.put("take_total", "5");
                //Log.d(TAG,+appUserId+magazine_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /*private void fatchFeed(String url){
        //url="http://10.0.3.2/doc5/api/allPosts";

        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                //handler.deleteAll();
                feedList.clear();
                try {
                    JSONArray allPosts=new JSONArray(response);
                    //Log.d("Test2", allPosts.toString());
                    for (int i=0;i<allPosts.length();++i) {
                        JSONObject singlePost=(JSONObject)allPosts.get(i);
                        String post_id=singlePost.getString("id");
                        String post_title=singlePost.getString("title");
                        String post_des=singlePost.getString("description");
                        String post_likes=singlePost.getString("good_count");
                        String post_answers=singlePost.getString("answer_count");
                        String post_follows=singlePost.getString("follow_count");
                        String post_userId=singlePost.getString("user_id");
                        String post_userGroupId=singlePost.getString("group_id");
                        String post_created=singlePost.getString("created_at");
                        String post_updated=singlePost.getString("updated_at");
                        String post_isdeleted=singlePost.getString("is_deleted");
                        String post_user_fname=singlePost.getString("first_name");
                        String post_user_lname=singlePost.getString("last_name");
                        String post_photo=singlePost.getString("photo");
                        String post_video=singlePost.getString("video");
                        //Toast.makeText(context,post_id+"\n"+post_title+"\n"+post_des,Toast.LENGTH_LONG).show();
                        SinglePostModel pst=new SinglePostModel(post_id,post_title,post_des,post_likes,post_answers,post_follows,post_userId,
                                post_userGroupId,post_created,post_updated,post_isdeleted,post_user_fname,post_user_lname,post_photo,post_video);
                        feedList.add(pst);
                        //handler.addContact(pst);
                    }

                    //refresh adapter
                    adapter.notifyDataSetChanged();

                    //feedList.indexOf();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Test", "Error: " + error.getMessage());
                //fatchFeedDb();
                Toast.makeText(context, "Some error occured \n fatched from db" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req + TAG);
    }*/

    @Override
    public void onPause() {
        super.onPause();
        //cancelling all request when fragment in pause....
        AppController.getInstance().cancelPendingRequests(tag_string_req + TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        String url=context.getResources().getString(R.string.MAIN_URL)+"allPosts";
        //fatchFeed(url);
        getNewsfeeds();
    }
}
