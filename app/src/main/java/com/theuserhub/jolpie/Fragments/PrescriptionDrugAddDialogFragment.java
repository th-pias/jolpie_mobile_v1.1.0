package com.theuserhub.jolpie.Fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.PrescriptionActivity;
import com.theuserhub.jolpie.Adapters.GroupMemberAddAdapter;
import com.theuserhub.jolpie.Models.GroupMemberModel;
import com.theuserhub.jolpie.Models.PrescribedDrugModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 1/25/2016.
 */
public class PrescriptionDrugAddDialogFragment extends DialogFragment{

    private Context context;
    private String TAG = "PrescriptionDrugAddDialogFragment", tag_string_req = "drug_add_str_req";

    private Dialog dialog;
    private String drug_name,power,dosage,noOfDays,isEdit, search_item;
    private EditText powerEditText,dosageEditText,daysEditText;
    private TextView addTextView;
    private int position;
    private AutoCompleteTextView membrAddAutoTextView;
    private List<String>eachItems;
    private ArrayAdapter adapter;
    String[] languages={"Android ","java","IOS","SQL","JDBC","Web services","Android 1","java1","IOS1","SQL1","JDBC1","Web services1","Android 2","java2","IOS2","SQL2","JDBC2","Web services2"};

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        isEdit = bundle.getString("isEdit");
        position = bundle.getInt("position");
        drug_name = bundle.getString("drug_name");
        power = bundle.getString("power");
        dosage = bundle.getString("dosage");
        noOfDays = bundle.getString("noofdays");

        search_item = "";
        context = getActivity().getApplicationContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_prescription_drug_add, null);
        membrAddAutoTextView = (AutoCompleteTextView) rootView.findViewById(R.id.group_member_add_autoComplete);
        powerEditText = (EditText) rootView.findViewById(R.id.pres_drug_add_quantity_et);
        dosageEditText = (EditText) rootView.findViewById(R.id.pres_drug_add_dosage_et);
        daysEditText = (EditText) rootView.findViewById(R.id.pres_drug_add_days_et);
        addTextView = (TextView) rootView.findViewById(R.id.pres_drug_add_tv);

        eachItems = new ArrayList<String>();
        adapter = new ArrayAdapter(getActivity().getApplicationContext(),R.layout.list_row_drug_autocomplete,R.id.drug_name_tv,eachItems);
        membrAddAutoTextView.setAdapter(adapter);
        membrAddAutoTextView.setThreshold(1);

        membrAddAutoTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = String.valueOf(s);
                if (temp.trim().length()>0)
                {
                    search_item = temp;
                    getDrugSugestion();
                }
            }
        });

        if (isEdit.equals("yes")) {
            membrAddAutoTextView.setText(drug_name);
            membrAddAutoTextView.setSelection(drug_name.length());
            powerEditText.setText(power);
            dosageEditText.setText(dosage);
            daysEditText.setText(noOfDays);
        }

        addTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drug_name = membrAddAutoTextView.getText().toString().trim();
                power = powerEditText.getText().toString().trim();
                dosage = dosageEditText.getText().toString().trim();
                noOfDays = daysEditText.getText().toString().trim();

                if (drug_name.length()>0) {
                    PrescribedDrugModel drugModel = new PrescribedDrugModel(drug_name, power, dosage, noOfDays);

                    if (isEdit.equals("yes")) {
                        ((PrescriptionActivity) getActivity()).editDrugFromList(position, drugModel);
                    } else {
                        ((PrescriptionActivity) getActivity()).addDrugToList(drugModel);
                    }
                    dialog.dismiss();
                }
                else {
                    Toast.makeText(getActivity().getApplicationContext(),"Please fill up Drug name",Toast.LENGTH_SHORT).show();
                }

            }
        });
        builder.setView(rootView);
        dialog = builder.create();
        return dialog;
    }

    private void getDrugSugestion() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getDrugNames";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                eachItems.clear();
                try {
                    JSONArray allUsers = new JSONArray(response);
                    for (int i = 0;i<allUsers.length();++i) {
                        JSONObject jsonObject = allUsers.getJSONObject(i);
                        String drug_gen_name = jsonObject.getString("first_name");
                        eachItems.add(drug_gen_name);
                    }

                    adapter.notifyDataSetChanged();
                    adapter = new ArrayAdapter(getActivity().getApplicationContext(),R.layout.list_row_drug_autocomplete,R.id.drug_name_tv,eachItems);
                    membrAddAutoTextView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("search_item", search_item);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
