package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.AppointmentsAdapter;
import com.theuserhub.jolpie.Adapters.MyAppointmentAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.AppointmentsModel;
import com.theuserhub.jolpie.Models.MyAppointmentModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 2/21/2016.
 */
public class AppointmentsFragment extends Fragment{
    private Context context;
    private String TAG = "AppointmentsFragment", tag_string_req = "appot_str_req";

    private Spinner dateSpinner;
    private TextView dateShowTextView, emptyTextView;
    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;
    private Date todayDate, nextDate;
    private String todayString, nextString, start_date, end_date;
    private List<String> datefilterList;
    private ArrayAdapter<String> dateSpinnerAdapter;

    private ListView myAppointmentListView;
    private AppointmentsAdapter appointmentAdapter;
    private List<AppointmentsModel> eachAppointments;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_appointments, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        todayDate = new Date();
        nextDate = new Date();

        dateSpinner = (Spinner) rootView.findViewById(R.id.date_filter_spinner);

        dateShowTextView = (TextView) rootView.findViewById(R.id.date_range_tv);
        emptyTextView = (TextView) rootView.findViewById(R.id.empty_data_tv);

        myAppointmentListView = (ListView) rootView.findViewById(R.id.my_appointments_lv);
        eachAppointments = new ArrayList<AppointmentsModel>();
        appointmentAdapter = new AppointmentsAdapter(context, eachAppointments);
        myAppointmentListView.setAdapter(appointmentAdapter);

        getCurrentDate();
        getNextDate(0);

        datefilterList = new ArrayList<String>();
        addItemDateSpinner();
        dateSpinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, datefilterList);
        dateSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateSpinner.setAdapter(dateSpinnerAdapter);

        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                if (position == 0) {
                    getCurrentDate();
                    getNextDate(0);
                } else if (position == 1) {
                    getCurrentDate();
                    getNextDate(1);
                } else if (position == 2) {
                    getCurrentDate();
                    getNextDate(6);
                } else if (position == 3) {
                    getCurrentDate();
                    getNextDate(29);
                } else if (position == 4) {
                    MyAppointmentDatePickerDialogFragment datePickerDialog = new MyAppointmentDatePickerDialogFragment();
                    datePickerDialog.setTargetFragment(getActivity().getSupportFragmentManager().getFragments().get(0), 4002);
                    datePickerDialog.show(getActivity().getSupportFragmentManager(), "Date_Picker");
                }
                Log.e("start", start_date);
                Log.e("end", end_date);

                appointmentsRequest();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 4002) {
            start_date = data.getStringExtra("start_date");
            end_date = data.getStringExtra("end_date");
            Log.e("test",start_date+"\n"+end_date);
            dateShowTextView.setText(start_date + "  ->  " + end_date);
            appointmentsRequest();
        }
    }

    private void getCurrentDate() {
        calendar = Calendar.getInstance();
        todayDate = calendar.getTime();
        todayString = simpleDateFormat.format(todayDate);
        start_date = todayString;
    }

    private void getNextDate(int distance) {
        calendar.add(Calendar.DAY_OF_YEAR, distance);
        nextDate = calendar.getTime();
        nextString = simpleDateFormat.format(nextDate);
        end_date = nextString;
        if (distance > 0) {
            dateShowTextView.setText(start_date + "  ->  " + end_date);
        } else {
            dateShowTextView.setText(start_date);
        }
    }

    private void addItemDateSpinner() {
        datefilterList.add("Today");
        datefilterList.add("Tomorrow");
        datefilterList.add("1 Week");
        datefilterList.add("1 Month");
        datefilterList.add("Custom");
    }


    private void appointmentsRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "ownAppointments";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                eachAppointments.clear();
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    if (jsonArray.length() < 1) {
                        //Toast.makeText(context, "You have no appointment on "+dateShowTextView.getText().toString(), Toast.LENGTH_SHORT).show();
                        emptyTextView.setVisibility(View.VISIBLE);
                    } else {
                        emptyTextView.setVisibility(View.GONE);
                    }
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String id = jsonObject.getString("id");
                        String userid = jsonObject.getString("doctor_id");
                        String chamberid = jsonObject.getString("chamber_id");
                        String appot_time = jsonObject.getString("appointment_time");
                        String dlt_msg = jsonObject.getString("delete_message");
                        String firstName = jsonObject.getString("first_name");
                        String lastName = jsonObject.getString("last_name");
                        String userPhoto = jsonObject.getString("photo");
                        String userGroupId = jsonObject.getString("user_group_id");

                        AppointmentsModel appointmentModel = new AppointmentsModel(id, userid, chamberid, appot_time, dlt_msg, firstName, lastName, userPhoto, userGroupId);

                        eachAppointments.add(appointmentModel);
                    }

                    appointmentAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("start_date", start_date);
                params.put("end_date", end_date);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onResume() {
        super.onResume();
        appointmentsRequest();
    }
}
