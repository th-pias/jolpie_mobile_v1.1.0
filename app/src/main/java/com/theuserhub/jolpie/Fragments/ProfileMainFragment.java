package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.theuserhub.jolpie.Adapters.ProfileScrolledTab_Adapter;
import com.theuserhub.jolpie.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 11/9/2015.
 */
public class ProfileMainFragment extends Fragment{
    private String TAG = "Profile_Main_Fragment";
    private Context context;
    /*private List<String> tabList;
    private ViewPager pager;

    private String[] tabs_jolpie = {"Info", "Updates", "Chamber", "Articles", "Reviews", "Gallery"};*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_profile_main, container, false);
        // Initialize the ViewPager and set an adapter
        /*pager = (ViewPager) rootView.findViewById(R.id.profile_pager);

        tabList = new ArrayList<>();
        for (int i = 0; i < tabs_jolpie.length; i++) {
            tabList.add(tabs_jolpie[i]);
        }

        pager.setAdapter(new ProfileScrolledTab_Adapter(context, getActivity().getSupportFragmentManager(), tabList));
        //pager.setCurrentItem(tab_position);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.profile_tabs);
        tabs.setViewPager(pager);
        tabs.setIndicatorHeight(8);
        tabs.setIndicatorColorResource(R.color.colorPrimary);
        //tabs.set
        tabs.setTextColor(0xFFFFFFFF);
        tabs.setAllCaps(false);

*/
        return rootView;
    }
}
