package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Adapters.OnlineConsultBoughtAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.OnlineConsultLogModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 4/4/2016.
 */
public class OnlineConsultSoldFragment extends Fragment{
    private Context context;
    private String TAG = "OnlineConsultSoldFragment", tag_string_req = "consult_sold_str_req";

    private List<OnlineConsultLogModel> eachLog;
    private ListView logListView;
    private OnlineConsultBoughtAdapter consultBoughtAdapter;
    private TextView emptyTextView;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_online_consult_sold, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        emptyTextView = (TextView) rootView.findViewById(R.id.empty_tv);
        logListView = (ListView) rootView.findViewById(R.id.log_lv);

        eachLog = new ArrayList<OnlineConsultLogModel>();
        consultBoughtAdapter = new OnlineConsultBoughtAdapter(context, eachLog);
        logListView.setAdapter(consultBoughtAdapter);

        consultBoughtRequest();

        logListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context, ProfileDetailNewActivity.class);
                if (eachLog.get(position).getDoctor_id().equals(appUserId)) {
                    intent.putExtra("user_id", eachLog.get(position).getPatient_id());
                }
                else {
                    intent.putExtra("user_id", eachLog.get(position).getDoctor_id());
                }
                intent.putExtra("user_fname", eachLog.get(position).getFirst_name());
                intent.putExtra("user_lname", eachLog.get(position).getLast_name());
                intent.putExtra("user_email", "");
                intent.putExtra("group_id", eachLog.get(position).getUser_group_id());
                intent.putExtra("user_image_url", eachLog.get(position).getPhoto());
                startActivity(intent);
            }
        });

        return rootView;
    }

    private void consultBoughtRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "onlineConsultSoldLog";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() <= 0) {
                        emptyTextView.setVisibility(View.VISIBLE);
                        emptyTextView.setText("No one bought your online consultancy packages.");
                        logListView.setVisibility(View.GONE);
                    } else {
                        emptyTextView.setVisibility(View.GONE);
                        logListView.setVisibility(View.VISIBLE);

                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            String consultancy_id, package_name, audio, video, text,
                                    consultancy_type, duration, price, created_at, doctor_id,
                                    patient_id, tran_date, first_name, last_name, photo, degree,
                                    user_group_id;
                            consultancy_id = jsonObject.getString("consultancy_id");
                            package_name = jsonObject.getString("package_name");
                            audio = jsonObject.getString("audio");
                            video = jsonObject.getString("video");
                            text = jsonObject.getString("text");
                            consultancy_type = jsonObject.getString("consultancy_type");
                            duration = jsonObject.getString("duration");
                            price = jsonObject.getString("price");
                            created_at = jsonObject.getString("created_at");
                            doctor_id = appUserId;
                            patient_id = jsonObject.getString("patient_id");
                            tran_date = jsonObject.getString("tran_date");
                            first_name = jsonObject.getString("first_name");
                            last_name = jsonObject.getString("last_name");
                            photo = jsonObject.getString("photo");
                            degree = jsonObject.getString("degree");
                            user_group_id = jsonObject.getString("user_group_id");

                            OnlineConsultLogModel consultLogModel = new OnlineConsultLogModel(consultancy_id, package_name, audio, video, text,
                                    consultancy_type, duration, price, created_at, doctor_id,
                                    patient_id, tran_date, first_name, last_name, photo, degree,
                                    user_group_id);

                            eachLog.add(consultLogModel);
                        }
                        consultBoughtAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load data.\nTry again.");
                logListView.setVisibility(View.GONE);

                emptyTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        consultBoughtRequest();
                    }
                });
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
