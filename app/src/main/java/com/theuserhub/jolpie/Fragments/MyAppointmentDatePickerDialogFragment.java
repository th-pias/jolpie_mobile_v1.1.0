package com.theuserhub.jolpie.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.andexert.calendarlistview.library.DayPickerView;
import com.andexert.calendarlistview.library.SimpleMonthAdapter;
import com.theuserhub.jolpie.Activity.AppointmentActivity;
import com.theuserhub.jolpie.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Admin on 2/20/2016.
 */
public class MyAppointmentDatePickerDialogFragment extends DialogFragment implements com.andexert.calendarlistview.library.DatePickerController{

    private Dialog dialog;
    private DayPickerView dayPickerView;
    private TextView okTextView,cancelTextView;
    private String start_date, end_date;
    private Date startDate,endDate;
    private SimpleDateFormat simpleDateFormat;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        startDate = new Date();
        endDate = new Date();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_date_range_picker, null);
        dayPickerView = (DayPickerView) rootView.findViewById(R.id.pickerView);
        okTextView = (TextView) rootView.findViewById(R.id.date_picker_ok_tv);
        cancelTextView = (TextView) rootView.findViewById(R.id.date_picker_cancel_tv);

        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dayPickerView.setController(this);
        builder.setView(rootView);
        dialog = builder.create();
        return dialog;
    }

    @Override
    public int getMaxYear()
    {
        return 2017;
    }

    @Override
    public void onDayOfMonthSelected(int year, int month, int day)
    {
        Log.e("Day Selected", day + " / " + month + " / " + year);
    }

    @Override
    public void onDateRangeSelected(SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays)
    {
        Log.e("Date range selected", selectedDays.getFirst().toString() + " --> " + selectedDays.getLast().toString());
        startDate = selectedDays.getFirst().getDate();
        endDate = selectedDays.getLast().getDate();

        start_date = simpleDateFormat.format(startDate);
        end_date = simpleDateFormat.format(endDate);

        //((AppointmentActivity) getActivity()).customDateRange(start_date,end_date);

        Intent intent = new Intent();
        intent.putExtra("start_date",start_date);
        intent.putExtra("end_date",end_date);
        getTargetFragment().onActivityResult(getTargetRequestCode(), 4002, intent);
        dialog.dismiss();
    }
}
