package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.SavedDoctorsModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Adapters.SavedDoctorsFragmentAdapter;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 2/1/2016.
 */
public class SavedDoctorsFragment extends Fragment{

    private Context context;
    private String TAG = "SavedDoctorsFragment", tag_string_req = "save_doc_str_req";

    private String isAll,speciality_id;
    private ListView doctorsListView;
    private List<SavedDoctorsModel> eachDoctors;
    private List<SpecialityModel>specialityList;
    private SavedDoctorsFragmentAdapter savedDoctorsAdapter;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_saved_doctors, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Bundle bundle = getArguments();
        isAll = bundle.getString("isAll");
        speciality_id = bundle.getString("speciality_id");
        Log.d("isAll", "" + isAll);

        eachDoctors = new ArrayList<SavedDoctorsModel>();
        doctorsListView = (ListView) rootView.findViewById(R.id.saved_doctors_lv);
        savedDoctorsAdapter = new SavedDoctorsFragmentAdapter(context,eachDoctors);
        doctorsListView.setAdapter(savedDoctorsAdapter);

        if (isAll.equals("0")) {
            getSavedDoctors();
        }
        else {
            getSpecialityDoctors();
        }

        doctorsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(context, ProfileDetailNewActivity.class);
                intent.putExtra("user_id", eachDoctors.get(position).getUser_id());
                intent.putExtra("user_fname", eachDoctors.get(position).getFirst_name());
                intent.putExtra("user_lname", eachDoctors.get(position).getLast_name());
                intent.putExtra("user_email", "");
                intent.putExtra("group_id", eachDoctors.get(position).getUser_group_id());
                intent.putExtra("user_image_url", eachDoctors.get(position).getImage());
                startActivity(intent);
            }
        });

        return rootView;
    }

    private void getSavedDoctors() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "savedDoctors";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "doctors: "+response.toString());
                parseJsonData(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getSpecialityDoctors() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "filterSavedDoctors";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "SpecialityDocs: " + response.toString());
                parseJsonData(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("specialization_id", speciality_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void parseJsonData(String response) {
        eachDoctors.clear();
        try {
            JSONArray allPatients = new JSONArray(response);
            for (int i=0;i<allPatients.length();++i) {
                specialityList = new ArrayList<SpecialityModel>();
                JSONObject singlePatients = (JSONObject) allPatients.get(i);
                String save_id = singlePatients.getString("save_id");
                String user_id = singlePatients.getString("doctor_id");
                String first_name = singlePatients.getString("first_name");
                String last_name = singlePatients.getString("last_name");
                String user_group_id = singlePatients.getString("user_group_id");
                String image = singlePatients.getString("photo");
                String degree = singlePatients.getString("degree");
                JSONArray allConditions = singlePatients.getJSONArray("specializations");
                for (int j=0;j<allConditions.length();++j) {
                    JSONObject singleCondition = (JSONObject) allConditions.get(j);
                    String speciality_id = singleCondition.getString("specialization_id");
                    String speciality_name = singleCondition.getString("name");

                    specialityList.add(new SpecialityModel(speciality_id,speciality_name));
                }

                eachDoctors.add(new SavedDoctorsModel(save_id,user_id,first_name,last_name,image,user_group_id,degree,specialityList));
            }
            savedDoctorsAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
