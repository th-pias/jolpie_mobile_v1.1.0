package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.theuserhub.jolpie.Adapters.FeedListAdapter;
import com.theuserhub.jolpie.Adapters.PatientsLikeMeUpdatesAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.MyConditionsModel;
import com.theuserhub.jolpie.Models.NewsfeedModel;
import com.theuserhub.jolpie.Models.PatientsLikeMeModel;
import com.theuserhub.jolpie.Models.PostInfoModel;
import com.theuserhub.jolpie.Models.SinglePostModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 1/27/2016.
 */
public class UpdatesPatientsLikeMeFragment extends Fragment{
    private Context context;
    private String TAG = "UpdatesPatientsLikeMeFragment";
    private String tag_string_req = "feed_str_req";

    private MaterialRefreshLayout materialRefreshLayout;
    private ListView feedListView;
    private List<SinglePostModel> feedList;
    private PatientsLikeMeUpdatesAdapter adapter;
    private boolean isEnd;
    private TextView emptyTextView;
    private String isAll,condition_id;
    private List<NewsfeedModel>eachNewsfeed;
    private List<PostInfoModel>eachPost;
    private List<PatientsLikeMeModel>eachPatients;
    private List<MyConditionsModel>conditionsList;
    private int start , take;

    //private HomeDatabaseHandler handler;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName,appUserLName,appUserEmail,appUserId,appUserGroupId,appUserImgUrl;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_patient_like_me_updates, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl=user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Bundle bundle = getArguments();
        isAll = bundle.getString("isAll");
        condition_id = bundle.getString("condition_id");

        isEnd = false;
        start = 0;
        take = 5;

        emptyTextView = (TextView) rootView.findViewById(R.id.empty_tv);
        feedListView= (ListView) rootView.findViewById(R.id.feedlist_lv);
        feedList = new ArrayList<SinglePostModel>();
        eachNewsfeed = new ArrayList<NewsfeedModel>();
        eachPatients = new ArrayList<PatientsLikeMeModel>();
        //adapter = new FeedListAdapter(context, eachNewsfeed, appUserId);
        //feedListView.setAdapter(adapter);

        //handler=new HomeDatabaseHandler(context);

        if (!AppFunctions.isNetworkConnected(context)) {
            Toast.makeText(context, "No network found \n fatched from db", Toast.LENGTH_LONG).show();
            //fatchFeedDb();
        }
        else {
            //String url=context.getResources().getString(R.string.MAIN_URL)+"allPosts";
            //fatchFeed(url);
            //getNewsfeeds();
            /*if (isAll.equals("0")) {
                getPatientsLikeMe();
            }
            else {
                getConditionPatients();
            }*/
        }


        materialRefreshLayout = (MaterialRefreshLayout) rootView.findViewById(R.id.home_refresh);

        //materialRefreshLayout.setLoadMore(true);
        //materialRefreshLayout.finishRefreshLoadMore();
        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                materialRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ////request from here..............................
                        if (!AppFunctions.isNetworkConnected(context)) {
                            Toast.makeText(context, "No network found \n fatched from db", Toast.LENGTH_LONG).show();
                            //fatchFeedDb();
                        }
                        else {
                            String url=context.getResources().getString(R.string.MAIN_URL)+"allPosts";
                            //fatchFeed(url);
                            //getNewsfeeds();
                        }
                        materialRefreshLayout.finishRefresh();
                    }
                }, 3000);

            }

            @Override
            public void onfinish() {
                //Toast.makeText(context, "finish", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRefreshLoadMore(final MaterialRefreshLayout materialRefreshLayout) {
                //Toast.makeText(context, "load more", Toast.LENGTH_LONG).show();

                ////load more request from here.....................
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //materialRefreshLayout.autoRefresh();
            }
        },3000);

        return rootView;
    }

    /*private void fatchFeedDb() {
        List<SinglePostModel> posts = handler.getAllContacts();

        for (SinglePostModel pst : posts)
        {
            feedList.add(pst);
        }
        adapter.notifyDataSetChanged();
    }*/

    private void getNewsfeeds() {
        String url = context.getResources().getString(R.string.MAIN_URL)+"getNewsfeeds";

        final StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                eachNewsfeed.clear();
                String id,section,section_id,activity,activity_at,by_user_id,by_user_fname,by_user_lname,by_user_photo,by_user_group_id;
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()<=0) {
                        emptyTextView.setVisibility(View.VISIBLE);
                        emptyTextView.setText("You have no Updates");
                        feedListView.setVisibility(View.GONE);
                    }
                    else {
                        emptyTextView.setVisibility(View.GONE);
                        feedListView.setVisibility(View.VISIBLE);
                        if (jsonArray.length() < 5) {
                            isEnd = true;
                        }
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            //eac
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            id = jsonObject.getString("newsfeed_id");
                            section = jsonObject.getString("section");
                            section_id = jsonObject.getString("section_id");
                            activity = jsonObject.getString("activity");
                            activity_at = jsonObject.getString("activity_at");
                            by_user_id = jsonObject.getString("by_user_id");
                            by_user_fname = jsonObject.getString("by_user_fname");
                            by_user_lname = jsonObject.getString("by_user_lname");
                            by_user_photo = jsonObject.getString("by_user_photo");
                            by_user_group_id = jsonObject.getString("by_user_group_id");

                            JSONObject infoJsonObject = jsonObject.getJSONObject("info");

                            String post_id, title, description, feeling, photo, video, created_at,
                                    updated_at, by_id, by_fname, by_lname, by_photo, by_group_id,
                                    to_id, to_fname, to_lname, to_photo, to_user_group_id,
                                    like_count, answer_count, share_count, star_count, liked_by_me, shared_by_me;
                            post_id = infoJsonObject.getString("section_post_id");
                            title = infoJsonObject.getString("section_title");
                            description = infoJsonObject.getString("section_description");
                            feeling = infoJsonObject.getString("section_feeling").trim();
                            photo = infoJsonObject.getString("section_photo");
                            video = infoJsonObject.getString("section_video");
                            created_at = infoJsonObject.getString("section_at");
                            updated_at = infoJsonObject.getString("section_updated_at");
                            by_id = infoJsonObject.getString("section_by");
                            by_fname = infoJsonObject.getString("section_by_fname");
                            by_lname = infoJsonObject.getString("section_by_lname");
                            by_photo = infoJsonObject.getString("section_by_photo");
                            by_group_id = infoJsonObject.getString("section_by_user_group_id");
                            to_id = infoJsonObject.getString("section_to");
                            to_fname = infoJsonObject.getString("section_to_fname");
                            to_lname = infoJsonObject.getString("section_to_lname");
                            to_photo = infoJsonObject.getString("section_to_photo");
                            to_user_group_id = infoJsonObject.getString("section_to_user_group_id");
                            like_count = infoJsonObject.getString("good_count");
                            answer_count = infoJsonObject.getString("answer_count");
                            share_count = infoJsonObject.getString("share_count");
                            star_count = infoJsonObject.getString("star_count");
                            liked_by_me = infoJsonObject.getString("liked_by_me");
                            shared_by_me = infoJsonObject.getString("shared_by_me");

                            PostInfoModel infoModel = new PostInfoModel(post_id, title, description, feeling, photo, video, created_at,
                                    updated_at, by_id, by_fname, by_lname, by_photo, by_group_id,
                                    to_id, to_fname, to_lname, to_photo, to_user_group_id,
                                    like_count, answer_count, share_count, star_count, liked_by_me, shared_by_me);

                            int patFlag = 0;
                            for (int k=0;k<eachPatients.size();++k) {
                                if (eachPatients.get(k).getUser_id().equals(by_id)) {
                                    patFlag = 1;
                                    break;
                                }
                            }
                            if (patFlag == 1) {
                                NewsfeedModel newsfeedModel = new NewsfeedModel(id, section, section_id, activity, activity_at, by_user_id, by_user_fname, by_user_lname, by_user_photo, by_user_group_id, infoModel);
                                eachNewsfeed.add(newsfeedModel);
                            }

                        }

                        adapter = new PatientsLikeMeUpdatesAdapter(context, eachNewsfeed, appUserId, appUserGroupId, isEnd, eachPatients);
                        feedListView.setAdapter(adapter);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*if (eachNewsfeed.size()<=0) {
                    start = start+take;
                    getNewsfeeds();
                }*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load Updates.\nTry again.");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("user_group_id", appUserGroupId);
                params.put("start_index", String.valueOf(start));
                params.put("take_total", String.valueOf(take));
                //Log.d(TAG,+appUserId+magazine_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /*private void fatchFeed(String url){
        //url="http://10.0.3.2/doc5/api/allPosts";

        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                //handler.deleteAll();
                feedList.clear();
                try {
                    JSONArray allPosts=new JSONArray(response);
                    //Log.d("Test2", allPosts.toString());
                    for (int i=0;i<allPosts.length();++i) {
                        JSONObject singlePost=(JSONObject)allPosts.get(i);
                        String post_id=singlePost.getString("id");
                        String post_title=singlePost.getString("title");
                        String post_des=singlePost.getString("description");
                        String post_likes=singlePost.getString("good_count");
                        String post_answers=singlePost.getString("answer_count");
                        String post_follows=singlePost.getString("follow_count");
                        String post_userId=singlePost.getString("user_id");
                        String post_userGroupId=singlePost.getString("group_id");
                        String post_created=singlePost.getString("created_at");
                        String post_updated=singlePost.getString("updated_at");
                        String post_isdeleted=singlePost.getString("is_deleted");
                        String post_user_fname=singlePost.getString("first_name");
                        String post_user_lname=singlePost.getString("last_name");
                        String post_photo=singlePost.getString("photo");
                        String post_video=singlePost.getString("video");
                        //Toast.makeText(context,post_id+"\n"+post_title+"\n"+post_des,Toast.LENGTH_LONG).show();
                        SinglePostModel pst=new SinglePostModel(post_id,post_title,post_des,post_likes,post_answers,post_follows,post_userId,
                                post_userGroupId,post_created,post_updated,post_isdeleted,post_user_fname,post_user_lname,post_photo,post_video);
                        feedList.add(pst);
                        //handler.addContact(pst);
                    }

                    //refresh adapter
                    adapter.notifyDataSetChanged();

                    //feedList.indexOf();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Test", "Error: " + error.getMessage());
                //fatchFeedDb();
                Toast.makeText(context, "Some error occured \n fatched from db" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req + TAG);
    }*/

    private void getPatientsLikeMe() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "patientsLikeMe";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "patientsLikeMe: " + response.toString());
                parseJsonData(response);
                getNewsfeeds();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getConditionPatients() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "conditionPatients";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "conditionpatients: " + response.toString());
                parseJsonData(response);
                getNewsfeeds();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("condition_id", condition_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void parseJsonData(String response) {
        eachPatients.clear();
        try {
            JSONArray allPatients = new JSONArray(response);
            for (int i=0;i<allPatients.length();++i) {
                conditionsList = new ArrayList<MyConditionsModel>();
                JSONObject singlePatients = (JSONObject) allPatients.get(i);
                String user_id = singlePatients.getString("user_id");
                String first_name = singlePatients.getString("first_name");
                String last_name = singlePatients.getString("last_name");
                String image = singlePatients.getString("photo");
                String user_group_id = singlePatients.getString("user_group_id");
                JSONArray allConditions = singlePatients.getJSONArray("conditions");
                for (int j=0;j<allConditions.length();++j) {
                    JSONObject singleCondition = (JSONObject) allConditions.get(j);
                    String condition_id = singleCondition.getString("condition_id");
                    String conditionName_en = singleCondition.getString("condition_name");
                    String conditionName_bn = singleCondition.getString("bangla_condition_name");

                    conditionsList.add(new MyConditionsModel(condition_id,conditionName_en,conditionName_bn));
                }
                eachPatients.add(new PatientsLikeMeModel(user_id,first_name,last_name,image,user_group_id,conditionsList));
            }
            //patientsLikeMeAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //cancelling all request when fragment in pause....
        AppController.getInstance().cancelPendingRequests(tag_string_req + TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        //String url=context.getResources().getString(R.string.MAIN_URL)+"allPosts";
        //fatchFeed(url);
        //getNewsfeeds();
        if (isAll.equals("0")) {
            getPatientsLikeMe();
        }
        else {
            getConditionPatients();
        }
    }
}
