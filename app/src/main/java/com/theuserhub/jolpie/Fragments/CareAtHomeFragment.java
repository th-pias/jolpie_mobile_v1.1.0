package com.theuserhub.jolpie.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.HomeCareAreaModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 3/9/2016.
 */
public class CareAtHomeFragment extends Fragment{
    private Context context;
    private String TAG = "CareAtHomeFragment";
    private String tag_string_req = "care_string_req";

    private Spinner areaSpinner,serviceSpinner;
    private EditText mobileEditText, remarkEditText;
    private TextView emptyTextView;
    private Button submitButton;
    private String areaString,serviceString,mobileString,remarkString;
    private List<String>areaList,serviceList;
    private List<HomeCareAreaModel> areaAllList,serviceAllList;
    private ArrayAdapter<String> serviceSpinnerAdapter,areaSpinnerAdapter;
    private String mobileEditString = "";
    private String remarkEditString = "";
    private RelativeLayout progressLayout;
    private ProgressDialog pDialog;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_care_at_home, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        progressLayout = (RelativeLayout) rootView.findViewById(R.id.progress_layout);
        emptyTextView = (TextView) rootView.findViewById(R.id.empty_tv);
        areaSpinner = (Spinner) rootView.findViewById(R.id.area_spinner);
        serviceSpinner = (Spinner) rootView.findViewById(R.id.service_spinner);
        mobileEditText = (EditText) rootView.findViewById(R.id.mobile_editText);
        remarkEditText = (EditText) rootView.findViewById(R.id.remark_editText);
        submitButton = (Button) rootView.findViewById(R.id.submit_button);

        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);

        areaList = new ArrayList<String>();
        areaAllList = new ArrayList<HomeCareAreaModel>();
        serviceList = new ArrayList<String>();
        serviceAllList = new ArrayList<HomeCareAreaModel>();

        areaSpinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, areaList);
        areaSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        areaSpinner.setAdapter(areaSpinnerAdapter);

        serviceSpinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, serviceList);
        serviceSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceSpinner.setAdapter(serviceSpinnerAdapter);

        getAreasServicesRequest();

        areaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                areaString = areaAllList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        serviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                serviceString = serviceAllList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileEditString = mobileEditText.getText().toString();
                remarkEditString = remarkEditText.getText().toString();

                if (mobileEditString.equals("")) {
                    Toast.makeText(context, "Mobile Number field remains empty", Toast.LENGTH_SHORT).show();
                } else
                    setCareAtHome(mobileEditString, remarkEditString);
            }
        });

        emptyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyTextView.setVisibility(View.GONE);
                getAreasServicesRequest();
            }
        });

        return rootView;
    }
    private void setCareAtHome(final String mobile, final String remark){
        pDialog.setMessage("Submitting...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL)+"takeCareAtHome";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                Log.d(TAG, response.toString());
                mobileEditText.setText("");
                remarkEditText.setText("");
                Toast.makeText(context, "Care At Home Submitted.", Toast.LENGTH_SHORT).show();
                //finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Error: Try Again", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
                mobileEditText.setText("");
                remarkEditText.setText("");
                Toast.makeText(context, "Care At Home Submitted.", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("service_id", serviceString);
                params.put("mobile", mobile);
                params.put("area_id", areaString);
                params.put("remark", remark);
                params.put("user_id", appUserId);
                Log.d("area_id service_id",areaString+" "+serviceString );

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    private void getAreasServicesRequest() {
        progressLayout.setVisibility(View.VISIBLE);
        String url = getResources().getString(R.string.MAIN_URL)+"homeCareAreasServices";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                progressLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray areaJsonArray = jsonObject.getJSONArray("areas");
                    for (int i=0;i<areaJsonArray.length();++i){
                        JSONObject areaJsonObject = (JSONObject) areaJsonArray.get(i);
                        String area_id = areaJsonObject.getString("home_care_id");
                        String area_name = areaJsonObject.getString("address");

                        areaList.add(area_name);
                        areaAllList.add(new HomeCareAreaModel(area_id,area_name));
                    }
                    areaSpinnerAdapter.notifyDataSetChanged();

                    JSONArray serviceJsonArray = jsonObject.getJSONArray("services");
                    for (int i=0;i<serviceJsonArray.length();++i){
                        JSONObject serviceJsonObject = (JSONObject) serviceJsonArray.get(i);
                        String service_id = serviceJsonObject.getString("home_list_id");
                        String service_name = serviceJsonObject.getString("home_service_name");

                        serviceList.add(service_name);
                        serviceAllList.add(new HomeCareAreaModel(service_id,service_name));
                    }
                    serviceSpinnerAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                    emptyTextView.setVisibility(View.VISIBLE);
                    emptyTextView.setText("Can't load data.\nTap to retry.");
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "try again later" + error.getMessage(), Toast.LENGTH_LONG).show();
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load data.\nTap to retry.");
                progressLayout.setVisibility(View.GONE);
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
