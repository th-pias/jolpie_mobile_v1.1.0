package com.theuserhub.jolpie.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.ReviewListActivity;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 1/13/2016.
 */
public class ReviewEditDialogFragment extends DialogFragment {

    private String tag_string_req = "review_edit_str_req";
    String user_id, review_id, reviewer_id, review_title, review_description, review_star,position;
    private Dialog dialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_review_layout, null);

        Bundle bundle = getArguments();
        position = bundle.getString("position");
        user_id = bundle.getString("user_id");
        review_id = bundle.getString("review_id");
        reviewer_id = bundle.getString("reviewer_id");
        review_title = bundle.getString("review_title");
        review_description = bundle.getString("review_des");
        review_star = bundle.getString("review_star");
        Log.d("Bundle", user_id + ":" + reviewer_id);

        RatingBar ratingBar = (RatingBar) rootView.findViewById(R.id.review_rating_star);
        float rating = Float.parseFloat(review_star);
        ratingBar.setRating(rating);
        final TextView ratingText = (TextView) rootView.findViewById(R.id.review_rating_text);
        if (rating == 5.0) {
            ratingText.setText("Excellent");
        } else if (rating == 4.0) {
            ratingText.setText("Good");
        } else if (rating == 3.0) {
            ratingText.setText("It's OK");
        } else if (rating == 2.0) {
            ratingText.setText("Not Bad");
        } else if (rating == 1.0) {
            ratingText.setText("Bad");
        }
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                //Log.d("Rating","Rating: "+rating);
                if (rating == 5.0) {
                    ratingText.setText("Excellent");
                    review_star = "5.0";
                } else if (rating == 4.0) {
                    ratingText.setText("Good");
                    review_star = "4.0";
                } else if (rating == 3.0) {
                    ratingText.setText("It's OK");
                    review_star = "3.0";
                } else if (rating == 2.0) {
                    ratingText.setText("Not Bad");
                    review_star = "2.0";
                } else if (rating == 1.0) {
                    ratingText.setText("Bad");
                    review_star = "1.0";
                }
            }
        });

        final TextView reviewTitle = (TextView) rootView.findViewById(R.id.review_title_tv);
        reviewTitle.setText(review_title);
        final TextView reviewDes = (TextView) rootView.findViewById(R.id.review_description_tv);
        reviewDes.setText(review_description);
        TextView reviewSubmit = (TextView) rootView.findViewById(R.id.review_submit_tv);
        reviewSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                review_title = reviewTitle.getText().toString();
                review_description = reviewDes.getText().toString();
                updateReviewRequest();
                dialog.hide();
            }
        });
        builder.setView(rootView);
        dialog = builder.create();
        return dialog;
    }

    private void updateReviewRequest() {
        String url = getActivity().getResources().getString(R.string.MAIN_URL) + "reviewUpdate";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("ReviewUpdate", response.toString());
                ((ReviewListActivity)getActivity()).setUpdateDataReviewList();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("ReviewUpdate", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("review_id", review_id);
                params.put("review_title", review_title);
                params.put("review_description", review_description);
                params.put("star_count",review_star);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req );
    }
}
