package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.MyAppointmentAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.AppointmentsModel;
import com.theuserhub.jolpie.Models.ChamberModel;
import com.theuserhub.jolpie.Models.MyAppointmentModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 2/21/2016.
 */
public class MyAppointmentFragment extends Fragment {
    private Context context;
    private String TAG = "MyAppointmentFragment", tag_string_req = "my_appot_str_req";

    private String start_date, end_date, chamber_id;
    private Spinner chambersSpinner, dateSpinner;
    private List<String> myChambersNames, datefilterList;
    private List<ChamberModel> myChambersList;
    private ArrayAdapter<String> spinnerAdapter, dateSpinnerAdapter;
    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;
    private Date todayDate, nextDate;
    private String todayString, nextString;
    private boolean flag;
    private TextView dateShowTextView, emptyTextView;

    private ListView myAppointmentListView;
    private MyAppointmentAdapter appointmentAdapter;
    private List<MyAppointmentModel> eachAppointments;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_my_appointments, container, false);

        flag = false;

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        chamber_id = "0";

        //simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        todayDate = new Date();
        nextDate = new Date();

        chambersSpinner = (Spinner) rootView.findViewById(R.id.my_appointment_chambers);
        dateSpinner = (Spinner) rootView.findViewById(R.id.date_filter_spinner);

        dateShowTextView = (TextView) rootView.findViewById(R.id.date_range_tv);
        emptyTextView = (TextView) rootView.findViewById(R.id.empty_data_tv);

        myAppointmentListView = (ListView) rootView.findViewById(R.id.my_appointments_lv);
        eachAppointments = new ArrayList<MyAppointmentModel>();
        appointmentAdapter = new MyAppointmentAdapter(context, eachAppointments);
        myAppointmentListView.setAdapter(appointmentAdapter);

        getCurrentDate();
        getNextDate(0);

        myChambersNames = new ArrayList<String>();
        myChambersList = new ArrayList<ChamberModel>();
        myChambersNames.add("My Chambers...");
        spinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, myChambersNames);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chambersSpinner.setAdapter(spinnerAdapter);

        myChambersRequest();

        chambersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                if (flag) {
                    if (position == 0) {
                        chamber_id = "0";
                        myAppointmentsRequest();
                    } else {
                        chamber_id = myChambersList.get(position - 1).getChamber_id();
                        myAppointmentsRequest();
                    }
                }
                flag = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        datefilterList = new ArrayList<String>();
        addItemDateSpinner();
        dateSpinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, datefilterList);
        dateSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateSpinner.setAdapter(dateSpinnerAdapter);

        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                if (position == 0) {
                    getCurrentDate();
                    getNextDate(0);
                } else if (position == 1) {
                    getCurrentDate();
                    getNextDate(1);
                } else if (position == 2) {
                    getCurrentDate();
                    getNextDate(6);
                } else if (position == 3) {
                    getCurrentDate();
                    getNextDate(29);
                } else if (position == 4) {
                    MyAppointmentDatePickerDialogFragment datePickerDialog = new MyAppointmentDatePickerDialogFragment();
                    datePickerDialog.setTargetFragment(getActivity().getSupportFragmentManager().getFragments().get(1), 4002);
                    datePickerDialog.show(getActivity().getSupportFragmentManager(), "Date_Picker");
                }
                Log.e("start", start_date);
                Log.e("end", end_date);

                myAppointmentsRequest();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 4002) {
            start_date = data.getStringExtra("start_date");
            end_date = data.getStringExtra("end_date");
            Log.e("test",start_date+"\n"+end_date);
            dateShowTextView.setText(start_date + "  ->  " + end_date);
            myAppointmentsRequest();
        }
    }

    public void customDateRange(String first_date, String last_date) {
        start_date = first_date;
        end_date = last_date;

        dateShowTextView.setText(start_date + "  ->  " + end_date);
        myAppointmentsRequest();
    }

    private void getCurrentDate() {
        calendar = Calendar.getInstance();
        todayDate = calendar.getTime();
        todayString = simpleDateFormat.format(todayDate);
        start_date = todayString;
    }

    private void getNextDate(int distance) {
        calendar.add(Calendar.DAY_OF_YEAR, distance);
        nextDate = calendar.getTime();
        nextString = simpleDateFormat.format(nextDate);
        end_date = nextString;
        if (distance > 0) {
            dateShowTextView.setText(start_date + " -> " + end_date);
        } else {
            dateShowTextView.setText(start_date);
        }
    }

    private void addItemDateSpinner() {
        datefilterList.add("Today");
        datefilterList.add("Tomorrow");
        datefilterList.add("1 Week");
        datefilterList.add("1 Month");
        datefilterList.add("Custom");
    }

    private void myChambersRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getChambers";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String chamber_id,organisation_id,chamber_first_name,
                                chamber_last_name,chamber_photo,chamber_user_group_id,
                                chamber_street_address,chamber_address,cut_of_time,
                                chamber_fee,each_patient_time;
                        chamber_id = jsonObject.getString("chamber_id");
                        organisation_id = jsonObject.getString("organisation_id");
                        chamber_first_name = jsonObject.getString("chamber_first_name");
                        chamber_last_name = jsonObject.getString("chamber_last_name");
                        chamber_photo = jsonObject.getString("chamber_photo");
                        chamber_user_group_id = jsonObject.getString("chamber_user_group_id");
                        chamber_street_address = jsonObject.getString("chamber_street_address");
                        chamber_address = jsonObject.getString("chamber_address");
                        cut_of_time = jsonObject.getString("cut_of_time");
                        chamber_fee = jsonObject.getString("chamber_fee");
                        each_patient_time = jsonObject.getString("each_patient_time");

                        ChamberModel chamberModel = new ChamberModel(chamber_id,organisation_id,chamber_first_name,
                                chamber_last_name,chamber_photo,chamber_user_group_id,
                                chamber_street_address,chamber_address,cut_of_time,
                                chamber_fee,each_patient_time);
                        myChambersList.add(chamberModel);
                        if (organisation_id.equals("0")) {
                            myChambersNames.add(chamber_address);
                        }
                        else {
                            myChambersNames.add(chamber_first_name+" "+chamber_last_name);
                        }
                    }
                    spinnerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void myAppointmentsRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "myAppointments";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                eachAppointments.clear();
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    if (jsonArray.length() < 1) {
                        //Toast.makeText(context, "You have no appointment on "+dateShowTextView.getText().toString(), Toast.LENGTH_SHORT).show();
                        emptyTextView.setVisibility(View.VISIBLE);
                    } else {
                        emptyTextView.setVisibility(View.GONE);
                    }
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String id = jsonObject.getString("id");
                        String userid = jsonObject.getString("user_id");
                        String chamberid = jsonObject.getString("chamber_id");
                        String appot_time = jsonObject.getString("appointment_time");
                        String dlt_msg = jsonObject.getString("delete_message");
                        String firstName = jsonObject.getString("first_name");
                        String lastName = jsonObject.getString("last_name");
                        String userPhoto = jsonObject.getString("photo");
                        String userGroupId = jsonObject.getString("user_group_id");

                        MyAppointmentModel appointmentModel = new MyAppointmentModel(id, userid, chamberid, appot_time, dlt_msg, firstName, lastName, userPhoto, userGroupId);

                        eachAppointments.add(appointmentModel);
                    }

                    appointmentAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("start_date", start_date);
                params.put("end_date", end_date);
                params.put("chamber_id", chamber_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onResume() {
        super.onResume();
        myAppointmentsRequest();
    }
}
