package com.theuserhub.jolpie.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.CreateGroupActivity;
import com.theuserhub.jolpie.Activity.GroupDetailActivity;
import com.theuserhub.jolpie.Adapters.GroupsAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.GroupItem;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 11/8/2015.
 */
public class GroupsFragment extends Fragment {
    private Context context;
    private String TAG = "GroupsFragment", tag_string_req = "group_str_req";

    private TextView grpTypeTextView, grpFilterImageView, emptyTextView;
    private ListView grpItemListView;
    private List<GroupItem> eachItems;
    private GroupsAdapter groupsAdapter;
    private FloatingActionButton fab;
    private RelativeLayout groupTypeLayout, progressLayout;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_groups, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent intent = new Intent(context, CreateGroupActivity.class);
                startActivity(intent);
            }
        });

        progressLayout = (RelativeLayout) rootView.findViewById(R.id.progress_layout);
        groupTypeLayout = (RelativeLayout) rootView.findViewById(R.id.group_type_layout);
        emptyTextView = (TextView) rootView.findViewById(R.id.empty_tv);
        grpTypeTextView = (TextView) rootView.findViewById(R.id.group_type_tv);
        grpFilterImageView = (TextView) rootView.findViewById(R.id.group_filter_iv);
        Typeface filterFont = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        grpFilterImageView.setTypeface(filterFont);

        groupTypeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu filterMenu = new PopupMenu(context, grpFilterImageView);
                filterMenu.getMenuInflater().inflate(R.menu.group_type_popup, filterMenu.getMenu());
                filterMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context, "You Clicked : "+item.getTitle() , Toast.LENGTH_SHORT).show();
                        if (item.getItemId() == R.id.default_group) {
                            grpTypeTextView.setText(item.getTitle().toString());
                            eachItems.clear();
                            getDefaultGroupsRequest();
                        } else if (item.getItemId() == R.id.my_group) {
                            grpTypeTextView.setText(item.getTitle().toString());
                            eachItems.clear();
                            getMyGroupsRequest();
                        } else if (item.getItemId() == R.id.new_group) {
                            grpTypeTextView.setText(item.getTitle().toString());
                            eachItems.clear();
                            getNewGroupsRequest();
                        }
                        return true;
                    }
                });
                filterMenu.show();//showing popup menu
            }
        });

        grpItemListView = (ListView) rootView.findViewById(R.id.groupItem_lv);
        eachItems = new ArrayList<GroupItem>();
        groupsAdapter = new GroupsAdapter(context, eachItems, 1, appUserId);
        grpItemListView.setAdapter(groupsAdapter);

        //if (!AppFunctions.isNetworkConnected(context)) {
        //Toast.makeText(context, "No network found", Toast.LENGTH_LONG).show();
        //fatchFeedDb();
        //} else {
        grpTypeTextView.setText("Jolpie Groups");
        eachItems.clear();
        getDefaultGroupsRequest();
        //}

        grpItemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context, GroupDetailActivity.class);
                intent.putExtra("group_id", eachItems.get(position).getPostgroup_id());
                intent.putExtra("group_name", eachItems.get(position).getPostgroup_name());
                intent.putExtra("group_des", eachItems.get(position).getPostgroup_description());
                intent.putExtra("group_img", eachItems.get(position).getPostgroup_image());
                intent.putExtra("group_membr", eachItems.get(position).getPostgroup_member_count());
                intent.putExtra("group_discuss", eachItems.get(position).getPostgroup_discussion_count());
                intent.putExtra("group_created_by", eachItems.get(position).getPostgroup_created_by());
                startActivity(intent);
            }
        });

        emptyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyTextView.setVisibility(View.GONE);
                String temp = grpTypeTextView.getText().toString();
                if (temp.equals("My Groups")) {
                    getMyGroupsRequest();
                } else if (temp.equals("Groups")) {
                    getNewGroupsRequest();
                } else {
                    getDefaultGroupsRequest();
                }

            }
        });

        return rootView;
    }

    private void getNewGroupsRequest() {
        progressLayout.setVisibility(View.VISIBLE);
        String url = context.getResources().getString(R.string.MAIN_URL) + "newGroups";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                progressLayout.setVisibility(View.GONE);

                eachItems.clear();
                try {
                    JSONArray allPosts = new JSONArray(response);
                    if (allPosts.length() <= 0) {
                        emptyTextView.setVisibility(View.VISIBLE);
                        emptyTextView.setText("No " + grpTypeTextView.getText().toString() + " Found");
                        grpItemListView.setVisibility(View.GONE);
                    } else {
                        emptyTextView.setVisibility(View.GONE);
                        grpItemListView.setVisibility(View.VISIBLE);
                        for (int i = 0; i < allPosts.length(); ++i) {
                            JSONObject singlePost = (JSONObject) allPosts.get(i);
                            String postgroup_id, postgroup_name, postgroup_description, postgroup_type, postgroup_image,
                                    postgroup_member_count, postgroup_discussion_count, postgroup_created_at, postgroup_updated_at,
                                    postgroup_created_by;
                            postgroup_id = singlePost.getString("id");
                            postgroup_name = singlePost.getString("name");
                            postgroup_description = singlePost.getString("description");
                            postgroup_type = singlePost.getString("type");
                            postgroup_image = singlePost.getString("image");
                            postgroup_member_count = singlePost.getString("total_member");
                            postgroup_discussion_count = singlePost.getString("total_discussion");
                            postgroup_created_at = singlePost.getString("created_at");
                            postgroup_updated_at = singlePost.getString("updated_at");
                            postgroup_created_by = singlePost.getString("created_by");
                            GroupItem groupItem = new GroupItem(postgroup_id, postgroup_name, postgroup_description, postgroup_type,
                                    postgroup_image, postgroup_member_count, postgroup_discussion_count, postgroup_created_at,
                                    postgroup_updated_at, postgroup_created_by);
                            eachItems.add(groupItem);
                            //handler.addContact(pst);
                        }
                        //refresh adapter
                        //groupsAdapter.notifyDataSetChanged();
                        groupsAdapter = new GroupsAdapter(context, eachItems, 3, appUserId);
                        grpItemListView.setAdapter(groupsAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                progressLayout.setVisibility(View.GONE);
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load " + grpTypeTextView.getText().toString() + "\nTap to retry.");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getMyGroupsRequest() {
        progressLayout.setVisibility(View.VISIBLE);
        String url = context.getResources().getString(R.string.MAIN_URL) + "myJoinedGroups";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                progressLayout.setVisibility(View.GONE);

                eachItems.clear();
                try {
                    JSONArray allPosts = new JSONArray(response);
                    if (allPosts.length() <= 0) {
                        emptyTextView.setVisibility(View.VISIBLE);
                        emptyTextView.setText("No " + grpTypeTextView.getText().toString() + " Found");
                        grpItemListView.setVisibility(View.GONE);
                    } else {
                        emptyTextView.setVisibility(View.GONE);
                        grpItemListView.setVisibility(View.VISIBLE);
                        for (int i = 0; i < allPosts.length(); ++i) {
                            JSONObject singlePost = (JSONObject) allPosts.get(i);
                            String postgroup_id, postgroup_name, postgroup_description, postgroup_type, postgroup_image,
                                    postgroup_member_count, postgroup_discussion_count, postgroup_created_at, postgroup_updated_at,
                                    postgroup_created_by;
                            postgroup_id = singlePost.getString("id");
                            postgroup_name = singlePost.getString("name");
                            postgroup_description = singlePost.getString("description");
                            postgroup_type = singlePost.getString("type");
                            postgroup_image = singlePost.getString("image");
                            postgroup_member_count = singlePost.getString("total_member");
                            postgroup_discussion_count = singlePost.getString("total_discussion");
                            postgroup_created_at = singlePost.getString("created_at");
                            postgroup_updated_at = singlePost.getString("updated_at");
                            postgroup_created_by = singlePost.getString("created_by");
                            GroupItem groupItem = new GroupItem(postgroup_id, postgroup_name, postgroup_description, postgroup_type,
                                    postgroup_image, postgroup_member_count, postgroup_discussion_count, postgroup_created_at,
                                    postgroup_updated_at, postgroup_created_by);
                            eachItems.add(groupItem);
                            //handler.addContact(pst);
                        }
                        //refresh adapter
                        //groupsAdapter.notifyDataSetChanged();
                        groupsAdapter = new GroupsAdapter(context, eachItems, 2, appUserId);
                        grpItemListView.setAdapter(groupsAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressLayout.setVisibility(View.GONE);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load " + grpTypeTextView.getText().toString() + "\nTap to retry.");
                //grpItemListView.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("created_by", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getDefaultGroupsRequest() {
        progressLayout.setVisibility(View.VISIBLE);
        String url = context.getResources().getString(R.string.MAIN_URL) + "defaultGroups";
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response.toString());

                        //pDialog.dismiss();
                        progressLayout.setVisibility(View.GONE);

                        eachItems.clear();
                        try {
                            JSONArray allPosts = new JSONArray(response);
                            if (allPosts.length() <= 0) {
                                emptyTextView.setVisibility(View.VISIBLE);
                                emptyTextView.setText("No " + grpTypeTextView.getText().toString() + " Found");
                                grpItemListView.setVisibility(View.GONE);
                            } else {
                                emptyTextView.setVisibility(View.GONE);
                                grpItemListView.setVisibility(View.VISIBLE);
                                //Log.d("Test2", allPosts.toString());
                                for (int i = 0; i < allPosts.length(); ++i) {
                                    JSONObject singlePost = (JSONObject) allPosts.get(i);
                                    String postgroup_id, postgroup_name, postgroup_description, postgroup_type, postgroup_image,
                                            postgroup_member_count, postgroup_discussion_count, postgroup_created_at, postgroup_updated_at,
                                            postgroup_created_by;
                                    postgroup_id = singlePost.getString("id");
                                    postgroup_name = singlePost.getString("name");
                                    postgroup_description = singlePost.getString("description");
                                    postgroup_type = singlePost.getString("type");
                                    postgroup_image = singlePost.getString("image");
                                    postgroup_member_count = singlePost.getString("total_member");
                                    postgroup_discussion_count = singlePost.getString("total_discussion");
                                    postgroup_created_at = singlePost.getString("created_at");
                                    postgroup_updated_at = singlePost.getString("updated_at");
                                    postgroup_created_by = singlePost.getString("created_by");
                                    GroupItem groupItem = new GroupItem(postgroup_id, postgroup_name, postgroup_description, postgroup_type,
                                            postgroup_image, postgroup_member_count, postgroup_discussion_count, postgroup_created_at,
                                            postgroup_updated_at, postgroup_created_by);
                                    eachItems.add(groupItem);
                                    //handler.addContact(pst);
                                }
                                //refresh adapter
                                //groupsAdapter.notifyDataSetChanged();
                                groupsAdapter = new GroupsAdapter(context, eachItems, 1, appUserId);
                                grpItemListView.setAdapter(groupsAdapter);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Test", "Error: " + error.getMessage());
                //fatchFeedDb();
                //pDialog.dismiss();
                //Toast.makeText(context, "Some error occured \n fatched from db" + error.getMessage(), Toast.LENGTH_LONG).show();
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load " + grpTypeTextView.getText().toString() + "\nTap to retry.");
                //grpItemListView.setVisibility(View.GONE);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onResume() {
        super.onResume();
        String temp = grpTypeTextView.getText().toString();
        /*if (temp.equals("Jolpie Groups")) {

        }*/
        if (temp.equals("My Groups")) {
            getMyGroupsRequest();
        } else if (temp.equals("Groups")) {
            getNewGroupsRequest();
        } else {
            getDefaultGroupsRequest();
        }
    }
}
