package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.PatientsLikeMeFragmentAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.MyConditionsModel;
import com.theuserhub.jolpie.Models.PatientsLikeMeModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 1/27/2016.
 */
public class PatientsLikeMeFragment extends Fragment {
    private Context context;
    private String TAG = "PatientsLikeMeFragment", tag_string_req = "patients_str_req";

    private String isAll,condition_id;
    private ListView patientsListView;
    private List<PatientsLikeMeModel>eachPatients;
    private List<MyConditionsModel>conditionsList;
    private PatientsLikeMeFragmentAdapter patientsLikeMeAdapter;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_patients_like_me_patients, container, false);

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Bundle bundle = getArguments();
        isAll = bundle.getString("isAll");
        condition_id = bundle.getString("condition_id");
        Log.d("isAll",""+isAll);

        eachPatients = new ArrayList<PatientsLikeMeModel>();
        patientsListView = (ListView) rootView.findViewById(R.id.patients_like_lv);
        patientsLikeMeAdapter = new PatientsLikeMeFragmentAdapter(context,eachPatients);
        patientsListView.setAdapter(patientsLikeMeAdapter);

        if (isAll.equals("0")) {
            getPatientsLikeMe();
        }
        else {
            getConditionPatients();
        }

        return rootView;
    }

    private void getPatientsLikeMe() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "patientsLikeMe";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "patientsLikeMe: " + response.toString());
                parseJsonData(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getConditionPatients() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "conditionPatients";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "conditionpatients: " + response.toString());
                parseJsonData(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("condition_id", condition_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void parseJsonData(String response) {
        eachPatients.clear();
        try {
            JSONArray allPatients = new JSONArray(response);
            for (int i=0;i<allPatients.length();++i) {
                conditionsList = new ArrayList<MyConditionsModel>();
                JSONObject singlePatients = (JSONObject) allPatients.get(i);
                String user_id = singlePatients.getString("user_id");
                String first_name = singlePatients.getString("first_name");
                String last_name = singlePatients.getString("last_name");
                String image = singlePatients.getString("photo");
                String user_group_id = singlePatients.getString("user_group_id");
                JSONArray allConditions = singlePatients.getJSONArray("conditions");
                for (int j=0;j<allConditions.length();++j) {
                    JSONObject singleCondition = (JSONObject) allConditions.get(j);
                    String condition_id = singleCondition.getString("condition_id");
                    String conditionName_en = singleCondition.getString("condition_name");
                    String conditionName_bn = singleCondition.getString("bangla_condition_name");

                    conditionsList.add(new MyConditionsModel(condition_id,conditionName_en,conditionName_bn));
                }

                eachPatients.add(new PatientsLikeMeModel(user_id,first_name,last_name,image,user_group_id,conditionsList));
            }
            patientsLikeMeAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
