package com.theuserhub.jolpie.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.DrugProfileActivity;
import com.theuserhub.jolpie.Adapters.DrugsAdapter;
import com.theuserhub.jolpie.Models.DrugItem;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 11/18/2015.
 */
public class DrugsFragment extends Fragment{
    private Context context;
    private String TAG = "DrugsFragment";
    private String tag_string_req = "string_req";

    private ListView drugItemListView;
    private List<DrugItem> eachItems;
    DrugsAdapter drugsAdapter;
    DrugItem drugItem;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_drugs, container, false);

        drugItemListView= (ListView) rootView.findViewById(R.id.drugItem_lv);
        eachItems=new ArrayList<DrugItem>();
        drugsAdapter = new DrugsAdapter(context,eachItems);
        drugItemListView.setAdapter(drugsAdapter);

        drugItemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DrugItem drugItem = eachItems.get(position);
                Intent i = new Intent(context, DrugProfileActivity.class);
                i.putExtra("drug_id",drugItem.getDrug_id().toString());
                i.putExtra("brand_name",drugItem.getBrand_name().toString());
                i.putExtra("description",drugItem.getDescription().toString());
                i.putExtra("packet_image",drugItem.getPacket_image().toString());
                i.putExtra("packet_title",drugItem.getPacket_title().toString());
                i.putExtra("packet_description",drugItem.getPacket_description().toString());
                i.putExtra("drug_image",drugItem.getDrug_image().toString());
                i.putExtra("overview",drugItem.getOverview().toString());
                i.putExtra("side_effect",drugItem.getSide_effect().toString());
                i.putExtra("dosage",drugItem.getDosage().toString());
                i.putExtra("drugs_profile",drugItem.getDrugs_profile().toString());
                i.putExtra("generic_name",drugItem.getGeneric_name().toString());
                i.putExtra("first_name",drugItem.getFirst_name().toString());
                i.putExtra("user_id", drugItem.getUser_id().toString());
                context.startActivity(i);
            }
        });


        if (!AppFunctions.isNetworkConnected(context)) {
            Toast.makeText(context, "No network found \n fatched from db", Toast.LENGTH_LONG).show();
            //fatchFeedDb();
        } else {
            String url = context.getResources().getString(R.string.MAIN_URL)+"drugs";
            fetchDrugItems(url);
        }


        return rootView;
    }

    private void fetchDrugItems(String url){
        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Drug response Test1", response.toString());
                //handler.deleteAll();
                eachItems.clear();
                try {
                    JSONArray allPosts=new JSONArray(response);
                    Log.d("Test2", allPosts.toString());
                    for (int i=0;i<allPosts.length();++i)
                    {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        String drug_id, generic_id, brand_name, description, packet_image,packet_title,packet_description,drug_image,overview,side_effect,dosage,user_id,drugs_profile,generic_name,first_name;
                        drug_id = singlePost.getString("drug_id");
                        generic_id = singlePost.getString("generic_id");
                        brand_name = singlePost.getString("brand_name");
                        description = singlePost.getString("description");
                        packet_image = singlePost.getString("packet_image");
                        packet_title = singlePost.getString("packet_title");
                        packet_description = singlePost.getString("packet_description");
                        drug_image = singlePost.getString("drug_image");
                        overview = singlePost.getString("overview");
                        side_effect = singlePost.getString("side_effect");
                        dosage = singlePost.getString("dosage");
                        user_id = singlePost.getString("user_id");
                        drugs_profile = singlePost.getString("drugs_profile");
                        generic_name = singlePost.getString("generic_name");
                        first_name = singlePost.getString("first_name");
                        DrugItem drugItem=new DrugItem( drug_id,  generic_id,  brand_name,  description,  packet_image,  packet_title, packet_description,  drug_image,  overview,  side_effect,  dosage,  user_id,  drugs_profile,  generic_name,  first_name);
                        eachItems.add(drugItem);
                        //handler.addContact(pst);
                    }
                    //refresh adapter
                    drugsAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Test", "Error: " + error.getMessage());
                //fatchFeedDb();
                //Toast.makeText(context, "Some error occured \n fatched from db" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req + TAG);
    }
}
