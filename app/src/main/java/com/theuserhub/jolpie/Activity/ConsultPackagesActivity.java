package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.ConsultContentAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.ConsultContentData;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConsultPackagesActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "ConsultPackagesActivity";
    private String tag_string_req = "consult_pkg_string_req";

    private String user_id;
    public TextView errorMsg;
    private List<ConsultContentData> consultContentDatas = null;
    private UserDatabaseHandler userDatabaseHandler;
    public ConsultContentAdapter ccAdapter;
    //public static String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult_packages);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Consultancy Packages");

        errorMsg = (TextView) findViewById(R.id.message);
        GridView consultGridView = (GridView) findViewById(R.id.gridview);
        consultContentDatas = new ArrayList<ConsultContentData>();

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent consultIntent = getIntent();
        user_id = consultIntent.getStringExtra("user_id");

        ccAdapter = new ConsultContentAdapter(ConsultPackagesActivity.this,consultContentDatas,appUserId) ;
        consultGridView.setAdapter(ccAdapter);

        consultPackagesRequest();
    }

    private void consultPackagesRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL)+"consultPackages";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("consult content", response.toString());

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        if(jsonArray.length()<=0){
                            errorMsg.setText("No data found !!!");
                            errorMsg.setVisibility(View.VISIBLE);
                        }
                        for (int i=0;i<jsonArray.length();++i) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                            int consultancy_id = jsonObject.getInt("consultancy_id");
                            String package_name = jsonObject.getString("package_name").toString();
                            int audio = jsonObject.getInt("audio");
                            int video = jsonObject.getInt("video");
                            int text= jsonObject.getInt("text");
                            int duration= jsonObject.getInt("duration");
                            int price= jsonObject.getInt("price");
                            int user_id= jsonObject.getInt("user_id");
                            ConsultContentData ccData;
                            ccData = new ConsultContentData(Integer.toString(consultancy_id),package_name,Integer.toString(video),Integer.toString(audio),Integer.toString(text),Integer.toString(duration),Integer.toString(price),Integer.toString(user_id));
                            consultContentDatas.add(ccData);

                        }
                        ccAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
