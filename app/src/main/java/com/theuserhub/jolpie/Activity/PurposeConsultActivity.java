package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.theuserhub.jolpie.AndroidRTC.RtcActivity;
import com.theuserhub.jolpie.R;

public class PurposeConsultActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "PurposeConsultActivity", tag_string_req = "purpose_str_req";

    private EditText purposeEditText;
    private TextView consultTextView;
    private String purposeString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purpose_consult);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Purpose");

        purposeEditText = (EditText) findViewById(R.id.purpose_et);
        consultTextView = (TextView) findViewById(R.id.consult_now_tv);

        Intent purposeIntent = getIntent();
        final String user_id = purposeIntent.getStringExtra("user_id");
        final String flag = purposeIntent.getStringExtra("flag");
        final String type = purposeIntent.getStringExtra("type");

        consultTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purposeString = purposeEditText.getText().toString();

                Log.e("contents",user_id+":"+flag+":"+type+":"+purposeString);
                if (purposeString.trim().length() <= 0 ) {
                    Toast.makeText(context, "Please write your purpose.", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(context, RtcActivity.class);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("flag", flag);
                    intent.putExtra("type", type);
                    intent.putExtra("purpose",purposeString);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
