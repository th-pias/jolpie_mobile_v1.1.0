package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Models.CompareModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompareActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "CompareActivity", tag_string_req = "compare_str_req";

    private TableLayout compareTableLayout;
    private List<String> compareList, compare_ids;
    private List<CompareModel> eachCompares;
    private ProgressDialog pDialog;
    //private TableRow row;

    private String user_ids = "",compare_group_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compare);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Compare Doctors");

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        ArrayList<String> sendIds = new ArrayList<String>();
        Intent compareIntent = getIntent();
        sendIds = compareIntent.getStringArrayListExtra("compare_ids");
        compare_group_id = compareIntent.getStringExtra("user_group_id");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        compareTableLayout = (TableLayout) findViewById(R.id.compare_table);

        if (compare_group_id.equals("2")) {
            TableRow tableRow = new TableRow(this);
            tableRow.setPadding(5, 5, 5, 5);

            TableRow.LayoutParams params = new TableRow.LayoutParams(225, 230);

            TextView attribute_name = new TextView(this);
            attribute_name.setLayoutParams(params);
            attribute_name.setGravity(Gravity.CENTER_VERTICAL);
            attribute_name.setText("");
            attribute_name.setTextSize(15.0f);
            attribute_name.setPadding(5, 5, 5, 5);
            attribute_name.setTextColor(Color.parseColor("#000000"));
            attribute_name.setTypeface(null, Typeface.BOLD);

            tableRow.addView(attribute_name);

            compareTableLayout.addView(tableRow);

            setTableRowHeaderLeft("Rating");
            setTableRowHeaderLeft("Education");
            setTableRowHeaderLeft("Specialization");
            setTableRowHeaderLeft("Affiliation");
            setTableRowHeaderLeft("Company name");
            setTableRowHeaderLeft("Position");
            setTableRowHeaderLeft("Fees");
            setTableRowHeaderLeft("Online Consultancy");
        }
        compare_ids = new ArrayList<String>();
        compare_ids.addAll(sendIds);
        user_ids = compare_ids.get(0);
        for (int i=1;i<compare_ids.size();++i) {
            user_ids = user_ids+","+compare_ids.get(i);
        }

        compareList = new ArrayList<String>();
        eachCompares = new ArrayList<CompareModel>();

        getCompareList();

        //compareTableLayout.getChildAt(0);
    }

    private void setTableRowHeaderLeft(String text) {
        TableRow tableRow = new TableRow(this);
        tableRow.setPadding(5, 5, 5, 5);

        TableRow.LayoutParams params = new TableRow.LayoutParams(225, TableRow.LayoutParams.WRAP_CONTENT);

        TextView attribute_name = new TextView(this);
        attribute_name.setGravity(Gravity.CENTER_VERTICAL);
        attribute_name.setLayoutParams(params);
        attribute_name.setText(text);
        attribute_name.setTextSize(15.0f);
        attribute_name.setPadding(5, 5, 5, 5);
        attribute_name.setTextColor(Color.parseColor("#000000"));
        attribute_name.setTypeface(null, Typeface.BOLD);

        tableRow.addView(attribute_name);

        compareTableLayout.addView(tableRow);
    }

    private void setTableRowHeaderTop(String text, String photo) {
        TableRow tableRow = (TableRow) compareTableLayout.getChildAt(0);

        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + photo;

        LayoutInflater inflater_header = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View header_view = inflater_header.inflate(R.layout.header_compare_table, tableRow, false);

        TextView nameTextView = (TextView) header_view.findViewById(R.id.compare_table_header_name_tv);
        nameTextView.setText(text);

        NetworkImageView docImageView = (NetworkImageView) header_view.findViewById(R.id.compare_item_niv);
        docImageView.setImageUrl(meta_url, imageLoader);

        tableRow.addView(header_view);
    }

    private void setTableRowCell(String text, int position) {
        TableRow tableRow = (TableRow) compareTableLayout.getChildAt(position);

        TableRow.LayoutParams params = new TableRow.LayoutParams(225, TableRow.LayoutParams.WRAP_CONTENT);

        TextView attribute_name = new TextView(this);
        attribute_name.setGravity(Gravity.CENTER);
        attribute_name.setLayoutParams(params);
        attribute_name.setText(text);
        attribute_name.setTextSize(13.0f);
        attribute_name.setPadding(5, 5, 5, 5);
        attribute_name.setTextColor(Color.parseColor("#000000"));

        tableRow.addView(attribute_name);
    }

    private void setTableRowRating(String rating, String count,int position) {
        TableRow tableRow = (TableRow) compareTableLayout.getChildAt(position);
        //tableRow.setGravity(Gravity.CENTER);
        TableRow.LayoutParams params = new TableRow.LayoutParams(225, TableRow.LayoutParams.WRAP_CONTENT);

        LayoutInflater inflater_header = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View rating_view = inflater_header.inflate(R.layout.compare_rating_layout, tableRow, false);

        RatingBar ratingBar = (RatingBar) rating_view.findViewById(R.id.compare_ratingBar);
        float sum = Float.parseFloat(rating);
        float total = Float.parseFloat(count);
        float temp = sum/total;
        ratingBar.setRating(temp);

        tableRow.addView(rating_view);
    }

    private void getCompareList() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "compareList";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        JSONObject userJsonObject = (JSONObject) jsonObject.get("user_info");
                        String user_id = userJsonObject.getString("id");
                        String first_name = userJsonObject.getString("first_name");
                        String last_name = userJsonObject.getString("last_name");
                        String photo = userJsonObject.getString("photo");
                        String degree = userJsonObject.getString("degree");
                        String type = userJsonObject.getString("type");
                        String affiliation = userJsonObject.getString("affiliation");

                        String company_name = jsonObject.getString("company_name");
                        String position = jsonObject.getString("position");


                        List<SpecialityModel>specialityList = new ArrayList<SpecialityModel>();
                        JSONArray specialityJsonArray = (JSONArray) jsonObject.get("specialityList");
                        String specialization = "";
                        for (int j=0;j<specialityJsonArray.length();++j) {
                            JSONObject specialityJsonObject = (JSONObject) specialityJsonArray.get(j);
                            String specialization_id = specialityJsonObject.getString("specialization_id");
                            String name = specialityJsonObject.getString("name");
                            if (j==0) {
                                specialization = name;
                            }
                            else {
                                specialization = specialization+ ", "+name;
                            }
                            SpecialityModel specialityModel = new SpecialityModel(specialization_id,name);
                            specialityList.add(specialityModel);
                        }
                        JSONArray feesJsonArray = (JSONArray) jsonObject.get("chamber_fees");
                        String feesFull = "";
                        for (int j=0;j<feesJsonArray.length();++j) {
                            JSONObject feesJsonObject = (JSONObject) feesJsonArray.get(j);
                            String fee = feesJsonObject.getString("chamber_fee");

                            if (j==0) {
                                feesFull = fee;
                            }
                            else {
                                feesFull = feesFull+ ", "+fee;
                            }
                        }

                        String onlineConst = jsonObject.getString("onlineconsultancy");
                        int isOnline = 0;

                        try {
                            isOnline = Integer.parseInt(onlineConst);
                        }catch (NumberFormatException e){
                            isOnline = 0;
                        }
                        if (isOnline > 0 ){
                            onlineConst = "Yes";
                        }
                        else {
                            onlineConst = "No";
                        }

                        JSONArray reviewJsonArray = jsonObject.getJSONArray("reviews");
                        JSONObject reviewJsonObject = reviewJsonArray.getJSONObject(0);
                        String count = reviewJsonObject.getString("count");
                        String rating = "0";
                        if (!count.equals("0")) {
                            rating = reviewJsonObject.getString("rating");
                        }

                        setTableRowHeaderTop(first_name + " " + last_name, photo);
                        setTableRowRating(rating, count, 1);
                        setTableRowCell(degree, 2);
                        setTableRowCell(specialization, 3);
                        setTableRowCell(affiliation,4);
                        setTableRowCell(company_name,5);
                        setTableRowCell(position,6);
                        setTableRowCell(feesFull,7);
                        setTableRowCell(onlineConst,8);
                        //setTableRowCell(registration_number,9);
                        //setTableRowCell(registration_number,10);

                        /*CompareModel compareModel = new CompareModel(user_id,first_name,last_name,photo,
                                gender,user_group_id,contact_no_1,contact_no_2,degree,type,bed_capacity,address,birth_date,
                                company_name,position,last_certificate_name,last_certificate_year,last_certificate_institute,registration_number,
                                trade_license,tin,"","","","","", "",specialityList);
                        eachCompares.add(compareModel);*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: Try Again.", Toast.LENGTH_LONG).show();
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_ids", user_ids);
                params.put("user_group_id",compare_group_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }



    private void addTableRow() {
        TableRow row = new TableRow(this);

        /*TableLayout.LayoutParams lp = new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);*/

        row.setPadding(5, 5, 5, 5);

        TableRow.LayoutParams params = new TableRow.LayoutParams(225, 180);

        TextView attribute_name = new TextView(this);
        attribute_name.setLayoutParams(params);
        attribute_name.setGravity(Gravity.CENTER_VERTICAL);
        attribute_name.setText("Attribute Name");
        attribute_name.setTextSize(18.0f);
        attribute_name.setPadding(5, 5, 5, 5);
        attribute_name.setTextColor(Color.parseColor("#000000"));
        /*attribute_name.setBackgroundColor(Color.parseColor("#453e45"));*/
        attribute_name.setTypeface(null, Typeface.BOLD);

        row.addView(attribute_name);

        for (int j = 0; j < 10; j++) {
            LayoutInflater inflater_header = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View header_view = inflater_header.inflate(R.layout.header_compare_table, row, false);

            row.addView(header_view);
        }

        compareTableLayout.addView(row);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
