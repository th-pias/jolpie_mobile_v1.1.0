package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.GalleryImageAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.AlbumImage;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tumpa on 2/24/2016.
 */
public class GalleryImageActivity extends AppCompatActivity {
    public static String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
    Context context = this;
    private String album_id,user_id;
    private GalleryImageAdapter galleryImageAdapter;
    private List<AlbumImage> albumImagesList;
    private String TAG = "GalleryActivity", tag_string_req = "chamber_str_req";

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_image);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pictures");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        album_id = getIntent().getStringExtra("album_id");
        user_id = getIntent().getStringExtra("user_id");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (appUserId.equals(user_id)) {
            fab.setVisibility(View.VISIBLE);
        }
        else {
            fab.setVisibility(View.GONE);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,AddPhotoAlbumActivity.class);
                intent.putExtra("album_id",album_id);
                startActivity(intent);
            }
        });

        albumImagesList = new ArrayList<AlbumImage>();

        GridView imageGV = (GridView) findViewById(R.id.grid_view_image);

        galleryImageAdapter = new GalleryImageAdapter(GalleryImageActivity.this,albumImagesList);
        imageGV.setAdapter(galleryImageAdapter);

        getPhotosRequest();

    }

    private void getPhotosRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL)+"getAlbumsImage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("galleryImageActivity", response.toString());
                albumImagesList.clear();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                        int id = jsonObject.getInt("id");
                        String picture = jsonObject.getString("picture");
                        String picture_caption = jsonObject.getString("picture_caption");
                        String title = jsonObject.getString("title");
                        String description = jsonObject.getString("description");
                        int album_id = jsonObject.getInt("album_id");
                        String created_at = jsonObject.getString("created_at");
                        int user_id = jsonObject.getInt("user_id");


                        AlbumImage albumImage = new AlbumImage(Integer.toString(id),Integer.toString(user_id),created_at,Integer.toString(album_id),description,title,picture_caption,picture);

                        albumImagesList.add(albumImage);
                    }
                    galleryImageAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("album_id", album_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPhotosRequest();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
