package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.SpecialityAddAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddSpecialityActivity extends AppCompatActivity {
    private Context context = this;
    ListView addSpecialityLV;
    SpecialityAddAdapter specialityAddAdapter;
    List<SpecialityModel> specialityModelsArrayList;
    TextView noData;
    String user_id;
    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_speciality);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Speciality");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        user_id = getIntent().getStringExtra("user_id");


        noData = (TextView) findViewById(R.id.no_data);
        specialityModelsArrayList = new ArrayList<SpecialityModel>();
        addSpecialityLV = (ListView) findViewById(R.id.add_speciality);
        specialityAddAdapter = new SpecialityAddAdapter(context,specialityModelsArrayList,user_id);
        addSpecialityLV.setAdapter(specialityAddAdapter);

        /*specialityModelsArrayList.add(new SpecialityModel("1", "sdasdsds"));
        specialityModelsArrayList.add(new SpecialityModel("1","sdasdsds"));
        specialityModelsArrayList.add(new SpecialityModel("1","sdasdsds"));
        specialityModelsArrayList.add(new SpecialityModel("1","sdasdsds"));*/
        //specialityAddAdapter.notifyDataSetChanged();

        getSpecialityData();


    }

    public void getSpecialityData(){
        String url = context.getResources().getString(R.string.MAIN_URL) + "otherSpecialities";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("SpecialityActivity", "serviceActivity: " + response.toString());

                try {
                    JSONArray allCond = new JSONArray(response);
                    if(allCond.length()==0){
                        noData.setVisibility(View.VISIBLE);
                    }
                    for (int i=0;i<allCond.length();++i) {
                        JSONObject singleCond = (JSONObject) allCond.get(i);
                        String id = singleCond.getString("id");
                        String name = singleCond.getString("name");
                        SpecialityModel specialityModel = new SpecialityModel(id, name);

                        specialityModelsArrayList.add(specialityModel);
                    }
                    specialityAddAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d("SpecialityActivity", "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                Log.e("user_id",appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "tag_string_req");
    }

}
