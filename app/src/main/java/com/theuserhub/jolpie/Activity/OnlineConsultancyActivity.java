package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.ConsultContentAdapter;
import com.theuserhub.jolpie.AndroidRTC.ChattingActivity;
import com.theuserhub.jolpie.AndroidRTC.RtcActivity;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.ConsultContentData;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OnlineConsultancyActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "OnlineConsultancyActivity";
    private String tag_string_req = "consult_string_req";

    private ProgressDialog pDialog;
    private RelativeLayout progressLayout;
    private TextView videoMsgTextView, audioMsgTextView, textMsgTextView, isSubsTextView,
            textImageView, videoImageView, audioImageView, textCountTextView, textTotalTextView, videoCountTextView, videoTotalTextView,
            audioCountTextView, audioTotalTextView, textConsultTextView, videoConsultTextView,
            audioConsultTextView, emptyTextView, noDataTextView, availTimeTextView;
    private String user_id, user_fname, user_lname, user_photo, user_group_id;
    private int videoAnInt, audioAnInt, textAnInt;
    private List<ConsultContentData> consultContentDatas;
    private ConsultContentAdapter ccAdapter;
    private ListView consultGridView;
    private LinearLayout consultLayout, textMsgLayout, videoCallLayout, audioCallLayout;
    private ViewGroup onlineViewGroup;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_consultancy);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Online Consultancy");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent consIntent = getIntent();
        user_id = consIntent.getStringExtra("user_id");

        Typeface onlineFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        progressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        emptyTextView = (TextView) findViewById(R.id.empty_tv);
        isSubsTextView = (TextView) findViewById(R.id.is_subs_tv);
        consultLayout = (LinearLayout) findViewById(R.id.online_consult_layout);
        videoMsgTextView = (TextView) findViewById(R.id.vedio_balance);
        audioMsgTextView = (TextView) findViewById(R.id.audio_balance);
        textMsgTextView = (TextView) findViewById(R.id.text_balance);
        availTimeTextView = (TextView) findViewById(R.id.available_time_tv);

        consultGridView = (ListView) findViewById(R.id.gridview);

        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        onlineViewGroup = (ViewGroup) inflater.inflate(R.layout.header_online_consultancy, null);

        textMsgLayout = (LinearLayout) onlineViewGroup.findViewById(R.id.text_msg_layout);
        textImageView = (TextView) onlineViewGroup.findViewById(R.id.text_iv);
        textImageView.setTypeface(onlineFont);
        textCountTextView = (TextView) onlineViewGroup.findViewById(R.id.text_msg_count_tv);
        textTotalTextView = (TextView) onlineViewGroup.findViewById(R.id.text_msg_total_tv);
        textConsultTextView = (TextView) onlineViewGroup.findViewById(R.id.text_msg_consult_tv);

        videoCallLayout = (LinearLayout) onlineViewGroup.findViewById(R.id.video_call_layout);
        videoImageView = (TextView) onlineViewGroup.findViewById(R.id.video_iv);
        videoImageView.setTypeface(onlineFont);
        videoCountTextView = (TextView) onlineViewGroup.findViewById(R.id.video_call_count_tv);
        videoTotalTextView = (TextView) onlineViewGroup.findViewById(R.id.video_call_total_tv);
        videoConsultTextView = (TextView) onlineViewGroup.findViewById(R.id.video_call_consult_tv);

        audioCallLayout = (LinearLayout) onlineViewGroup.findViewById(R.id.audio_call_layout);
        audioImageView = (TextView) onlineViewGroup.findViewById(R.id.audeo_iv);
        audioImageView.setTypeface(onlineFont);
        audioCountTextView = (TextView) onlineViewGroup.findViewById(R.id.audio_call_count_tv);
        audioTotalTextView = (TextView) onlineViewGroup.findViewById(R.id.audeo_call_total_tv);
        audioConsultTextView = (TextView) onlineViewGroup.findViewById(R.id.audio_call_consult_tv);

        noDataTextView = (TextView) onlineViewGroup.findViewById(R.id.no_data_tv);

        consultGridView.addHeaderView(onlineViewGroup);

        consultContentDatas = new ArrayList<ConsultContentData>();
        ccAdapter = new ConsultContentAdapter(context, consultContentDatas, appUserId);
        consultGridView.setAdapter(ccAdapter);

        isSubscribeRequest();

        /*videoMsgTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*if (videoAnInt >0 ) {
                    --videoAnInt;
                    videoMsgTextView.setText("Video Call ( " + String.valueOf(videoAnInt) + " available )");
                }*//*

                Intent intent = new Intent(context, RtcActivity.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("flag", "0");
                intent.putExtra("type", "v");
                startActivity(intent);
            }
        });*/

        videoConsultTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(context, RtcActivity.class);
                Intent intent = new Intent(context, PurposeConsultActivity.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("flag", "0");
                intent.putExtra("type", "video");
                startActivity(intent);
            }
        });
        audioConsultTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(context, RtcActivity.class);
                Intent intent = new Intent(context, PurposeConsultActivity.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("flag", "0");
                intent.putExtra("type", "audio");
                startActivity(intent);
            }
        });

        /*audioMsgTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (audioAnInt > 0) {
                    --audioAnInt;
                    audioMsgTextView.setText("Audio Call ( " + String.valueOf(audioAnInt) + " available )");
                    Toast.makeText(context, "Not available now", Toast.LENGTH_LONG).show();
                }
            }
        });*/

        textConsultTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textAnInt > 0) {
                    --textAnInt;
                    Intent intent = new Intent(context, ChattingActivity.class);
                    intent.putExtra("to_user_id",user_id);
                    intent.putExtra("first_name",user_fname);
                    intent.putExtra("last_name",user_lname);
                    intent.putExtra("photo",user_photo);
                    intent.putExtra("user_group_id",user_group_id);
                    startActivity(intent);
                }
            }
        });

        /*textMsgTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textAnInt > 0) {
                    --textAnInt;
                    textMsgTextView.setText("Text Chat ( " + String.valueOf(textAnInt) + " available )");
                    Toast.makeText(context, "Not available now", Toast.LENGTH_LONG).show();
                }
            }
        });*/

        getAvailableTime();
    }

    private void isSubscribeRequest() {
        //pDialog.setMessage("Gathering Information...");
        //pDialog.show();
        progressLayout.setVisibility(View.VISIBLE);
        String url = context.getResources().getString(R.string.MAIN_URL) + "getIsSubscribed";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                //pDialog.dismiss();
                progressLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String isSubscribed = jsonObject.getString("isSubscribed");
                    String sub_msg = "";
                    if (isSubscribed.equalsIgnoreCase("0")) {
                        if (appUserId.equals(user_id)) {
                            isSubsTextView.setVisibility(View.VISIBLE);
                            isSubsTextView.setText("You are not a subcribed user. Please buy subscription.");
                            consultGridView.setVisibility(View.GONE);
                        } else {
                            isSubsTextView.setVisibility(View.VISIBLE);
                            isSubsTextView.setText("No data found.");
                            consultGridView.setVisibility(View.GONE);
                        }
                    } else {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("subscription");
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String subs_date = jsonObject1.getString("tran_date");
                        String durationString = jsonObject1.getString("duration");
                        int duration = 0;
                        try {
                            duration = Integer.parseInt(durationString);
                        }
                        catch (NumberFormatException e) {

                        }

                        Date transDate = dateFormat.parse(subs_date);
                        Calendar calendar = Calendar.getInstance();
                        Date todayDate = calendar.getTime();
                        String todayString = dateFormat.format(todayDate);

                        todayDate = dateFormat.parse(todayString);
                        Log.e("date", "" + todayString + " : " + subs_date);
                        //double difference = (double)((double)todayDate.getTime() - (double)transDate.getTime())/((double)(365*24*60*60*1000));
                        long diff = todayDate.getTime() - transDate.getTime();
                        long dayDiff = TimeUnit.MILLISECONDS.toDays(diff);

                        if (dayDiff > duration) {
                            if (appUserId.equals(user_id)) {
                                isSubsTextView.setVisibility(View.VISIBLE);
                                isSubsTextView.setText("Your subscription is expired. Please renew.");
                            } else {
                                isSubsTextView.setVisibility(View.VISIBLE);
                                isSubsTextView.setText("No data found.");
                                consultGridView.setVisibility(View.GONE);
                            }
                        } else {
                            if (appUserId.equals(user_id)) {
                                consultGridView.removeHeaderView(onlineViewGroup);
                                consultPackagesRequest();
                            } else {
                                getUserInfo();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //pDialog.dismiss();
                progressLayout.setVisibility(View.GONE);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load data.\nTap to retry.");
                emptyTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emptyTextView.setVisibility(View.GONE);
                        isSubscribeRequest();
                    }
                });
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void consultancyBalaceRequest() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "consultancyBalance";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("consult", "consult: " + response.toString());
                pDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String vedio_count = jsonObject.getString("video");
                    String audio_count = jsonObject.getString("audio");
                    String text_count = jsonObject.getString("text");
                    try {
                        videoAnInt = Integer.parseInt(vedio_count);
                    } catch (NumberFormatException e) {
                        videoAnInt = 0;
                    }

                    try {
                        audioAnInt = Integer.parseInt(audio_count);
                    } catch (NumberFormatException e) {
                        audioAnInt = 0;
                    }

                    try {
                        textAnInt = Integer.parseInt(text_count);
                    } catch (NumberFormatException e) {
                        textAnInt = 0;
                    }
                    videoMsgTextView.setText("Video Call ( " + vedio_count + " available )");
                    audioMsgTextView.setText("Audio Call ( " + audio_count + " available )");
                    textMsgTextView.setText("Text Chat ( " + text_count + " available )");

                    textCountTextView.setText(text_count);
                    textTotalTextView.setText(text_count);

                    videoCountTextView.setText(vedio_count);
                    videoTotalTextView.setText(vedio_count);

                    audioCountTextView.setText(audio_count);
                    audioTotalTextView.setText(audio_count);

                    if (textAnInt <= 0) {
                        textConsultTextView.setBackgroundColor(Color.parseColor("#9e9e9e"));
                    }
                    if (videoAnInt <= 0) {
                        videoConsultTextView.setBackgroundColor(Color.parseColor("#9e9e9e"));
                    }
                    if (audioAnInt <= 0) {
                        audioConsultTextView.setBackgroundColor(Color.parseColor("#9e9e9e"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    videoMsgTextView.setText("Video Call ( 0 available )");
                    audioMsgTextView.setText("Audio Call ( 0 available )");
                    textMsgTextView.setText("Text Chat ( 0 available )");

                    textCountTextView.setText("0");
                    textTotalTextView.setText("0");

                    videoCountTextView.setText("0");
                    videoTotalTextView.setText("0");

                    audioCountTextView.setText("0");
                    audioTotalTextView.setText("0");
                }
                consultPackagesRequest();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
                Toast.makeText(context, "Error: Try Again.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("doctor_id", user_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void consultPackagesRequest() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "consultPackages";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("consult content", response.toString());
                pDialog.dismiss();
                consultContentDatas.clear();

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() <= 0) {
                        noDataTextView.setText("No package found !!!");
                        noDataTextView.setVisibility(View.VISIBLE);
                    } else {
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                            int consultancy_id = jsonObject.getInt("consultancy_id");
                            String package_name = jsonObject.getString("package_name").toString();
                            int audio = jsonObject.getInt("audio");
                            int video = jsonObject.getInt("video");
                            int text = jsonObject.getInt("text");
                            int duration = jsonObject.getInt("duration");
                            int price = jsonObject.getInt("price");
                            int user_id = jsonObject.getInt("user_id");
                            ConsultContentData ccData;
                            ccData = new ConsultContentData(Integer.toString(consultancy_id), package_name, Integer.toString(video), Integer.toString(audio), Integer.toString(text), Integer.toString(duration), Integer.toString(price), Integer.toString(user_id));
                            consultContentDatas.add(ccData);
                        }
                        ccAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
                noDataTextView.setText("No package found !!!");
                noDataTextView.setVisibility(View.VISIBLE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getUserInfo() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "getUserGeneralInfo";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("consult content", response.toString());
                pDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    user_fname = jsonObject.getString("first_name");
                    user_lname = jsonObject.getString("last_name");
                    user_photo = jsonObject.getString("photo");
                    user_group_id = jsonObject.getString("user_group_id");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                consultancyBalaceRequest();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
                Toast.makeText(context,"Error: Try again later.",Toast.LENGTH_LONG).show();
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getAvailableTime() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getAvailableTime";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Available Time", response.toString());

                if (response.equalsIgnoreCase("null")) {
                    availTimeTextView.setText("not added");
                }
                else {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String start_time = jsonObject.getString("start_time");
                        String end_time = jsonObject.getString("end_time");

                        availTimeTextView.setText(start_time + "  to  " + end_time);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
                Toast.makeText(context,"Error: Try again later.",Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onResume() {
        super.onResume();
        consultancyBalaceRequest();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
