package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.GroupsAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.GroupItem;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyGroupsActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "MyGroupsActivity", tag_string_req = "my_groups_str_req";

    private ListView grpItemListView;
    private List<GroupItem> eachItems;
    private GroupsAdapter groupsAdapter;
    private TextView emptyTextView;
    private ProgressDialog pDialog;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_groups);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Groups");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CreateGroupActivity.class);
                startActivity(intent);
            }
        });

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        emptyTextView = (TextView) findViewById(R.id.empty_grp_tv);

        grpItemListView = (ListView) findViewById(R.id.my_groups_lv);
        eachItems = new ArrayList<GroupItem>();
        groupsAdapter = new GroupsAdapter(context, eachItems,2,appUserId);
        grpItemListView.setAdapter(groupsAdapter);

        myGroupsRequest();

        grpItemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context, GroupDetailActivity.class);
                intent.putExtra("group_id",eachItems.get(position).getPostgroup_id());
                intent.putExtra("group_name",eachItems.get(position).getPostgroup_name());
                intent.putExtra("group_des",eachItems.get(position).getPostgroup_description());
                intent.putExtra("group_img",eachItems.get(position).getPostgroup_image());
                intent.putExtra("group_membr",eachItems.get(position).getPostgroup_member_count());
                intent.putExtra("group_discuss",eachItems.get(position).getPostgroup_discussion_count());
                intent.putExtra("group_created_by",eachItems.get(position).getPostgroup_created_by());
                startActivity(intent);
            }
        });
    }

    private void myGroupsRequest() {
        pDialog.setMessage("Gathering Information");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "myJoinedGroups";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                eachItems.clear();
                try {
                    JSONArray allPosts = new JSONArray(response);
                    if (allPosts.length()>0){
                        emptyTextView.setVisibility(View.GONE);
                        grpItemListView.setVisibility(View.VISIBLE);
                    }
                    else {
                        emptyTextView.setVisibility(View.VISIBLE);
                        grpItemListView.setVisibility(View.GONE);
                    }
                    for (int i = 0; i < allPosts.length(); ++i) {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        String postgroup_id, postgroup_name, postgroup_description, postgroup_type, postgroup_image,
                                postgroup_member_count, postgroup_discussion_count, postgroup_created_at, postgroup_updated_at,
                                postgroup_created_by;
                        postgroup_id = singlePost.getString("id");
                        postgroup_name = singlePost.getString("name");
                        postgroup_description = singlePost.getString("description");
                        postgroup_type = singlePost.getString("type");
                        postgroup_image = singlePost.getString("image");
                        postgroup_member_count = singlePost.getString("total_member");
                        postgroup_discussion_count = singlePost.getString("total_discussion");
                        postgroup_created_at = singlePost.getString("created_at");
                        postgroup_updated_at = singlePost.getString("updated_at");
                        postgroup_created_by = singlePost.getString("created_by");

                        GroupItem groupItem = new GroupItem(postgroup_id, postgroup_name, postgroup_description, postgroup_type,
                                postgroup_image, postgroup_member_count, postgroup_discussion_count, postgroup_created_at,
                                postgroup_updated_at, postgroup_created_by);
                        eachItems.add(groupItem);
                        //handler.addContact(pst);
                    }
                    //refresh adapter
                    groupsAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("created_by", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
