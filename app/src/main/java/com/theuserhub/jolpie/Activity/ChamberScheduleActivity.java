package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.ChamberScheduleModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChamberScheduleActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "ScheduleActivity", tag_string_req = "schedule_str_req";

    private ProgressDialog pDialog;
    private LinearLayout firstLayout,secondLayout,thirdLayout;
    private TextView firstTextView,secondTextView,thirdTextView,selectedTextView, nextTextView, leftImageView,rightImageView;
    private Calendar calendar,timeCalendar, todayCalendar;
    private Date firstDate,secondDate,thirdDate;
    private int dateDistance,patient_time_int;
    private String selectedDateString,selectedDateFullString,selectedDayString,selectedTimeString,firstDateString,firstDateFullString,secondDateString,thirdDateString,thirdDateFullString,firstDayString,secondDayString,thirdDayString,patient_time;
    private SimpleDateFormat dateFormat, _24DateFormat, _12DateFormat, simpleDateFormat;
    private List<ChamberScheduleModel> eachSchedule;
    private List<Date>appointedScheduleList;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    private String name, degree,image;
    private String chamber_id, organisation_id,chamber_first_name,chamber_last_name,chamber_photo,chamber_user_group_id,chamber_street_address,chamber_address,cut_of_time,chamber_fee,each_patient_time;
    private TextView appint_schedule_next_tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chamber_schedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Chamber Schedules");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent scheduleIntent = getIntent();
        chamber_id = scheduleIntent.getStringExtra("chamber_id");
        patient_time = scheduleIntent.getStringExtra("patient_time");
        patient_time_int = Integer.parseInt(patient_time.trim());
        organisation_id = scheduleIntent.getStringExtra("organisation_id");
        chamber_first_name = scheduleIntent.getStringExtra("chamber_first_name");
        chamber_last_name = scheduleIntent.getStringExtra("chamber_last_name");
        chamber_photo = scheduleIntent.getStringExtra("chamber_photo");
        chamber_user_group_id = scheduleIntent.getStringExtra("chamber_user_group_id");
        chamber_street_address = scheduleIntent.getStringExtra("chamber_street_address");
        chamber_address = scheduleIntent.getStringExtra("chamber_address");
        cut_of_time = scheduleIntent.getStringExtra("cut_of_time");
        chamber_fee = scheduleIntent.getStringExtra("chamber_fee");
        each_patient_time = scheduleIntent.getStringExtra("each_patient_time");
        name = scheduleIntent.getStringExtra("name");
        degree = scheduleIntent.getStringExtra("degree");
        image = scheduleIntent.getStringExtra("image");

        Typeface scheduleFont = Typeface.createFromAsset( getAssets(), "fontawesome-webfont.ttf" );

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        selectedDateString = "";
        selectedDayString = "";
        selectedTimeString = "";

        //appint_schedule_next_tv = (TextView) findViewById(R.id.appint_schedule_next_tv);

        nextTextView = (TextView) findViewById(R.id.appint_schedule_next_tv);
        //nextTextView.setTextColor();
        nextTextView.setVisibility(View.GONE);
        selectedTextView = (TextView) findViewById(R.id.schedule_selected_tv);

        appointedScheduleList = new ArrayList<Date>();
        eachSchedule = new ArrayList<ChamberScheduleModel>();

        firstDate = new Date();
        secondDate = new Date();
        thirdDate = new Date();

        _24DateFormat = new SimpleDateFormat("HH:mm:ss");
        _12DateFormat = new SimpleDateFormat("hh:mm a");

        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateDistance = 1;

        todayCalendar = Calendar.getInstance();
        timeCalendar = Calendar.getInstance();
        calendar = Calendar.getInstance();
        firstDate = calendar.getTime();
        firstDateString = dateFormat.format(firstDate);
        firstDateFullString = simpleDateFormat.format(firstDate);
        firstDayString = new SimpleDateFormat("EE").format(firstDate);

        calendar.add(Calendar.DAY_OF_YEAR, dateDistance);
        secondDate = calendar.getTime();
        secondDateString = dateFormat.format(secondDate);
        secondDayString = new SimpleDateFormat("EE").format(secondDate);

        calendar.add(Calendar.DAY_OF_YEAR, dateDistance);
        thirdDate = calendar.getTime();
        thirdDateString = dateFormat.format(thirdDate);
        thirdDateFullString = simpleDateFormat.format(thirdDate);
        thirdDayString = new SimpleDateFormat("EE").format(thirdDate);

        firstLayout = (LinearLayout) findViewById(R.id.date_first_layout);
        secondLayout = (LinearLayout) findViewById(R.id.date_second_layout);
        thirdLayout = (LinearLayout) findViewById(R.id.date_third_layout);

        firstTextView = (TextView) findViewById(R.id.date_first_tv);
        secondTextView = (TextView) findViewById(R.id.date_second_tv);
        thirdTextView = (TextView) findViewById(R.id.date_third_tv);

        leftImageView = (TextView) findViewById(R.id.chamber_schedule_left_iv);
        leftImageView.setTypeface(scheduleFont);
        rightImageView = (TextView) findViewById(R.id.chamber_schedule_right_iv);
        rightImageView.setTypeface(scheduleFont);

        firstTextView.setText(firstDayString+"\n"+firstDateString);
        secondTextView.setText(secondDayString+"\n"+secondDateString);
        thirdTextView.setText(thirdDayString+"\n"+thirdDateString);

        rightImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                firstLayout.removeAllViews();
                secondLayout.removeAllViews();
                thirdLayout.removeAllViews();

                calendar.add(Calendar.DAY_OF_YEAR, dateDistance);
                firstDate = calendar.getTime();
                firstDateString = dateFormat.format(firstDate);
                firstDateFullString = simpleDateFormat.format(firstDate);
                firstDayString = new SimpleDateFormat("EE").format(firstDate);

                Date todaysDate = todayCalendar.getTime();
                if (!todaysDate.equals(firstDate)) {
                    leftImageView.setBackgroundColor(Color.parseColor("#3a773a"));
                }

                calendar.add(Calendar.DAY_OF_YEAR, dateDistance);
                secondDate = calendar.getTime();
                secondDateString = dateFormat.format(secondDate);
                secondDayString = new SimpleDateFormat("EE").format(secondDate);

                calendar.add(Calendar.DAY_OF_YEAR, dateDistance);
                thirdDate = calendar.getTime();
                thirdDateString = dateFormat.format(thirdDate);
                thirdDateFullString = simpleDateFormat.format(thirdDate);
                thirdDayString = new SimpleDateFormat("EE").format(thirdDate);

                firstTextView.setText(firstDayString + "\n" + firstDateString);
                secondTextView.setText(secondDayString + "\n" + secondDateString);
                thirdTextView.setText(thirdDayString + "\n" + thirdDateString);

                //addSchedule();
                appointedScheduleRequest(firstDateFullString,thirdDateFullString);
            }
        });

        leftImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Date todaysDate = todayCalendar.getTime();
                if (!todaysDate.equals(firstDate)) {
                    firstLayout.removeAllViews();
                    secondLayout.removeAllViews();
                    thirdLayout.removeAllViews();

                    calendar.add(Calendar.DAY_OF_YEAR, -5);
                    firstDate = calendar.getTime();
                    firstDateString = dateFormat.format(firstDate);
                    firstDateFullString = simpleDateFormat.format(firstDate);
                    firstDayString = new SimpleDateFormat("EE").format(firstDate);

                    if (todaysDate.equals(firstDate)) {
                        leftImageView.setBackgroundColor(Color.parseColor("#616161"));
                    }

                    calendar.add(Calendar.DAY_OF_YEAR, dateDistance);
                    secondDate = calendar.getTime();
                    secondDateString = dateFormat.format(secondDate);
                    secondDayString = new SimpleDateFormat("EE").format(secondDate);

                    calendar.add(Calendar.DAY_OF_YEAR, dateDistance);
                    thirdDate = calendar.getTime();
                    thirdDateString = dateFormat.format(thirdDate);
                    thirdDateFullString = simpleDateFormat.format(thirdDate);
                    thirdDayString = new SimpleDateFormat("EE").format(thirdDate);

                    firstTextView.setText(firstDayString + "\n" + firstDateString);
                    secondTextView.setText(secondDayString + "\n" + secondDateString);
                    thirdTextView.setText(thirdDayString + "\n" + thirdDateString);

                    //addSchedule();
                    appointedScheduleRequest(firstDateFullString,thirdDateFullString);
                }
                else {
                    //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                }
            }
        });

        getChamberSchedule();

        nextTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,AppointmentConfirmActivity.class);
                intent.putExtra("selected_date",selectedDateString);
                intent.putExtra("selected_time",selectedTimeString);
                intent.putExtra("selected_day",selectedDayString);
                intent.putExtra("chamber_id",chamber_id);
                intent.putExtra("patient_time",patient_time);
                intent.putExtra("organisation_id",organisation_id);
                intent.putExtra("chamber_first_name",chamber_first_name);
                intent.putExtra("chamber_last_name",chamber_last_name);
                intent.putExtra("chamber_photo",chamber_photo);
                intent.putExtra("chamber_user_group_id",chamber_user_group_id);
                intent.putExtra("chamber_street_address",chamber_street_address);
                intent.putExtra("chamber_address",chamber_address);
                intent.putExtra("cut_of_time",cut_of_time);
                intent.putExtra("chamber_fee",chamber_fee);
                intent.putExtra("each_patient_time",each_patient_time);
                intent.putExtra("name",name);
                intent.putExtra("degree",degree);
                intent.putExtra("image",image);
                startActivity(intent);
                finish();
            }
        });
    }

    private void appointedScheduleRequest(final String start_date, final String end_date) {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL)+"getAppointedSchedule";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG,response.toString());
                pDialog.dismiss();
                appointedScheduleList.clear();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String appointment_time;

                        appointment_time = jsonObject.getString("appointment_time");
                        Date date = new Date();
                        date = simpleDateFormat.parse(appointment_time);
                        appointedScheduleList.add(date);
                    }
                    addSchedule();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context,"Error: Try again.", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("chamber_id", chamber_id);
                params.put("start_date", start_date);
                params.put("end_date", end_date);

                //Log.e("st : end :", start_date + " + " + end_date);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getChamberSchedule() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL)+"getChamberSchedule";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG,response.toString());
                pDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String schedule_id,user_id,chamberId,start_time,end_time,day;

                        schedule_id = jsonObject.getString("id");
                        user_id = jsonObject.getString("user_id");
                        chamberId = jsonObject.getString("chamber_id");
                        start_time = jsonObject.getString("start_time");
                        end_time = jsonObject.getString("end_time");
                        day = jsonObject.getString("day");

                        Date startDate = new Date();
                        Date endDate = new Date();
                        try {

                            startDate = _24DateFormat.parse(start_time);
                            String timeStr = _12DateFormat.format(startDate);
                            startDate = _12DateFormat.parse(timeStr);

                            endDate = _24DateFormat.parse(end_time);
                            timeStr = _12DateFormat.format(endDate);
                            endDate = _12DateFormat.parse(timeStr);

                        } catch (ParseException e) {
                            e.printStackTrace();
                            Toast.makeText(context,"Exception",Toast.LENGTH_SHORT).show();
                        }

                        ChamberScheduleModel scheduleModel = new ChamberScheduleModel(schedule_id,user_id,chamberId,startDate,endDate,day,start_time,end_time);
                        int f = 0;
                        for (int j=0;j<eachSchedule.size(); ++j) {
                            if (day.equals(eachSchedule.get(j).getDay())) {
                                if (startDate.before(eachSchedule.get(j).getStart_time())) {
                                    f=1;
                                    Toast.makeText(context,schedule_id+": before: "+day,Toast.LENGTH_SHORT).show();
                                    eachSchedule.add(j,scheduleModel);
                                    break;
                                }
                            }
                        }
                        if (f==0) {
                            eachSchedule.add(scheduleModel);
                        }

                    }
                    for (int j=0;j<eachSchedule.size(); ++j) {
                        //Log.e("id",eachSchedule.get(j).getSchedule_id());
                    }

                    //addSchedule();
                    appointedScheduleRequest(firstDateFullString,thirdDateFullString);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context,"Error: Try again.", Toast.LENGTH_LONG).show();
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("chamber_id", chamber_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void addSchedule() throws ParseException {
        for (int i=0 ; i<eachSchedule.size() ; ++i) {
            if (firstDayString.toLowerCase().equals(eachSchedule.get(i).getDay().toLowerCase())) {

                timeCalendar.setTime(eachSchedule.get(i).getStart_time());
                Date newdate = new Date();
                newdate = eachSchedule.get(i).getStart_time();
                //Log.e("getStart_time",eachSchedule.get(i).getStart_time_str());
                while (newdate.before(eachSchedule.get(i).getEnd_time())) {
                    Date date = new Date();
                    String temp = _24DateFormat.format(newdate);
                    temp = firstDateString + " " + temp;
                    Log.e("time",temp);
                    SimpleDateFormat tempDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    date = tempDateFormat.parse(temp);
                    temp = simpleDateFormat.format(date);
                    date = simpleDateFormat.parse(temp);
                    Log.e("Date: ",simpleDateFormat.format(date));

                    if (!appointedScheduleList.contains(date)) {
                        addFirstSchedule(_12DateFormat.format(newdate));
                    }
                    timeCalendar.add(Calendar.MINUTE, patient_time_int);
                    newdate = timeCalendar.getTime();
                }
            }
            else if (secondDayString.toLowerCase().equals(eachSchedule.get(i).getDay().toLowerCase())) {

                timeCalendar.setTime(eachSchedule.get(i).getStart_time());
                Date newdate = new Date();
                newdate = eachSchedule.get(i).getStart_time();
                while (newdate.before(eachSchedule.get(i).getEnd_time())) {
                    Date date = new Date();
                    String temp = _24DateFormat.format(newdate);
                    temp = secondDateString + " " + temp;
                    Log.e("time",temp);
                    SimpleDateFormat tempDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    date = tempDateFormat.parse(temp);
                    temp = simpleDateFormat.format(date);
                    date = simpleDateFormat.parse(temp);
                    Log.e("Date: ",simpleDateFormat.format(date));
                    if (!appointedScheduleList.contains(date)) {
                        addSecondSchedule(_12DateFormat.format(newdate));
                    }
                    timeCalendar.add(Calendar.MINUTE, patient_time_int);
                    newdate = timeCalendar.getTime();
                }
            }
            else if (thirdDayString.toLowerCase().equals(eachSchedule.get(i).getDay().toLowerCase())) {

                timeCalendar.setTime(eachSchedule.get(i).getStart_time());
                Date newdate = new Date();
                newdate = eachSchedule.get(i).getStart_time();
                while (newdate.before(eachSchedule.get(i).getEnd_time())) {
                    Date date = new Date();
                    String temp = _24DateFormat.format(newdate);
                    temp = thirdDateString + " " + temp;
                    Log.e("time",temp);
                    SimpleDateFormat tempDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    date = tempDateFormat.parse(temp);
                    temp = simpleDateFormat.format(date);
                    date = simpleDateFormat.parse(temp);
                    Log.e("Date: ",simpleDateFormat.format(date));
                    if (!appointedScheduleList.contains(date)) {
                        addThirdSchedule(_12DateFormat.format(newdate));
                    }
                    timeCalendar.add(Calendar.MINUTE, patient_time_int);
                    newdate = timeCalendar.getTime();
                }
            }
        }
    }

    private void addFirstSchedule(String text) {
        final TextView textView = addScheduleText(text);
        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(200);

        final Animation out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(200);

        out.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                selectedTextView.setText(firstDayString +" "+ firstDateString +"\n"+ textView.getText().toString());
                selectedTextView.startAnimation(in);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedTextView.setText("");
                Toast.makeText(context,textView.getText().toString(),Toast.LENGTH_SHORT).show();
                selectedTextView.startAnimation(out);
                //selectedTextView.setText();
                //String s = "";
                //s = selectedTextView.getText().toString();
                //if(!s.equals("")){
                    nextTextView.setVisibility(View.VISIBLE);
                //}

                selectedDateString = firstDateString;
                selectedTimeString = textView.getText().toString();
                selectedDayString = firstDayString;
            }
        });
        firstLayout.addView(textView);
    }

    private void addSecondSchedule(String text) {
        final TextView textView = addScheduleText(text);
        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(200);

        final Animation out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(200);

        out.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                selectedTextView.setText(secondDayString +" "+ secondDateString +"\n"+ textView.getText().toString());
                selectedTextView.startAnimation(in);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedTextView.setText("");
                Toast.makeText(context,textView.getText().toString(),Toast.LENGTH_SHORT).show();
                selectedTextView.startAnimation(out);
                //selectedTextView.setText(secondDayString + " " + secondDateString + " " + textView.getText().toString());
                //String s = "";
                //s = selectedTextView.getText().toString();
                //if(!s.equals("")){
                    nextTextView.setVisibility(View.VISIBLE);
                //}
                selectedDateString = secondDateString;
                selectedTimeString = textView.getText().toString();
                selectedDayString = secondDayString;
            }
        });
        secondLayout.addView(textView);
    }

    private void addThirdSchedule(String text) {
        final TextView textView = addScheduleText(text);
        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(200);

        final Animation out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(200);

        out.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                selectedTextView.setText(thirdDayString +" "+ thirdDateString +"\n"+ textView.getText().toString());
                selectedTextView.startAnimation(in);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedTextView.setText("");
                Toast.makeText(context,textView.getText().toString(),Toast.LENGTH_SHORT).show();
                //selectedTextView.setText(thirdDayString + " " + thirdDateString + " " + textView.getText().toString());
                selectedTextView.startAnimation(out);
                //String s = "";
                //s = selectedTextView.getText().toString();
                //if(!s.equals("")){
                    nextTextView.setVisibility(View.VISIBLE);
                //}
                selectedDateString = thirdDateString;
                selectedTimeString = textView.getText().toString();
                selectedDayString = thirdDayString;
            }
        });
        thirdLayout.addView(textView);
    }

    private TextView addScheduleText(String text) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0,0,0,5);
        TextView textView = new TextView(this);
        textView.setLayoutParams(params);
        textView.setGravity(Gravity.CENTER);
        textView.setText(text);
        textView.setTextSize(15.0f);
        textView.setPadding(2, 10, 2, 10);
        textView.setTextColor(Color.parseColor("#000000"));
        textView.setBackgroundColor(Color.parseColor("#e0e0e0"));
        return textView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
