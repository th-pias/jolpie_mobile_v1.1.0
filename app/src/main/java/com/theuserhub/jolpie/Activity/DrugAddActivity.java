package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class DrugAddActivity extends AppCompatActivity {

    private String TAG = "GrugAddActivity";
    private Context context = this;
    private String tag_string_req = "string_req";

    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString, fileName_packetImg,fileName_drugImg;
    private int imageGetFlag = 0, ImageUpFlag = 0;

    private ImageView getpacket_img,getdrug_img;
    private EditText getgenericName,getbrandName,getdescription,getpacket_title,getpacket_des,getoverview,getside_effect,getdosage;
    private TextView addNewDrug;
    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName,appUserEmail,appUserId,appUserGroupId,appUserImgUrl;
    private String genericName,brandName,description,packet_img_encoded,packet_title,packet_des,drug_img_encoded,overview,side_effect,dosage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drug_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Add New Drug");

        packet_img_encoded = "";
        drug_img_encoded = "";

        userDatabaseHandler = new UserDatabaseHandler(DrugAddActivity.this);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl=user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        getgenericName = (EditText) findViewById(R.id.drug_generic_name_et);
        getbrandName = (EditText) findViewById(R.id.drug_brand_name_et);
        getdescription = (EditText) findViewById(R.id.drug_description_et);

        getpacket_img = (ImageView) findViewById(R.id.drug_packet_iv);
        getpacket_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageGetFlag = 0;
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
            }
        });

        getpacket_title = (EditText) findViewById(R.id.drug_packettitle_tv);
        getpacket_des = (EditText) findViewById(R.id.drug_packetdes_tv);

        getdrug_img = (ImageView) findViewById(R.id.drug_drugimg_iv);
        getdrug_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageGetFlag = 1;
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
            }
        });

        getoverview = (EditText) findViewById(R.id.drug_overview_et);
        getside_effect = (EditText) findViewById(R.id.drug_sideeffect_et);
        getdosage = (EditText) findViewById(R.id.drug_dosage_et);

        addNewDrug = (TextView) findViewById(R.id.drug_addnewdrug_tv);
        addNewDrug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genericName = getgenericName.getText().toString();
                brandName = getbrandName.getText().toString();
                description = getdescription.getText().toString();
                packet_title = getpacket_title.getText().toString();
                packet_des = getpacket_des.getText().toString();
                overview = getoverview.getText().toString();
                side_effect = getside_effect.getText().toString();
                dosage = getdosage.getText().toString();

                if (packet_img_encoded.trim().length() > 0) {
                    uploadPacketImage(packet_img_encoded, fileName_packetImg);
                }
                else if (drug_img_encoded.trim().length() > 0) {
                    //Toast.makeText(context, "upload img ", Toast.LENGTH_LONG).show();
                    uploadDrugImage(drug_img_encoded, fileName_drugImg);
                }
                else {
                    String url = context.getResources().getString(R.string.MAIN_URL)+"saveDrug";
                    saveNewDrug(url);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_LOAD_IMG) {
            if (resultCode == RESULT_OK) {
                if (null != data) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    //String picturePath = cursor.getString(columnIndex);
                    imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    //license_number.setVisibility(View.GONE);
                    //licensePlate_img.setVisibility(View.VISIBLE);
                    String fileNameSegments[] = imgDecodableString.split("/");
                    if (imageGetFlag == 0) {
                        fileName_packetImg = fileNameSegments[fileNameSegments.length - 1];
                    }
                    else {
                        fileName_drugImg = fileNameSegments[fileNameSegments.length - 1];
                    }
                    // Set the Image in ImageView after decoding the String
                    Bitmap myImg = BitmapFactory.decodeFile(imgDecodableString);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // Must compress the Image to reduce image size to make upload easy
                    myImg.compress(Bitmap.CompressFormat.PNG, 50, stream);
                    byte[] byte_arr = stream.toByteArray();

                    if (imageGetFlag == 0) {
                        getpacket_img.setImageBitmap(myImg);
                        // Encode Image to String
                        packet_img_encoded = Base64.encodeToString(byte_arr, 0);
                    }
                    else {
                        getdrug_img.setImageBitmap(myImg);
                        // Encode Image to String
                        drug_img_encoded = Base64.encodeToString(byte_arr, 0);
                    }

                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(context, "User cancelled Gallery", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Sorry! Failed to load image", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void saveNewDrug(String url) {

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                Intent intent = new Intent(DrugAddActivity.this,UserDrugActivity.class);
                startActivity(intent);
                finish();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Some error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("generic_name", genericName);
                params.put("brand_name", brandName);
                params.put("description", description);
                params.put("packet_image", fileName_packetImg);
                params.put("packet_title", packet_title);
                params.put("packet_description", packet_des);
                params.put("drug_image", fileName_drugImg);
                params.put("overview", overview);
                params.put("side_effect", side_effect);
                params.put("dosage", dosage);
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void uploadPacketImage(final String encodedString, final String fileName) {

        String url = context.getResources().getString(R.string.MAIN_URL)+"uploadPostImage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                /*Intent intent=new Intent(PostActivity.this,MainActivity.class);
                startActivity(intent);
                finish();*/
                if (response.toString().trim().equals("yes")) {
                    if (drug_img_encoded.trim().length() > 0) {
                        //Toast.makeText(context, "upload img ", Toast.LENGTH_LONG).show();
                        uploadDrugImage(drug_img_encoded, fileName_drugImg);
                    }
                    else {
                        String url = context.getResources().getString(R.string.MAIN_URL)+"saveDrug";
                        saveNewDrug(url);
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Some error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                if (drug_img_encoded.trim().length() > 0) {
                    //Toast.makeText(context, "upload img ", Toast.LENGTH_LONG).show();
                    uploadDrugImage(drug_img_encoded, fileName_drugImg);
                }
                else {
                    String url = context.getResources().getString(R.string.MAIN_URL)+"saveDrug";
                    saveNewDrug(url);
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("image", encodedString);
                params.put("filename", fileName);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void uploadDrugImage(final String encodedString, final String fileName) {

        String url = context.getResources().getString(R.string.MAIN_URL)+"uploadPostImage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                /*Intent intent=new Intent(PostActivity.this,MainActivity.class);
                startActivity(intent);
                finish();*/
                if (response.toString().trim().equals("yes")) {
                }
                String url = context.getResources().getString(R.string.MAIN_URL)+"saveDrug";
                saveNewDrug(url);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Some error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                String url = context.getResources().getString(R.string.MAIN_URL)+"saveDrug";
                saveNewDrug(url);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("image", encodedString);
                params.put("filename", fileName);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
