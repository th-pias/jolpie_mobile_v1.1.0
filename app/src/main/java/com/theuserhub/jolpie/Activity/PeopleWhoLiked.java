package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.CategorizedAdapter;
import com.theuserhub.jolpie.Adapters.PeopleWhoLikedAdapter;
import com.theuserhub.jolpie.Models.CategoryItem;
import com.theuserhub.jolpie.Models.SimpleUserModel;
import com.theuserhub.jolpie.Models.SinglePostModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PeopleWhoLiked extends AppCompatActivity {

    private Context context = this;
    private String TAG = "PeopleWhoLikedActivity";
    private String tag_string_req = "string_req";

    private ListView peopleWhoListView;
    private List<SimpleUserModel> eachItems;
    private PeopleWhoLikedAdapter peopleWhoLikedAdapter;
    private String isPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_who_liked);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String peopleWhoFlag = intent.getStringExtra("peopleWhoFlag");
        String post_id = intent.getStringExtra("post_id");
        //isPost = intent.getStringExtra("isPost");
        String url;
        if (peopleWhoFlag.equals("-1")) {
            getSupportActionBar().setTitle("People who liked");
            url = context.getResources().getString(R.string.MAIN_URL)+"who_liked";
        }
        else {
            getSupportActionBar().setTitle("People who shared");
            url = context.getResources().getString(R.string.MAIN_URL)+"whoShared";
        }

        peopleWhoListView= (ListView) findViewById(R.id.peoplewho_lv);
        eachItems=new ArrayList<SimpleUserModel>();
        peopleWhoLikedAdapter = new PeopleWhoLikedAdapter(context,eachItems);
        peopleWhoListView.setAdapter(peopleWhoLikedAdapter);

        if (!AppFunctions.isNetworkConnected(context)) {
            Toast.makeText(context, "No network found \n fatched from db", Toast.LENGTH_LONG).show();
            //fatchFeedDb();
        } else {
            fatchWhoLiked(url,post_id);
        }

        peopleWhoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context,ProfileDetailNewActivity.class);
                intent.putExtra("user_id",eachItems.get(position).getId());
                intent.putExtra("user_fname",eachItems.get(position).getFirst_name());
                intent.putExtra("user_lname",eachItems.get(position).getLast_name());
                intent.putExtra("user_email",eachItems.get(position).getEmail());
                intent.putExtra("group_id",eachItems.get(position).getUser_group_id());
                intent.putExtra("user_image_url",eachItems.get(position).getPhoto());
                startActivity(intent);
            }
        });

    }


    private void fatchWhoLiked(String url, final String post_id){

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG,response.toString());

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();++i){
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String user_id = jsonObject.getString("id");
                        String first_name = jsonObject.getString("first_name");
                        String last_name = jsonObject.getString("last_name");
                        String photo = jsonObject.getString("photo");
                        String user_group_id = jsonObject.getString("user_group_id");

                        SimpleUserModel simpleUserModel = new SimpleUserModel(user_id,first_name,last_name,"",photo,user_group_id);
                        eachItems.add(simpleUserModel);
                    }
                    peopleWhoLikedAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("errshowComment", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("post_id", post_id);
                params.put("isPost", isPost);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
