package com.theuserhub.jolpie.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.CommentListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.ArticlesModel;
import com.theuserhub.jolpie.Models.CommentModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MagazineDetailActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "DetailActivity", tag_string_req = "detail_str_req";

    private EditText commentEditText;
    private ListView commentsListView;
    private List<CommentModel>eachComments;
    private CommentListAdapter commentListAdapter;
    private String magazine_id,title,description,author_id,created_at,picture,
            like_count,comment_count,share_count,category_id,author_fname,
            author_lname,author_photo,author_group_id,comment_text, isLiked;

    private TextView dateTextView,authorTextView,titleTextView,goodCountTV,
            followCountTV,answerCountTV,goodImage,goodTextView,answerImage,
            followImage, commentSendImageView;
    private HtmlTextView desHtmlTextView;
    private ImageView magazineImageView;
    private NetworkImageView magazineNImageView;
    private LinearLayout goodLinearLayout, shareLayout, goodCountLayout, followCountLayout,
            likeCommentShareLayout;

    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magazine_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Magazine");

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent magazineIntent = getIntent();
        magazine_id = magazineIntent.getStringExtra("magazine_id");
        title = magazineIntent.getStringExtra("title");
        description = magazineIntent.getStringExtra("description");
        author_id = magazineIntent.getStringExtra("author_id");
        created_at = magazineIntent.getStringExtra("created_at");
        picture = magazineIntent.getStringExtra("picture");
        like_count = magazineIntent.getStringExtra("like_count");
        comment_count = magazineIntent.getStringExtra("comment_count");
        share_count = magazineIntent.getStringExtra("share_count");
        category_id = magazineIntent.getStringExtra("category_id");
        author_fname = magazineIntent.getStringExtra("first_name");
        author_lname = magazineIntent.getStringExtra("last_name");
        author_photo = magazineIntent.getStringExtra("photo");
        author_group_id = magazineIntent.getStringExtra("user_group_id");

        //description = description.replaceAll("\n", "<br>");

        Typeface articleFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        commentEditText = (EditText) findViewById(R.id.commentedittext);
        commentSendImageView = (TextView) findViewById(R.id.commentsendimageView);
        commentSendImageView.setTypeface(articleFont);

        commentsListView = (ListView) findViewById(R.id.magazine_detail_lv);

        //header view
        final LayoutInflater header = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup detailViewGroup = (ViewGroup) header.inflate(R.layout.header_magazine_detail, null);

        magazineImageView = (ImageView) detailViewGroup.findViewById(R.id.detail_iv);
        magazineNImageView = (NetworkImageView) detailViewGroup.findViewById(R.id.detail_niv);
        titleTextView = (TextView) detailViewGroup.findViewById(R.id.detail_title_tv);
        authorTextView = (TextView) detailViewGroup.findViewById(R.id.detail_author_tv);
        dateTextView = (TextView) detailViewGroup.findViewById(R.id.detail_date_tv);
        desHtmlTextView = (HtmlTextView) detailViewGroup.findViewById(R.id.des_html_text);

        likeCommentShareLayout = (LinearLayout) detailViewGroup.findViewById(R.id.like_comment_share_layout);
        goodCountLayout = (LinearLayout) detailViewGroup.findViewById(R.id.singlepostgoodcount_linlay);
        goodCountTV = (TextView) detailViewGroup.findViewById(R.id.singlepostgoodcount);
        followCountLayout = (LinearLayout) detailViewGroup.findViewById(R.id.singlepostfollowcount_linlay);
        followCountTV = (TextView) detailViewGroup.findViewById(R.id.singlepostfollowcount);
        answerCountTV = (TextView) detailViewGroup.findViewById(R.id.singlepostanswercount);

        goodLinearLayout = (LinearLayout) detailViewGroup.findViewById(R.id.singlepostgoodlinlay);
        goodImage = (TextView) detailViewGroup.findViewById(R.id.singlepostgoodimageview);
        goodImage.setTypeface(articleFont);
        goodTextView = (TextView) detailViewGroup.findViewById(R.id.goodTextView);
        answerImage = (TextView) detailViewGroup.findViewById(R.id.singlepostanswerimageview);
        answerImage.setTypeface(articleFont);
        shareLayout = (LinearLayout) detailViewGroup.findViewById(R.id.singlepostfollowlinlay);
        followImage = (TextView) detailViewGroup.findViewById(R.id.singlepostfollowimageview);
        followImage.setTypeface(articleFont);

        commentsListView.addHeaderView(detailViewGroup);

        eachComments = new ArrayList<CommentModel>();
        commentListAdapter = new CommentListAdapter(context,eachComments);
        commentsListView.setAdapter(commentListAdapter);

        if (picture.length()>1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + picture;
            magazineNImageView.setImageUrl(meta_url, imageLoader);
        }
        else {
            magazineNImageView.setVisibility(View.GONE);
            magazineImageView.setVisibility(View.GONE);
        }
        titleTextView.setText(title);
        authorTextView.setText("By: " + author_fname + " " + author_lname);
        dateTextView.setText(created_at);
        desHtmlTextView.setHtmlFromString(description, new HtmlTextView.RemoteImageGetter());
        goodCountTV.setText(like_count);
        followCountTV.setText(share_count);
        answerCountTV.setText(comment_count);

        if (appUserGroupId.equals("4") || appUserGroupId.equals("5") || appUserGroupId.equals("6")) {
            /*if (author_group_id.equals("4") || author_group_id.equals("5") || author_group_id.equals("6")) {
            }
            else {*/
                likeCommentShareLayout.setVisibility(View.GONE);
            //}
        }

        getArticleDetail();

        commentSendImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment_text = commentEditText.getText().toString();
                if (comment_text.trim().length() <= 0) {
                    Toast.makeText(context, "Please write something", Toast.LENGTH_SHORT).show();
                } else {
                    sendCommentRequest();
                }
            }
        });

        goodCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(context, PeopleWhoLiked.class);
                intent.putExtra("peopleWhoFlag", "-1");
                intent.putExtra("isPost","0");
                intent.putExtra("post_id", magazine_id);
                startActivity(intent);*/
            }
        });

        followCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(context, PeopleWhoLiked.class);
                intent.putExtra("peopleWhoFlag", "1");
                intent.putExtra("isPost","0");
                intent.putExtra("post_id", magazine_id);
                startActivity(intent);*/
            }
        });

        goodLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLiked.equals("0")) {
                    isLiked = "1";
                    //goodLinearLayout.setBackgroundColor(Color.parseColor("#e3f2fd"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                    int numliked = Integer.parseInt(goodCountTV.getText().toString());
                    ++numliked;
                    /*feedList.get(position).setPost_likes(String.valueOf(numliked));*/
                    goodCountTV.setText(String.valueOf(numliked));

                    AppFunctions.likeUnlikeArticle(context, magazine_id, "1", appUserId);
                } else {
                    isLiked = "0";
                    //goodLinearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    int numliked = Integer.parseInt(goodCountTV.getText().toString());
                    --numliked;
                    /*feedList.get(position).setPost_likes(String.valueOf(numliked));*/
                    goodCountTV.setText(String.valueOf(numliked));

                    AppFunctions.likeUnlikeArticle(context, magazine_id, "0", appUserId);
                }
            }
        });

        shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are you sure?");
                alertDialogBuilder.setMessage("You wanted to share this article?");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        int numShared = Integer.parseInt(followCountTV.getText().toString());
                        ++numShared;
                        followCountTV.setText(String.valueOf(numShared));

                        shareArticleRequest();
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ////////////
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    private void getArticleDetail() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "articleDetail";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Detail: "+response.toString());
                try {
                    JSONArray articleInfo = new JSONArray(response);
                    for (int i=0; i<articleInfo.length();++i) {
                        JSONObject articleObject = (JSONObject) articleInfo.get(i);
                        magazine_id = articleObject.getString("magazine_id");
                        title = articleObject.getString("title");
                        description = articleObject.getString("description");
                        author_id = articleObject.getString("author_id");
                        created_at = articleObject.getString("created_at");
                        picture = articleObject.getString("picture");
                        like_count = articleObject.getString("good_count");
                        comment_count = articleObject.getString("answer_count");
                        share_count = articleObject.getString("share_count");
                        category_id = articleObject.getString("category_id");
                        author_fname = articleObject.getString("first_name");
                        author_lname = articleObject.getString("last_name");
                        author_photo = articleObject.getString("photo");
                        author_group_id = articleObject.getString("user_group_id");

                        //description = description.replaceAll("\n", "<br />");

                        if (picture.length()>1) {
                            if (imageLoader == null)
                                imageLoader = AppController.getInstance().getImageLoader();

                            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + picture;
                            magazineNImageView.setImageUrl(meta_url, imageLoader);
                        }
                        else {
                            magazineNImageView.setVisibility(View.GONE);
                            magazineImageView.setVisibility(View.GONE);
                        }

                        titleTextView.setText(title);
                        authorTextView.setText("By: " + author_fname + " " + author_lname);
                        dateTextView.setText(created_at);
                        desHtmlTextView.setHtmlFromString(description, new HtmlTextView.RemoteImageGetter());
                        goodCountTV.setText(like_count);
                        followCountTV.setText(share_count);
                        answerCountTV.setText(comment_count);
                    }
                    isLikedArticle();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("magazine_id", magazine_id);
                params.put("user_id", appUserId);
                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getAllComment() {
        String url = context.getResources().getString(R.string.MAIN_URL)+"articleAllComments";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Comments: "+response.toString());
                eachComments.clear();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String comment_id = jsonObject.getString("magazine_comment_id");
                        String comment_user_id = jsonObject.getString("user_id");
                        String comment_post_id = jsonObject.getString("magazine_id");
                        String comments = jsonObject.getString("comments");
                        String created_at = jsonObject.getString("created_at");
                        String comment_user_fname = jsonObject.getString("first_name");
                        String comment_user_lname = jsonObject.getString("last_name");
                        String comment_user_photo = jsonObject.getString("photo");
                        String comment_user_group_id = jsonObject.getString("user_group_id");

                        CommentModel commentModel = new CommentModel(comment_id,comments,comment_user_id,comment_user_fname,comment_user_lname,comment_user_photo,comment_user_group_id,created_at,comment_post_id);

                        eachComments.add(commentModel);
                    }
                    commentListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("magazine_id", magazine_id);
                Log.d(TAG, magazine_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void sendCommentRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL)+"articleSendComment";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("saveComment", response.toString());
                commentEditText.setText("");
                commentsListView.setSelection(eachComments.size() - 1);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String comment_id = jsonObject.getString("magazine_comment_id");
                        String comment_user_id = jsonObject.getString("user_id");
                        String comment_post_id = jsonObject.getString("magazine_id");
                        String comments = jsonObject.getString("comments");
                        String created_at = jsonObject.getString("created_at");

                        CommentModel commentModel = new CommentModel(comment_id,comments,appUserId,appUserFName,appUserLName,appUserImgUrl,appUserGroupId,created_at,comment_post_id);

                        eachComments.add(commentModel);

                        String answerString = answerCountTV.getText().toString();
                        int answerInt = Integer.parseInt(answerString);
                        answerCountTV.setText(String.valueOf(answerInt + 1));
                    }
                    commentListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("saveComment", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("comments", comment_text);
                params.put("user_id", appUserId);
                params.put("magazine_id", magazine_id);
                Log.d(TAG,comment_text+appUserId+magazine_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void isLikedArticle() {
        String url = context.getResources().getString(R.string.MAIN_URL)+"isLikedArticle";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "isliked :"+response.toString());
                if (response.toString().trim().equals("0")) {
                    isLiked = "0";
                    //goodLinearLayout.setBackgroundColor(Color.parseColor("#e3f2fd"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                }
                else {
                    isLiked = "1";
                    //goodLinearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                }
                getAllComment();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("article_id", magazine_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void shareArticleRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "shareArticle";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context,"Article Shared Successfully",Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("magazine_id", magazine_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
