package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.MagazineListActivityAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.ArticlesModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyArticlesActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "MyArticlesActivity", tag_string_req = "my_article_str_req";

    private ListView magazineListView;
    private List<ArticlesModel> articlesList;
    private MagazineListActivityAdapter magazineListAdapter;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserId,appUserFName,appUserLName,appUserEmail,appUserGroupId,appUserImgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_articles);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Articles");

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl=user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        magazineListView = (ListView) findViewById(R.id.my_articles_lv);
        articlesList = new ArrayList<ArticlesModel>();
        magazineListAdapter = new MagazineListActivityAdapter(context, articlesList);
        magazineListView.setAdapter(magazineListAdapter);

        getMyArticles();

        magazineListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context, MagazineDetailActivity.class);
                intent.putExtra("magazine_id", articlesList.get(position).getId());
                intent.putExtra("title", articlesList.get(position).getTitle());
                intent.putExtra("description", articlesList.get(position).getDescription());
                intent.putExtra("author_id", articlesList.get(position).getAuthor_id());
                intent.putExtra("created_at", articlesList.get(position).getCreated_at());
                intent.putExtra("picture", articlesList.get(position).getPicture());
                intent.putExtra("like_count", articlesList.get(position).getLike_count());
                intent.putExtra("comment_count", articlesList.get(position).getComment_count());
                intent.putExtra("share_count", articlesList.get(position).getShare_count());
                intent.putExtra("category_id", articlesList.get(position).getCategory_id());
                intent.putExtra("first_name", articlesList.get(position).getAuthor_fname());
                intent.putExtra("last_name", articlesList.get(position).getAuthor_lname());
                intent.putExtra("photo", articlesList.get(position).getAuthor_photo());
                intent.putExtra("user_group_id", articlesList.get(position).getAuthor_group_id());
                context.startActivity(intent);
            }
        });
    }

    private void getMyArticles() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "myArticles";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try {
                    JSONArray articleInfo = new JSONArray(response);
                    for (int i=0; i<articleInfo.length();++i) {
                        JSONObject articleObject = (JSONObject) articleInfo.get(i);
                        String magazine_id = articleObject.getString("magazine_id");
                        String title = articleObject.getString("title");
                        String description = articleObject.getString("description");
                        String author_id = articleObject.getString("author_id");
                        String created_at = articleObject.getString("created_at");
                        String picture = articleObject.getString("picture");
                        String like_count = articleObject.getString("good_count");
                        String comment_count = articleObject.getString("answer_count");
                        String share_count = articleObject.getString("share_count");
                        String category_id = articleObject.getString("category_id");

                        ArticlesModel articlesModel = new ArticlesModel(magazine_id,title,description,author_id,created_at,picture,
                                like_count,comment_count,share_count,category_id,appUserFName,appUserLName,appUserImgUrl,appUserGroupId);
                        articlesList.add(articlesModel);
                    }
                    magazineListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
