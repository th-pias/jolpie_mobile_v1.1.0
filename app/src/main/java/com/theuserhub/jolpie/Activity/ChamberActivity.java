package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.ChamberListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.ChamberExModel;
import com.theuserhub.jolpie.Models.ChamberModel;
import com.theuserhub.jolpie.Models.ScheduleModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ChamberActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "ChamberActivity", tag_string_req = "chamber_str_req";

    private ListView chamberListView;
    private ChamberListAdapter chamberAdapter;
    private List<ChamberExModel>eachChambers;
    private String user_id,name,degree,image;
    private TextView emptyTextView,docNameTextView, isSubsTextView;
    private RelativeLayout progressLayout;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chamber);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Chambers");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent chamberIntent = getIntent();
        user_id = chamberIntent.getStringExtra("user_id");
        name = chamberIntent.getStringExtra("name");
        degree = chamberIntent.getStringExtra("degree");
        image = chamberIntent.getStringExtra("image_url");

        //Log.e("info",d)

        progressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        docNameTextView = (TextView) findViewById(R.id.doctors_name);
        isSubsTextView = (TextView) findViewById(R.id.is_subs_tv);
        chamberListView = (ListView) findViewById(R.id.chamber_lv);
        eachChambers = new ArrayList<ChamberExModel>();
        chamberAdapter = new ChamberListAdapter(context,eachChambers, appUserId,appUserGroupId, user_id,name,degree,image);
        chamberListView.setAdapter(chamberAdapter);

        emptyTextView = (TextView) findViewById(R.id.no_data);
        //emptyTextView.

        isSubscribeRequest();

        docNameTextView.setText("Chambers of "+name);
        /*Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
        String todayStr  = dateFormat2.format(today);
        String tomoStr  = dateFormat2.format(tomorrow);
        String day1 = new SimpleDateFormat("EE").format(today);
        String day2 = new SimpleDateFormat("EE").format(tomorrow);

        Toast.makeText(context,day1+todayStr+"\n"+day2+tomoStr,Toast.LENGTH_SHORT).show();*/

    }

    private void isSubscribeRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getIsSubscribed";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                progressLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String isSubscribed = jsonObject.getString("isSubscribed");
                    String sub_msg = "";
                    if (isSubscribed.equalsIgnoreCase("0")) {
                        if (appUserId.equals(user_id)) {
                            isSubsTextView.setVisibility(View.VISIBLE);
                            isSubsTextView.setText("You are not a subcribed user. Please buy subscription.");
                        }
                        else {
                            isSubsTextView.setVisibility(View.GONE);
                            emptyTextView.setVisibility(View.VISIBLE);
                            emptyTextView.setText("No chamber.");
                        }
                    }
                    else {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("subscription");
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String subs_date = jsonObject1.getString("tran_date");
                        String durationString = jsonObject1.getString("duration");
                        int duration = 0;
                        try {
                            duration = Integer.parseInt(durationString);
                        }
                        catch (NumberFormatException e) {

                        }

                        Date transDate = dateFormat.parse(subs_date);
                        Calendar calendar = Calendar.getInstance();
                        Date todayDate = calendar.getTime();
                        String todayString = dateFormat.format(todayDate);

                        todayDate = dateFormat.parse(todayString);
                        Log.e("date",""+todayString +" : "+subs_date);
                        //double difference = (double)((double)todayDate.getTime() - (double)transDate.getTime())/((double)(365*24*60*60*1000));
                        long diff = todayDate.getTime() - transDate.getTime();
                        long dayDiff = TimeUnit.MILLISECONDS.toDays(diff);

                        if (dayDiff > duration) {
                            if (appUserId.equals(user_id)) {
                                isSubsTextView.setVisibility(View.VISIBLE);
                                isSubsTextView.setText("Your subscription is expired. Please renew.");
                            }
                            else {
                                isSubsTextView.setVisibility(View.GONE);
                                emptyTextView.setVisibility(View.VISIBLE);
                                emptyTextView.setText("No Chamber");
                            }
                        }
                        else {
                            getChamberRequest();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                emptyTextView.setVisibility(View.VISIBLE);
                progressLayout.setVisibility(View.GONE);
                emptyTextView.setText("Can't load data.\nTap to retry.");
                emptyTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emptyTextView.setVisibility(View.GONE);
                        getChamberRequest();
                    }
                });
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getChamberRequest() {
        progressLayout.setVisibility(View.VISIBLE);
        String url = context.getResources().getString(R.string.MAIN_URL)+"getChambers";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG,response.toString());
                progressLayout.setVisibility(View.GONE);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()>0){
                        emptyTextView.setVisibility(View.GONE);
                        chamberListView.setVisibility(View.VISIBLE);

                        for (int i=0;i<jsonArray.length();++i) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            String chamber_id,organisation_id,chamber_first_name,
                                    chamber_last_name,chamber_photo,chamber_mobile, chamber_user_group_id,
                                    chamber_street_address,chamber_address,cut_of_time,
                                    chamber_fee,each_patient_time;
                            chamber_id = jsonObject.getString("chamber_id");
                            organisation_id = jsonObject.getString("organisation_id");
                            chamber_first_name = jsonObject.getString("chamber_first_name");
                            chamber_last_name = jsonObject.getString("chamber_last_name");
                            chamber_photo = jsonObject.getString("chamber_photo");
                            chamber_mobile = jsonObject.getString("chamber_mobile");
                            chamber_user_group_id = jsonObject.getString("chamber_user_group_id");
                            chamber_street_address = jsonObject.getString("chamber_street_address");
                            chamber_address = jsonObject.getString("chamber_address");
                            cut_of_time = jsonObject.getString("cut_of_time");
                            chamber_fee = jsonObject.getString("chamber_fee");
                            each_patient_time = jsonObject.getString("each_patient_time");

                            JSONArray schedule_list = jsonObject.getJSONArray("schedule_list");
                            List<ScheduleModel>scheduleModelList = new ArrayList<ScheduleModel>();
                            for (int j=0;j<schedule_list.length();++j) {
                                JSONObject schedule = schedule_list.getJSONObject(j);
                                String schedule_id = schedule.getString("id");
                                String user_id = schedule.getString("user_id");
                                String schedule_chamber_id = schedule.getString("chamber_id");
                                String start_time = schedule.getString("start_time");
                                String end_time = schedule.getString("end_time");
                                String day = schedule.getString("day");

                                scheduleModelList.add(new ScheduleModel(schedule_id,user_id,schedule_chamber_id,start_time,end_time,day));
                            }

                            ChamberExModel chamberModel = new ChamberExModel(chamber_id,organisation_id,chamber_first_name,
                                    chamber_last_name,chamber_photo,chamber_mobile, chamber_user_group_id,
                                    chamber_street_address,chamber_address,cut_of_time,
                                    chamber_fee,each_patient_time,scheduleModelList);
                            eachChambers.add(chamberModel);
                        }
                        chamberAdapter.notifyDataSetChanged();
                    }
                    else {
                        emptyTextView.setVisibility(View.VISIBLE);
                        chamberListView.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                progressLayout.setVisibility(View.GONE);
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load data.\nTap to retry.");
                emptyTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emptyTextView.setVisibility(View.GONE);
                        getChamberRequest();
                    }
                });
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
