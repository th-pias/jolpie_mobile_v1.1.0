package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.ServicesAdapter;
import com.theuserhub.jolpie.Models.ServiceModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServicesActivity extends AppCompatActivity {
    private Context context = this;
    ListView serviceListView ;
    List<ServiceModel> serviceArrayList;
    ServicesAdapter servicesAdapter;
    String appUserId;
    TextView noDataTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Services");

        noDataTv = (TextView) findViewById(R.id.no_data);

        appUserId = getIntent().getStringExtra("user_id");

        serviceArrayList = new ArrayList<ServiceModel>();
        serviceListView = (ListView) findViewById(R.id.listView);
        servicesAdapter = new ServicesAdapter(context, serviceArrayList);
        serviceListView.setAdapter(servicesAdapter);

        getHospitalsServices();

    }

    private void getHospitalsServices() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getHospitalServices";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("serviceActivity", "serviceActivity: " + response.toString());
                if(response.length()<=2){
                    //noDataTv.setVisibility(View.VISIBLE);
                    //noDataTv.setText("No data found!!!");
                }
                else{
                    try {
                        JSONArray allCond = new JSONArray(response);
                        for (int i=0;i<allCond.length();++i) {
                            JSONObject singleCond = (JSONObject) allCond.get(i);
                            String id = singleCond.getString("id");
                            String category_id = singleCond.getString("category_id");
                            String service_id = singleCond.getString("service_id");
                            String service_price = singleCond.getString("service_price");
                            String user_id = singleCond.getString("user_id");
                            String category_name = singleCond.getString("category_name");
                            String service_name = singleCond.getString("service_name");

                            serviceArrayList.add(new ServiceModel(id, category_id,  service_id,  service_price,  user_id,  category_name,  service_name));
                        }
                        servicesAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d("serviceActivity", "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "tag_string_req");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
