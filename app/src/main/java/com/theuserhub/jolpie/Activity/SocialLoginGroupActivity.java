package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.SessionManager;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SocialLoginGroupActivity extends AppCompatActivity {
    private Context context = this;
    private String TAG = "SocialLoginGroupActivity", tag_string_req = "group_select_str_req";

    private EditText passwordEditText;
    private TextView submitTextView;
    private List<String>userType;
    private String user_id,first_name,last_name,user_group_id,user_image_url,user_email,password;

    private ProgressDialog pDialog;
    private Spinner group_sp, daySpinner, monthSpinner, yearSpinner, genderSpinner;
    private Button signup_btn;
    private String group_id, genderString, dayString, monthString, yearString,mobile_num,bmdc_num;
    private List<String> dayList, monthList, yearList, genderList;
    private EditText fname_et, lname_et, bmdc_et, mobile_et, email_et, password_et, confirmPass_et;
    private TextView genderTextView, dobTextView, wrongEmailTextView;

    private UserDatabaseHandler db;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_login_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signed up as");

        user_id = getIntent().getStringExtra("user_id");
        first_name = getIntent().getStringExtra("first_name");
        last_name = getIntent().getStringExtra("last_name");
        user_email = getIntent().getStringExtra("email_address");
        user_image_url = getIntent().getStringExtra("photo");
        user_group_id = getIntent().getStringExtra("user_group_id");

        passwordEditText = (EditText) findViewById(R.id.signup_password_et);
        submitTextView = (TextView) findViewById(R.id.submit_button);
        mobile_et = (EditText) findViewById(R.id.signup_mobile_et);
        bmdc_et = (EditText) findViewById(R.id.signup_bmdc_et);
        group_sp = (Spinner) findViewById(R.id.signup_group_spinner);
        dobTextView = (TextView) findViewById(R.id.dateOfBirth_tv);
        daySpinner = (Spinner) findViewById(R.id.day_spinner);
        monthSpinner = (Spinner) findViewById(R.id.month_spinner);
        yearSpinner = (Spinner) findViewById(R.id.year_spinner);
        genderTextView = (TextView) findViewById(R.id.gender_tv);
        genderSpinner = (Spinner) findViewById(R.id.gender_spinner);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        userType = new ArrayList<String>();
        dayList = new ArrayList<String>();
        monthList = new ArrayList<String>();
        yearList = new ArrayList<String>();
        genderList = new ArrayList<String>();

        insertData();

        ArrayAdapter<String> groupdataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, userType);
        // Drop down layout style - list view with radio button
        groupdataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        group_sp.setAdapter(groupdataAdapter);

        group_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();
                group_id = String.valueOf(position + 1);
                // Showing selected spinner item
                //Toast.makeText(context, "Selected: " + item + " : " + group_id, Toast.LENGTH_LONG).show();

                if (position == 0 || position == 1 || position == 2) {
                    bmdc_et.setVisibility(View.VISIBLE);
                    dobTextView.setText("Date of Birth :");
                    genderTextView.setVisibility(View.VISIBLE);
                    genderSpinner.setVisibility(View.VISIBLE);
                    if (position == 1) {
                        bmdc_et.setVisibility(View.VISIBLE);
                    } else {
                        bmdc_et.setVisibility(View.GONE);
                    }
                } else {
                    bmdc_et.setVisibility(View.GONE);
                    dobTextView.setText("Date of Establishment :");
                    genderTextView.setVisibility(View.GONE);
                    genderSpinner.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> genderSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderList);
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderSpinnerAdapter);
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genderString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> daySpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dayList);
        daySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daySpinner.setAdapter(daySpinnerAdapter);
        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dayString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> monthSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, monthList);
        monthSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthSpinner.setAdapter(monthSpinnerAdapter);
        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> yearSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, yearList);
        yearSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(yearSpinnerAdapter);
        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yearString = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submitTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password = passwordEditText.getText().toString();
                mobile_num = mobile_et.getText().toString();
                bmdc_num = bmdc_et.getText().toString();
                if (password.trim().length()<=0) {
                    Toast.makeText(context,"Please give a password",Toast.LENGTH_SHORT).show();
                }
                else {
                    if (password.length()<6) {
                        Toast.makeText(context,"Password must be 6 character",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        updateUserGroupId();
                    }
                }
            }
        });
    }

    private void insertData() {
        userType.add("Patient");
        userType.add("Doctor");
        userType.add("Medical Student");
        userType.add("Hospital");
        userType.add("Diagnostic Center");
        userType.add("Pharmaceutical Company");

        for (int i = 1; i <= 31; ++i) {
            dayList.add(String.valueOf(i));
        }

        monthList.add("January");
        monthList.add("February");
        monthList.add("March");
        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("September");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");

        Calendar today = Calendar.getInstance();
        int currentYear = today.get(Calendar.YEAR);
        for (int i = currentYear - 100; i <= currentYear; ++i) {
            yearList.add(String.valueOf(i));
        }

        genderList.add("Male");
        genderList.add("Female");
    }

    private void updateUserGroupId() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "updateUserGroup";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG,response);
                if (response.trim().equalsIgnoreCase("ok")) {
                    db = new UserDatabaseHandler(context);
                    // Session manager
                    session = new SessionManager(context);
                    session.setLogin(true);
                    db.addUser(first_name, last_name, user_email, user_id, user_image_url, user_group_id);

                    Intent intent = new Intent(context,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    Toast.makeText(context, "Error: try again later", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: try again later", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("user_group_id", user_group_id);
                params.put("password", password);
                params.put("contact_no_1", mobile_num);
                params.put("gender", genderString);
                params.put("birth_day", dayString);
                params.put("birth_month", monthString);
                params.put("birth_year", yearString);
                params.put("birth_year", yearString);
                params.put("registration_number", bmdc_num);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
