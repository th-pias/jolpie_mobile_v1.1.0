package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.HospitalsDoctorAdapter;
import com.theuserhub.jolpie.Models.DoctorsModel;
import com.theuserhub.jolpie.Models.SimpleUserModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HospitalsDoctorsActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "HospitalsDoctorsActivity", tag_string_req = "hos_docs_str_req";

    private TextView emptyTextView;
    private ListView doctorsListView;
    private List<DoctorsModel>eachDoctors;
    private HospitalsDoctorAdapter doctorAdapter;
    private String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospitals_doctors);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Doctors");

        user_id  = getIntent().getStringExtra("user_id");

        emptyTextView = (TextView) findViewById(R.id.empty_tv);
        doctorsListView = (ListView) findViewById(R.id.doctors_lv);
        eachDoctors = new ArrayList<DoctorsModel>();
        doctorAdapter = new HospitalsDoctorAdapter(context,eachDoctors);
        doctorsListView.setAdapter(doctorAdapter);

        getHospitalsDoctors();
    }

    private void getHospitalsDoctors(){

        String url = context.getResources().getString(R.string.MAIN_URL)+"getHospitalsDoctor";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()<=0) {
                        emptyTextView.setVisibility(View.VISIBLE);
                    }
                    else {
                        emptyTextView.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); ++i) {
                            List<SpecialityModel> tempModel = new ArrayList<SpecialityModel>();
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            String user_id = jsonObject.getString("id");
                            String first_name = jsonObject.getString("first_name");
                            String last_name = jsonObject.getString("last_name");
                            String photo = jsonObject.getString("photo");
                            String user_group_id = jsonObject.getString("user_group_id");
                            String degree = jsonObject.getString("degree");

                            JSONArray jsonArray1 = jsonObject.getJSONArray("speciality");
                            for (int j = 0; j < jsonArray1.length(); ++j) {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(j);
                                String spe_id = jsonObject1.getString("specialization_id");
                                String spe_name = jsonObject1.getString("name");
                                tempModel.add(new SpecialityModel(spe_id, spe_name));
                            }
                            eachDoctors.add(new DoctorsModel(user_id, first_name, last_name, photo, user_group_id, degree, tempModel));
                        }
                        doctorAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("errshowComment", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
