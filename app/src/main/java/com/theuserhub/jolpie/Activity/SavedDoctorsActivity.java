package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.PatientsLikeMeTabsAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Fragments.PatientsLikeMeFragment;
import com.theuserhub.jolpie.Fragments.SavedDoctorsFragment;
import com.theuserhub.jolpie.Fragments.UpdatesPatientsLikeMeFragment;
import com.theuserhub.jolpie.Fragments.UpdatesSavedDoctorsFragment;
import com.theuserhub.jolpie.Models.ArticlesModel;
import com.theuserhub.jolpie.Models.MyConditionsModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SavedDoctorsActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "SavedDoctorsActivity", tag_string_req = "sav_doc_str_req";

    private Spinner specialitySpinner;
    private List<String> specialitySpinnerList;
    private List<SpecialityModel>specialityList;
    private ArrayAdapter<String> specialitySpinnerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressDialog pDialog;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_doctors);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Saved Doctors");

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorHeight(8);

        specialitySpinner = (Spinner) findViewById(R.id.saved_specility_sp);
        specialitySpinnerList = new ArrayList<String>();
        specialityList = new ArrayList<SpecialityModel>();
        specialitySpinnerList.add("Specialities...");
        specialitySpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, specialitySpinnerList);
        specialitySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        specialitySpinner.setAdapter(specialitySpinnerAdapter);

        //getSavedDoctors();
        savedDoctorsSpeciality();

        specialitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(context, "Sugestion : " + parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                //spinner_text = parent.getItemAtPosition(position).toString();

                Bundle bundle = new Bundle();
                if (position == 0) {
                    //Log.d("Spinner",""+position);
                    bundle.putString("isAll", "0");
                    bundle.putString("speciality_id", "0");
                } else {
                    //Log.d("Spinner",""+position);
                    bundle.putString("isAll", "1");
                    bundle.putString("speciality_id", specialityList.get(position - 1).getId());
                }
                Fragment updatesFragment = new UpdatesSavedDoctorsFragment();
                Fragment savedFragment = new SavedDoctorsFragment();
                //Log.d("SpinnerisAll",""+bundle.getString("isAll"));
                updatesFragment.setArguments(bundle);
                savedFragment.setArguments(bundle);

                setupViewPager(viewPager, updatesFragment, savedFragment);

                tabLayout.setupWithViewPager(viewPager);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager, Fragment updatesFragment, Fragment savedFragment) {
        PatientsLikeMeTabsAdapter adapter = new PatientsLikeMeTabsAdapter(getSupportFragmentManager());
        adapter.addFragment(savedFragment, "DOCTORS");
        adapter.addFragment(updatesFragment, "UPDATES");
        viewPager.setAdapter(adapter);
    }

    private void savedDoctorsSpeciality() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "savedDoctorsSpeciality";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Speciality: "+response.toString());
                pDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String sp_id = jsonObject.getString("specialization_id");
                        String sp_name = jsonObject.getString("name");

                        SpecialityModel specialityModel = new SpecialityModel(sp_id,sp_name);
                        specialitySpinnerList.add(sp_name);
                        specialityList.add(specialityModel);
                    }
                    specialitySpinnerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
