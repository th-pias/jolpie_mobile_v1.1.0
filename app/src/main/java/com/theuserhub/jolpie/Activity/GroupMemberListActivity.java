package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.GroupMemberListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.GroupMemberModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupMemberListActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "GroupMemberListActivity";
    private String tag_string_req = "grp_membr_str_req";

    private String group_id,group_name,created_by;
    private ListView membrListView;
    private List<GroupMemberModel> eachItems;
    private GroupMemberListAdapter memberListAdapter;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserId,appUserFName,appUserLName,appUserEmail,appUserGroupId,appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Members");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl=user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent groupMembersIntent = getIntent();
        group_id = groupMembersIntent.getStringExtra("group_id");
        group_name = groupMembersIntent.getStringExtra("group_name");
        created_by = groupMembersIntent.getStringExtra("created_by");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

        membrListView = (ListView) findViewById(R.id.grp_membr_lv);
        eachItems = new ArrayList<GroupMemberModel>();
        memberListAdapter = new GroupMemberListAdapter(context,eachItems,appUserId,created_by,group_id);
        membrListView.setAdapter(memberListAdapter);

        getGroupMembersRequest();


    }

    private void getGroupMembersRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getGroupMembers";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try {
                    JSONArray allMembers = new JSONArray(response);
                    for (int i=0;i<allMembers.length();++i) {
                        JSONObject singleMember = (JSONObject) allMembers.get(i);
                        String membr_id = singleMember.getString("id");
                        String membr_user_id = singleMember.getString("user_id");
                        String membr_fname = singleMember.getString("first_name");
                        String membr_lname = singleMember.getString("last_name");
                        String membr_image = singleMember.getString("photo");
                        String membr_group_id = singleMember.getString("user_group_id");

                        GroupMemberModel memberModel = new GroupMemberModel(membr_id,membr_user_id,membr_fname,membr_lname,membr_image,membr_group_id);
                        eachItems.add(memberModel);
                    }

                    memberListAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("group_id", group_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
