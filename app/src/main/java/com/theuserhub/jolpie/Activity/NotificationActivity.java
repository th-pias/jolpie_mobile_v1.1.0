package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.NotificationAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.NotificationModel;
import com.theuserhub.jolpie.Models.ReviewModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity {
    private Context context = this;
    private String TAG = "NotificationActivity", tag_string_req = "notify_str_req";

    private ProgressDialog pDialog;
    private ListView notifyListView;
    private NotificationAdapter notificationAdapter;
    private List<NotificationModel> eachNotify;
    private TextView emptyTextView;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notifications");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);

        emptyTextView = (TextView) findViewById(R.id.empty_tv);
        notifyListView = (ListView) findViewById(R.id.notify_lv);
        eachNotify = new ArrayList<NotificationModel>();
        notificationAdapter = new NotificationAdapter(context, eachNotify, appUserId, appUserFName+ " "+appUserLName);
        notifyListView.setAdapter(notificationAdapter);

        getNotifyRequest();

        notifyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (eachNotify.get(position).getSection().equalsIgnoreCase("post") || eachNotify.get(position).getSection().equalsIgnoreCase("group")) {
                    Intent intent = new Intent(context,PostDetailActivity.class);
                    intent.putExtra("id", eachNotify.get(position).getSection_id());
                    intent.putExtra("title", "");
                    intent.putExtra("description", "");
                    intent.putExtra("feeling", "");
                    intent.putExtra("photo", "");
                    intent.putExtra("video", "");
                    intent.putExtra("created_at", "");
                    intent.putExtra("updated_at", "");
                    intent.putExtra("user_id", "");
                    intent.putExtra("user_fname", "");
                    intent.putExtra("user_lname", "");
                    intent.putExtra("user_photo", "");
                    intent.putExtra("user_group_id", "");
                    intent.putExtra("good_count", "0");
                    intent.putExtra("answer_count", "0");
                    intent.putExtra("share_count", "0");
                    intent.putExtra("liked_by_me", "0");
                    intent.putExtra("shared_by_me", "0");
                    startActivity(intent);
                }
                else if (eachNotify.get(position).getSection().equalsIgnoreCase("magazine")) {
                    Intent intent = new Intent(context,MagazineDetailActivity.class);
                    intent.putExtra("magazine_id",eachNotify.get(position).getSection_id());
                    intent.putExtra("title","");
                    intent.putExtra("description","");
                    intent.putExtra("author_id","");
                    intent.putExtra("created_at","");
                    intent.putExtra("picture","");
                    intent.putExtra("like_count","0");
                    intent.putExtra("comment_count","0");
                    intent.putExtra("share_count","0");
                    intent.putExtra("category_id","");
                    intent.putExtra("first_name","");
                    intent.putExtra("last_name","");
                    intent.putExtra("photo","");
                    intent.putExtra("user_group_id","");
                    startActivity(intent);
                }
            }
        });
    }

    private void getNotifyRequest() {
        pDialog.setMessage("Gathering Information");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "getNotifications";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Notify: " + response.toString());
                pDialog.dismiss();

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()<=0) {
                        emptyTextView.setVisibility(View.VISIBLE);
                        emptyTextView.setText("You have no notification");
                        notifyListView.setVisibility(View.GONE);
                    }
                    else {
                        emptyTextView.setVisibility(View.GONE);
                        notifyListView.setVisibility(View.VISIBLE);

                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject singleReview = (JSONObject) jsonArray.get(i);
                            String id, section, section_id, activity, activity_by_id, activity_by_fname, activity_by_lname, activity_by_photo, activity_by_group_id, seen, created_at;
                            id = singleReview.getString("id");
                            section = singleReview.getString("section");
                            section_id = singleReview.getString("section_id");
                            activity = singleReview.getString("activity");
                            activity_by_id = singleReview.getString("user_id");
                            activity_by_fname = singleReview.getString("first_name");
                            activity_by_lname = singleReview.getString("last_name");
                            activity_by_photo = singleReview.getString("photo");
                            activity_by_group_id = singleReview.getString("user_group_id");
                            seen = singleReview.getString("seen");
                            created_at = singleReview.getString("created_at");

                            if (!section.equals("review")) {
                                NotificationModel notificationModel = new NotificationModel(id, section, section_id, activity, activity_by_id, activity_by_fname, activity_by_lname, activity_by_photo, activity_by_group_id, seen, created_at);
                                eachNotify.add(notificationModel);
                            }
                        }
                        notificationAdapter.notifyDataSetChanged();
                        makeSeen();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load notification.\n Try again.");
                notifyListView.setVisibility(View.GONE);
                emptyTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getNotifyRequest();
                    }
                });
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void makeSeen() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "makeNotificationSeen";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG,response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
