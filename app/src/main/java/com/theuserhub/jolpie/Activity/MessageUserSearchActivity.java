package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.facebook.Profile;
import com.theuserhub.jolpie.Adapters.SearchAutoCompleteAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.AutoCompleteModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MessageUserSearchActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "MsgUsrSearchActivity", tag_string_req = "msguser_string_req", tag_json_arry = "msguser_jarray_req";

    private String search_item, search_type = "0";
    private ListView msgUserListView;
    private AutoCompleteTextView msgUserAutoTextView;
    private ArrayAdapter<AutoCompleteModel> msgAutoCompleteAdapter;
    private AutoCompleteModel[] objectItemData;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_user_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        search_type = appUserGroupId;

        msgUserListView = (ListView) findViewById(R.id.msguser_search_lv);
        msgUserAutoTextView = (AutoCompleteTextView) findViewById(R.id.msguser_search_autoTextView);
        AutoCompleteModel[] data = new AutoCompleteModel[0];
        msgAutoCompleteAdapter = new SearchAutoCompleteAdapter(this, R.layout.list_row_autocomplete, data);
        msgUserListView.setAdapter(msgAutoCompleteAdapter);
        //msgUserAutoTextView.setAdapter(msgAutoCompleteAdapter);

        msgUserAutoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (msgUserAutoTextView.getText().toString().trim().length() > 0) {
                    search_item = msgUserAutoTextView.getText().toString();
                    Toast.makeText(context, search_item, Toast.LENGTH_SHORT).show();
                    searchItems();
                }
            }
        });

        msgUserAutoTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = String.valueOf(s);
                search_item = temp;
                //Toast.makeText(context,search_item,Toast.LENGTH_SHORT).show();
                if (search_item.trim().length() > 0) {
                    searchItems();
                }
            }
        });

        msgUserListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MessageUserSearchActivity.this,ConversationActivity.class);
                intent.putExtra("to_user_id",objectItemData[position].getAutoID());
                intent.putExtra("first_name",objectItemData[position].getAutoTitle());
                intent.putExtra("last_name","");
                intent.putExtra("photo",objectItemData[position].getAutoImage());
                intent.putExtra("user_group_id",objectItemData[position].getAutoGroupId());
                startActivity(intent);
            }
        });

    }

    private void searchItems() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "searchUsers";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try {
                    JSONArray allPosts = new JSONArray(response);
                    objectItemData = new AutoCompleteModel[allPosts.length()];
                    //Log.d(TAG, allPosts.toString());
                    //int k=0;
                    for (int i = 0; i < allPosts.length(); ++i) {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        String item_id, item_title, item_count, item_type, item_profile, item_photo, item_group_id;

                        item_id = singlePost.getString("id");
                        item_title = singlePost.getString("first_name") + " " + singlePost.getString("last_name");
                        item_count = "";//singlePost.getString("counts");
                        item_type = "";//singlePost.getString("specialization_name");
                        item_profile = "";//singlePost.getString("profile");
                        item_photo = singlePost.getString("photo");
                        item_group_id = singlePost.getString("user_group_id");
                        //Toast.makeText(context,item_id+item_title+item_title+item_count,Toast.LENGTH_SHORT).show();
                        /*if (item_profile.equals("0")) {
                            item_type = "Speciality";
                            if (item_count.equals("0")) {
                                continue;
                            }
                        } else {
                            //item_type = item_type;
                        }*/


                        //Toast.makeText(context,item_title,Toast.LENGTH_SHORT).show();
                        //if (item_group_id.equals(appUserGroupId)) {
                            AutoCompleteModel autoItem = new AutoCompleteModel(item_id, item_title, item_type, item_count, item_photo, item_group_id, item_profile);
                            objectItemData[i] = autoItem;
                            //++k;
                        //}
                        //mList.add(autoItem);
                        //handler.addContact(pst);
                    }
                    //refresh adapter
                    msgAutoCompleteAdapter.notifyDataSetChanged();
                    msgAutoCompleteAdapter = new SearchAutoCompleteAdapter(context, R.layout.list_row_autocomplete, objectItemData);
                    //msgUserAutoTextView.setAdapter(msgAutoCompleteAdapter);
                    msgUserListView.setAdapter(msgAutoCompleteAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //fatchFeedDb();
                Toast.makeText(context, "Doc error occured " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("search_item", search_item);
                params.put("search_type", search_type);
                Log.d("search_type", search_type);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req + TAG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
