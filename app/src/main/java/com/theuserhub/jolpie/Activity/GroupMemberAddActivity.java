package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.GroupMemberAddAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.GroupMemberModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupMemberAddActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "GroupMemberAddActivity", tag_string_req = "grp_memebr_add_str_req";

    private ListView grpMembrAddListView;
    private AutoCompleteTextView membrAddAutoTextView;
    private ArrayAdapter<GroupMemberModel> autoCompleteAdapter;
    private GroupMemberModel[] objectItemData;
    private List<GroupMemberModel> eachItems;
    private GroupMemberAddAdapter memberAddAdapter;

    private String group_id,group_name,search_item;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName,appUserLName,appUserEmail,appUserId,appUserGroupId,appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Members");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl=user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent groupDetailIntent = getIntent();
        group_id = groupDetailIntent.getStringExtra("group_id");
        group_name = groupDetailIntent.getStringExtra("group_name");

        search_item = "";

        membrAddAutoTextView = (AutoCompleteTextView) findViewById(R.id.group_member_add_autoComplete);
        grpMembrAddListView = (ListView) findViewById(R.id.grp_membr_add_lv);

        eachItems = new ArrayList<GroupMemberModel>();
        /*GroupMemberModel[] data = new GroupMemberModel[0];
        autoCompleteAdapter = new GroupMemberAddListAdapter(this, R.layout.list_row_grp_member_add_list, data);
        grpMembrAddListView.setAdapter(autoCompleteAdapter);*/
        memberAddAdapter = new GroupMemberAddAdapter(context,eachItems,group_id);
        grpMembrAddListView.setAdapter(memberAddAdapter);

        getUsersRequest();

        membrAddAutoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = membrAddAutoTextView.getText().toString();
                if (temp.trim().length()>0)
                {
                    search_item = temp;
                    getUsersRequest();
                }
            }
        });

        membrAddAutoTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = String.valueOf(s);
                if (temp.trim().length()>0)
                {
                    search_item = temp;
                    getUsersRequest();
                }
            }
        });
    }

    private void getUsersRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "searchGroupMembers";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                eachItems.clear();
                try {
                    JSONArray allUsers = new JSONArray(response);
                    objectItemData = new GroupMemberModel[allUsers.length()];
                    for (int i=0;i<allUsers.length();++i) {
                        JSONObject singleUser = (JSONObject) allUsers.get(i);
                        String user_id = singleUser.getString("id");
                        String user_fname = singleUser.getString("first_name");
                        String user_lname = singleUser.getString("last_name");
                        String user_image = singleUser.getString("photo");
                        String user_group_id = singleUser.getString("user_group_id");

                        GroupMemberModel autoItem = new GroupMemberModel(user_id, user_fname, user_lname, user_image, user_group_id);
                        //objectItemData[i] = autoItem;
                        eachItems.add(autoItem);
                    }

                    //autoCompleteAdapter.notifyDataSetChanged();
                    //autoCompleteAdapter = new GroupMemberAddListAdapter(context, R.layout.list_row_grp_member_add_list, objectItemData);
                    //grpMembrAddListView.setAdapter(autoCompleteAdapter);

                    memberAddAdapter = new GroupMemberAddAdapter(context,eachItems,group_id);
                    grpMembrAddListView.setAdapter(memberAddAdapter);
                    memberAddAdapter.notifyDataSetChanged();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("group_id", group_id);
                params.put("user_id", appUserId);
                params.put("search_item", search_item);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
