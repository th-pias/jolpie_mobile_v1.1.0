package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.SubscriptionLogAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.SubscriptionLogModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BuySubscriptionActivity extends AppCompatActivity {
    private Context context = this;
    private String TAG = "BuySubscriptionActivity",tag_string_req = "buy_sub_str_req";

    private TextView buySubsTextView;
    private ListView subsLogListView;
    private SubscriptionLogAdapter logAdapter;
    private List<SubscriptionLogModel> eachSubscript;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_subscription);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Buy Subscription");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        buySubsTextView = (TextView) findViewById(R.id.buy_subs_tv);
        subsLogListView = (ListView) findViewById(R.id.subs_log_tv);

        eachSubscript = new ArrayList<>();
        logAdapter = new SubscriptionLogAdapter(context,eachSubscript,buySubsTextView);
        subsLogListView.setAdapter(logAdapter);

        getSubscriptionRequest();

        buySubsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getResources().getString(R.string.BUY_SUBSCRIPTION_URL)));
                startActivity(browserIntent);
            }
        });
    }

    private void getSubscriptionRequest(){
        String url = context.getResources().getString(R.string.MAIN_URL) + "getSubscription";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String isSubscribed = jsonObject.getString("isSubscribed");
                    if (isSubscribed.equalsIgnoreCase("0")) {
                        buySubsTextView.setVisibility(View.VISIBLE);
                        subsLogListView.setVisibility(View.GONE);
                    }
                    else {
                        buySubsTextView.setVisibility(View.GONE);
                        subsLogListView.setVisibility(View.VISIBLE);

                        eachSubscript.clear();
                        JSONArray jsonArray = jsonObject.getJSONArray("subscription");
                        for (int i = 0; i<jsonArray.length(); ++i) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String confirm_id = jsonObject1.getString("confirm_id");
                            String user_id = jsonObject1.getString("user_id");
                            String tran_id = jsonObject1.getString("tran_id");
                            String amount = jsonObject1.getString("amount");
                            String val_id = jsonObject1.getString("val_id");
                            String card_type = jsonObject1.getString("card_type");
                            String bank_tran_id = jsonObject1.getString("bank_tran_id");
                            String tran_date = jsonObject1.getString("tran_date");
                            String card_issuer = jsonObject1.getString("card_issuer");
                            String card_brand = jsonObject1.getString("card_brand");
                            String card_issuer_country = jsonObject1.getString("card_issuer_country");
                            String store_id = jsonObject1.getString("store_id");
                            String verify_sign = jsonObject1.getString("verify_sign");
                            String duration = jsonObject1.getString("duration");

                            SubscriptionLogModel subscriptionLogModel = new SubscriptionLogModel(confirm_id,user_id,tran_id,amount,val_id,card_type,bank_tran_id,tran_date,card_issuer,card_brand,card_issuer_country,store_id,verify_sign,duration);
                            eachSubscript.add(subscriptionLogModel);
                        }
                        logAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
