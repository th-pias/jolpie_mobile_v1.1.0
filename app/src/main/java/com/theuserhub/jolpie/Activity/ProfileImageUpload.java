package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.isseiaoki.simplecropview.CropImageView;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tumpa on 2/28/2016.
 */
public class ProfileImageUpload extends AppCompatActivity {
    private Context context = this;
    private String TAG = "PostActivity",tag_string_req = "upload_img_req";

    CropImageView cropImageView;
    ImageView croppedImageView;
    Button cropButton;
    TextView uploadImageTextView;
    Bitmap uploadImageBitmap;
    Bitmap bitmap;
    String path,extension,encodedString = "",isProfilePic;
    private ProgressDialog prgDialog;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_image_upload);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        path = getIntent().getStringExtra("path");
        isProfilePic = getIntent().getStringExtra("flag");

        if (isProfilePic.equals("0")) {
            getSupportActionBar().setTitle("Change Banner Image");
        }
        else {
            getSupportActionBar().setTitle("Change Profile Picture");
        }
        extension = path.substring(path.lastIndexOf(".") + 1);
        Log.e("path2",path);
        Uri filePath = Uri.fromFile(new File(path));

        encodedString = "";

        uploadImageTextView = (TextView) findViewById(R.id.upload_image_tv);

        prgDialog = new ProgressDialog(this);
        prgDialog.setCancelable(false);

        cropImageView = (CropImageView)findViewById(R.id.cropImageView);
        croppedImageView = (ImageView)findViewById(R.id.croppedImageView);
        cropImageView.setHandleShowMode(CropImageView.ShowMode.SHOW_ALWAYS);
        cropImageView.setMinFrameSizeInDp(150);
        if (isProfilePic.equals("0")) {
            cropImageView.setCropMode(CropImageView.CropMode.RATIO_16_9);
        }else {
            cropImageView.setCropMode(CropImageView.CropMode.RATIO_1_1);
        }


        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
            cropImageView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

        cropButton = (Button)findViewById(R.id.crop_button);
        cropButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                croppedImageView.setImageBitmap(cropImageView.getCroppedBitmap());
                uploadImageTextView.setVisibility(View.VISIBLE);
                uploadImageBitmap = cropImageView.getCroppedBitmap();

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                try {
                    if (extension.trim().toLowerCase().equals("png"))
                        uploadImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                    else if (extension.trim().toLowerCase().equals("jpeg") || extension.trim().toLowerCase().equals("jpg"))
                        uploadImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                } catch (Exception e) {
                    Toast.makeText(context, "Something error occured on image loading", Toast.LENGTH_SHORT).show();
                }

                byte[] byte_arr = out.toByteArray();
                // Encode Image to String
                encodedString = Base64.encodeToString(byte_arr, 0);
            }
        });


        uploadImageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ImageString",encodedString);
                uploadImageRequest();
            }
        });
    }

    private void uploadImageRequest() {
        prgDialog.setMessage("Uploading...");
        prgDialog.show();
        String url = "";
        if (isProfilePic.equals("0")) {
            url = context.getResources().getString(R.string.MAIN_URL) + "uploadBannerImage";
        }
        else {
            url = context.getResources().getString(R.string.MAIN_URL) + "uploadPostImage";
        }


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                prgDialog.dismiss();
                //if (response.toString().equals("yes")) {
                    Toast.makeText(context, "Uploaded successfully", Toast.LENGTH_LONG).show();
                    //newPostRequest();
                //} else {
                    //Toast.makeText(context, "Error: Try again later", Toast.LENGTH_LONG).show();
                //}
                finish();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: Try again later", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("image", encodedString);
                params.put("extension",extension);
                params.put("flag",isProfilePic);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
