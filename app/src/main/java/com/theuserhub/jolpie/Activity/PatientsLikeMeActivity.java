package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.PatientsLikeMeTabsAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Fragments.PatientsLikeMeFragment;
import com.theuserhub.jolpie.Fragments.UpdatesPatientsLikeMeFragment;
import com.theuserhub.jolpie.Models.MyConditionsModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PatientsLikeMeActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "PatientsLikeMeActivity", tag_string_req = "patients_str_req";

    private Spinner myConditionSpinner;
    private List<String>myConditionsSpinnerList;
    private List<MyConditionsModel>myConditionsList;
    private ArrayAdapter<String> myConditionsSpinnerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressDialog pDialog;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients_like_me);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Patients Like Me");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorHeight(8);
        //tabLayout.set

        myConditionSpinner = (Spinner) findViewById(R.id.my_conditions_sp);
        myConditionsSpinnerList = new ArrayList<String>();
        myConditionsList = new ArrayList<MyConditionsModel>();
        myConditionsSpinnerList.add("My Conditions...");
        myConditionsSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, myConditionsSpinnerList);
        myConditionsSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        myConditionSpinner.setAdapter(myConditionsSpinnerAdapter);

        myConditionSrequest();

        myConditionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(context, "Sugestion : " + parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                //spinner_text = parent.getItemAtPosition(position).toString();

                Bundle bundle = new Bundle();
                if (position == 0) {
                    //Log.d("Spinner",""+position);
                    bundle.putString("isAll","0");
                    bundle.putString("condition_id","0");
                }
                else {
                    //Log.d("Spinner",""+position);
                    bundle.putString("isAll", "1");
                    bundle.putString("condition_id",myConditionsList.get(position-1).getConditionId());
                }
                Fragment updatesFragment = new UpdatesPatientsLikeMeFragment();
                Fragment patientsFragment = new PatientsLikeMeFragment();
                //Log.d("SpinnerisAll",""+bundle.getString("isAll"));
                updatesFragment.setArguments(bundle);
                patientsFragment.setArguments(bundle);

                setupViewPager(viewPager, updatesFragment, patientsFragment);

                tabLayout.setupWithViewPager(viewPager);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setupViewPager(ViewPager viewPager, Fragment updatesFragment, Fragment patientsFragment) {
        PatientsLikeMeTabsAdapter adapter = new PatientsLikeMeTabsAdapter(getSupportFragmentManager());
        adapter.addFragment(patientsFragment, "PATIENTS");
        adapter.addFragment(updatesFragment, "UPDATES");
        viewPager.setAdapter(adapter);
        //viewPager.setCurrentItem();
    }

    private void myConditionSrequest() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "myConditions";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "mycondition: " + response.toString());
                pDialog.dismiss();
                try {
                    JSONArray allCond = new JSONArray(response);
                    for (int i=0;i<allCond.length();++i) {
                        JSONObject singleCond = (JSONObject) allCond.get(i);
                        String myCond_id = singleCond.getString("my_condition_id");
                        String cond_id = singleCond.getString("condition_id");
                        String condName_en = singleCond.getString("condition_name");
                        String condName_bn = singleCond.getString("bangla_condition_name");
                        myConditionsSpinnerList.add(condName_en +"("+condName_bn+")");
                        myConditionsList.add(new MyConditionsModel(myCond_id,cond_id,condName_en,condName_bn));
                    }
                    myConditionsSpinnerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getPatientsLikeMe() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "patientsLikeMe";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "patientsLikeMe: " + response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getConditionPatients() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "conditionPatients";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "patientsLikeMe: " + response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("condition_id", "2");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
