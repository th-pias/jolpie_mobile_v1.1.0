package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.ReviewListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Fragments.ReviewDialogFragment;
import com.theuserhub.jolpie.Models.ReviewModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReviewListActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "ReviewListActivity", tag_string_req = "rev_list_str_req";

    private String reviewer_id;

    private ListView reviewsListView;
    private List<ReviewModel> eachItems;
    private ReviewListAdapter reviewListAdapter;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Reviews");

        Intent reviewIntent = getIntent();
        reviewer_id = reviewIntent.getStringExtra("reviewer_id");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        fab = (FloatingActionButton) findViewById(R.id.fab);
        if (appUserId.equals(reviewer_id)) {
            fab.setVisibility(View.GONE);
        } else {
            isReviewedReq(appUserId, reviewer_id);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("flag", "revlist");
                bundle.putString("user_id", appUserId);
                bundle.putString("reviewer_id", reviewer_id);
                ReviewDialogFragment reviewDialog = new ReviewDialogFragment();
                reviewDialog.setArguments(bundle);
                reviewDialog.show(getSupportFragmentManager(), "My Review");
            }
        });

        reviewsListView = (ListView) findViewById(R.id.review_list_lv);
        eachItems = new ArrayList<ReviewModel>();
        reviewListAdapter = new ReviewListAdapter(context, eachItems, appUserId, getSupportFragmentManager());
        reviewsListView.setAdapter(reviewListAdapter);

        getAllReviews();

    }

    public void setUpdateDataReviewList(){
        getAllReviews();
        /*int pos = Integer.parseInt(position);
        Log.d("pos",""+pos);
        eachItems.get(pos).setReview_title(review_title);
        eachItems.get(pos).setReview_description(review_description);
        eachItems.get(pos).setReview_star_count(review_star);
        reviewListAdapter.notifyDataSetChanged();*/
        //reviewListAdapter = new ReviewListAdapter(context, eachItems, appUserId, getSupportFragmentManager());
        //reviewsListView.setAdapter(reviewListAdapter);
    }

    public  void enableReviewListFAB() {
        fab.setVisibility(View.VISIBLE);
    }
    public  void disableReviewListFAB() {
        fab.setVisibility(View.GONE);
    }

    private void getAllReviews() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "userReviews";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                eachItems.clear();
                try {
                    JSONArray allReview = new JSONArray(response);
                    for (int i = 0; i < allReview.length(); ++i) {
                        JSONObject singleReview = (JSONObject) allReview.get(i);
                        String review_id, review_by, review_title, review_description, review_created_at, review_star_count, review_fname, review_lname, review_by_photo, review_group_id;
                        review_id = singleReview.getString("review_id");
                        review_by = singleReview.getString("user_id");
                        review_title = singleReview.getString("review_title");
                        review_description = singleReview.getString("review_description");
                        review_created_at = singleReview.getString("created_at");
                        review_star_count = singleReview.getString("star_count");
                        review_fname = singleReview.getString("first_name");
                        review_lname = singleReview.getString("last_name");
                        review_by_photo = singleReview.getString("photo");
                        review_group_id = singleReview.getString("user_group_id");

                        ReviewModel reviewModel = new ReviewModel(review_id, review_by, review_title, review_description, review_created_at, review_star_count, review_fname, review_lname, review_by_photo, review_group_id);
                        if (appUserId.equals(review_by)) {
                            eachItems.add(0, reviewModel);
                        } else {
                            eachItems.add(reviewModel);
                        }
                    }
                    reviewListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("reviewer_id", reviewer_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void isReviewedReq(final String user_id, final String reviewer_id) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "isReviewed";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("isReviewed", response.toString());
                if (response.toString().trim().equals("yes")) {
                    fab.setVisibility(View.GONE);
                } else {
                    fab.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d("isReviewed", "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("reviewer_id", reviewer_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        isReviewedReq(appUserId,reviewer_id);
    }*/
}
