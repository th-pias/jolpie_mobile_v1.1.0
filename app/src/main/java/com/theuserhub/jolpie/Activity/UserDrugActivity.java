package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.DrugsAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.DrugItem;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDrugActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "UserDrugActivity";
    private String tag_string_req = "string_req";

    private ProgressDialog pDialog;
    private ListView drugItemListView;
    private List<DrugItem> eachItems;
    DrugsAdapter drugsAdapter;
    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName,appUserLName,appUserEmail,appUserId,appUserGroupId,appUserImgUrl;
    String user_id;
    String first_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_drug);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Drugs");

        user_id = getIntent().getStringExtra("user_id");
        first_name = getIntent().getStringExtra("first_name");

        userDatabaseHandler = new UserDatabaseHandler(UserDrugActivity.this);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl=user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.userdrug_fab);
        if (appUserGroupId.equals("")) {
            fab.setVisibility(View.VISIBLE);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent intent = new Intent(UserDrugActivity.this, DrugAddActivity.class);
                startActivity(intent);
            }
        });

        drugItemListView= (ListView) findViewById(R.id.userprofile_item_lv);
        eachItems=new ArrayList<DrugItem>();
        drugsAdapter = new DrugsAdapter(context,eachItems);
        drugItemListView.setAdapter(drugsAdapter);

        if (!AppFunctions.isNetworkConnected(context)) {
            Toast.makeText(context, "No network found \n fatched from db", Toast.LENGTH_LONG).show();
            //fatchFeedDb();
        } else {
            String url = context.getResources().getString(R.string.MAIN_URL)+"userDrugs";
            fetchUserDrugs(url);
        }

        drugItemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(context, DrugProfileActivity.class);
                i.putExtra("drug_id",eachItems.get(position).getDrug_id().toString());
                i.putExtra("brand_name", eachItems.get(position).getBrand_name().toString());
                i.putExtra("description",eachItems.get(position).getDescription().toString());
                i.putExtra("packet_image",eachItems.get(position).getPacket_image().toString());
                i.putExtra("packet_title",eachItems.get(position).getPacket_title().toString());
                i.putExtra("packet_description",eachItems.get(position).getPacket_description().toString());
                i.putExtra("drug_image",eachItems.get(position).getDrug_image().toString());
                i.putExtra("overview",eachItems.get(position).getOverview().toString());
                i.putExtra("side_effect",eachItems.get(position).getSide_effect().toString());
                i.putExtra("dosage",eachItems.get(position).getDosage().toString());
                i.putExtra("drugs_profile",eachItems.get(position).getDrugs_profile().toString());
                i.putExtra("generic_name",eachItems.get(position).getGeneric_name().toString());
                i.putExtra("first_name",eachItems.get(position).getFirst_name().toString());
                i.putExtra("user_id", eachItems.get(position).getUser_id().toString());
                startActivity(i);
            }
        });
    }

    private void fetchUserDrugs(String url) {
        //url = "http://10.0.3.2/doc5/api/savePost";
        pDialog.setMessage("Wait...");
        pDialog.show();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                eachItems.clear();
                pDialog.dismiss();
                try {
                    JSONArray allPosts=new JSONArray(response);
                    Log.d("Test2", allPosts.toString());
                    for (int i=0;i<allPosts.length();++i)
                    {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        String drug_id, generic_id, brand_name, description, packet_image,packet_title,packet_description,drug_image,overview,side_effect,dosage,user_id,drugs_profile,generic_name;
                        drug_id = singlePost.getString("drug_id");
                        generic_id = singlePost.getString("generic_id");
                        brand_name = singlePost.getString("brand_name");
                        description = singlePost.getString("description");
                        packet_image = singlePost.getString("packet_image");
                        packet_title = singlePost.getString("packet_title");
                        packet_description = singlePost.getString("packet_description");
                        drug_image = singlePost.getString("drug_image");
                        overview = singlePost.getString("overview");
                        side_effect = singlePost.getString("side_effect");
                        dosage = singlePost.getString("dosage");
                        user_id = singlePost.getString("user_id");
                        drugs_profile = singlePost.getString("drugs_profile");
                        generic_name = singlePost.getString("generic_name");
                        DrugItem drugItem=new DrugItem( drug_id,  generic_id,  brand_name,  description,  packet_image,  packet_title, packet_description,  drug_image,  overview,  side_effect,  dosage,  user_id,  drugs_profile,  generic_name,  first_name);
                        eachItems.add(drugItem);
                        //handler.addContact(pst);
                    }
                    //refresh adapter
                    drugsAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
                Toast.makeText(context, "Error: Try again." + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
