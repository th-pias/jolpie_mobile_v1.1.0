package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.SessionManager;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//implements SinchService.StartFailedListener
public class SignUpActivity extends AppCompatActivity {

    private String TAG = "SignUpActivity";
    private String tag_string_req = "signup_str_req";
    private Context context = this;

    private UserDatabaseHandler db;
    private SessionManager session;
    private ProgressDialog pDialog;

    private EditText fname_et, lname_et, bmdc_et, mobile_et, email_et, password_et, confirmPass_et;
    private TextView genderTextView, dobTextView, wrongEmailTextView;
    private Spinner group_sp, daySpinner, monthSpinner, yearSpinner, genderSpinner;
    private TextView signup_btn;
    private String group_id, genderString, dayString, monthString, yearString;
    private List<String> dayList, monthList, yearList, genderList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign up");

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        fname_et = (EditText) findViewById(R.id.signup_fname_et);
        lname_et = (EditText) findViewById(R.id.signup_lname_et);
        bmdc_et = (EditText) findViewById(R.id.signup_bmdc_et);
        mobile_et = (EditText) findViewById(R.id.signup_mobile_et);
        email_et = (EditText) findViewById(R.id.signup_email_et);
        wrongEmailTextView = (TextView) findViewById(R.id.wrong_email_tv);
        password_et = (EditText) findViewById(R.id.signup_password_et);
        confirmPass_et = (EditText) findViewById(R.id.signup_confirmpass_et);
        group_sp = (Spinner) findViewById(R.id.signup_group_spinner);
        signup_btn = (TextView) findViewById(R.id.signup_btn);
        dobTextView = (TextView) findViewById(R.id.dateOfBirth_tv);
        daySpinner = (Spinner) findViewById(R.id.day_spinner);
        monthSpinner = (Spinner) findViewById(R.id.month_spinner);
        yearSpinner = (Spinner) findViewById(R.id.year_spinner);
        genderTextView = (TextView) findViewById(R.id.gender_tv);
        genderSpinner = (Spinner) findViewById(R.id.gender_spinner);
        //signup_btn.setEnabled(false);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        final List<String> group_names = new ArrayList<String>();
        group_names.add("Patient");
        group_names.add("Doctor");
        group_names.add("Medical Student");
        group_names.add("Hospital");
        group_names.add("Diagnostic Center");
        group_names.add("Pharmaceutical Company");

        ArrayAdapter<String> groupdataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, group_names);
        // Drop down layout style - list view with radio button
        groupdataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        group_sp.setAdapter(groupdataAdapter);

        group_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();
                group_id = String.valueOf(position + 1);
                // Showing selected spinner item
                //Toast.makeText(context, "Selected: " + item + " : " + group_id, Toast.LENGTH_LONG).show();

                if (position == 0 || position == 1 || position == 2) {
                    fname_et.setHint("First Name");
                    lname_et.setVisibility(View.VISIBLE);
                    bmdc_et.setVisibility(View.VISIBLE);
                    dobTextView.setText("Date of Birth :");
                    genderTextView.setVisibility(View.VISIBLE);
                    genderSpinner.setVisibility(View.VISIBLE);
                    if (position == 1) {
                        bmdc_et.setVisibility(View.VISIBLE);
                    } else {
                        bmdc_et.setVisibility(View.GONE);
                    }
                } else {
                    lname_et.setVisibility(View.GONE);
                    bmdc_et.setVisibility(View.GONE);
                    dobTextView.setText("Date of Establishment :");
                    genderTextView.setVisibility(View.GONE);
                    genderSpinner.setVisibility(View.GONE);
                    if (position == 3) {
                        fname_et.setHint("Hospital Name");
                    } else if (position == 4) {
                        fname_et.setHint("Lab Name");
                    } else if (position == 5) {
                        fname_et.setHint("Pharmaceutical Company Name");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dayList = new ArrayList<String>();
        monthList = new ArrayList<String>();
        yearList = new ArrayList<String>();
        genderList = new ArrayList<String>();

        insertDateData();

        ArrayAdapter<String> genderSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderList);
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderSpinnerAdapter);
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genderString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> daySpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dayList);
        daySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daySpinner.setAdapter(daySpinnerAdapter);
        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dayString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> monthSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, monthList);
        monthSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthSpinner.setAdapter(monthSpinnerAdapter);
        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> yearSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, yearList);
        yearSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(yearSpinnerAdapter);
        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yearString = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String first_name = fname_et.getText().toString();
                final String last_name = lname_et.getText().toString();
                String bmdc_num = bmdc_et.getText().toString();
                String mobile_num = mobile_et.getText().toString();
                final String email_address = email_et.getText().toString();
                final String password = password_et.getText().toString();
                String confirm_pass = confirmPass_et.getText().toString();

                if (password.length() >= 6) {
                    if (AppFunctions.isValidEmail(email_address)) {
                        wrongEmailTextView.setVisibility(View.GONE);
                        if (first_name.trim().length() > 0 && mobile_num.trim().length() > 0) {
                            if (group_id.equalsIgnoreCase("2")) {
                                if (bmdc_num.trim().length() > 0) {
                                    pDialog.setMessage("Signing up ...");
                                    pDialog.show();
                                    signUpRequest(first_name, last_name, bmdc_num, mobile_num, email_address, password);
                                }
                                else {
                                    Toast.makeText(context, "Please enter your BMDC number", Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                pDialog.setMessage("Signing up ...");
                                pDialog.show();
                                signUpRequest(first_name, last_name, bmdc_num, mobile_num, email_address, password);
                            }

                        } else {
                            Toast.makeText(context, "Please fill up all", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        //Toast.makeText(context, "Please enter an valid Email", Toast.LENGTH_LONG).show();
                        wrongEmailTextView.setText("Email address not valid.");
                        wrongEmailTextView.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(context, "Password length must be six", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void signUpRequest(final String first_name, final String last_name, final String bmdc_num, final String mobile_num, final String email_address, final String password) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "signup";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                Log.d(TAG, response.toString());
                if (response.trim().toString().equalsIgnoreCase("exist")) {
                    //Toast.makeText(context, "Sorry, it looks like "+ email_address +" belongs to an existing account.", Toast.LENGTH_LONG).show();
                    wrongEmailTextView.setText("Sorry, it looks like "+ email_address +" belongs to an existing account.");
                    wrongEmailTextView.setVisibility(View.VISIBLE);
                }
                else if (response.trim().equalsIgnoreCase("ok")) {
                    wrongEmailTextView.setVisibility(View.GONE);
                    Toast.makeText(context, "Please check your email to verify your account.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context, EmailConfirmationActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    wrongEmailTextView.setVisibility(View.GONE);
                    Toast.makeText(context, "Error: try again later", Toast.LENGTH_LONG).show();
                }
                /*String user_id = response.toString().trim();
                db = new UserDatabaseHandler(context);
                // Session manager
                session = new SessionManager(context);
                session.setLogin(true);
                db.addUser(first_name, last_name, email_address, user_id, email_address, group_id);

                //sinchAddUser(user_id);
                openMainActivity();*/
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Error: try again later", Toast.LENGTH_LONG).show();
                //Toast.makeText(context,"Please check your email to verify your account",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context, EmailConfirmationActivity.class);
                startActivity(intent);
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("contact_no_1", mobile_num);
                params.put("birth_day", dayString);
                params.put("birth_month", monthString);
                params.put("birth_year", yearString);
                params.put("first_name", first_name);
                params.put("email_address", email_address);
                params.put("password", password);
                params.put("user_group_id", group_id);

                if (group_id.equalsIgnoreCase("1") || group_id.equalsIgnoreCase("2") || group_id.equalsIgnoreCase("3")) {
                    params.put("last_name", last_name);
                    params.put("gender", genderString);
                    if (group_id.equalsIgnoreCase("2")) {
                        params.put("registration_number", bmdc_num);
                    }
                    else {
                        params.put("registration_number", "");
                    }
                }
                else {
                    params.put("last_name", "");
                    params.put("gender", "");
                    params.put("registration_number", "");
                }

                //params.put("app_code",app_code);

                Log.d(TAG, first_name + " : " + last_name + " : " + email_address + " : " + password + " : " + group_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /*@Override
    protected void onServiceConnected() {
        Log.e("conn2","conn2");
        signup_btn.setEnabled(true);
        getSinchServiceInterface().setStartListener(this);
    }

    private void sinchAddUser(String userName) {

        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(userName);
        } else {
            //openMainActivity();
        }
    }*/

    private void openMainActivity() {
        Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void insertDateData() {

        for (int i = 1; i <= 31; ++i) {
            dayList.add(String.valueOf(i));
        }

        monthList.add("January");
        monthList.add("February");
        monthList.add("March");
        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("September");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");

        Calendar today = Calendar.getInstance();
        int currentYear = today.get(Calendar.YEAR);
        for (int i = currentYear - 100; i <= currentYear; ++i) {
            yearList.add(String.valueOf(i));
        }

        genderList.add("Male");
        genderList.add("Female");
    }

    /*@Override
    public void onStartFailed(SinchError error) {
        //Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
        //openMainActivity();
        Toast.makeText(this, "Your Sign up is successful. Please Sign in with this credential. Sorry for the disturbance.", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(context,LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onStarted() {
        Toast.makeText(this, "Success", Toast.LENGTH_LONG).show();
        openMainActivity();
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        //case R.id.menu_share

        if (id == R.id.menu_share) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Detail")
                    .setAction("View")
                    .build());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
