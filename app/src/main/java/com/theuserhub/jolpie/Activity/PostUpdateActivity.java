package com.theuserhub.jolpie.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AndroidMultiPartEntity;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PostUpdateActivity extends AppCompatActivity {

    private String TAG = "PostActivity";
    private String tag_string_req = "post_str_req";
    private Context context = this;

    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_LOAD_VDO = 2;
    private String imgDecodableString, fileName = "", encodedString = "",
            vdoDecodableString, videoFileName = "", encodedVdoString = "",group_id,
            privacy_id = "0", post_id, post_text, feeling_text, privacy_text, picture_name, video_name;
    private int maxWidth = 350,maxHeight = 350,isImgVdo = 0;
    private long totalSize = 0;

    private Spinner privacySpinner;

    private ProgressDialog prgDialog;
    //private ProgressBar progressBar;
    private TextView postTextView, feelingShowTextView,imageCamera, imageVideo,imageFeeling;//,txtPercentage;
    private EditText postDescription_et;
    private ImageView imagePreview, feelingShowImageView;
    private VideoView videoPreview;
    private Uri selectedImgVdo;
    private LinearLayout feelingLayout,privacyLayout;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl, post_des;

    private String[] privacyTypes = {"All","Patients","Doctors","Medical Students", "Hospitals", "Diagnostic Centers", "Pharmaceutical Companies"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_update);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit");

        prgDialog = new ProgressDialog(this);
        prgDialog.setCancelable(false);

        encodedString = "";
        fileName = "";
        encodedVdoString = "";
        videoFileName = "";
        isImgVdo = 0;
        privacy_id = "0";

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent postIntent = getIntent();
        group_id = postIntent.getStringExtra("group_id");
        post_id = postIntent.getStringExtra("post_id");
        Typeface postFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");


        privacyLayout = (LinearLayout) findViewById(R.id.post_privacy_layout);

        feelingLayout = (LinearLayout) findViewById(R.id.feeling_show_layout);
        feelingShowTextView = (TextView) findViewById(R.id.feeling_show_tv);

        privacySpinner = (Spinner) findViewById(R.id.privacy_spinner);

        postDescription_et = (EditText) findViewById(R.id.post_des_et);
        imagePreview = (ImageView) findViewById(R.id.post_image_upload_preview);
        imageCamera = (TextView) findViewById(R.id.post_image_upload);
        imageCamera.setTypeface(postFont);
        videoPreview = (VideoView) findViewById(R.id.videoPreview);
        imageVideo = (TextView) findViewById(R.id.post_video_upload);
        imageVideo.setTypeface(postFont);
        imageFeeling = (TextView) findViewById(R.id.post_feeling_iv);
        imageFeeling.setTypeface(postFont);
        feelingShowImageView = (ImageView) findViewById(R.id.feeling_iv);
        //txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        //progressBar = (ProgressBar) findViewById(R.id.progressBar);
        postTextView = (TextView) findViewById(R.id.post_send_tv);

        if (!group_id.equals("0")) {
            privacyLayout.setVisibility(View.GONE);
        }
        else {
            privacyLayout.setVisibility(View.VISIBLE);
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, privacyTypes);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        privacySpinner.setAdapter(dataAdapter);

        privacySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //spinner_text = parent.getItemAtPosition(position).toString();
                privacy_id = String.valueOf(position);
                //Toast.makeText(context, "Sugestion : " + privacy_id, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imagePreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
                /*// Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);*/
            }
        });

        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (encodedString.length() > 0) {
                    imagePreview.setVisibility(View.GONE);
                    //imageCamera.setImageResource(R.drawable.camera_icon);
                    encodedString = "";
                    fileName = "";
                } else {
                    // Create intent to Open Image applications like Gallery, Google Photos
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    // Start the Intent
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
                }
            }
        });

        videoPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imageVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_VDO);
            }
        });

        postTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                post_des = postDescription_et.getText().toString();//next check string length
                if (post_des.trim().length() > 0) {
                    // uploading the file to server
                    //new UploadFileToServer().execute();
                    updatePostDetail();
                } else {
                    Toast.makeText(context, "Please write something", Toast.LENGTH_LONG).show();
                }
                //Toast.makeText(context, "Id: " + appUserId + " grp: " + appUserGroupId, Toast.LENGTH_LONG).show();
            }
        });

        imageFeeling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_feeling);
                dialog.setTitle("Today i am feeling");
                TextView vryGdTextView = (TextView) dialog.findViewById(R.id.very_good_tv);
                TextView gdTextView = (TextView) dialog.findViewById(R.id.good_tv);
                TextView neutralTextView = (TextView) dialog.findViewById(R.id.neutral_tv);
                TextView badTextView = (TextView) dialog.findViewById(R.id.bad_tv);
                TextView vryBadTextView = (TextView) dialog.findViewById(R.id.very_bad_tv);

                vryGdTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feelingLayout.setVisibility(View.VISIBLE);
                        feelingShowImageView.setImageResource(R.drawable.feeling_very_good);
                        feelingShowTextView.setText("Very Good");
                        dialog.dismiss();
                    }
                });

                gdTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feelingLayout.setVisibility(View.VISIBLE);
                        feelingShowImageView.setImageResource(R.drawable.feeling_good);
                        feelingShowTextView.setText("Good");
                        dialog.dismiss();
                    }
                });

                neutralTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feelingLayout.setVisibility(View.VISIBLE);
                        feelingShowImageView.setImageResource(R.drawable.feeling_neutral);
                        feelingShowTextView.setText("Neutral");
                        dialog.dismiss();
                    }
                });

                badTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feelingLayout.setVisibility(View.VISIBLE);
                        feelingShowImageView.setImageResource(R.drawable.feeling_bad);
                        feelingShowTextView.setText("Bad");
                        dialog.dismiss();
                    }
                });

                vryBadTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feelingLayout.setVisibility(View.VISIBLE);
                        feelingShowImageView.setImageResource(R.drawable.feeling_very_bad);
                        feelingShowTextView.setText("Very Bad");
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        getPostDetail();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_LOAD_IMG) {
            if (resultCode == RESULT_OK) {
                if (null != data) {
                    selectedImgVdo = data.getData();

                    imagePreview.setImageURI(selectedImgVdo);
                    imagePreview.setVisibility(View.VISIBLE);
                    videoPreview.setVisibility(View.GONE);
                    //imageCamera.setImageResource(R.drawable.cross_icon);
                    isImgVdo = 1;

                    /*
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    //String picturePath = cursor.getString(columnIndex);
                    imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();

                    prgDialog.setMessage("Converting Image to Binary Data");
                    prgDialog.show();

                    String fileNameSegments[] = imgDecodableString.split("/");
                    fileName = fileNameSegments[fileNameSegments.length - 1];
                    String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
                    // Set the Image in ImageView after decoding the String
                    Bitmap myImg = resize(imgDecodableString);//BitmapFactory.decodeFile(imgDecodableString);
                    Log.d("ImageBitmap", "" + myImg);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    try {
                        if (extension.trim().toLowerCase().equals("png"))
                            myImg.compress(Bitmap.CompressFormat.PNG, 100, out);
                        else if (extension.trim().toLowerCase().equals("jpeg") || extension.trim().toLowerCase().equals("jpg"))
                            myImg.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    } catch (Exception e) {
                        Toast.makeText(context, "Something error occured on image loading", Toast.LENGTH_SHORT).show();
                    }


                    byte[] byte_arr = out.toByteArray();
                    // Encode Image to String
                    encodedString = Base64.encodeToString(byte_arr, 0);
                    prgDialog.hide();*/

                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(context, "User cancelled Gallery", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Sorry! Failed to load image", Toast.LENGTH_SHORT).show();
            }
        }
        else if (requestCode == RESULT_LOAD_VDO) {
            if (resultCode == RESULT_OK) {
                selectedImgVdo = data.getData();
                videoPreview.setVideoURI(selectedImgVdo);
                videoPreview.setVisibility(View.VISIBLE);
                imagePreview.setVisibility(View.GONE);
                videoPreview.requestFocus();
                // start playing
                videoPreview.start();
                isImgVdo = 2;
            }
        }
    }

    /**
     * Uploading the file to server
     * */
    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            //progressBar.setProgress(0);
            super.onPreExecute();
            prgDialog.setMessage("Posting...");
            prgDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            //progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            //progressBar.setProgress(progress[0]);

            // updating percentage value
            //txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(context.getResources().getString(R.string.MAIN_URL) + "savePost");

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                Log.e("Check", "" + isImgVdo);
                if (isImgVdo != 0) {
                    File sourceFile = new File(getRealPathFromURI(selectedImgVdo));

                    // Adding file data to http body
                    if (isImgVdo == 1) {
                        entity.addPart("user_image", new FileBody(sourceFile));
                    } else if (isImgVdo == 2) {
                        entity.addPart("user_video", new FileBody(sourceFile));
                    }
                }
                // Extra parameters if you want to pass to server
                entity.addPart("description", new StringBody(post_des));
                entity.addPart("group_id", new StringBody(group_id));
                entity.addPart("feeling", new StringBody(feelingShowTextView.getText().toString()));
                entity.addPart("user_group", new StringBody(privacy_id));
                entity.addPart("user_id", new StringBody(appUserId));
                //entity.addPart("drud",new Body);

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "error";
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);

            // showing the server response in an alert dialog
            //showAlert(result);

            prgDialog.dismiss();
            if (result.equals("error")) {
                Toast.makeText(context,"Failed to post the new update. Try again.",Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(context,"Posted successfully.",Toast.LENGTH_LONG).show();
                finish();
            }

            super.onPostExecute(result);
        }

    }

    /**
     * Method to show alert dialog
     * */
    private void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setTitle("Response from Servers")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*private void uploadImage() {

        prgDialog.setMessage("Upload Image");
        prgDialog.show();

        String url = context.getResources().getString(R.string.MAIN_URL) + "uploadPostImage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                *//*Intent intent=new Intent(PostActivity.this,MainActivity.class);
                startActivity(intent);
                finish();*//*
                prgDialog.hide();
                if (response.toString().equals("yes")) {
                    Toast.makeText(context, "Uploaded successfully", Toast.LENGTH_LONG).show();
                    newPostRequest();
                } else {
                    Toast.makeText(context, "Uploading Error", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "uploading error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                newPostRequest();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("image", encodedString);
                params.put("filename", fileName);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }*/

    /*private void newPostRequest() {
        prgDialog.setMessage("Posting...");
        prgDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "savePost";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                prgDialog.hide();
                Intent intent = new Intent(PostActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("group_id", appUserGroupId);
                params.put("description", post_des);
                params.put("photo", fileName);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }*/

    /*private Bitmap resize(String path) {
        // create the options
        BitmapFactory.Options opts = new BitmapFactory.Options();

        //just decode the file
        opts.inJustDecodeBounds = true;
        Bitmap bp = BitmapFactory.decodeFile(path, opts);

        //get the original size
        int orignalHeight = opts.outHeight;
        int orignalWidth = opts.outWidth;
        //initialization of the scale
        int resizeScale = 1;
        //get the good scale
        if (orignalWidth > maxWidth || orignalHeight > maxHeight) {
            final int heightRatio = Math.round((float) orignalHeight / (float) maxHeight);
            final int widthRatio = Math.round((float) orignalWidth / (float) maxWidth);
            resizeScale = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        //put the scale instruction (1 -> scale to (1/1); 8-> scale to 1/8)
        opts.inSampleSize = resizeScale;
        opts.inJustDecodeBounds = false;
        //get the futur size of the bitmap
        int bmSize = (orignalWidth / resizeScale) * (orignalHeight / resizeScale) * 4;
        //check if it's possible to store into the vm java the picture
        if (Runtime.getRuntime().freeMemory() > bmSize) {
            //decode the file
            bp = BitmapFactory.decodeFile(path, opts);
        } else
            return null;
        return bp;
    }*/

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void updatePostDetail() {
        prgDialog.setMessage("Updating...");
        prgDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "updatePost";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                prgDialog.dismiss();
                if (response.trim().equals("ok")) {
                    Toast.makeText(context, "Updated successfully", Toast.LENGTH_LONG).show();
                    finish();
                }
                else {
                    Toast.makeText(context, "Error: Try again later", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: Try again later", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("post_id", post_id);
                params.put("description", post_des);
                params.put("post_privacy_id", privacy_id);
                params.put("feeling", feelingShowTextView.getText().toString());

                Log.d("Post_id",post_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getPostDetail() {
        prgDialog.setMessage("Gathering Infromation");
        prgDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "getPostDetail";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                prgDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String id = jsonObject.getString("section_post_id");
                    String title = jsonObject.getString("section_title");
                    String description = jsonObject.getString("section_description");
                    String post_privacy_id = jsonObject.getString("section_privacy");
                    String feeling = jsonObject.getString("section_feeling");
                    String photo = jsonObject.getString("section_photo");
                    String video = jsonObject.getString("section_video");


                    postDescription_et.setText(description);
                    privacySpinner.setSelection(Integer.parseInt(post_privacy_id));
                    if (feeling.trim().length()>0 && group_id.equals("0")) {
                        feelingLayout.setVisibility(View.VISIBLE);
                        feelingShowTextView.setText(feeling);

                        if (feeling.trim().equalsIgnoreCase("Very Good")) {
                            feelingShowImageView.setImageResource(R.drawable.feeling_very_good);
                        }
                        else if (feeling.trim().equalsIgnoreCase("Good")) {
                            feelingShowImageView.setImageResource(R.drawable.feeling_good);
                        }
                        else if (feeling.trim().equalsIgnoreCase("Neutral")) {
                            feelingShowImageView.setImageResource(R.drawable.feeling_neutral);
                        }
                        else if (feeling.trim().equalsIgnoreCase("Bad")) {
                            feelingShowImageView.setImageResource(R.drawable.feeling_bad);
                        }
                        else if (feeling.trim().equalsIgnoreCase("Very Bad")) {
                            feelingShowImageView.setImageResource(R.drawable.feeling_very_bad);
                        }
                    }
                    else {
                        feelingLayout.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: Try again later", Toast.LENGTH_LONG).show();
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("post_id", post_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
