package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.theuserhub.jolpie.Databases.SessionManager;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.ChamberExModel;
import com.theuserhub.jolpie.Models.ScheduleModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountSettingsActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    private Context context = this;
    private String TAG = "AccountSettingsActivity",tag_string_req = "acc_set_str_req";

    private String first_name, last_name, email_address, password, contact_no_1,
            registration_number, birth_day, birth_month, birth_year, gender,
            dayString, monthString, yearString, genderString;
    private TextView nameShowTextView,nameEditTextView, firstNameTextView,bmdcShowTextView,bmdcEditTextView,
            contactShowTextView,contactcEditTextView,genderEditTextView,emailEditTextView,
            dobTextView,dobShowTextView,dobEditTextView,genderShowTextView,emailShowTextView,
            passShowTextView,passEditTextView, nameSaveTextView, bmdcSaveTextView,contactSaveTextView,
            dobSaveTextView, genderSaveTextView, emailSaveTextView, passSaveTextView,
            nameCanTextView, bmdcCanTextView, contactCanTextView, dobCanTextView, genderCanTextView,
            emailCanTextView, passCanTextView, logoutTextView, logoutImgaeView,emptyTextView;
    private EditText firstNameEditText,lastNameEditText,bmdcEditText,contactEditText,emailEditText,passEditText;
    private Spinner genderSpinner,daySpinner,monthSpinner,yearSpinner;
    private LinearLayout nameEditLayout, bmdcShowLayout, bmdcEditLayout, contactcEditLayout,
            dobEditLayout, genderShowLayout, genderEditLayout, emailEditLayout,
            passEditLayout;
    private RelativeLayout lastNameLayout;
    private List<String> dayList, monthList, yearList, genderList;

    private ProgressDialog pDialog;
    private GoogleApiClient mGoogleApiClient;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_account_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Account Settings");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().requestProfile().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        Typeface settingsFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        emptyTextView = (TextView) findViewById(R.id.empty_tv);

        logoutTextView = (TextView) findViewById(R.id.logout_tv);
        logoutImgaeView = (TextView) findViewById(R.id.logout_iv);
        logoutImgaeView.setTypeface(settingsFont);

        nameEditLayout = (LinearLayout) findViewById(R.id.name_edit_layout);
        bmdcShowLayout = (LinearLayout) findViewById(R.id.bmdc_show_layout);
        bmdcEditLayout = (LinearLayout) findViewById(R.id.bmdc_edit_layout);
        contactcEditLayout = (LinearLayout) findViewById(R.id.contact_edit_layout);
        dobEditLayout = (LinearLayout) findViewById(R.id.dob_edit_layout);
        genderShowLayout = (LinearLayout) findViewById(R.id.gender_show_layout);
        genderEditLayout = (LinearLayout) findViewById(R.id.gender_edit_layout);
        emailEditLayout = (LinearLayout) findViewById(R.id.email_edit_layout);
        passEditLayout = (LinearLayout) findViewById(R.id.pass_edit_layout);
        lastNameLayout = (RelativeLayout) findViewById(R.id.last_name_layout);

        firstNameTextView = (TextView) findViewById(R.id.textView26);
        nameShowTextView = (TextView) findViewById(R.id.name_show_tv);
        nameEditTextView = (TextView) findViewById(R.id.name_edit_tv);
        nameSaveTextView = (TextView) findViewById(R.id.name_save_tv);
        nameCanTextView = (TextView) findViewById(R.id.name_can_tv);

        bmdcShowTextView = (TextView) findViewById(R.id.bmdc_show_tv);
        bmdcEditTextView = (TextView) findViewById(R.id.bmdc_edit_tv);
        bmdcSaveTextView = (TextView) findViewById(R.id.bmdc_save_tv);
        bmdcCanTextView = (TextView) findViewById(R.id.bmdc_can_tv);

        contactShowTextView = (TextView) findViewById(R.id.contact_show_tv);
        contactcEditTextView = (TextView) findViewById(R.id.contact_edit_tv);
        contactSaveTextView = (TextView) findViewById(R.id.contact_save_tv);
        contactCanTextView = (TextView) findViewById(R.id.contact_can_tv);

        genderShowTextView = (TextView) findViewById(R.id.gender_show_tv);
        genderEditTextView = (TextView) findViewById(R.id.gender_edit_tv);
        genderSaveTextView = (TextView) findViewById(R.id.gender_save_tv);
        genderCanTextView = (TextView) findViewById(R.id.gender_can_tv);

        emailShowTextView = (TextView) findViewById(R.id.email_show_tv);
        emailEditTextView = (TextView) findViewById(R.id.email_edit_tv);
        emailSaveTextView = (TextView) findViewById(R.id.email_save_tv);
        emailCanTextView = (TextView) findViewById(R.id.email_can_tv);

        passShowTextView = (TextView) findViewById(R.id.pass_show_tv);
        passEditTextView = (TextView) findViewById(R.id.pass_edit_tv);
        passSaveTextView = (TextView) findViewById(R.id.pass_save_tv);
        passCanTextView = (TextView) findViewById(R.id.pass_can_tv);

        dobTextView = (TextView) findViewById(R.id.dob_tv);
        dobShowTextView = (TextView) findViewById(R.id.dob_show_tv);
        dobEditTextView = (TextView) findViewById(R.id.dob_edit_tv);
        dobSaveTextView = (TextView) findViewById(R.id.dob_save_tv);
        dobCanTextView = (TextView) findViewById(R.id.dob_can_tv);

        firstNameEditText = (EditText) findViewById(R.id.editText);
        lastNameEditText = (EditText) findViewById(R.id.editText2);
        bmdcEditText = (EditText) findViewById(R.id.bmdc_et);
        contactEditText = (EditText) findViewById(R.id.contact_et);
        emailEditText = (EditText) findViewById(R.id.email_et);
        passEditText = (EditText) findViewById(R.id.new_pass_et);

        daySpinner = (Spinner) findViewById(R.id.day_spinner);
        monthSpinner = (Spinner) findViewById(R.id.month_spinner);
        yearSpinner = (Spinner) findViewById(R.id.year_spinner);
        genderSpinner = (Spinner) findViewById(R.id.gender_spinner);

        if (appUserGroupId.equals("1") || appUserGroupId.equals("2") || appUserGroupId.equals("3")) {
            genderShowLayout.setVisibility(View.VISIBLE);
            dobTextView.setText("Date Of Birth : ");
            if (appUserGroupId.equals("2")) {
                bmdcShowLayout.setVisibility(View.VISIBLE);
            }
            else {
                bmdcShowLayout.setVisibility(View.GONE);
            }
        }
        else {
            bmdcShowLayout.setVisibility(View.GONE);
            genderShowLayout.setVisibility(View.GONE);
            dobTextView.setText("Date Of Establishment : ");
        }

        dayList = new ArrayList<String>();
        monthList = new ArrayList<String>();
        yearList = new ArrayList<String>();
        genderList = new ArrayList<String>();
        insertDateData();

        getUserInfo();

        ArrayAdapter<String> genderSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderList);
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderSpinnerAdapter);
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genderString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> daySpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dayList);
        daySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daySpinner.setAdapter(daySpinnerAdapter);
        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dayString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> monthSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, monthList);
        monthSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthSpinner.setAdapter(monthSpinnerAdapter);
        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> yearSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, yearList);
        yearSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(yearSpinnerAdapter);
        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yearString = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        nameEditTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameEditLayout.setVisibility(View.VISIBLE);
                firstNameEditText.setText(first_name);
                if (appUserGroupId.equals("1") || appUserGroupId.equals("2") || appUserGroupId.equals("3")) {
                    firstNameTextView.setText("First Name : ");
                    lastNameLayout.setVisibility(View.VISIBLE);
                    lastNameEditText.setText(last_name);
                } else {
                    firstNameTextView.setText("Name : ");
                    lastNameLayout.setVisibility(View.GONE);
                }
            }
        });

        nameCanTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameEditLayout.setVisibility(View.GONE);
            }
        });

        bmdcEditTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bmdcEditLayout.setVisibility(View.VISIBLE);
                bmdcEditText.setText(registration_number);
            }
        });

        bmdcCanTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bmdcEditLayout.setVisibility(View.GONE);
            }
        });

        contactcEditTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactcEditLayout.setVisibility(View.VISIBLE);
                contactEditText.setText(contact_no_1);
            }
        });

        contactCanTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactcEditLayout.setVisibility(View.GONE);
            }
        });

        dobEditTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dobEditLayout.setVisibility(View.VISIBLE);
            }
        });

        dobCanTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dobEditLayout.setVisibility(View.GONE);
            }
        });

        genderEditTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderEditLayout.setVisibility(View.VISIBLE);
            }
        });

        genderCanTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderEditLayout.setVisibility(View.GONE);
            }
        });

        emailEditTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailEditLayout.setVisibility(View.VISIBLE);
                emailEditText.setText(email_address);
            }
        });

        emailCanTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailEditLayout.setVisibility(View.GONE);
            }
        });

        passEditTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passEditLayout.setVisibility(View.VISIBLE);
            }
        });

        passCanTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passEditLayout.setVisibility(View.GONE);
            }
        });

        nameSaveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (appUserGroupId.equals("1") || appUserGroupId.equals("2") || appUserGroupId.equals("3")) {
                    //signUpRequest(first_name,last_name,registration_number,contact_no_1,email_address,password,birth_day, birth_month,birth_year,gender);
                    signUpRequest(firstNameEditText.getText().toString(),lastNameEditText.getText().toString(),registration_number,contact_no_1,email_address,password,birth_day, birth_month,birth_year,gender);
                }
                else {
                    signUpRequest(firstNameEditText.getText().toString(),"",registration_number,contact_no_1,email_address,password,birth_day, birth_month,birth_year,gender);
                }
            }
        });

        bmdcSaveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpRequest(first_name,last_name,bmdcEditText.getText().toString(),contact_no_1,email_address,password,birth_day, birth_month,birth_year,gender);
            }
        });

        contactSaveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpRequest(first_name,last_name,registration_number,contactEditText.getText().toString(),email_address,password,birth_day, birth_month,birth_year,gender);
            }
        });

        dobSaveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpRequest(first_name,last_name,registration_number,contact_no_1,email_address,password,dayString, monthString,yearString,gender);
            }
        });

        genderSaveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpRequest(first_name,last_name,registration_number,contact_no_1,email_address,password,birth_day, birth_month,birth_year,genderString);
            }
        });

        emailSaveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpRequest(first_name,last_name,registration_number,contact_no_1,emailEditText.getText().toString(),password,birth_day, birth_month,birth_year,gender);
            }
        });

        passSaveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passEditText.getText().toString().trim().length() >= 6 ) {
                    signUpRequest(first_name, last_name, registration_number, contact_no_1, email_address, passEditText.getText().toString(), birth_day, birth_month, birth_year, gender);
                }
                else {
                    Toast.makeText(AccountSettingsActivity.this, "Password length minimum six", Toast.LENGTH_SHORT).show();
                }
            }
        });

        logoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog.setMessage("Log Out...");
                pDialog.show();
                SessionManager sessionManager = new SessionManager(context);
                sessionManager.setLogin(false);
                userDatabaseHandler.deleteUsers();

                LoginManager.getInstance().logOut();

                if (mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    // [START_EXCLUDE]
                                    //updateUI(false);
                                    // [END_EXCLUDE]
                                }
                            });
                }
                /*if (mGoogleApiClient.isConnected()) {
                    Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                    mGoogleApiClient.disconnect();
                    mGoogleApiClient.connect();
                    // updateUI(false);
                    System.err.println("LOG OUT ^^^^^^^^^^^^^^^^^^^^ SUCESS");
                }*/

                pDialog.dismiss();
                Intent intent = new Intent(context, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    private void getUserInfo() {
        pDialog.setMessage("Gathering Information...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL)+"getUserAccount";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    first_name = jsonObject.getString("first_name");
                    last_name = jsonObject.getString("last_name");
                    email_address = jsonObject.getString("email_address");
                    password = jsonObject.getString("password");
                    registration_number = jsonObject.getString("registration_number");
                    contact_no_1 = jsonObject.getString("contact_no_1");
                    birth_day = jsonObject.getString("birth_day");
                    birth_month = jsonObject.getString("birth_month");
                    birth_year = jsonObject.getString("birth_year");
                    gender = jsonObject.getString("gender");

                    updateUi();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText("Can't load data.\nTap to retry.");
                emptyTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emptyTextView.setVisibility(View.GONE);
                        getUserInfo();
                    }
                });
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void updateUi() {
        if (appUserGroupId.equals("2")) {
            nameShowTextView.setText("Dr. "+first_name+" "+last_name);
        }
        else {
            nameShowTextView.setText(first_name+" "+last_name);
        }
        bmdcShowTextView.setText(registration_number);
        contactShowTextView.setText(contact_no_1);
        if (!birth_year.equals("0")) {
            dobShowTextView.setText(birth_day + " - " + birth_month + " - " + birth_year);
        }
        if (gender.equals("1")) {
            genderShowTextView.setText("Male");
        }
        else if (gender.equals("2")) {
            genderShowTextView.setText("Female");
        }

        emailShowTextView.setText(email_address);
        passShowTextView.setText("");
        for (int i=0;i<password.length();++i) {
            passShowTextView.setText(passShowTextView.getText().toString()+"*");
        }
    }

    private void signUpRequest(final String first_name1, final String last_name1, final String bmdc_num1, final String mobile_num1, final String email_address1, final String password1, final String dayString1, final String monthString1, final String yearString1, final String genderString1) {
        pDialog.setMessage("Saving...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "updateAccountSettings";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                Log.d(TAG, response.toString());

                first_name = first_name1;
                last_name = last_name1;
                registration_number = bmdc_num1;
                contact_no_1 = mobile_num1;
                birth_day = dayString1;
                birth_month = monthString1;
                birth_year = yearString1;
                gender = genderString1;
                email_address = email_address1;
                password = password1;

                updateUi();

                removeEditLayouts();

                Toast.makeText(context, "Successfully saved",Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: try again later", Toast.LENGTH_LONG).show();
                //Toast.makeText(context,"Please check your email to verify your account",Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("first_name", first_name1);
                params.put("last_name", last_name1);
                params.put("contact_no_1", mobile_num1);
                params.put("birth_day", dayString1);
                params.put("birth_month", monthString1);
                params.put("birth_year", yearString1);
                params.put("gender", genderString1);
                params.put("registration_number", bmdc_num1);
                params.put("email_address", email_address1);
                params.put("password", password1);

                Log.d(TAG, first_name + " : " + last_name + " : " + email_address + " : " + password + " : " + appUserGroupId);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void removeEditLayouts() {
        nameEditLayout.setVisibility(View.GONE);
        bmdcEditLayout.setVisibility(View.GONE);
        contactcEditLayout.setVisibility(View.GONE);
        dobEditLayout.setVisibility(View.GONE);
        genderEditLayout.setVisibility(View.GONE);
        emailEditLayout.setVisibility(View.GONE);
        passEditLayout.setVisibility(View.GONE);
    }

    private void insertDateData() {

        for (int i = 1; i <= 31; ++i) {
            dayList.add(String.valueOf(i));
        }

        monthList.add("January");
        monthList.add("February");
        monthList.add("March");
        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("September");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");

        Calendar today = Calendar.getInstance();
        int currentYear = today.get(Calendar.YEAR);
        for (int i = currentYear - 100; i <= currentYear; ++i) {
            yearList.add(String.valueOf(i));
        }

        genderList.add("Male");
        genderList.add("Female");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
