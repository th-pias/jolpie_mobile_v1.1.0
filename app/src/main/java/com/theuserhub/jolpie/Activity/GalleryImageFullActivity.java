package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

public class GalleryImageFullActivity extends AppCompatActivity {
    private Context context = this;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private String image,isprofilePic;
    private ProgressBar progressBar;
    private NetworkImageView image_img_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_image_full);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        image = getIntent().getStringExtra("image");
        isprofilePic = getIntent().getStringExtra("flag");
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        image_img_view = (NetworkImageView) findViewById(R.id.image_img_view);
        if(isprofilePic.equals("0")){
            if (image.length()>1) {
                progressBar.setVisibility(View.VISIBLE);
                if (imageLoader == null)
                    imageLoader = AppController.getInstance().getImageLoader();

                String meta_url = context.getResources().getString(R.string.BANNER_URL) + image;
                image_img_view.setImageUrl(meta_url, imageLoader);
                progressBar.setVisibility(View.GONE);
            }
        }else{
            if (image.length()>1) {
                progressBar.setVisibility(View.VISIBLE);
                if (imageLoader == null)
                    imageLoader = AppController.getInstance().getImageLoader();

                String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + image;
                image_img_view.setImageUrl(meta_url, imageLoader);
                progressBar.setVisibility(View.GONE);
            }
        }





    }

}
