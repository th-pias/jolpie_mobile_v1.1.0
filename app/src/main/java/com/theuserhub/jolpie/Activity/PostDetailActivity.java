package com.theuserhub.jolpie.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.CommentListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.CommentModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostDetailActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "PostDetailActivity", tag_string_req = "post_detail_request";

    private EditText commentEditText;
    private ImageView postByImageView;
    private NetworkImageView postByNImageView,postNImageView;
    private VideoView postVideoView;
    private TextView userNameOfFeed,dateTextView,titleTextView,goodCountTV,followCountTV,answerCountTV,description,
            goodImage,answerImage,followImage,goodTextView,commentSendImageView;
    private LinearLayout goodCountLayout,shareCountLayout, answerLinearLayout, postUserLayout,goodLayout,shareLayout,
            likeCommentShareLayout,likeCommentShareActivityLayout;
    private Button editButton;
    private String post_id, post_title, post_des, post_feeling, post_photo, post_video, post_created, post_updated,
            post_userId, post_user_fname, post_user_lname, post_user_photo, post_userGroupId,
            post_likes, post_answers, post_follows, post_liked_by_me, post_shared_by_me,post_group_id,comment_text;
    private ListView commentListView;
    private CommentListAdapter commentAdapter;
    private List<CommentModel>eachComment;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private MediaController mediaController;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("New Update");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent postIntent = getIntent();
        post_id = postIntent.getStringExtra("id");
        post_title = postIntent.getStringExtra("title");
        post_des = postIntent.getStringExtra("description");
        post_feeling = postIntent.getStringExtra("feeling");
        post_photo = postIntent.getStringExtra("photo");
        post_video = postIntent.getStringExtra("video");
        post_created = postIntent.getStringExtra("created_at");
        post_updated = postIntent.getStringExtra("updated_at");
        post_userId = postIntent.getStringExtra("user_id");
        post_user_fname = postIntent.getStringExtra("user_fname");
        post_user_lname = postIntent.getStringExtra("user_lname");
        post_user_photo = postIntent.getStringExtra("user_photo");
        post_userGroupId = postIntent.getStringExtra("user_group_id");
        post_likes = postIntent.getStringExtra("good_count");
        post_answers = postIntent.getStringExtra("answer_count");
        post_follows = postIntent.getStringExtra("share_count");
        post_liked_by_me  = postIntent.getStringExtra("liked_by_me");
        post_shared_by_me = postIntent.getStringExtra("shared_by_me");
        Log.e("like", post_liked_by_me);

        Typeface postDetailFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        commentEditText = (EditText) findViewById(R.id.commentedittext);
        commentSendImageView = (TextView) findViewById(R.id.commentsendimageView);
        commentSendImageView.setTypeface(postDetailFont);

        commentListView = (ListView) findViewById(R.id.post_detail_lv);

        final LayoutInflater header = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup detailViewGroup = (ViewGroup) header.inflate(R.layout.header_post_detail, null);

        editButton = (Button) detailViewGroup.findViewById(R.id.post_edit_delete_button);
        editButton.setTypeface(postDetailFont);

        postUserLayout = (LinearLayout) detailViewGroup.findViewById(R.id.feedlist_user_layout);
        postByImageView = (ImageView) detailViewGroup.findViewById(R.id.userimageoffeed);
        postByNImageView = (NetworkImageView) detailViewGroup.findViewById(R.id.userNimageoffeed);
        userNameOfFeed = (TextView) detailViewGroup.findViewById(R.id.usernameoffeed);
        dateTextView = (TextView) detailViewGroup.findViewById(R.id.datetime);

        postNImageView = (NetworkImageView) detailViewGroup.findViewById(R.id.feedimage_network);
        postVideoView = (VideoView) detailViewGroup.findViewById(R.id.feedvideo_vv);

        description = (TextView) detailViewGroup.findViewById(R.id.description);

        goodCountLayout = (LinearLayout) detailViewGroup.findViewById(R.id.goodcount_linlay);
        shareCountLayout = (LinearLayout) detailViewGroup.findViewById(R.id.followcount_linlay);
        goodCountTV = (TextView) detailViewGroup.findViewById(R.id.goodcount);
        followCountTV = (TextView) detailViewGroup.findViewById(R.id.followcount);
        answerCountTV = (TextView) detailViewGroup.findViewById(R.id.answercount);

        likeCommentShareLayout = (LinearLayout) detailViewGroup.findViewById(R.id.like_comm_share_layout);
        likeCommentShareActivityLayout = (LinearLayout) detailViewGroup.findViewById(R.id.like_comment_share_activity_layout);
        goodLayout = (LinearLayout) detailViewGroup.findViewById(R.id.goodlinlay);
        goodImage = (TextView) detailViewGroup.findViewById(R.id.goodimageview);
        goodImage.setTypeface(postDetailFont);
        goodTextView = (TextView) detailViewGroup.findViewById(R.id.goodTextView);

        answerImage = (TextView) detailViewGroup.findViewById(R.id.answerimageview);
        answerImage.setTypeface(postDetailFont);

        shareLayout = (LinearLayout) detailViewGroup.findViewById(R.id.followlinlay);
        followImage = (TextView) detailViewGroup.findViewById(R.id.followimageview);
        followImage.setTypeface(postDetailFont);

        commentListView.addHeaderView(detailViewGroup);

        eachComment = new ArrayList<CommentModel>();
        commentAdapter = new CommentListAdapter(context,eachComment);
        commentListView.setAdapter(commentAdapter);

        updatingUi();

        getPostDetail();

        Log.e("ID",appUserId+" : "+post_userId);
        if (appUserId.equals(post_userId)) {
            editButton.setVisibility(View.VISIBLE);
        }
        else {
            editButton.setVisibility(View.GONE);
        }
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu filterMenu = new PopupMenu(context, editButton);
                filterMenu.getMenuInflater().inflate(R.menu.menu_edit_delete, filterMenu.getMenu());
                filterMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context, "You Clicked : "+item.getTitle() , Toast.LENGTH_SHORT).show();
                        if (item.getItemId()==R.id.edit_item) {

                            Intent intent = new Intent(context, PostUpdateActivity.class);
                            intent.putExtra("group_id", post_group_id);
                            intent.putExtra("post_id", post_id);
                            startActivity(intent);

                        }
                        else if (item.getItemId()==R.id.delete_item) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                            alertDialogBuilder.setTitle("Are you sure?");
                            alertDialogBuilder.setMessage("You wanted to delete this?");

                            alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    postDeleteRequset();
                                }
                            });

                            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ////////////
                                }
                            });
                            final AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                        return true;
                    }
                });
                filterMenu.show();//showing popup menu
            }
        });

        postUserLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileDetailNewActivity.class);
                intent.putExtra("user_id", post_userId);
                intent.putExtra("user_fname", post_user_fname);
                intent.putExtra("user_lname", post_user_lname);
                intent.putExtra("user_email", "");
                intent.putExtra("group_id", post_userGroupId);
                intent.putExtra("user_image_url", post_user_photo);
                startActivity(intent);
            }
        });

        goodCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PeopleWhoLiked.class);
                intent.putExtra("peopleWhoFlag", "-1");
                intent.putExtra("post_id", post_id);
                startActivity(intent);
            }
        });

        shareCountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PeopleWhoLiked.class);
                intent.putExtra("peopleWhoFlag", "1");
                intent.putExtra("post_id", post_id);
                startActivity(intent);

            }
        });

        goodLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (post_liked_by_me.trim().equalsIgnoreCase("0")) {
                    post_liked_by_me = "1";

                    //goodLayout.setBackgroundColor(Color.parseColor("#e3f2fd"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                    int numliked = Integer.parseInt(goodCountTV.getText().toString());
                    ++numliked;
                    goodCountTV.setText(String.valueOf(numliked));

                    AppFunctions.likeUnlikeRequest(context, post_id, "1", appUserId);
                } else {
                    post_liked_by_me = "0";
                    //goodLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    int numliked = Integer.parseInt(goodCountTV.getText().toString());
                    --numliked;
                    goodCountTV.setText(String.valueOf(numliked));

                    AppFunctions.likeUnlikeRequest(context, post_id, "0", appUserId);
                }
            }
        });

        shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are you sure?");
                alertDialogBuilder.setMessage("You wanted to share this post?");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        try {
                            int numShared = Integer.parseInt(followCountTV.getText().toString());
                            ++numShared;
                            followCountTV.setText(String.valueOf(numShared));
                        }
                        catch (NumberFormatException e){

                        }

                        postShareRequest();
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        commentSendImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment_text = commentEditText.getText().toString();
                if (comment_text.trim().length() <= 0) {
                    Toast.makeText(context, "Please write something", Toast.LENGTH_SHORT).show();
                } else {
                    commentEditText.setText("");
                    saveCommentRequest();
                }
            }
        });
    }

    private void saveCommentRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "makeComment";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("saveComment", response.toString());

                String comment_id = response.toString().toString();
                int numliked = Integer.parseInt(answerCountTV.getText().toString());
                ++numliked;
                post_answers = String.valueOf(numliked);
                answerCountTV.setText(String.valueOf(numliked));

                CommentModel commentModel = new CommentModel(comment_id,comment_text,appUserId,appUserFName,appUserLName,appUserImgUrl,appUserGroupId,"2016-4-21",post_id);
                eachComment.add(commentModel);
                commentAdapter.notifyDataSetChanged();
                commentListView.setSelection(eachComment.size()-1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("saveComment", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("comment_text", comment_text);
                params.put("user_id", appUserId);
                params.put("post_id", post_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void postDeleteRequset() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "deletePost";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context,"Post Deleted Successfully",Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("post_id", post_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void postShareRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "postShare";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context, "Post Shared Successfully", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("post_id", post_id);
                params.put("increase_share", "1");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getAllComment() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "showComment";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try {
                    JSONArray allPosts = new JSONArray(response);
                    for (int i = 0; i < allPosts.length(); ++i) {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        final String comment_id = singlePost.getString("comment_id");
                        final String comment_text = singlePost.getString("comment_text");
                        String comment_post_userid = singlePost.getString("user_id");
                        String comment_post_id = singlePost.getString("post_id");
                        String comment_created_at = singlePost.getString("created_at");
                        String comment_updated_at = singlePost.getString("updated_at");
                        String comment_fname = singlePost.getString("first_name");
                        String comment_lname = singlePost.getString("last_name");
                        String comment_userPhoto = singlePost.getString("photo");
                        String comment_userGroupId = singlePost.getString("user_group_id");

                        CommentModel commentModel = new CommentModel(comment_id,comment_text,comment_post_userid,comment_fname,comment_lname,comment_userPhoto,comment_userGroupId,comment_created_at,comment_post_id);
                        eachComment.add(commentModel);
                    }
                    commentAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("saveComment", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("post_id", post_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getPostDetail() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getPostDetail";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                jsonParsing(response);
                getAllComment();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("saveComment", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("post_id", post_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void jsonParsing(String response) {
        try {
            JSONObject infoJsonObject = new JSONObject(response);

            post_id = infoJsonObject.getString("section_post_id");
            post_group_id = infoJsonObject.getString("section_group_id");
            post_title = infoJsonObject.getString("section_title");
            post_des = infoJsonObject.getString("section_description");
            post_feeling = infoJsonObject.getString("section_feeling");
            post_photo = infoJsonObject.getString("section_photo");
            post_video = infoJsonObject.getString("section_video");
            post_created = infoJsonObject.getString("section_at");
            post_updated = infoJsonObject.getString("section_updated_at");
            post_userId = infoJsonObject.getString("section_by");
            post_user_fname = infoJsonObject.getString("section_by_fname");
            post_user_lname = infoJsonObject.getString("section_by_lname");
            post_user_photo =  infoJsonObject.getString("section_by_photo");
            post_userGroupId = infoJsonObject.getString("section_by_user_group_id");
            post_likes = infoJsonObject.getString("good_count");
            post_answers = infoJsonObject.getString("answer_count");
            post_follows = infoJsonObject.getString("share_count");
            post_liked_by_me = infoJsonObject.getString("liked_by_me");
            post_shared_by_me = infoJsonObject.getString("shared_by_me");

            updatingUi();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updatingUi() {

        if (post_user_photo.length() > 1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + post_user_photo;
            postByNImageView.setImageUrl(meta_url, imageLoader);
            //activityByImageView.setVisibility(View.GONE);
        }else {
            if (post_userGroupId.equals("1") || post_userGroupId.equals("3")) {
                postByImageView.setImageResource(R.drawable.patient_student_default);
            } else if (post_userGroupId.equals("2")) {
                postByImageView.setImageResource(R.drawable.doctor_default);
            } else if (post_userGroupId.equals("4")) {
                postByImageView.setImageResource(R.drawable.hospital_default);
            } else if (post_userGroupId.equals("5")) {
                postByImageView.setImageResource(R.drawable.labs_default);
            } else if (post_userGroupId.equals("6")) {
                postByImageView.setImageResource(R.drawable.pharmaceutical_default);
            }
        }

        String post_by_user_name;
        if (post_userGroupId.equals("2")) {
            post_by_user_name = "Dr. "+post_user_fname + " " + post_user_lname;
        }
        else {
            post_by_user_name = post_user_fname + " " + post_user_lname;
        }

        SpannableStringBuilder builder = new SpannableStringBuilder();

        StyleSpan boldStyle = new StyleSpan(android.graphics.Typeface.BOLD);
        Spannable wordtoSpan = new SpannableString(post_by_user_name+ " ");
        wordtoSpan.setSpan(boldStyle, 0, (post_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        if (post_feeling.trim().length()>0) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                builder.append(wordtoSpan).append(" feeling ");

                if (post_feeling.equalsIgnoreCase("Very Good")) {
                    builder.append(" ", new ImageSpan(context, R.drawable.feeling_very_good), 0);
                } else if (post_feeling.equalsIgnoreCase("Good")) {
                    builder.append(" ", new ImageSpan(context, R.drawable.feeling_good), 0);
                } else if (post_feeling.equalsIgnoreCase("Neutral")) {
                    builder.append(" ", new ImageSpan(context, R.drawable.feeling_neutral), 0);
                } else if (post_feeling.equalsIgnoreCase("Bad")) {
                    builder.append(" ", new ImageSpan(context, R.drawable.feeling_bad), 0);
                } else if (post_feeling.equalsIgnoreCase("Very Bad")) {
                    builder.append(" ", new ImageSpan(context, R.drawable.feeling_very_bad), 0);
                }

                builder.append(" " + post_feeling);

            } else {
                // do something for phones running an SDK before lollipop
                builder.append(wordtoSpan).append(" feeling ").append(" ");

                if (post_feeling.equalsIgnoreCase("Very Good")) {
                    builder.setSpan(new ImageSpan(context, R.drawable.feeling_very_good),
                            builder.length() - 1, builder.length(), 0);
                } else if (post_feeling.equalsIgnoreCase("Good")) {
                    builder.setSpan(new ImageSpan(context, R.drawable.feeling_good),
                            builder.length() - 1, builder.length(), 0);
                } else if (post_feeling.equalsIgnoreCase("Neutral")) {
                    builder.setSpan(new ImageSpan(context, R.drawable.feeling_neutral),
                            builder.length() - 1, builder.length(), 0);
                } else if (post_feeling.equalsIgnoreCase("Bad")) {
                    builder.setSpan(new ImageSpan(context, R.drawable.feeling_bad),
                            builder.length() - 1, builder.length(), 0);
                } else if (post_feeling.equalsIgnoreCase("Very Bad")) {
                    builder.setSpan(new ImageSpan(context, R.drawable.feeling_very_bad),
                            builder.length() - 1, builder.length(), 0);
                }

                builder.append(" " + post_feeling);
            }

            userNameOfFeed.setText(builder);
        } else {
            userNameOfFeed.setText(wordtoSpan);
        }

        dateTextView.setText(post_created);

        if (post_photo.trim().length() > 0) {
            if (imageLoader == null) {
                imageLoader = AppController.getInstance().getImageLoader();
            }
            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + post_photo;
            postNImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //thumbNail.setLayoutParams(new RelativeLayout.LayoutParams(imageWidth, imageWidth));
            postNImageView.setImageUrl(meta_url, imageLoader);
            postNImageView.setVisibility(View.VISIBLE);
        }

        else if (post_video.trim().length() > 0) {
            postVideoView.setVisibility(View.VISIBLE);
            if (mediaController == null) {
                mediaController = new MediaController(this);
            }
            postVideoView.setMediaController(new MediaController(this));
            postVideoView.setVideoURI(Uri.parse(context.getResources().getString(R.string.UPLOADS_URL) + "video/" + post_video));
            //postFeedVideoView.setVideoPath(context.getResources().getString(R.string.UPLOADS_URL) + "video/" + post_video);
            postVideoView.requestFocus();
            postVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    postVideoView.start();
                    Log.e("video", "start");
                }
            });
        }

        description.setText(post_des);

        if (appUserGroupId.equals("4") || appUserGroupId.equals("5") || appUserGroupId.equals("6")) {
            if (post_userGroupId.equals("4") || post_userGroupId.equals("5") || post_userGroupId.equals("6")) {
            }
            else {
                likeCommentShareLayout.setVisibility(View.GONE);
            }
        }

        goodCountTV.setText(post_likes);
        answerCountTV.setText(post_answers);
        followCountTV.setText(post_follows);

        if (post_liked_by_me.trim().equalsIgnoreCase("0")) {
            //liked[0]  = true;
            //goodLayout.setBackgroundColor(Color.parseColor("#ffffff"));
            goodImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
            goodTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
        } else {
            //liked[0] = false;
            //goodLayout.setBackgroundColor(Color.parseColor("#e3f2fd"));
            goodImage.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
            goodTextView.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
