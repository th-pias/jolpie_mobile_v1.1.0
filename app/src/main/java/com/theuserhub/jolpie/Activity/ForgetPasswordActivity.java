package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Models.CompareModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForgetPasswordActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "ForgetPasswordActivity", tag_string_req = "forget_pass_str_req";

    private TextView loginTextView;
    private EditText emailEditText;
    private Button submitButton;
    private String emailString;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Password Reset");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        loginTextView = (TextView) findViewById(R.id.login_signup_tv);
        emailEditText = (EditText) findViewById(R.id.email_et);
        submitButton = (Button) findViewById(R.id.submit_btn);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailString = emailEditText.getText().toString().trim();
                if (emailString.length()>0) {
                    if (AppFunctions.isValidEmail(emailString)) {
                        fogetPasswordRequest();
                    }
                    else {
                        Toast.makeText(context, "Email address not valid.", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(context, "Please enter a valid email address.", Toast.LENGTH_LONG).show();
                }
            }
        });

        loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(context,LoginActivity.class);
                //startActivity(intent);
                finish();
            }
        });
    }

    private void fogetPasswordRequest() {
        pDialog.setMessage("Sending...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "forgetPasswordMail";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                Toast.makeText(context, "Please check your email.", Toast.LENGTH_LONG).show();
                emailEditText.setText("");

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Please check your email.", Toast.LENGTH_LONG).show();
                emailEditText.setText("");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email_address", emailString);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
