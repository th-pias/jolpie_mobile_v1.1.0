package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.MessagesListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.MessagesModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "MessageActivity", tag_string_req = "messages_str_req";

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    private ListView msgsListView;
    private List<MessagesModel> eachItems;
    private MessagesListAdapter messagesListAdapter;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Messages");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MessageActivity.this,MessageUserSearchActivity.class);
                startActivity(intent);
            }
        });

        userDatabaseHandler = new UserDatabaseHandler(MessageActivity.this);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        msgsListView = (ListView) findViewById(R.id.messages_lv);
        eachItems = new ArrayList<MessagesModel>();
        messagesListAdapter = new MessagesListAdapter(context, eachItems, appUserId);
        msgsListView.setAdapter(messagesListAdapter);

        getAllMessages();

        msgsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String temp,first_name,last_name,photo,user_group_id;
                if (!eachItems.get(position).getFrom_user_id().equals(appUserId)) {
                    temp = eachItems.get(position).getFrom_user_id();
                }
                else {
                    temp = eachItems.get(position).getTo_user_id();
                }
                first_name = eachItems.get(position).getTo_first_name();
                last_name = eachItems.get(position).getTo_last_name();
                photo = eachItems.get(position).getTo_photo();
                user_group_id = eachItems.get(position).getTo_group_id();

                Intent intent = new Intent(MessageActivity.this,ConversationActivity.class);
                intent.putExtra("to_user_id",temp);
                intent.putExtra("first_name",first_name);
                intent.putExtra("last_name",last_name);
                intent.putExtra("photo",photo);
                intent.putExtra("user_group_id",user_group_id);
                startActivity(intent);
            }
        });

    }

    private void getAllMessages() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "allMessages";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                try {
                    JSONArray allMsg = new JSONArray(response);
                    for (int i = 0; i < allMsg.length(); ++i) {
                        int flag = 0;
                        JSONObject singleMsg = (JSONObject) allMsg.get(i);
                        String msg_id = singleMsg.getString("sent_message_id");
                        String msg_from_id = singleMsg.getString("from_user_id");
                        String msg_to_id = singleMsg.getString("to_user_id");
                        String msg_text = singleMsg.getString("send_message");
                        String msg_ceated_at = singleMsg.getString("created_at");
                        String msg_fname = singleMsg.getString("first_name");
                        String msg_lname = singleMsg.getString("last_name");
                        String msg_photo = singleMsg.getString("photo");
                        String msg_user_group_id = singleMsg.getString("user_group_id");

                        for (int j = 0; j < eachItems.size(); ++j) {
                            if (eachItems.get(j).getFrom_user_id().equals(msg_from_id)
                                    && eachItems.get(j).getTo_user_id().equals(msg_to_id)) {
                                Date newDate = AppFunctions.getDate(msg_ceated_at);
                                Date oldDate = AppFunctions.getDate(eachItems.get(j).getMsg_created_at());
                                if (newDate.after(oldDate)) {
                                    eachItems.get(j).setMsg_text(msg_text);
                                    eachItems.get(j).setMsg_created_at(msg_ceated_at);
                                }
                                flag = 1;
                                break;
                            }
                            if (eachItems.get(j).getFrom_user_id().equals(msg_to_id)
                                    && eachItems.get(j).getTo_user_id().equals(msg_from_id)) {
                                Date newDate = AppFunctions.getDate(msg_ceated_at);
                                Date oldDate = AppFunctions.getDate(eachItems.get(j).getMsg_created_at());
                                if (newDate.after(oldDate)) {
                                    //swap from to....................................................
                                    String temp = eachItems.get(j).getFrom_user_id();
                                    eachItems.get(j).setFrom_user_id(eachItems.get(j).getTo_user_id());
                                    eachItems.get(j).setTo_user_id(temp);

                                    eachItems.get(j).setMsg_text(msg_text);
                                    eachItems.get(j).setMsg_created_at(msg_ceated_at);
                                }
                                flag = 1;
                                break;
                            }
                        }
                        if (flag == 0) {
                            eachItems.add(new MessagesModel(msg_id, msg_from_id, msg_to_id, msg_text, msg_ceated_at, msg_fname, msg_lname, msg_photo, msg_user_group_id));
                        }

                    }
                    //adapter notified
                    messagesListAdapter.notifyDataSetChanged();
                    //messagesListAdapter = new MessagesListAdapter(context, eachItems, appUserId);
                    //msgsListView.setAdapter(messagesListAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
               // Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllMessages();
    }
}
