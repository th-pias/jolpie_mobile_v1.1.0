package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.FeedListAdapter;
import com.theuserhub.jolpie.Adapters.ProfileUpdatesAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Fragments.ReviewDialogFragment;
import com.theuserhub.jolpie.Models.CategoryItem;
import com.theuserhub.jolpie.Models.NewsfeedModel;
import com.theuserhub.jolpie.Models.PostInfoModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileDetailNewActivity extends AppCompatActivity {
    private Context context = this;
    private String TAG = "ProfileDetailActivity", tag_string_req = "proDetail_str_req";

    private boolean isEnd;
    private int PICK_IMAGE_REQUEST = 1;
    private TextView emptyTextView, proDetail_speciality, proDetail_name, proDetail_degree, proDetail_company_name, proDetail_company_position,
            write_review, review_countTextView, messageImageView,
            saveImageView, serviceTextView, myMessageTextView;
    private RatingBar proDetail_rating;
    private LinearLayout profile_info_linlay, profile_chamber_linlay,
            profile_gallery_linlay, profile_doctors_linlay, profile_cabin_bed_linlay,
            appot_consult_Layout;
    private RelativeLayout appointmentLayout, consultLayout;
    private ImageView profileImg, bannerImageView;
    private NetworkImageView bannerNImageView, profileNImg;
    private Bitmap bitmap;
    private String user_id, user_fname, user_lname, user_email, user_image_url, group_id, isProfilePic;

    private ListView updatesListView;
    private List<NewsfeedModel> eachNewsfeed;
    private ProfileUpdatesAdapter updatesAdapter;
    private CategoryItem profileModel;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_detail_new);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent profileIntent = getIntent();
        user_id = profileIntent.getStringExtra("user_id");
        user_fname = profileIntent.getStringExtra("user_fname");
        user_lname = profileIntent.getStringExtra("user_lname");
        user_email = profileIntent.getStringExtra("user_email");
        group_id = profileIntent.getStringExtra("group_id");
        user_image_url = profileIntent.getStringExtra("user_image_url");

        List<SpecialityModel> emptyList = new ArrayList<SpecialityModel>();
        profileModel = new CategoryItem(user_id, user_fname, user_lname, user_image_url, "", group_id, "", "", "", "0", "0", emptyList, "0", "0", "0", "0", "0");

        isEnd = false;
        isProfilePic = "1";

        Typeface profileFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        myMessageTextView = (TextView) findViewById(R.id.message_tv);
        myMessageTextView.setTypeface(profileFont);

        emptyTextView = (TextView) findViewById(R.id.empty_tv);
        updatesListView = (ListView) findViewById(R.id.profile_updates_lv);

        final LayoutInflater header = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup detailViewGroup = (ViewGroup) header.inflate(R.layout.header_profile_detail_new, null);

        appot_consult_Layout = (LinearLayout) detailViewGroup.findViewById(R.id.appot_consult_layout);
        appointmentLayout = (RelativeLayout) detailViewGroup.findViewById(R.id.proDetail_appot_layout);
        TextView appotTextView = (TextView) detailViewGroup.findViewById(R.id.appot_iv);
        appotTextView.setTypeface(profileFont);

        consultLayout = (RelativeLayout) detailViewGroup.findViewById(R.id.proDetail_consult_layout);
        TextView consultTextView = (TextView) detailViewGroup.findViewById(R.id.consult_iv);
        consultTextView.setTypeface(profileFont);

        bannerImageView = (ImageView) detailViewGroup.findViewById(R.id.proDetail_banner_iv);
        bannerNImageView = (NetworkImageView) detailViewGroup.findViewById(R.id.proDetail_banner_niv);

        profileImg = (ImageView) detailViewGroup.findViewById(R.id.proDetail_item_profile_iv);
        profileNImg = (NetworkImageView) detailViewGroup.findViewById(R.id.proDetail_item_profile_niv);

        proDetail_name = (TextView) detailViewGroup.findViewById(R.id.proDetail_item_title_tv);
        proDetail_degree = (TextView) detailViewGroup.findViewById(R.id.proDetail_item_degree_tv);
        proDetail_speciality = (TextView) detailViewGroup.findViewById(R.id.proDetail_speciality_tv);
        proDetail_rating = (RatingBar) detailViewGroup.findViewById(R.id.proDetail_item_rating_star);
        review_countTextView = (TextView) detailViewGroup.findViewById(R.id.proDetail_item_reviews_tv);

        saveImageView = (TextView) detailViewGroup.findViewById(R.id.profile_save_iv);
        saveImageView.setTypeface(profileFont);

        messageImageView = (TextView) detailViewGroup.findViewById(R.id.profile_message_iv);
        messageImageView.setTypeface(profileFont);

        write_review = (TextView) detailViewGroup.findViewById(R.id.proDetail_item_writeRev_tv);
        write_review.setTypeface(profileFont);

        profile_info_linlay = (LinearLayout) detailViewGroup.findViewById(R.id.profile_info_linlay);
        TextView infoTextView = (TextView) detailViewGroup.findViewById(R.id.info_iv);
        infoTextView.setTypeface(profileFont);

        profile_chamber_linlay = (LinearLayout) detailViewGroup.findViewById(R.id.profile_chember_linlay);
        TextView chamberTextView = (TextView) detailViewGroup.findViewById(R.id.chamber_iv);
        chamberTextView.setTypeface(profileFont);
        serviceTextView = (TextView) detailViewGroup.findViewById(R.id.chamber_tv);

        profile_doctors_linlay = (LinearLayout) detailViewGroup.findViewById(R.id.profile_doctors_linlay);
        TextView doctorsTextView = (TextView) detailViewGroup.findViewById(R.id.doctors_iv);
        doctorsTextView.setTypeface(profileFont);

        profile_cabin_bed_linlay = (LinearLayout) detailViewGroup.findViewById(R.id.profile_cabin_bed_linlay);
        TextView cabinTextView = (TextView) detailViewGroup.findViewById(R.id.cabin_bed_iv);
        cabinTextView.setTypeface(profileFont);

        profile_gallery_linlay = (LinearLayout) detailViewGroup.findViewById(R.id.profile_gallery_linlay);
        TextView galleryTextView = (TextView) detailViewGroup.findViewById(R.id.gallery_iv);
        galleryTextView.setTypeface(profileFont);

        updatesListView.addHeaderView(detailViewGroup);

        eachNewsfeed = new ArrayList<NewsfeedModel>();
        updatesAdapter = new ProfileUpdatesAdapter(context, eachNewsfeed, user_id, appUserGroupId, isEnd, appUserId);
        updatesListView.setAdapter(updatesAdapter);

        updatingUi();

        getProfileDetail();
        getNewsfeeds();

        if (appUserId.equals(user_id)) {
            myMessageTextView.setVisibility(View.VISIBLE);
        }
        else {
            myMessageTextView.setVisibility(View.GONE);
        }
        myMessageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessageActivity.class);
                startActivity(intent);
            }
        });

    }

    private void getProfileDetail() {
        String getUserDetail_url = context.getResources().getString(R.string.MAIN_URL) + "getUserDetail";

        StringRequest getUserDetail_strReq = new StringRequest(Request.Method.POST,
                getUserDetail_url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                        List<SpecialityModel> specialityList = new ArrayList<SpecialityModel>();
                        String id, firstName, lastName, photo, banner, user_group_id, degree, type,
                                district, review_count, review_star, isSaved, isConsult, isReviewed, isSubscribed, isAppointment;
                        id = jsonObject.getString("id");
                        firstName = jsonObject.getString("first_name");
                        lastName = jsonObject.getString("last_name");
                        photo = jsonObject.getString("photo");
                        banner = jsonObject.getString("banner_image");
                        user_group_id = jsonObject.getString("user_group_id");
                        degree = jsonObject.getString("degree");
                        type = jsonObject.getString("type");//////
                        district = jsonObject.getString("district");
                        isConsult = jsonObject.getString("isConsult");
                        isReviewed = jsonObject.getString("isReviewed");
                        isSubscribed = jsonObject.getString("isSubscribed");
                        isAppointment = jsonObject.getString("isAppointment");

                        user_id = id;
                        user_fname = firstName;
                        user_lname = lastName;
                        user_image_url = photo;
                        group_id = user_group_id;

                        if (user_group_id.equals("2")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("speciality");
                            for (int j = 0; j < jsonArray1.length(); ++j) {
                                JSONObject jsonObject1 = (JSONObject) jsonArray1.get(j);
                                String speciality_id = jsonObject1.getString("specialization_id");
                                String speciality_name = jsonObject1.getString("name");

                                SpecialityModel specialityModel = new SpecialityModel(speciality_id, speciality_name);
                                specialityList.add(specialityModel);
                            }
                        }
                        if (user_group_id.equals("2") || user_group_id.equals("4") || user_group_id.equals("5")) {
                            JSONArray jsonArray2 = jsonObject.getJSONArray("review");
                            JSONObject jsonObject2 = (JSONObject) jsonArray2.get(0);

                            review_count = jsonObject2.getString("count");
                            review_star = jsonObject2.getString("rating");
                        } else {
                            review_count = "0";
                            review_star = "0";
                        }

                        isSaved = jsonObject.getString("isSaved");

                        profileModel = new CategoryItem(id, firstName, lastName, photo, banner, user_group_id, degree, type, district, review_count, review_star, specialityList, isSaved, isConsult, isReviewed, isSubscribed, isAppointment);

                        updatingUi();
                        //getNewsfeeds();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // Toast.makeText(context, "user detail error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("my_user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(getUserDetail_strReq, tag_string_req);
    }

    private void updatingUi() {

        int layout_weight = 0;
        if (appUserGroupId.equals("1") || appUserGroupId.equals("2") || appUserGroupId.equals("3")) {
            if (group_id.equals("2")) {
                if (appUserId.equals(user_id)) {
                    appointmentLayout.setVisibility(View.GONE);
                } else {
                    if (profileModel.getIsSubscribed().equals("0")) {
                        appointmentLayout.setVisibility(View.GONE);
                    }
                    else {
                        appointmentLayout.setVisibility(View.VISIBLE);
                        ++layout_weight;
                        if (layout_weight <= 1) {
                            appot_consult_Layout.setWeightSum(1.0f);
                        } else {
                            appot_consult_Layout.setWeightSum(2.0f);
                        }
                        appointmentLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(context, ChamberActivity.class);
                                intent.putExtra("name", "Dr. " + user_fname + " " + user_lname);
                                intent.putExtra("degree", profileModel.getDegree());
                                intent.putExtra("image_url", user_image_url);
                                intent.putExtra("user_id", user_id);
                                startActivity(intent);
                            }
                        });
                    }
                }
            } else {
                appointmentLayout.setVisibility(View.GONE);
            }
        } else {
            appointmentLayout.setVisibility(View.GONE);
        }

        if (appUserGroupId.equals("1") || appUserGroupId.equals("2") || appUserGroupId.equals("3")) {
            if (group_id.equals("2")) {
                if (appUserId.equals(user_id)) {
                        consultLayout.setVisibility(View.VISIBLE);
                        ++layout_weight;
                        if (layout_weight <= 1) {
                            appot_consult_Layout.setWeightSum(1.0f);
                        } else {
                            appot_consult_Layout.setWeightSum(2.0f);
                        }
                        consultLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Toast.makeText(context, "Consultation not working. Try again later", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(context, OnlineConsultancyActivity.class);
                                intent.putExtra("user_id", user_id);
                                startActivity(intent);
                            }
                        });
                } else {
                    if (profileModel.getIsSubscribed().equals("0")) {
                        consultLayout.setVisibility(View.GONE);
                    } else {
                        if (profileModel.getIsConsult().equals("0")) {
                            consultLayout.setVisibility(View.GONE);
                        } else {
                            consultLayout.setVisibility(View.VISIBLE);
                            ++layout_weight;
                            if (layout_weight <= 1) {
                                appot_consult_Layout.setWeightSum(1.0f);
                            } else {
                                appot_consult_Layout.setWeightSum(2.0f);
                            }
                            consultLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //Toast.makeText(context, "Consultation not working. Try again later", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(context, OnlineConsultancyActivity.class);
                                    intent.putExtra("user_id", user_id);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                }
            } else {
                consultLayout.setVisibility(View.GONE);
            }
        } else {
            consultLayout.setVisibility(View.GONE);
        }


        if (profileModel.getBanner().length() > 1) {
            imageLoader = null;
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.BANNER_URL) + profileModel.getBanner();
            bannerNImageView.setImageUrl(meta_url, imageLoader);
            //bannerImageView.setVisibility(View.GONE);
        }
        if (appUserId.equals(user_id)) {
            bannerImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu propicPopup = new PopupMenu(context, bannerImageView);
                    propicPopup.getMenuInflater().inflate(R.menu.pro_pic_popup, propicPopup.getMenu());
                    propicPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            isProfilePic = "0";
                            // Create intent to Open Image applications like Gallery, Google Photos
                            switch (item.getItemId()) {
                                case R.id.upload_pro_pic:
                                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    // Start the Intent
                                    startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
                                    return true;
                                case R.id.view_image:
                                    Intent i = new Intent(context, GalleryImageFullActivity.class);
                                    i.putExtra("flag", isProfilePic);
                                    i.putExtra("image", profileModel.getBanner());
                                    startActivity(i);
                                    return true;
                            }

                            return false;
                        }
                    });
                    propicPopup.show();
                }
            });

            bannerNImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu propicPopup = new PopupMenu(context, bannerImageView);
                    propicPopup.getMenuInflater().inflate(R.menu.pro_pic_popup, propicPopup.getMenu());
                    propicPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            isProfilePic = "0";
                            // Create intent to Open Image applications like Gallery, Google Photos
                            switch (item.getItemId()) {
                                case R.id.upload_pro_pic:
                                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    // Start the Intent
                                    startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
                                    return true;
                                case R.id.view_image:
                                    Intent i = new Intent(context, GalleryImageFullActivity.class);
                                    i.putExtra("flag", isProfilePic);
                                    i.putExtra("image", profileModel.getBanner());
                                    startActivity(i);
                                    return true;
                            }

                            return false;
                        }
                    });
                    propicPopup.show();
                }
            });
        }

        if (profileModel.getPhoto().length() > 1) {
            imageLoader = null;
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + profileModel.getPhoto();
            profileNImg.setImageUrl(meta_url, imageLoader);
        } else {
            if (group_id.equals("1") || group_id.equals("3")) {
                profileImg.setImageResource(R.drawable.patient_student_default);
            } else if (group_id.equals("2")) {
                profileImg.setImageResource(R.drawable.doctor_default);
            } else if (group_id.equals("4")) {
                profileImg.setImageResource(R.drawable.hospital_default);
            } else if (group_id.equals("5")) {
                profileImg.setImageResource(R.drawable.labs_default);
            } else if (group_id.equals("6")) {
                profileImg.setImageResource(R.drawable.pharmaceutical_default);
            }
        }

        if (appUserId.equals(user_id)) {
            profileImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu propicPopup = new PopupMenu(context, profileImg);
                    propicPopup.getMenuInflater().inflate(R.menu.pro_pic_popup, propicPopup.getMenu());
                    propicPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            isProfilePic = "1";
                            // Create intent to Open Image applications like Gallery, Google Photos
                            switch (item.getItemId()) {
                                case R.id.upload_pro_pic:
                                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    // Start the Intent
                                    startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
                                    return true;
                                case R.id.view_image:
                                    Intent i = new Intent(context, GalleryImageFullActivity.class);
                                    i.putExtra("image", user_image_url);
                                    i.putExtra("flag", isProfilePic);
                                    startActivity(i);
                                    return true;
                            }

                            return false;
                        }
                    });
                    propicPopup.show();

                }
            });
            profileNImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu propicPopup = new PopupMenu(context, profileImg);
                    propicPopup.getMenuInflater().inflate(R.menu.pro_pic_popup, propicPopup.getMenu());
                    propicPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            isProfilePic = "1";
                            // Create intent to Open Image applications like Gallery, Google Photos
                            switch (item.getItemId()) {
                                case R.id.upload_pro_pic:
                                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    // Start the Intent
                                    startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
                                    return true;
                                case R.id.view_image:
                                    Intent i = new Intent(context, GalleryImageFullActivity.class);
                                    i.putExtra("image", user_image_url);
                                    i.putExtra("flag", isProfilePic);
                                    startActivity(i);
                                    return true;
                            }

                            return false;
                        }
                    });
                    propicPopup.show();

                }
            });
        }

        if (group_id.equals("2")) {
            proDetail_degree.setText(profileModel.getDegree());
            proDetail_name.setText("Dr. " + profileModel.getFirstName() + " " + profileModel.getLastName());
        } else if (group_id.equals("4")) {
            if (profileModel.getType().equals("1")) {
                proDetail_degree.setText("General Hospital");
            } else if (profileModel.getType().equals("2")) {
                proDetail_degree.setText("Specialized Hospital");
            } else if (profileModel.getType().equals("3")) {
                proDetail_degree.setText("Clinic");
            } else {
                proDetail_degree.setVisibility(View.GONE);
            }
            proDetail_name.setText(profileModel.getFirstName() + " " + profileModel.getLastName());
        } else {
            proDetail_degree.setVisibility(View.GONE);
            proDetail_name.setText(profileModel.getFirstName() + " " + profileModel.getLastName());
        }

        if (group_id.equals("2")) {

            List<SpecialityModel> specialityList = profileModel.getSpecialityModels();
            if (specialityList.size() <= 0) {
                proDetail_speciality.setVisibility(View.GONE);
            } else {
                proDetail_speciality.setVisibility(View.VISIBLE);
                String tempSpeciality = "";
                for (int i = 0; i < specialityList.size(); ++i) {
                    if (i != 0) {
                        tempSpeciality = tempSpeciality + ", ";
                    }
                    tempSpeciality = tempSpeciality + specialityList.get(i).getName();
                }
                proDetail_speciality.setText(tempSpeciality);
            }
        } else {
            proDetail_speciality.setVisibility(View.GONE);
        }


        if (group_id.equals("2") || group_id.equals("4") || group_id.equals("5")) {
            if (profileModel.getReview_count().equals("0")) {
                review_countTextView.setText("0 Review");
                proDetail_rating.setRating(0.0f);
            } else {
                if (profileModel.getReview_count().equals("1")) {
                    //review_count.setText("1 Review");
                    review_countTextView.setText(Html.fromHtml("<u>1 Review</u>"));
                    proDetail_rating.setRating(Float.parseFloat(profileModel.getReview_star()));
                } else {
                    //review_count.setText(rev_count + " Reviews");
                    review_countTextView.setText(Html.fromHtml("<u>" + profileModel.getReview_count() + " Review</u>"));
                    float sum = Float.parseFloat(profileModel.getReview_star());
                    float count = Float.parseFloat(profileModel.getReview_count());
                    float temp = sum / count;
                    proDetail_rating.setRating(temp);
                }

                review_countTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ReviewListActivity.class);
                        intent.putExtra("reviewer_id", user_id);
                        startActivity(intent);
                    }
                });
            }
        }
        else {
            proDetail_rating.setVisibility(View.GONE);
            review_countTextView.setVisibility(View.GONE);
        }

        if (group_id.equals("2")) {
            if (appUserId.equals(user_id)) {
                saveImageView.setVisibility(View.GONE);
            } else {
                if (profileModel.getIsSaved().equals("0")) {
                    saveImageView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                } else {
                    saveImageView.setTextColor(ContextCompat.getColor(context, R.color.red_900));
                }

                saveImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (profileModel.getIsSaved().equals("0")) {
                            saveDoctorRequest();
                            //saveTextView.setText("Saved");
                            saveImageView.setTextColor(ContextCompat.getColor(context, R.color.red_900));
                            profileModel.setIsSaved("1");
                        } else {
                            unsaveDoctorRequest();
                            //saveTextView.setText("Save");
                            saveImageView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                            profileModel.setIsSaved("0");
                        }
                    }
                });
            }
        } else {
            saveImageView.setVisibility(View.GONE);
        }

        if (appUserGroupId.equals(group_id)) {
            if (appUserId.equals(user_id)) {
                messageImageView.setVisibility(View.GONE);
            } else {
                messageImageView.setVisibility(View.VISIBLE);
                messageImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ConversationActivity.class);
                        intent.putExtra("to_user_id", user_id);
                        intent.putExtra("first_name", user_fname);
                        intent.putExtra("last_name", user_lname);
                        intent.putExtra("photo",user_image_url);
                        intent.putExtra("user_group_id",group_id);
                        startActivity(intent);
                    }
                });
            }
        } else {
            messageImageView.setVisibility(View.GONE);
        }

        if ((group_id.equals("2") || group_id.equals("4") || group_id.equals("5"))) {
            if (appUserId.equals(user_id)) {
                write_review.setVisibility(View.GONE);
            } else {
                if (profileModel.getIsReviewed().equals("0")) {
                    write_review.setVisibility(View.VISIBLE);
                    write_review.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putString("flag", "prodtl");
                            bundle.putString("user_id", appUserId);
                            bundle.putString("reviewer_id", user_id);
                            ReviewDialogFragment reviewDialog = new ReviewDialogFragment();
                            reviewDialog.setArguments(bundle);
                            reviewDialog.show(getSupportFragmentManager(), "My Review");
                        }
                    });
                } else {
                    write_review.setVisibility(View.GONE);
                }
            }
        }
        else {
            write_review.setVisibility(View.GONE);
        }

        profile_info_linlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileInfoActivity.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("user_fname", user_fname);
                intent.putExtra("user_lname", user_lname);
                intent.putExtra("group_id", group_id);
                startActivity(intent);
            }
        });

        if (group_id.equals("1")) {
            if (appUserId.equals(user_id)) {
                serviceTextView.setText("Conditions");
                profile_chamber_linlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, MyConditionActivity.class);
                        i.putExtra("user_id", user_id);
                        startActivity(i);
                    }
                });
            } else {
                profile_chamber_linlay.setVisibility(View.GONE);
            }
        } else if (group_id.equals("2")) {
            serviceTextView.setText("Chambers");
            profile_chamber_linlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ChamberActivity.class);
                    intent.putExtra("name", "Dr. " + user_fname + " " + user_lname);
                    intent.putExtra("degree", profileModel.getDegree());
                    intent.putExtra("image_url", user_image_url);
                    intent.putExtra("user_id", user_id);
                    startActivity(intent);
                }
            });

        } else if (group_id.equals("3")) {
            if (appUserId.equals(user_id)) {
                serviceTextView.setText("Conditions");
                profile_chamber_linlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, MyConditionActivity.class);
                        i.putExtra("user_id", user_id);
                        startActivity(i);
                    }
                });
            } else {
                profile_chamber_linlay.setVisibility(View.GONE);
            }
            //profile_chamber_linlay.setVisibility(View.GONE);
        } else if (group_id.equals("4")) {
            serviceTextView.setText("Services");
            profile_chamber_linlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ServicesActivity.class);
                    intent.putExtra("user_id", user_id);
                    startActivity(intent);
                }
            });

        } else if (group_id.equals("5")) {
            serviceTextView.setText("Tests");
            profile_chamber_linlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ServicesActivity.class);
                    intent.putExtra("user_id", user_id);
                    startActivity(intent);
                }
            });

        } else if (group_id.equals("6")) {
            serviceTextView.setText("Drugs");
            profile_chamber_linlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, UserDrugActivity.class);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("first_name", user_fname);
                    startActivity(intent);
                }
            });

        }

        if (group_id.equals("4") || group_id.equals("5")) {
            profile_doctors_linlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HospitalsDoctorsActivity.class);
                    intent.putExtra("user_id", user_id);
                    startActivity(intent);
                }
            });
        } else {
            profile_doctors_linlay.setVisibility(View.GONE);
        }

        if (group_id.equals("4")) {
            profile_cabin_bed_linlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, HospitalsCabinBedActivity.class);
                    intent.putExtra("user_id", user_id);
                    startActivity(intent);
                }
            });
        } else {
            profile_cabin_bed_linlay.setVisibility(View.GONE);
        }


        profile_gallery_linlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, GalleryActivity.class);
                intent.putExtra("user_id", user_id);
                startActivity(intent);
            }
        });
    }

    private void getNewsfeeds() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getProfileUpdates";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                eachNewsfeed.clear();
                String id, section, section_id, activity, activity_at, by_user_id, by_user_fname, by_user_lname, by_user_photo, by_user_group_id;
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() <= 0) {
                        //emptyTextView.setVisibility(View.VISIBLE);
                        //emptyTextView.setText("You have no Updates");
                        //updatesListView.setVisibility(View.GONE);
                    } else {
                        emptyTextView.setVisibility(View.GONE);
                        updatesListView.setVisibility(View.VISIBLE);
                        if (jsonArray.length() < 5) {
                            isEnd = true;
                        }
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            //eac
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            id = jsonObject.getString("newsfeed_id");
                            section = jsonObject.getString("section");
                            section_id = jsonObject.getString("section_id");
                            activity = jsonObject.getString("activity");
                            activity_at = jsonObject.getString("activity_at");
                            by_user_id = jsonObject.getString("by_user_id");
                            by_user_fname = jsonObject.getString("by_user_fname");
                            by_user_lname = jsonObject.getString("by_user_lname");
                            by_user_photo = jsonObject.getString("by_user_photo");
                            by_user_group_id = jsonObject.getString("by_user_group_id");

                            JSONObject infoJsonObject = jsonObject.getJSONObject("info");

                            String post_id, title, description, feeling, photo, video, created_at,
                                    updated_at, by_id, by_fname, by_lname, by_photo, by_group_id,
                                    to_id, to_fname, to_lname, to_photo, to_user_group_id,
                                    like_count, answer_count, share_count, star_count, liked_by_me, shared_by_me;
                            post_id = infoJsonObject.getString("section_post_id");
                            title = infoJsonObject.getString("section_title");
                            description = infoJsonObject.getString("section_description");
                            feeling = infoJsonObject.getString("section_feeling").trim();
                            photo = infoJsonObject.getString("section_photo");
                            video = infoJsonObject.getString("section_video");
                            created_at = infoJsonObject.getString("section_at");
                            updated_at = infoJsonObject.getString("section_updated_at");
                            by_id = infoJsonObject.getString("section_by");
                            by_fname = infoJsonObject.getString("section_by_fname");
                            by_lname = infoJsonObject.getString("section_by_lname");
                            by_photo = infoJsonObject.getString("section_by_photo");
                            by_group_id = infoJsonObject.getString("section_by_user_group_id");
                            to_id = infoJsonObject.getString("section_to");
                            to_fname = infoJsonObject.getString("section_to_fname");
                            to_lname = infoJsonObject.getString("section_to_lname");
                            to_photo = infoJsonObject.getString("section_to_photo");
                            to_user_group_id = infoJsonObject.getString("section_to_user_group_id");
                            like_count = infoJsonObject.getString("good_count");
                            answer_count = infoJsonObject.getString("answer_count");
                            share_count = infoJsonObject.getString("share_count");
                            star_count = infoJsonObject.getString("star_count");
                            liked_by_me = infoJsonObject.getString("liked_by_me");
                            shared_by_me = infoJsonObject.getString("shared_by_me");

                            PostInfoModel infoModel = new PostInfoModel(post_id, title, description, feeling, photo, video, created_at,
                                    updated_at, by_id, by_fname, by_lname, by_photo, by_group_id,
                                    to_id, to_fname, to_lname, to_photo, to_user_group_id,
                                    like_count, answer_count, share_count, star_count, liked_by_me, shared_by_me);

                            NewsfeedModel newsfeedModel = new NewsfeedModel(id, section, section_id, activity, activity_at, by_user_id, by_user_fname, by_user_lname, by_user_photo, by_user_group_id, infoModel);

                            eachNewsfeed.add(newsfeedModel);
                        }
                        //adapter.notifyDataSetChanged();
                        updatesAdapter = new ProfileUpdatesAdapter(context, eachNewsfeed, user_id, appUserGroupId, isEnd, appUserId);
                        updatesListView.setAdapter(updatesAdapter);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //emptyTextView.setVisibility(View.VISIBLE);
                //emptyTextView.setText("Can't load Updates.\nTry again.");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("user_group_id", group_id);
                params.put("start_index", "0");
                params.put("take_total", "5");
                //Log.d(TAG,+appUserId+magazine_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void saveDoctorRequest() {

        String url = context.getResources().getString(R.string.MAIN_URL) + "saveDoctor";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("doctor_id", user_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void unsaveDoctorRequest() {

        String url = context.getResources().getString(R.string.MAIN_URL) + "unsaveDoctor";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("doctor_id", user_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                String path = getRealPathFromURI(filePath);

                Log.e("path1", path);
                Intent i = new Intent(context, ProfileImageUpload.class);
                i.putExtra("path", path);
                i.putExtra("flag", isProfilePic);
                startActivity(i);
                //Setting the Bitmap to ImageView
                //profileImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    public void updateReviewProfile() {
        write_review.setVisibility(View.GONE);
        //reviewCountRequest(user_id);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfileDetail();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (appUserId.equals(user_id))
            getMenuInflater().inflate(R.menu.menu_profile_detail, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        /*if (id == R.id.action_message) {
            Intent intent = new Intent(context, MessageActivity.class);
            startActivity(intent);
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
