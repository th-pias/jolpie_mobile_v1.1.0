package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.theuserhub.jolpie.Adapters.PlaceAutocompleteAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Fragments.DatePickerFragment;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditAboutActivity extends AppCompatActivity {

    private Context context=this;
    private static String TAG = "EditAboutActivity";
    private String tag_string_req = "string_req";

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName,appUserLName,appUserEmail,appUserId,appUserGroupId,appUserImgUrl;

    private String user_id="",user_fname="",user_lname="",user_email="",group_id="",item_contactNo1="",item_contactNo2="",item_street_address="",item_address="",item_birth_day="",item_birth_month="",item_birth_year="",item_degree="",item_type="",genderString="",item_gender="";
    private Spinner daySpinner,monthSpinner,yearSpinner,genderSpinner,typeSpinner;
    private List<String> dayList,monthList,yearList,genderList,typeList;
    protected GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter mAdapter;
    private AutoCompleteTextView mAutocompleteView;
    private TextView mPlaceDetailsText;
    private TextView mPlaceDetailsAttribution;
    private Button datePickerBtn;
    private PopupWindow date_of_birth_pw;

    /*private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));*/
    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(21.335302, 89.203639), new LatLng(25.045384, 92.307729));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_about);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        /*Toast.makeText(context,
                                "connected to Google API Client: Error " + bundle.toString(),
                                Toast.LENGTH_SHORT).show();*/
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        /*Toast.makeText(context,
                                "connecting suspended to Google API Client: Error ",
                                Toast.LENGTH_SHORT).show();*/
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        Log.e(TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                                + connectionResult.getErrorCode());

                        // TODO(Developer): Check error code and notify the user of error state and resolution.
                        Toast.makeText(context,
                                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .build();

        mGoogleApiClient.connect();

        /*mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API) //.addApi(Places.PLACE_DETECTION_API)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {

                    }
                })
                .build();*/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit About");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl=user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent editIntent = getIntent();
        user_id = editIntent.getStringExtra("user_id");
        user_fname = editIntent.getStringExtra("user_fname");
        user_lname = editIntent.getStringExtra("user_lname");
        user_email = editIntent.getStringExtra("user_email");
        group_id = editIntent.getStringExtra("group_id");
        item_contactNo1= editIntent.getStringExtra("contact_no_1" );
        item_contactNo2= editIntent.getStringExtra("contact_no_2" );
        item_street_address = editIntent.getStringExtra("street_address" );
        item_address = editIntent.getStringExtra("address" );
        item_birth_day = editIntent.getStringExtra("birth_day"  );
        item_birth_month = editIntent.getStringExtra("birth_month" );
        item_birth_year = editIntent.getStringExtra("birth_year" );
        item_degree = editIntent.getStringExtra("degree" );
        item_type = editIntent.getStringExtra("type" );
        item_gender = editIntent.getStringExtra("gender" );

        final EditText inFName = (EditText) findViewById(R.id.edit_about_firstname_et);
        final EditText inLName = (EditText) findViewById(R.id.edit_about_lastname_et);
        final EditText inDesignation = (EditText) findViewById(R.id.edit_about_designation_et);
        final EditText inMobile = (EditText) findViewById(R.id.edit_about_mobile_et);
        final EditText inMobile2 = (EditText) findViewById(R.id.edit_about_mobile2_et);
        final EditText inStreetAddress = (EditText) findViewById(R.id.edit_about_street_et);
        final EditText inAreaAddress = (EditText) findViewById(R.id.autocomplete_places);
        TextView doBirthTextView = (TextView) findViewById(R.id.date_of_birth);
        daySpinner = (Spinner) findViewById(R.id.spinner_day);
        monthSpinner = (Spinner) findViewById(R.id.spinner_month);
        yearSpinner = (Spinner) findViewById(R.id.spinner_year);
        TextView genderTextView = (TextView) findViewById(R.id.edit_about_gender);
        genderSpinner = (Spinner) findViewById(R.id.gender_spinner);
        TextView typeTextView = (TextView) findViewById(R.id.edit_about_type);
        typeSpinner = (Spinner) findViewById(R.id.type_spinner);

        TextView editSaveBtn = (TextView) findViewById(R.id.edit_about_save_btn);
        TextView editCancelBtn = (TextView) findViewById(R.id.edit_about_cancel_btn);

        inFName.setText(appUserFName);
        if (appUserGroupId.equals("1") || appUserGroupId.equals("2") || appUserGroupId.equals("3")) {
            inLName.setText(appUserLName);

            typeSpinner.setVisibility(View.GONE);
            typeTextView.setVisibility(View.GONE);

            doBirthTextView.setText("Date of Birth : ");
            if (appUserGroupId.equals("1")) {
                inDesignation.setVisibility(View.GONE);
            }
            if (appUserGroupId.equals("2")) {

            }
            if (appUserGroupId.equals("3")) {
                inDesignation.setVisibility(View.GONE);
            }
        }
        else {
            inLName.setVisibility(View.GONE);
            inDesignation.setVisibility(View.GONE);
            inFName.setHint("Name");

            genderTextView.setVisibility(View.GONE);
            genderSpinner.setVisibility(View.GONE);

            doBirthTextView.setText("Date of Stablishment : ");

            if (appUserGroupId.equals("4")) {

            }
            if (appUserGroupId.equals("5")) {
                typeSpinner.setVisibility(View.GONE);
            }
            if (appUserGroupId.equals("6")) {
                typeSpinner.setVisibility(View.GONE);
            }
        }

        inDesignation.setText(item_degree);
        inMobile.setText(item_contactNo1);
        inMobile2.setText(item_contactNo2);
        inStreetAddress.setText(item_street_address);
        inAreaAddress.setText(item_address);

        dayList = new ArrayList<String>();
        monthList = new ArrayList<String>();
        yearList = new ArrayList<String>();
        genderList = new ArrayList<String>();
        typeList = new ArrayList<String>();

        insertDateData();

        ArrayAdapter<String> daySpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dayList);
        daySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daySpinner.setAdapter(daySpinnerAdapter);
        if (!item_birth_day.equals("0") && item_birth_day.length()>0 && item_birth_day!=null) {
            daySpinner.setSelection(Integer.parseInt(item_birth_day.trim())-1);
        }

        ArrayAdapter<String> monthSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, monthList);
        monthSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthSpinner.setAdapter(monthSpinnerAdapter);
        if (!item_birth_month.trim().equals("0") && item_birth_month.length()>0 && item_birth_month!=null) {
            monthSpinner.setSelection(Integer.parseInt(item_birth_month.trim())-1);
        }

        ArrayAdapter<String> yearSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, yearList);
        yearSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(yearSpinnerAdapter);
        if (!item_birth_year.trim().equals("0") && item_birth_year.length()>0 && item_birth_year!=null) {
            yearSpinner.setSelection(Integer.parseInt(item_birth_year.trim())-1901);
        }

        ArrayAdapter<String> genderSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderList);
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderSpinnerAdapter);
        if (!item_gender.trim().equals("0") && item_gender.length()>0 && item_gender!=null) {
            try {
                genderSpinner.setSelection(Integer.parseInt(item_gender.trim()) - 1);
            }catch (NumberFormatException e) {
                Log.e("gender",item_gender);
            }
        }

        ArrayAdapter<String> typeSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, typeList);
        typeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        if (!item_type.trim().equals("0") && item_type.length()>0 && item_type!=null) {
            monthSpinner.setSelection(Integer.parseInt(item_type.trim())-1);
        }

        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item_birth_day = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item_birth_month = String.valueOf(position+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item_birth_year = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genderString = String.valueOf(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item_type = String.valueOf(position+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_fname = inFName.getText().toString();
                user_lname = inLName.getText().toString();
                item_degree = inDesignation.getText().toString();
                item_contactNo1 = inMobile.getText().toString();
                item_contactNo2 = inMobile2.getText().toString();
                item_street_address = inStreetAddress.getText().toString();
                item_address = inAreaAddress.getText().toString();

                updateAboutInfo();
            }
        });

        editCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        // Retrieve the AutoCompleteTextView that will display Place suggestions.
        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);

        // Register a listener that receives callbacks when a suggestion has been selected
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        // Retrieve the TextViews that will display details and attributions of the selected place.
        mPlaceDetailsText = (TextView) findViewById(R.id.place_details);
        mPlaceDetailsAttribution = (TextView) findViewById(R.id.place_attribution);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY, null);
        mAutocompleteView.setAdapter(mAdapter);

        // Set up the 'clear text' button that clears the text in the autocomplete view
        Button clearButton = (Button) findViewById(R.id.button_clear);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutocompleteView.setText("");
            }
        });


        datePickerBtn = (Button) findViewById(R.id.btn_date_of_birth);
        datePickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
                }
        });
    }

    private void insertDateData() {

        for (int i=1;i<=31;++i) {
            dayList.add(String.valueOf(i));
        }

        monthList.add("January");
        monthList.add("February");
        monthList.add("March");
        monthList.add("April");
        monthList.add("May");
        monthList.add("June");
        monthList.add("July");
        monthList.add("August");
        monthList.add("September");
        monthList.add("October");
        monthList.add("November");
        monthList.add("December");

        for (int i=1901;i<=2100;++i) {
            yearList.add(String.valueOf(i));
        }

        genderList.add("Male");
        genderList.add("Female");

        typeList.add("General Hospital");
        typeList.add("Specialized Hospital");
        typeList.add("Clinic");
    }

    /*@Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }*/

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void updateAboutInfo() {
        String url = getResources().getString(R.string.MAIN_URL)+"updateGeneralInfo";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                finish();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Some error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("first_name", user_fname);
                params.put("last_name", user_lname);
                params.put("contact_no_1", item_contactNo1);
                params.put("contact_no_2", item_contactNo2);
                params.put("street_address", item_street_address);
                params.put("address", item_address);
                params.put("birth_day", item_birth_day);
                params.put("birth_month", item_birth_month);
                params.put("birth_year", item_birth_year);
                params.put("degree", item_degree);
                params.put("type", item_type);
                params.put("gender", genderString);

                Log.e("month",item_degree+": \n"+ item_type+"\n"+item_gender);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Toast.makeText(getApplicationContext(), "Clicked: " + primaryText, Toast.LENGTH_SHORT).show();
            Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);
            LatLng latLng = place.getLatLng();
            //latLng.latitude;
            // Format details of the place for display and show it in a TextView.
            mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
                    place.getId(), place.getAddress(), place.getPhoneNumber(),
                    place.getWebsiteUri()));

            // Display the third party attributions if set.
            final CharSequence thirdPartyAttribution = places.getAttributions();
            if (thirdPartyAttribution == null) {
                mPlaceDetailsAttribution.setVisibility(View.GONE);
            } else {
                mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
                mPlaceDetailsAttribution.setText(Html.fromHtml(thirdPartyAttribution.toString()));
            }

            Log.i(TAG, "Place details received: " + place.getName());

            places.release();
        }
    };

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        Log.e(TAG, res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }

    /**
     * Called when the Activity could not connect to Google Play services and the auto manager
     * could resolve the error automatically.
     * In this case the API is not available and notify the user.
     *
     * connectionResult can be inspected to determine the cause of the failure
     */
    /*@Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.e(TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
