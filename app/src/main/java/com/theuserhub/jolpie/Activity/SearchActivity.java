package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.theuserhub.jolpie.Adapters.CategorizedAdapter;
import com.theuserhub.jolpie.Adapters.PlaceAutocompleteAdapter;
import com.theuserhub.jolpie.Adapters.SearchAutoCompleteAdapter;
import com.theuserhub.jolpie.Adapters.SearchScrolledTabAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.AutoCompleteModel;
import com.theuserhub.jolpie.Models.CategoryItem;
import com.theuserhub.jolpie.Models.ChamberExModel;
import com.theuserhub.jolpie.Models.ScheduleModel;
import com.theuserhub.jolpie.Models.SearchFilterModel;
import com.theuserhub.jolpie.Models.SearchUserModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.RangeSeekBar;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Context context = this;
    private static String TAG = "SearchActivity";
    private String tag_string_req = "string_req";

    private ProgressDialog pDialog;
    private ListView catItemListView;
    private List<SearchFilterModel> eachFilter;
    private List<SearchUserModel> eachItems, eachItemsTemp;
    private List<String> compareIds;
    private CategorizedAdapter categorizedAdapter;
    private Button compareButton;

    private TextView searchClickImageView, emptyTextView, nearMeTextView, locationOnOffTextView, filterTextView,
            filter1TextView, filter2TextView, filterBackTextView, rangeMinTextView, rangeMaxTextView,
            availAnyTextView, availSunTextView, availMonTextView, availTueTextView, availWedTextView, availThuTextView, availFriTextView, availSatTextView;
    private boolean isEnd, isFilter, isLocation, isConsultFilter;
    private AutoCompleteTextView searchAutoTextView;
    private Spinner filterSpinner;
    private ArrayAdapter<AutoCompleteModel> autoCompleteAdapter;
    private AutoCompleteModel[] objectItemData;
    private String search_type, search_item, latLngArray, spinner_text, speciality_type, latitude_str, longitude_str,
            selected_day_string;
    private int selected_min_fee, selected_max_fee;
    private String[] searchTypes = {"Doctors", "Hospitals", "Labs", "Drugs", "Groups"};
    //private RelativeLayout progressLayout;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    //private List<String> searchTabList;
    //private ViewPager searchPager;
    //private String[] tabs_search = {"Doctors", "Hospitals", "Labs", "Drugs"};
    //private TextView filterDone,filterCancel;
    //private List<AutoCompleteModel> mList;
    //SearchAutoCompleteAdapter autoCompleteAdapter;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters

    private PlaceAutocompleteAdapter mAdapter;
    private AutoCompleteTextView mAutocompleteView;
    private RelativeLayout locationLayout;
    private LinearLayout filterLayout, checkboxFilterLayout, rangeBarLayout, availDayLayout;
    private CheckBox consultFilterCheckBox;
    private RangeSeekBar rangeSeekBar;

    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(20.7464107, 88.0085887), new LatLng(26.6342434, 92.6801153));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Find Health Care");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        Typeface searchFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        spinner_text = "Doctors";
        isFilter = false;
        isEnd = false;
        isLocation = false;
        isConsultFilter = false;
        search_type = "2";
        search_item = "";
        speciality_type = "0";
        latitude_str = "";
        longitude_str = "";
        selected_min_fee = 0;
        selected_max_fee = 1000;
        selected_day_string = "any";
        //mList = new ArrayList<AutoCompleteModel>();

        /*for (int i=0; i<=5;i++)
        {
            mList.add(new AutoCompleteModel("title"+i,"type"+i,"count"+i,"image"+i));
        }*/

        compareButton = (Button) findViewById(R.id.compare_button);

        catItemListView = (ListView) findViewById(R.id.categoryItem_lv);

        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);
        nearMeTextView = (TextView) findViewById(R.id.near_me_tv);
        nearMeTextView.setTypeface(searchFont);
        emptyTextView = (TextView) findViewById(R.id.empty_tv);
        filterBackTextView = (TextView) findViewById(R.id.filter_back_tv);
        locationLayout = (RelativeLayout) findViewById(R.id.location_layout);
        locationOnOffTextView = (TextView) findViewById(R.id.location_on_off_tv);
        locationOnOffTextView.setTypeface(searchFont);
        filterTextView = (TextView) findViewById(R.id.search_filter_tv);
        filterTextView.setTypeface(searchFont);
        filterLayout = (LinearLayout) findViewById(R.id.search_filter_layout);
        filter1TextView = (TextView) findViewById(R.id.filter_one_tv);
        filter2TextView = (TextView) findViewById(R.id.filter_two_tv);
        checkboxFilterLayout = (LinearLayout) findViewById(R.id.checkbox_filter_layout);
        consultFilterCheckBox = (CheckBox) findViewById(R.id.consult_filter_cb);
        //progressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        searchAutoTextView = (AutoCompleteTextView) findViewById(R.id.search_autoTextView);
        searchAutoTextView.setHint("Search doctors by name/speciality");
        searchAutoTextView.setThreshold(2);

        rangeBarLayout = (LinearLayout) findViewById(R.id.range_seekbar_layout);
        rangeMinTextView = (TextView) findViewById(R.id.range_min_tv);
        rangeMaxTextView = (TextView) findViewById(R.id.range_max_tv);
        rangeSeekBar = new RangeSeekBar(0, 1000, context);
        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Integer minValue, Integer maxValue) {
                Log.e("Value", minValue + " : " + maxValue);
                rangeMinTextView.setText("Min :  " + minValue);
                rangeMaxTextView.setText("Max :  " + maxValue);
                selected_min_fee = minValue;
                selected_max_fee = maxValue;

                redundantFilterLogic();
                /*eachItemsTemp.clear();
                int condFlag = 0;
                for (int l=0;l<eachFilter.size();++l) {
                    if (eachFilter.get(l).iscked()) {
                        ++condFlag;
                        addLocationItems(l);
                    }
                }

                if (selected_min_fee>0 || selected_max_fee<1000) {
                    if (condFlag == 0) {
                        addFeeRangeItemsMain(selected_min_fee,selected_max_fee);
                    }
                    else {
                        addFeeRangeItems(selected_min_fee,selected_max_fee);
                    }
                }

                if (!selected_day_string.equalsIgnoreCase("any")) {
                    if (condFlag == 0) {
                        addAvailableDayItemsMain(selected_day_string);
                    }
                    else {
                        addAvailableDayItems(selected_day_string);
                    }
                    ++condFlag;
                }

                if (isConsultFilter) {
                    if (condFlag == 0) {
                        addConsultItemsMain();
                    }
                    else {
                        addConsultItems();
                    }
                    ++condFlag;
                }

                if (condFlag == 0) {
                    categorizedAdapter = new CategorizedAdapter(context, eachItems, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                    catItemListView.setAdapter(categorizedAdapter);
                }
                else {
                    categorizedAdapter = new CategorizedAdapter(context, eachItemsTemp, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                    catItemListView.setAdapter(categorizedAdapter);
                }*/
            }
        });

        rangeBarLayout.addView(rangeSeekBar);

        availDayLayout = (LinearLayout) findViewById(R.id.avail_day_layout);
        availAnyTextView = (TextView) findViewById(R.id.avail_any_tv);
        availSunTextView = (TextView) findViewById(R.id.avail_sun_tv);
        availMonTextView = (TextView) findViewById(R.id.avail_mon_tv);
        availTueTextView = (TextView) findViewById(R.id.avail_tue_tv);
        availWedTextView = (TextView) findViewById(R.id.avail_wed_tv);
        availThuTextView = (TextView) findViewById(R.id.avail_thu_tv);
        availFriTextView = (TextView) findViewById(R.id.avail_fri_tv);
        availSatTextView = (TextView) findViewById(R.id.avail_sat_tv);

        daySelectionUi();


        //filterImageView = (ImageView) findViewById(R.id.search_filter_iv);
        //final LinearLayout filter_settings_layout = ( LinearLayout) findViewById(R.id.search_filter_linlay);
        //filterDone = (TextView) findViewById(R.id.search_filter_done_tv);
        //filterCancel = (TextView) findViewById(R.id.search_filter_cancel_tv);
        //locationImageView = (ImageView) findViewById(R.id.search_location_iv);

        filterSpinner = (Spinner) findViewById(R.id.search_filter_sp);

        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();

            createLocationRequest();
        }

        // Register a listener that receives callbacks when a suggestion has been selected
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY, null);
        mAutocompleteView.setAdapter(mAdapter);

        locationOnOffTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLocation) {
                    locationLayout.setVisibility(View.GONE);
                    isLocation = false;
                    mAutocompleteView.setText("");
                    latitude_str = "";
                    longitude_str = "";
                } else {
                    locationLayout.setVisibility(View.VISIBLE);
                    isLocation = true;
                    mAutocompleteView.setText("");
                    latitude_str = "";
                    longitude_str = "";
                }
            }
        });

        nearMeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayLocation();
            }
        });

        eachFilter = new ArrayList<>();
        AutoCompleteModel[] data = new AutoCompleteModel[0];
        autoCompleteAdapter = new SearchAutoCompleteAdapter(this, R.layout.list_row_autocomplete, data);
        searchAutoTextView.setAdapter(autoCompleteAdapter);

        //add speciality into list
        //searchSpeciality();
        //add items into list
        //searchItems();
        searchAutoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchAutoTextView.getText().toString().trim().length() > 0) {
                    search_item = searchAutoTextView.getText().toString();
                    searchItems();
                }
            }
        });

        searchAutoTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = String.valueOf(s);
                search_item = temp;
                //Toast.makeText(context,search_item,Toast.LENGTH_SHORT).show();
                if (search_item.trim().length() > 0) {
                    searchItems();
                }
            }
        });

        searchAutoTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, "Sugestion : "+objectItemData[position].getAutoTitle(), Toast.LENGTH_SHORT).show();
                searchAutoTextView.setText(objectItemData[position].getAutoTitle());
                searchAutoTextView.setSelection(objectItemData[position].getAutoTitle().length());
                if (objectItemData[position].getAutoProfile().equals("1")) {
                    if (objectItemData[position].getAutoGroupId().equals("6")) {
                        Intent intent = new Intent(SearchActivity.this, DrugProfileActivity.class);
                        intent.putExtra("drug_id", objectItemData[position].getAutoID());
                        intent.putExtra("brand_name", objectItemData[position].getAutoTitle());
                        intent.putExtra("description", "");
                        intent.putExtra("packet_image", "");
                        intent.putExtra("packet_title", "");
                        intent.putExtra("packet_description", "");
                        intent.putExtra("drug_image", objectItemData[position].getAutoImage());
                        intent.putExtra("overview", "");
                        intent.putExtra("side_effect", "");
                        intent.putExtra("dosage", "");
                        intent.putExtra("drugs_profile", "");
                        intent.putExtra("generic_name", "");
                        intent.putExtra("first_name", "");
                        intent.putExtra("user_id", "");
                        startActivity(intent);
                    } else if (objectItemData[position].getAutoGroupId().equals("7")) {
                        Intent intent = new Intent(SearchActivity.this, GroupDetailActivity.class);
                        intent.putExtra("group_id", objectItemData[position].getAutoID());
                        intent.putExtra("group_name", objectItemData[position].getAutoTitle());
                        intent.putExtra("group_des", "");
                        intent.putExtra("group_img", objectItemData[position].getAutoImage());
                        intent.putExtra("group_membr", "");
                        intent.putExtra("group_discuss", "");
                        intent.putExtra("group_created_by", "");
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(SearchActivity.this, ProfileDetailNewActivity.class);
                        intent.putExtra("user_id", objectItemData[position].getAutoID());
                        intent.putExtra("user_fname", objectItemData[position].getAutoTitle());
                        intent.putExtra("user_lname", "");
                        intent.putExtra("user_email", "");
                        intent.putExtra("group_id", objectItemData[position].getAutoGroupId());
                        intent.putExtra("user_image_url", objectItemData[position].getAutoImage());
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(context, "Search result page", Toast.LENGTH_SHORT).show();
                    //Intent intent = new Intent(SearchActivity.this, SearchResultActivity.class);
                    //intent.putExtra("speciality_id",objectItemData[position].getAutoID());
                    //startActivity(intent);
                    speciality_type = objectItemData[position].getAutoID();
                    search_item = "";
                    getSearchResults();
                }
            }
        });

        searchClickImageView = (TextView) findViewById(R.id.search_click_iv);
        searchClickImageView.setTypeface(searchFont);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, searchTypes);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        filterSpinner.setAdapter(dataAdapter);

        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, "Sugestion : " + parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                spinner_text = parent.getItemAtPosition(position).toString();
                speciality_type = "0";
                if (position == 0) {
                    search_type = "2";
                    searchClickImageView.setVisibility(View.VISIBLE);
                    locationOnOffTextView.setVisibility(View.VISIBLE);
                    //locationLayout.setVisibility(View.GONE);
                } else if (position == 1) {
                    search_type = "4";
                    searchClickImageView.setVisibility(View.VISIBLE);
                    locationOnOffTextView.setVisibility(View.VISIBLE);
                } else if (position == 2) {
                    search_type = "5";
                    searchClickImageView.setVisibility(View.VISIBLE);
                    locationOnOffTextView.setVisibility(View.VISIBLE);
                } else if (position == 3) {
                    search_type = "6";
                    searchClickImageView.setVisibility(View.VISIBLE);
                    locationOnOffTextView.setVisibility(View.GONE);
                    locationLayout.setVisibility(View.GONE);
                    mAutocompleteView.setText("");
                    latitude_str = "";
                    longitude_str = "";
                    isLocation = false;
                } else if (position == 4) {
                    search_type = "7";
                    searchClickImageView.setVisibility(View.GONE);
                    locationOnOffTextView.setVisibility(View.GONE);
                    locationLayout.setVisibility(View.GONE);
                    mAutocompleteView.setText("");
                    latitude_str = "";
                    longitude_str = "";
                    isLocation = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        searchClickImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                speciality_type = "0";
                search_item = searchAutoTextView.getText().toString().trim();

                String location_str = mAutocompleteView.getText().toString();
                if (location_str.trim().length() > 0 && isLocation) {
                    getLatLongRequest(location_str);
                } else {
                    getSearchResults();
                }

                filterLayout.setVisibility(View.GONE);
                isFilter = false;
                compareButton.setVisibility(View.GONE);
            }
        });

        compareIds = new ArrayList<String>();

        eachItems = new ArrayList<SearchUserModel>();
        eachItemsTemp = new ArrayList<SearchUserModel>();

        getSearchResults();

        compareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (compareIds.size() > 1) {
                    ArrayList<String> sendIds = new ArrayList<String>();
                    for (int c = 0; c < compareIds.size(); ++c) {
                        Log.e("ID", compareIds.get(c));
                        sendIds.add(compareIds.get(c));
                    }
                    Intent intent = new Intent(context, CompareActivity.class);
                    intent.putStringArrayListExtra("compare_ids", sendIds);
                    intent.putExtra("user_group_id", search_type);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "Please select 2 doctors to compare", Toast.LENGTH_SHORT).show();
                }
            }
        });

        /***************************search filter works********************/

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFilter) {
                    filterLayout.setVisibility(View.GONE);
                    isFilter = false;
                } else {
                    filterLayout.setVisibility(View.VISIBLE);
                    isFilter = true;
                    if (search_type.equals("6")) {
                        filter1TextView.setText("Generic Name");
                    } else {
                        filter1TextView.setText("Location");
                    }
                    if (search_type.equals("2")) {
                        filter2TextView.setVisibility(View.VISIBLE);
                        rangeBarLayout.setVisibility(View.VISIBLE);
                        availDayLayout.setVisibility(View.VISIBLE);
                        consultFilterCheckBox.setVisibility(View.VISIBLE);
                        if (isConsultFilter) {
                            consultFilterCheckBox.setChecked(true);
                        } else {
                            consultFilterCheckBox.setChecked(false);
                        }
                    } else {
                        filter2TextView.setVisibility(View.GONE);
                        rangeBarLayout.setVisibility(View.GONE);
                        availDayLayout.setVisibility(View.GONE);
                        consultFilterCheckBox.setVisibility(View.GONE);
                    }
                    checkboxFilterLayout.removeAllViews();
                    for (int f = 0; f < eachFilter.size(); ++f) {
                        final CheckBox checkBox = new CheckBox(context);
                        checkBox.setText(eachFilter.get(f).getName() + " (" + eachFilter.get(f).getCount() + ")");
                        if (eachFilter.get(f).iscked()) {
                            checkBox.setChecked(true);
                        } else {
                            checkBox.setChecked(false);
                        }
                        final int finalF = f;
                        final int finalF1 = f;
                        final int finalF2 = f;
                        checkBox.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (checkBox.isChecked()) {
                                    /*if (search_type.equals("2") && isConsultFilter) {
                                        //Toast.makeText(context, "Inside", Toast.LENGTH_SHORT).show();
                                        int checkCount = 0;
                                        for (int k = 0; k < eachFilter.size(); ++k) {
                                            if (eachFilter.get(k).iscked()) {
                                                ++checkCount;
                                            }
                                        }
                                        if (checkCount == 0) {
                                            Toast.makeText(context, "clear", Toast.LENGTH_SHORT).show();
                                            eachItemsTemp.clear();
                                        }
                                    }*/

                                    eachFilter.get(finalF2).setIscked(true);
                                    redundantFilterLogic();
                                    //Toast.makeText(context, checkBox.getText().toString(), Toast.LENGTH_SHORT).show();

                                    /*addLocationItems(finalF);
                                    if (search_type.equalsIgnoreCase("2")) {
                                        if (selected_min_fee > 0 || selected_max_fee < 1000) {
                                            addFeeRangeItems(selected_min_fee, selected_max_fee);
                                        }
                                        if (!selected_day_string.equalsIgnoreCase("any")) {
                                            addAvailableDayItems(selected_day_string);
                                        }
                                        if (isConsultFilter) {
                                            addConsultItems();
                                        }
                                    }

                                    categorizedAdapter = new CategorizedAdapter(context, eachItemsTemp, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                                    catItemListView.setAdapter(categorizedAdapter);*/

                                } else {
                                    //removeLocationItems(finalF);
                                    eachFilter.get(finalF2).setIscked(false);
                                    redundantFilterLogic();
                                    /*int locationFlag = 0;
                                    for (int i = 0; i < eachFilter.size(); ++i) {
                                        if (eachFilter.get(i).iscked()) {
                                            locationFlag = 1;
                                        }
                                    }

                                    if (locationFlag == 0) {
                                        if (search_type.equalsIgnoreCase("2")) {
                                            int condFlag = 0;
                                            if (selected_min_fee > 0 || selected_max_fee < 1000) {
                                                ++condFlag;
                                                addFeeRangeItemsMain(selected_min_fee, selected_max_fee);
                                            }
                                            if (!selected_day_string.equalsIgnoreCase("any")) {
                                                if (condFlag == 0) {
                                                    addAvailableDayItemsMain(selected_day_string);
                                                } else {
                                                    addAvailableDayItems(selected_day_string);
                                                }
                                                ++condFlag;
                                            }
                                            if (isConsultFilter) {
                                                if (condFlag == 0) {
                                                    addConsultItemsMain();
                                                } else {
                                                    addConsultItems();
                                                }
                                                ++condFlag;
                                            }

                                            if (condFlag == 0) {
                                                categorizedAdapter = new CategorizedAdapter(context, eachItems, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                                                catItemListView.setAdapter(categorizedAdapter);
                                            } else {
                                                categorizedAdapter = new CategorizedAdapter(context, eachItemsTemp, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                                                catItemListView.setAdapter(categorizedAdapter);
                                            }
                                        }
                                        else {
                                            categorizedAdapter = new CategorizedAdapter(context, eachItems, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                                            catItemListView.setAdapter(categorizedAdapter);
                                        }
                                    } else {
                                        categorizedAdapter = new CategorizedAdapter(context, eachItemsTemp, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                                        catItemListView.setAdapter(categorizedAdapter);
                                    }*/
                                }
                            }
                        });

                        checkboxFilterLayout.addView(checkBox);
                    }

                    ////day wise filte........................................
                    availAnyTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!selected_day_string.equalsIgnoreCase("any")) {
                                selected_day_string = "any";
                                daySelectionUi();
                                redundantFilterLogic();
                            }
                        }
                    });

                    availSunTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!selected_day_string.equalsIgnoreCase("sun")) {
                                selected_day_string = "sun";
                                daySelectionUi();
                                redundantFilterLogic();
                            }
                        }
                    });

                    availMonTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!selected_day_string.equalsIgnoreCase("mon")) {
                                selected_day_string = "mon";
                                daySelectionUi();
                                redundantFilterLogic();
                            }
                        }
                    });

                    availTueTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!selected_day_string.equalsIgnoreCase("tue")) {
                                selected_day_string = "tue";
                                daySelectionUi();
                                redundantFilterLogic();
                            }
                        }
                    });

                    availWedTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!selected_day_string.equalsIgnoreCase("wed")) {
                                selected_day_string = "wed";
                                daySelectionUi();
                                redundantFilterLogic();
                            }
                        }
                    });

                    availThuTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!selected_day_string.equalsIgnoreCase("thu")) {
                                selected_day_string = "thu";
                                daySelectionUi();
                                redundantFilterLogic();
                            }
                        }
                    });

                    availFriTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!selected_day_string.equalsIgnoreCase("fri")) {
                                selected_day_string = "fri";
                                daySelectionUi();
                                redundantFilterLogic();
                            }
                        }
                    });

                    availSatTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!selected_day_string.equalsIgnoreCase("sat")) {
                                selected_day_string = "sat";
                                daySelectionUi();
                                redundantFilterLogic();
                            }
                        }
                    });

                    //day filter end..........................................

                    consultFilterCheckBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (consultFilterCheckBox.isChecked()) {
                                isConsultFilter = true;
                                redundantFilterLogic();
                                /*if (eachItemsTemp.size() > 0) {
                                    for (int j = 0; j < eachItemsTemp.size(); ++j) {
                                        if (eachItemsTemp.get(j).getIsSubscribed().equals("0")) {
                                            eachItemsTemp.remove(j);
                                            --j;
                                        } else {
                                            if (eachItemsTemp.get(j).getIsConsult().equals("0")) {
                                                eachItemsTemp.remove(j);
                                                --j;
                                            }
                                        }
                                    }
                                    categorizedAdapter = new CategorizedAdapter(context, eachItemsTemp, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                                    catItemListView.setAdapter(categorizedAdapter);
                                } else {
                                    for (int j = 0; j < eachItems.size(); ++j) {
                                        if (!eachItems.get(j).getIsSubscribed().equals("0") && !eachItems.get(j).getIsConsult().equals("0")) {
                                            eachItemsTemp.add(eachItems.get(j));
                                        }
                                    }
                                    categorizedAdapter = new CategorizedAdapter(context, eachItemsTemp, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                                    catItemListView.setAdapter(categorizedAdapter);
                                }*/
                            } else {
                                isConsultFilter = false;
                                redundantFilterLogic();
                                /*eachItemsTemp.clear();
                                for (int j = 0; j < eachFilter.size(); ++j) {
                                    if (eachFilter.get(j).iscked()) {
                                        for (int k = 0; k < eachItems.size(); ++k) {
                                            if (eachItems.get(k).getDistrict().equalsIgnoreCase(eachFilter.get(j).getName())) {
                                                eachItemsTemp.add(eachItems.get(k));
                                            }
                                        }
                                    }
                                }

                                if (eachItemsTemp.size() > 0) {
                                    categorizedAdapter = new CategorizedAdapter(context, eachItemsTemp, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                                    catItemListView.setAdapter(categorizedAdapter);
                                } else {
                                    categorizedAdapter = new CategorizedAdapter(context, eachItems, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                                    catItemListView.setAdapter(categorizedAdapter);
                                }*/
                            }
                        }
                    });
                }
            }
        });

        filterBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterLayout.setVisibility(View.GONE);
                isFilter = false;
            }
        });
    }

    private void daySelectionUi() {
        availAnyTextView.setBackgroundColor(Color.parseColor("#ffffff"));
        availSunTextView.setBackgroundColor(Color.parseColor("#ffffff"));
        availMonTextView.setBackgroundColor(Color.parseColor("#ffffff"));
        availTueTextView.setBackgroundColor(Color.parseColor("#ffffff"));
        availWedTextView.setBackgroundColor(Color.parseColor("#ffffff"));
        availThuTextView.setBackgroundColor(Color.parseColor("#ffffff"));
        availFriTextView.setBackgroundColor(Color.parseColor("#ffffff"));
        availSatTextView.setBackgroundColor(Color.parseColor("#ffffff"));

        if (selected_day_string.equalsIgnoreCase("any")) {
            availAnyTextView.setBackgroundColor(Color.parseColor("#cccccc"));
        }
        else if (selected_day_string.equalsIgnoreCase("sun")) {
            availSunTextView.setBackgroundColor(Color.parseColor("#cccccc"));
        }
        else if (selected_day_string.equalsIgnoreCase("mon")) {
            availMonTextView.setBackgroundColor(Color.parseColor("#cccccc"));
        }
        else if (selected_day_string.equalsIgnoreCase("tue")) {
            availTueTextView.setBackgroundColor(Color.parseColor("#cccccc"));
        }
        else if (selected_day_string.equalsIgnoreCase("wed")) {
            availWedTextView.setBackgroundColor(Color.parseColor("#cccccc"));
        }
        else if (selected_day_string.equalsIgnoreCase("thu")) {
            availThuTextView.setBackgroundColor(Color.parseColor("#cccccc"));
        }
        else if (selected_day_string.equalsIgnoreCase("fri")) {
            availFriTextView.setBackgroundColor(Color.parseColor("#cccccc"));
        }
        else if (selected_day_string.equalsIgnoreCase("sat")) {
            availSatTextView.setBackgroundColor(Color.parseColor("#cccccc"));
        }
    }

    private void redundantFilterLogic() {
        eachItemsTemp.clear();
        int condFlag = 0;
        for (int l=0;l<eachFilter.size();++l) {
            if (eachFilter.get(l).iscked()) {
                ++condFlag;
                addLocationItems(l);
            }
        }

        if (selected_min_fee>0 || selected_max_fee<1000) {
            if (condFlag == 0) {
                addFeeRangeItemsMain(selected_min_fee,selected_max_fee);
            }
            else {
                addFeeRangeItems(selected_min_fee,selected_max_fee);
            }
        }

        if (!selected_day_string.equalsIgnoreCase("any")) {
            if (condFlag == 0) {
                addAvailableDayItemsMain(selected_day_string);
            }
            else {
                addAvailableDayItems(selected_day_string);
            }
            ++condFlag;
        }

        if (isConsultFilter) {
            if (condFlag == 0) {
                addConsultItemsMain();
            }
            else {
                addConsultItems();
            }
            ++condFlag;
        }

        if (condFlag == 0) {
            categorizedAdapter = new CategorizedAdapter(context, eachItems, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
            catItemListView.setAdapter(categorizedAdapter);
        }
        else {
            categorizedAdapter = new CategorizedAdapter(context, eachItemsTemp, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
            catItemListView.setAdapter(categorizedAdapter);
        }
    }

    private void addLocationItems(int position) {
        for (int i = 0; i < eachItems.size(); ++i) {
            if (search_type.equals("6")) {
                if (eachItems.get(i).getLastName().equalsIgnoreCase(eachFilter.get(position).getName())) {
                    eachItemsTemp.add(eachItems.get(i));
                }
            } else {
                if (eachItems.get(i).getDistrict().equalsIgnoreCase(eachFilter.get(position).getName())) {

                    eachItemsTemp.add(eachItems.get(i));
                }
            }
        }
    }

    private void removeLocationItems(int position) {
        for (int i = 0; i < eachItemsTemp.size(); ++i) {
            if (search_type.equals("6")) {
                if (eachItemsTemp.get(i).getLastName().equalsIgnoreCase(eachFilter.get(position).getName())) {
                    eachItemsTemp.remove(i);
                    --i;
                }
            } else {
                if (eachItemsTemp.get(i).getDistrict().equalsIgnoreCase(eachFilter.get(position).getName())) {
                    eachItemsTemp.remove(i);
                    --i;
                }
            }
        }
    }

    private void addFeeRangeItems(int minFee, int maxFee) {
        for (int i = 0; i < eachItemsTemp.size(); ++i) {
            List<ChamberExModel> chamberTemp = eachItemsTemp.get(i).getEachChambers();
            int feeFlag = 0;
            for (int j = 0; j < chamberTemp.size(); ++j) {
                if (chamberTemp.get(j).getChamber_fee().length() > 0) {
                    int chamber_fee = -1;
                    try {
                        chamber_fee = Integer.parseInt(chamberTemp.get(j).getChamber_fee());
                    } catch (NumberFormatException e) {
                    }

                    if (chamber_fee >= minFee && chamber_fee <= maxFee) {
                        feeFlag = 1;
                        break;
                    }
                }
            }

            if (feeFlag == 0) {
                eachItemsTemp.remove(i);
                --i;
            }
        }
    }

    private void addFeeRangeItemsMain(int minFee, int maxFee) {
        for (int i = 0; i < eachItems.size(); ++i) {
            List<ChamberExModel> chamberTemp = eachItems.get(i).getEachChambers();
            int feeFlag = 0;
            for (int j = 0; j < chamberTemp.size(); ++j) {
                if (chamberTemp.get(j).getChamber_fee().length() > 0) {
                    int chamber_fee = -1;
                    try {
                        chamber_fee = Integer.parseInt(chamberTemp.get(j).getChamber_fee());
                    } catch (NumberFormatException e) {
                    }

                    if (chamber_fee >= minFee && chamber_fee <= maxFee) {
                        feeFlag = 1;
                        break;
                    }
                }
            }

            if (feeFlag == 1) {
                eachItemsTemp.add(eachItems.get(i));
            }
        }
    }

    private void addAvailableDayItems(String selected_day) {
        for (int i = 0; i < eachItemsTemp.size(); ++i) {
            List<ChamberExModel> chamberTemp = eachItemsTemp.get(i).getEachChambers();
            int dayFlag = 0;
            for (int j = 0; j < chamberTemp.size(); ++j) {
                List<ScheduleModel> scheduleTemp = chamberTemp.get(j).getScheduleModelList();
                for (int k = 0; k < scheduleTemp.size(); ++k) {
                    if (scheduleTemp.get(k).getDay().equalsIgnoreCase(selected_day)) {
                        dayFlag = 1;
                        break;
                    }
                }
                if (dayFlag == 1) {
                    break;
                }
            }

            if (dayFlag == 0) {
                eachItemsTemp.remove(i);
                --i;
            }
        }

    }

    private void addAvailableDayItemsMain(String selected_day) {
        for (int i = 0; i < eachItems.size(); ++i) {
            List<ChamberExModel> chamberTemp = eachItems.get(i).getEachChambers();
            int dayFlag = 0;
            for (int j = 0; j < chamberTemp.size(); ++j) {
                List<ScheduleModel> scheduleTemp = chamberTemp.get(j).getScheduleModelList();
                for (int k = 0; k < scheduleTemp.size(); ++k) {
                    if (scheduleTemp.get(k).getDay().equalsIgnoreCase(selected_day)) {
                        dayFlag = 1;
                        break;
                    }
                }
                if (dayFlag == 1) {
                    break;
                }
            }

            if (dayFlag == 1) {
                eachItemsTemp.add(eachItems.get(i));
            }
        }

    }

    private void addConsultItems() {
        for (int j = 0; j < eachItemsTemp.size(); ++j) {
            if (eachItemsTemp.get(j).getIsSubscribed().equals("0")) {
                eachItemsTemp.remove(j);
                --j;
            } else {
                if (eachItemsTemp.get(j).getIsConsult().equals("0")) {
                    eachItemsTemp.remove(j);
                    --j;
                }
            }
        }
    }

    private void addConsultItemsMain() {
        for (int j = 0; j < eachItems.size(); ++j) {
            if (!eachItems.get(j).getIsSubscribed().equals("0") && !eachItems.get(j).getIsConsult().equals("0")) {
                eachItemsTemp.add(eachItems.get(j));
            }
        }

    }

    public void enableCompareButton() {
        compareButton.setVisibility(View.VISIBLE);
    }

    public void disableCompareButton() {
        compareButton.setVisibility(View.GONE);
    }

    private void searchItems() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "searchItems";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try {
                    JSONArray allPosts = new JSONArray(response);
                    objectItemData = new AutoCompleteModel[allPosts.length()];
                    //Log.d(TAG, allPosts.toString());
                    for (int i = 0; i < allPosts.length(); ++i) {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        String item_id, item_title, item_count, item_type, item_profile, item_photo, item_group_id;

                        item_id = singlePost.getString("id");
                        item_title = singlePost.getString("first_name") + " " + singlePost.getString("last_name");
                        item_count = singlePost.getString("counts");
                        item_type = singlePost.getString("specialization_name");
                        item_profile = singlePost.getString("profile");
                        item_photo = singlePost.getString("photo");
                        item_group_id = singlePost.getString("user_group_id");
                        //Toast.makeText(context,item_id+item_title+item_title+item_count,Toast.LENGTH_SHORT).show();
                        if (item_group_id.equals("2")) {
                            if (item_profile.equals("0")) {
                                item_type = "Speciality";
                                /*if (item_count.equals("0")) {
                                    continue;
                                }*/
                            }
                        } else if (item_group_id.equals("4") || item_group_id.equals("5")) {
                            if (item_profile.equals("0")) {
                                item_type = "Service";
                                /*if (item_count.equals("0")) {
                                    continue;
                                }*/
                            } else {
                                //item_type = "Service";
                            }
                        } else if (item_group_id.equals("6")) {
                            if (item_profile.equals("0")) {
                                item_type = "Generic Name";
                                /*if (item_count.equals("0")) {
                                    continue;
                                }*/
                            } else {
                                //item_type = "Service";
                            }
                        } else if (item_group_id.equals("7")) {
                            if (item_profile.equals("0")) {
                                item_type = "";
                                /*if (item_count.equals("0")) {
                                    continue;
                                }*/
                            } else {
                                //item_type = "Service";
                            }
                        }


                        //Toast.makeText(context,item_title,Toast.LENGTH_SHORT).show();
                        AutoCompleteModel autoItem = new AutoCompleteModel(item_id, item_title, item_type, item_count, item_photo, item_group_id, item_profile);
                        objectItemData[i] = autoItem;
                        //mList.add(autoItem);
                        //handler.addContact(pst);
                    }
                    //refresh adapter
                    autoCompleteAdapter.notifyDataSetChanged();
                    autoCompleteAdapter = new SearchAutoCompleteAdapter(context, R.layout.list_row_autocomplete, objectItemData);
                    searchAutoTextView.setAdapter(autoCompleteAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //fatchFeedDb();
                //Toast.makeText(context, "Doc error occured " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("search_type", search_type);
                params.put("search_item", search_item);
                params.put("latLngArray", "");
                params.put("user_id", appUserId);
                Log.d("search_type", search_type);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req + TAG);
    }

    private void getSearchResults() {
        showDialog();
        String url = context.getResources().getString(R.string.MAIN_URL) + "searchGetUsers";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                dismissDialog();
                eachItems.clear();
                eachFilter.clear();
                eachItemsTemp.clear();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() <= 0) {
                        emptyTextView.setText("No data available.");
                        emptyTextView.setVisibility(View.VISIBLE);
                    } else {
                        emptyTextView.setVisibility(View.GONE);
                        if (jsonArray.length() < 8) {
                            isEnd = true;
                        }
                        for (int i = 0; i < jsonArray.length(); ++i) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                            List<SpecialityModel> specialityList = new ArrayList<SpecialityModel>();
                            String id, firstName, lastName, photo, user_group_id, degree, type,
                                    district, review_count, review_star, isSaved, isConsult, item_price, isSubscribed, isAppointment;
                            id = jsonObject.getString("id");
                            firstName = jsonObject.getString("first_name");
                            lastName = jsonObject.getString("last_name");
                            photo = jsonObject.getString("photo");
                            user_group_id = jsonObject.getString("user_group_id");
                            degree = jsonObject.getString("degree");
                            type = jsonObject.getString("type");//////
                            district = jsonObject.getString("district");
                            isConsult = jsonObject.getString("isConsult");
                            isSubscribed = jsonObject.getString("isSubscribed");
                            isAppointment = jsonObject.getString("isAppointment");
                            item_price = jsonObject.getString("item_price");

                            if (user_group_id.equals("2")) {
                                JSONArray jsonArray1 = jsonObject.getJSONArray("speciality");
                                for (int j = 0; j < jsonArray1.length(); ++j) {
                                    JSONObject jsonObject1 = (JSONObject) jsonArray1.get(j);
                                    String speciality_id = jsonObject1.getString("specialization_id");
                                    String speciality_name = jsonObject1.getString("name");

                                    SpecialityModel specialityModel = new SpecialityModel(speciality_id, speciality_name);
                                    specialityList.add(specialityModel);
                                }
                            } else {

                            }

                            if (!user_group_id.equals("6")) {
                                JSONArray jsonArray2 = jsonObject.getJSONArray("review");
                                JSONObject jsonObject2 = (JSONObject) jsonArray2.get(0);

                                review_count = jsonObject2.getString("count");
                                review_star = jsonObject2.getString("rating");
                            } else {
                                review_count = "0";
                                review_star = "0";
                            }

                            isSaved = jsonObject.getString("isSaved");

                            List<ChamberExModel> eachChambers = new ArrayList<ChamberExModel>();

                            if (user_group_id.equalsIgnoreCase("2")) {
                                JSONArray chamberArray = jsonObject.getJSONArray("chambers");
                                for (int j = 0; j < chamberArray.length(); ++j) {
                                    JSONObject chamberObject = (JSONObject) chamberArray.get(j);
                                    String chamber_id, organisation_id, chamber_first_name,
                                            chamber_last_name, chamber_photo, chamber_mobile, chamber_user_group_id,
                                            chamber_street_address, chamber_address, cut_of_time,
                                            chamber_fee, each_patient_time;
                                    chamber_id = chamberObject.getString("chamber_id");
                                    organisation_id = chamberObject.getString("organisation_id");
                                    chamber_first_name = chamberObject.getString("chamber_first_name");
                                    chamber_last_name = chamberObject.getString("chamber_last_name");
                                    chamber_photo = chamberObject.getString("chamber_photo");
                                    chamber_mobile = chamberObject.getString("chamber_mobile");
                                    chamber_user_group_id = chamberObject.getString("chamber_user_group_id");
                                    chamber_street_address = chamberObject.getString("chamber_street_address");
                                    chamber_address = chamberObject.getString("chamber_address");
                                    cut_of_time = chamberObject.getString("cut_of_time");
                                    chamber_fee = chamberObject.getString("chamber_fee");
                                    each_patient_time = chamberObject.getString("each_patient_time");

                                    JSONArray schedule_list = chamberObject.getJSONArray("schedule_list");
                                    List<ScheduleModel> scheduleModelList = new ArrayList<ScheduleModel>();
                                    for (int k = 0; k < schedule_list.length(); ++k) {
                                        JSONObject schedule = schedule_list.getJSONObject(k);
                                        String schedule_id = schedule.getString("id");
                                        String user_id = schedule.getString("user_id");
                                        String schedule_chamber_id = schedule.getString("chamber_id");
                                        String start_time = schedule.getString("start_time");
                                        String end_time = schedule.getString("end_time");
                                        String day = schedule.getString("day");

                                        scheduleModelList.add(new ScheduleModel(schedule_id, user_id, schedule_chamber_id, start_time, end_time, day));
                                    }

                                    ChamberExModel chamberModel = new ChamberExModel(chamber_id, organisation_id, chamber_first_name,
                                            chamber_last_name, chamber_photo, chamber_mobile, chamber_user_group_id,
                                            chamber_street_address, chamber_address, cut_of_time,
                                            chamber_fee, each_patient_time, scheduleModelList);
                                    eachChambers.add(chamberModel);
                                }
                            }


                            if (user_group_id.equals("2") || user_group_id.equals("4") || user_group_id.equals("5")) {
                                if (district.length() > 1) {
                                    int isExist = -1;
                                    for (int fm = 0; fm < eachFilter.size(); ++fm) {
                                        if (eachFilter.get(fm).getName().equalsIgnoreCase(district)) {
                                            isExist = fm;
                                            break;
                                        }
                                    }

                                    if (isExist == -1) {
                                        eachFilter.add(new SearchFilterModel(district, 1, false));
                                    } else {
                                        eachFilter.get(isExist).setCount(eachFilter.get(isExist).getCount() + 1);
                                    }
                                }
                            } else if (user_group_id.equals("6")) {
                                if (lastName.length() > 1) {
                                    int isExist = -1;
                                    for (int fm = 0; fm < eachFilter.size(); ++fm) {
                                        if (eachFilter.get(fm).getName().equalsIgnoreCase(lastName)) {
                                            isExist = fm;
                                            break;
                                        }
                                    }

                                    if (isExist == -1) {
                                        eachFilter.add(new SearchFilterModel(lastName, 1, false));
                                    } else {
                                        eachFilter.get(isExist).setCount(eachFilter.get(isExist).getCount() + 1);
                                    }
                                }
                            }

                            //CategoryItem categoryItem = new CategoryItem(id, firstName, lastName, photo, user_group_id, degree, type, district, review_count, review_star, specialityList, isSaved, isConsult, item_price, isSubscribed, isAppointment);
                            SearchUserModel categoryItem = new SearchUserModel(id, firstName, lastName, photo, user_group_id, degree, type, district, review_count, review_star, specialityList, isSaved, isConsult, item_price, isSubscribed, isAppointment, eachChambers);

                            eachItems.add(categoryItem);
                            //eachItemsTemp.add(categoryItem);
                        }
                        //categorizedAdapter.notifyDataSetChanged();
                        categorizedAdapter = new CategorizedAdapter(context, eachItems, compareIds, compareButton, appUserId, appUserGroupId, search_type, isEnd, speciality_type, search_item, latitude_str, longitude_str);
                        catItemListView.setAdapter(categorizedAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                dismissDialog();
                eachItems.clear();
                emptyTextView.setText("Can't load data." + "\n" + "Tap to retry.");
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emptyTextView.setVisibility(View.GONE);
                        getSearchResults();
                    }
                });
                //fatchFeedDb();
                //Toast.makeText(context, "Doc error occured " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_group_id", search_type);
                params.put("user_id", appUserId);
                params.put("start_index", "0");
                params.put("take_total", "1000");
                params.put("speciality_type", speciality_type);
                params.put("search_item", search_item);
                params.put("latitude", latitude_str);
                params.put("longitude", longitude_str);
                Log.d("search_type", search_type);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req + TAG);
    }

    private void getGeoAddress(final double latitude, final double longitude) {
        showDialog();
        String url = context.getResources().getString(R.string.MAIN_URL) + "getGeoAddress";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                dismissDialog();
                if (response.toString().trim().equalsIgnoreCase("no")) {
                    mAutocompleteView.setText("(Couldn't get the location.)");
                    isLocation = false;
                } else {
                    mAutocompleteView.setText(response.toString());
                    isLocation = true;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                dismissDialog();
                isLocation = false;
                //fatchFeedDb();
                Toast.makeText(context, "Error: Try Again." + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("latitide", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req + TAG);
    }

    private void getLatLongRequest(final String location_str) {
        showDialog();
        String url = context.getResources().getString(R.string.MAIN_URL) + "getLatLongData";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                dismissDialog();
                if (response.toString().trim().equalsIgnoreCase("no")) {
                    //mAutocompleteView.setText("Couldn't get the location.");
                    Toast.makeText(context, "Please Enter Valid Address.", Toast.LENGTH_SHORT).show();
                } else {
                    //mAutocompleteView.setText(response.toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        latitude_str = jsonObject.getString("latitude");
                        longitude_str = jsonObject.getString("longitude");
                        getSearchResults();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                dismissDialog();

                //fatchFeedDb();
                //Toast.makeText(context, "Doc error occured " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("address", location_str);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req + TAG);
    }

    private void showDialog() {
        pDialog.setMessage("Wait...");
        pDialog.show();
    }

    private void dismissDialog() {
        pDialog.dismiss();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //displayLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        // Assign the new location
        //mLastLocation = location;

        //Toast.makeText(getApplicationContext(), "Location changed!",
        //Toast.LENGTH_SHORT).show();

        // Displaying the new location on UI
        //displayLocation();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    /**
     * Method to display the location on UI
     */
    private void displayLocation() {

        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();

            //locationEditText.setText(latitude + ", " + longitude);
            isLocation = true;
            getGeoAddress(latitude, longitude);

        } else {
            mAutocompleteView.setText("(Couldn't get the location. Make sure location is enabled on the device)");
            isLocation = false;
        }
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Toast.makeText(getApplicationContext(), "Clicked: " + primaryText, Toast.LENGTH_SHORT).show();
            Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);
            LatLng latLng = place.getLatLng();
            //latLng.latitude;
            // Format details of the place for display and show it in a TextView.
           /* mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
                    place.getId(), place.getAddress(), place.getPhoneNumber(),
                    place.getWebsiteUri()));*/

            // Display the third party attributions if set.
            final CharSequence thirdPartyAttribution = places.getAttributions();
            if (thirdPartyAttribution == null) {
                //mPlaceDetailsAttribution.setVisibility(View.GONE);
            } else {
                //mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
                //mPlaceDetailsAttribution.setText(Html.fromHtml(thirdPartyAttribution.toString()));
            }

            Log.i(TAG, "Place details received: " + place.getName());

            places.release();
        }
    };

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        Log.e(TAG, res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
