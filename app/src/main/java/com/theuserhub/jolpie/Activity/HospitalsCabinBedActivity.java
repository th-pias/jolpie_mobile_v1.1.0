package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.HospitalsCabinBedAdapter;
import com.theuserhub.jolpie.Adapters.HospitalsDoctorAdapter;
import com.theuserhub.jolpie.Models.CabinBedModel;
import com.theuserhub.jolpie.Models.DoctorsModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HospitalsCabinBedActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "HospitalsCabinBedActivity", tag_string_req = "hos_cabin_str_req";

    private ListView doctorsListView;
    private List<CabinBedModel> eachDoctors;
    private HospitalsCabinBedAdapter doctorAdapter;
    private String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospitals_cabin_bed);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cabin/Bed");

        user_id  = getIntent().getStringExtra("user_id");

        doctorsListView = (ListView) findViewById(R.id.cabin_lv);
        eachDoctors = new ArrayList<CabinBedModel>();
        doctorAdapter = new HospitalsCabinBedAdapter(context,eachDoctors);
        doctorsListView.setAdapter(doctorAdapter);

        getHospitalsDoctors();
    }

    private void getHospitalsDoctors(){

        String url = context.getResources().getString(R.string.MAIN_URL)+"getHospitalsCabinBed";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();++i){
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        String id = jsonObject.getString("id");
                        String service_id = jsonObject.getString("service_id");
                        String service_name = jsonObject.getString("service_name");
                        String service_des = jsonObject.getString("service_description");
                        String service_price = jsonObject.getString("service_price");
                        String service_user_id = jsonObject.getString("user_id");

                        eachDoctors.add(new CabinBedModel(id,service_id,service_name,service_des,service_price,service_user_id));
                    }
                    doctorAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("errshowComment", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
