package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.CategorizedAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.CategoryItem;
import com.theuserhub.jolpie.Models.DrugItem;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DrugProfileActivity extends AppCompatActivity {
    private Context context = this;
    private String TAG = "DrugProfileActivity", tag_string_req = "drug_info_str_req";

    private ProgressDialog pDialog;
    String drug_id,user_id,brand_name,description,packet_image,packet_title,packet_description,
            drug_image,overview,side_effect,dosage,drugs_profile,generic_name,
            first_name;;
    TextView brand_name_tv;
    TextView generic_name_tv;
    TextView description_tv;
    TextView company_name_tv;
    TextView packet_title_tv;
    TextView dosage_tv;
    TextView drugs_profile_tv;
    TextView packet_description_tv;
    TextView overview_tv;
    TextView side_effect_tv;
    LinearLayout side_effect_LinearLayout;
    LinearLayout overview_LinearLayout;
    LinearLayout packet_description_LinearLayout;
    LinearLayout side_effect_new_ll;
    LinearLayout overview_new_ll;
    LinearLayout description_new_ll;
    NetworkImageView profileNImageView, bannerNImageView;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drug_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Drug");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");


        drug_id = getIntent().getStringExtra("drug_id");
        brand_name = getIntent().getStringExtra("brand_name");
        description =getIntent().getStringExtra("description");
        packet_image =getIntent().getStringExtra("packet_image");
        packet_title =getIntent().getStringExtra("packet_title");
        packet_description =getIntent().getStringExtra("packet_description");
        drug_image =getIntent().getStringExtra("drug_image");
        overview =getIntent().getStringExtra("overview");
        side_effect =getIntent().getStringExtra("side_effect");
        dosage =getIntent().getStringExtra("dosage");
        drugs_profile =getIntent().getStringExtra("drugs_profile");
        generic_name =getIntent().getStringExtra("generic_name");
        first_name = getIntent().getStringExtra("first_name");
        user_id = getIntent().getStringExtra("user_id");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        brand_name_tv = (TextView) findViewById(R.id.brand_name);
        generic_name_tv = (TextView) findViewById(R.id.generic_name);
        description_tv = (TextView) findViewById(R.id.description_tv);
        company_name_tv = (TextView) findViewById(R.id.company_name);
        packet_title_tv = (TextView) findViewById(R.id.packet_title);
        dosage_tv = (TextView) findViewById(R.id.dosage);
        drugs_profile_tv = (TextView) findViewById(R.id.drugs_profile);
        packet_description_tv = (TextView) findViewById(R.id.packet_description_tv);
        profileNImageView = (NetworkImageView) findViewById(R.id.drug_image_niv);
        bannerNImageView = (NetworkImageView) findViewById(R.id.drug_banner_niv);

        if (packet_image.length()>1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL_DRUG) + drug_image;
            profileNImageView.setImageUrl(meta_url, imageLoader);
        }

        if (packet_image.length()>1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL_DRUG) + packet_image;
            bannerNImageView.setImageUrl(meta_url, imageLoader);
        }

        brand_name_tv.setText(brand_name);
        generic_name_tv.setText(generic_name);
        description_tv.setText(description);
        company_name_tv.setText(first_name);
        packet_title_tv.setText(packet_title);
        dosage_tv.setText(dosage);
        drugs_profile_tv.setText(drugs_profile);
        packet_description_tv.setText(packet_description);

        side_effect_LinearLayout = (LinearLayout) findViewById(R.id.side_effect);
        overview_LinearLayout = (LinearLayout) findViewById(R.id.overview);
        packet_description_LinearLayout = (LinearLayout) findViewById(R.id.packet_description);

        side_effect_new_ll = (LinearLayout) findViewById(R.id.side_effect_linear_layout);
        overview_new_ll = (LinearLayout) findViewById(R.id.overview_linear_layout);
        description_new_ll = (LinearLayout) findViewById(R.id.description_linear_layout);

        if (!appUserId.equals(user_id)) {
            company_name_tv.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
            company_name_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context,ProfileDetailNewActivity.class);
                    intent.putExtra("user_id",user_id);
                    intent.putExtra("user_fname",first_name);
                    intent.putExtra("user_lname","");
                    intent.putExtra("user_email","");
                    intent.putExtra("group_id","6");
                    intent.putExtra("user_image_url","");
                    startActivity(intent);
                }
            });
        }
        else {
            company_name_tv.setTextColor(ContextCompat.getColor(context, R.color.black));
        }

        side_effect_LinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                side_effect_new_ll.setVisibility(View.VISIBLE);
                overview_new_ll.setVisibility(View.GONE);
                description_new_ll.setVisibility(View.GONE);
                side_effect_tv = (TextView) findViewById(R.id.side_effect_tv);
                side_effect_tv.setText(side_effect);
            }
        });
        overview_LinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                side_effect_new_ll.setVisibility(View.GONE);
                overview_new_ll.setVisibility(View.VISIBLE);
                description_new_ll.setVisibility(View.GONE);
                overview_tv = (TextView) findViewById(R.id.overview_tv);

                overview_tv.setText(overview);//

            }
        });
        packet_description_LinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                side_effect_new_ll.setVisibility(View.GONE);
                overview_new_ll.setVisibility(View.GONE);
                description_new_ll.setVisibility(View.VISIBLE);
            }
        });

        getDrugsInfoRequest();
    }

    private void getDrugsInfoRequest() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "getDrugDetail";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                try {
                    JSONArray allPosts=new JSONArray(response);
                    Log.d("Test2", allPosts.toString());
                    for (int i=0;i<allPosts.length();++i)
                    {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);

                        drug_id = singlePost.getString("drug_id");
                        generic_name = singlePost.getString("generic_name");
                        brand_name = singlePost.getString("brand_name");
                        description = singlePost.getString("description");
                        packet_image = singlePost.getString("packet_image");
                        packet_title = singlePost.getString("packet_title");
                        packet_description = singlePost.getString("packet_description");
                        drug_image = singlePost.getString("drug_image");
                        overview = singlePost.getString("overview");
                        side_effect = singlePost.getString("side_effect");
                        dosage = singlePost.getString("dosage");
                        user_id = singlePost.getString("user_id");
                        drugs_profile = singlePost.getString("drugs_profile");
                        generic_name = singlePost.getString("generic_name");
                        first_name = singlePost.getString("first_name");

                        if (packet_image.length()>1) {
                            if (imageLoader == null)
                                imageLoader = AppController.getInstance().getImageLoader();

                            String meta_url = context.getResources().getString(R.string.UPLOADS_URL_DRUG) + drug_image;
                            profileNImageView.setImageUrl(meta_url, imageLoader);
                        }

                        if (packet_image.length()>1) {
                            if (imageLoader == null)
                                imageLoader = AppController.getInstance().getImageLoader();

                            String meta_url = context.getResources().getString(R.string.UPLOADS_URL_DRUG) + packet_image;
                            bannerNImageView.setImageUrl(meta_url, imageLoader);
                        }

                        brand_name_tv.setText(brand_name);
                        generic_name_tv.setText(generic_name);
                        description_tv.setText(description);
                        company_name_tv.setText(first_name);
                        packet_title_tv.setText(packet_title);
                        dosage_tv.setText(dosage);
                        drugs_profile_tv.setText(drugs_profile);
                        packet_description_tv.setText(packet_description);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
                Toast.makeText(context, "Error: Try again later", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("drug_id", drug_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
