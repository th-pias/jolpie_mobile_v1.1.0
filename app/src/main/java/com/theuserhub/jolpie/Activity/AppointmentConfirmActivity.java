package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AppointmentConfirmActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "AppointmentConfirmActivity", tag_string_req = "confirm_str_req";

    private String chamber_id, selectedDateString, selectedTimeString, selectedDayString, appointment_time;
    private Button confirmButton;
    private TextView dateTextView,confirmationTextView;
    private SimpleDateFormat _24DateFormat, _12DateFormat, dateFormat1, dateFormat2;
    private String organisation_id, chamber_first_name, chamber_last_name, chamber_photo, chamber_user_group_id, chamber_street_address, chamber_address, cut_of_time, chamber_fee, each_patient_time;
    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    private TextView consultation_fee, appointment_address, appt_confirm_drname_tv, doctor_degree_tv;
    private String name, degree, image = "";
    private ProgressDialog pDialog;

    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private NetworkImageView docProfileNImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_confirm);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Confirm Appointment");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent confirmIntent = getIntent();
        selectedDateString = confirmIntent.getStringExtra("selected_date");
        selectedTimeString = confirmIntent.getStringExtra("selected_time");
        selectedDayString = confirmIntent.getStringExtra("selected_day");
        chamber_id = confirmIntent.getStringExtra("chamber_id");

        organisation_id = confirmIntent.getStringExtra("organisation_id");
        chamber_first_name = confirmIntent.getStringExtra("chamber_first_name");
        chamber_last_name = confirmIntent.getStringExtra("chamber_last_name");
        chamber_photo = confirmIntent.getStringExtra("chamber_photo");
        chamber_user_group_id = confirmIntent.getStringExtra("chamber_user_group_id");
        chamber_street_address = confirmIntent.getStringExtra("chamber_street_address");
        chamber_address = confirmIntent.getStringExtra("chamber_address");
        cut_of_time = confirmIntent.getStringExtra("cut_of_time");
        chamber_fee = confirmIntent.getStringExtra("chamber_fee");
        each_patient_time = confirmIntent.getStringExtra("each_patient_time");
        name = confirmIntent.getStringExtra("name");
        degree = confirmIntent.getStringExtra("degree");
        image = confirmIntent.getStringExtra("image");

        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);

        _24DateFormat = new SimpleDateFormat("HH:mm:ss");
        _12DateFormat = new SimpleDateFormat("hh:mm a");
        dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");

        consultation_fee = (TextView) findViewById(R.id.consultation_fee);
        consultation_fee.setText("BDT  "+chamber_fee);

        appointment_address = (TextView) findViewById(R.id.appointment_address);
        appointment_address.setText(chamber_address + " " + chamber_street_address);

        confirmButton = (Button) findViewById(R.id.appt_confirm_btn);
        dateTextView = (TextView) findViewById(R.id.appt_confirm_time_tv);
        confirmationTextView = (TextView) findViewById(R.id.confirmation_tv);

        appt_confirm_drname_tv = (TextView) findViewById(R.id.appt_confirm_drname_tv);
        appt_confirm_drname_tv.setText("Dr. "+name);

        doctor_degree_tv = (TextView) findViewById(R.id.doctor_degree_tv);
        doctor_degree_tv.setText(degree);

        docProfileNImg = (NetworkImageView) findViewById(R.id.doctor_image);
        if (image.length() > 1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + image;
            docProfileNImg.setImageUrl(meta_url, imageLoader);
        }

        Date time = new Date();
        Date date = new Date();
        String tempTime = "", tempDate = "";
        try {
            time = _12DateFormat.parse(selectedTimeString);
            tempTime = _24DateFormat.format(time);

            date = dateFormat1.parse(selectedDateString);
            tempDate = dateFormat2.format(date);

            appointment_time = tempDate + " " + tempTime;
            Log.e("time", selectedTimeString + " : " + tempTime);
            Log.e("date", selectedDateString + " : " + tempDate);
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("time", "Exception");
        }
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeAppointment();
            }
        });

        dateTextView.setText(selectedDayString + " " + selectedDateString + " " + selectedTimeString);
    }

    private void takeAppointment() {
        pDialog.setMessage("Confirming...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "takeAppointment";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                Toast.makeText(context, "Appointment Confirmed", Toast.LENGTH_SHORT).show();
                //finish();
                confirmButton.setVisibility(View.GONE);
                confirmationTextView.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
                Toast.makeText(context, "Appointment Confirmed", Toast.LENGTH_SHORT).show();
                //finish();
                confirmButton.setVisibility(View.GONE);
                confirmationTextView.setVisibility(View.VISIBLE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("chamber_id", chamber_id);
                params.put("appointment_time", appointment_time);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
