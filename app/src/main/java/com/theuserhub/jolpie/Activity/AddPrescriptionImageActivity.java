package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class AddPrescriptionImageActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "AddPrescriptionImageActivity", tag_string_req = "uploads_pres_str_req";

    private ProgressDialog prgDialog;
    private static int RESULT_LOAD_IMG = 1;
    private String imgDecodableString, fileName = "", encodedString = "",image_name="";
    private int maxWidth = 350;
    private int maxHeight = 350;
    private ImageView imagePreview, imageCamera;
    private TextView createTextView;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_prescription_image);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Upload Prescription");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        prgDialog = new ProgressDialog(this);
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        encodedString = "";
        fileName = "";

        imagePreview = (ImageView) findViewById(R.id.grp_img_preview_iv);
        imageCamera = (ImageView) findViewById(R.id.grp_select_image_iv);
        createTextView = (TextView) findViewById(R.id.grp_create_tv);

        createTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (group_name.trim().length()>0 && group_description.trim().length()>0) {
                if (encodedString.trim().length() > 0) {
                    //Toast.makeText(context, "upload img ", Toast.LENGTH_LONG).show();
                    uploadImage();
                } else {
                    Toast.makeText(context, "Please choose an image", Toast.LENGTH_LONG).show();
                    //createGroupRequest();
                }
                //}
                //else {
                //Toast.makeText(context, "Please fill all", Toast.LENGTH_SHORT).show();
                //}
            }
        });

        imagePreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
            }
        });

        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (encodedString.length() > 0) {
                    imagePreview.setVisibility(View.GONE);
                    imageCamera.setImageResource(R.drawable.camera_icon);
                    encodedString = "";
                    fileName = "";
                } else {
                    // Create intent to Open Image applications like Gallery, Google Photos
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    // Start the Intent
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RESULT_LOAD_IMG) {
            if (resultCode == RESULT_OK) {
                if (null != data) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    //String picturePath = cursor.getString(columnIndex);
                    imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();

                    imagePreview.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                    imagePreview.setVisibility(View.VISIBLE);
                    imageCamera.setImageResource(R.drawable.cross_icon);
                    //license_number.setVisibility(View.GONE);
                    //licensePlate_img.setVisibility(View.VISIBLE);

                    prgDialog.setMessage("Converting Image to Binary Data");
                    prgDialog.show();

                    String fileNameSegments[] = imgDecodableString.split("/");
                    fileName = fileNameSegments[fileNameSegments.length - 1];
                    String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
                    fileName = extension;
                    //Set the Image in ImageView after decoding the String
                    Bitmap myImg = resize(imgDecodableString);//BitmapFactory.decodeFile(imgDecodableString);
                    Log.d("ImageBitmap", "" + myImg);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    try {
                        if (extension.trim().toLowerCase().equals("png"))
                            myImg.compress(Bitmap.CompressFormat.PNG, 100, out);
                        else if (extension.trim().toLowerCase().equals("jpeg") || extension.trim().toLowerCase().equals("jpg"))
                            myImg.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    } catch (Exception e) {
                        Toast.makeText(context, "Something error occured on image loading from gallery", Toast.LENGTH_SHORT).show();
                        encodedString = "";
                    }


                    byte[] byte_arr = out.toByteArray();
                    // Encode Image to String
                    encodedString = Base64.encodeToString(byte_arr, 0);
                    prgDialog.hide();

                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(context, "User cancelled Gallery", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Sorry! Failed to load image", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void uploadImage() {

        prgDialog.setMessage("Upload Image");
        prgDialog.show();

        String url = context.getResources().getString(R.string.MAIN_URL) + "uploadGroupImage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                prgDialog.dismiss();
                if (response.toString().trim().length()>0) {
                    image_name = response.toString().trim();
                    Toast.makeText(context, "Uploaded successfully", Toast.LENGTH_LONG).show();
                    createGroupRequest();
                } else {
                    Toast.makeText(context, "Uploading Error", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "uploading error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                //createGroupRequest();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("image", encodedString);
                params.put("filename", fileName);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private Bitmap resize(String path) {
        // create the options
        BitmapFactory.Options opts = new BitmapFactory.Options();

        //just decode the file
        opts.inJustDecodeBounds = true;
        Bitmap bp = BitmapFactory.decodeFile(path, opts);

        //get the original size
        int orignalHeight = opts.outHeight;
        int orignalWidth = opts.outWidth;
        //initialization of the scale
        int resizeScale = 1;
        //get the good scale
        if (orignalWidth > maxWidth || orignalHeight > maxHeight) {
            final int heightRatio = Math.round((float) orignalHeight / (float) maxHeight);
            final int widthRatio = Math.round((float) orignalWidth / (float) maxWidth);
            resizeScale = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        //put the scale instruction (1 -> scale to (1/1); 8-> scale to 1/8)
        opts.inSampleSize = resizeScale;
        opts.inJustDecodeBounds = false;
        //get the futur size of the bitmap
        int bmSize = (orignalWidth / resizeScale) * (orignalHeight / resizeScale) * 4;
        //check if it's possible to store into the vm java the picture
        if (Runtime.getRuntime().freeMemory() > bmSize) {
            //decode the file
            bp = BitmapFactory.decodeFile(path, opts);
        } else
            return null;
        return bp;
    }

    private void createGroupRequest() {
        prgDialog.setMessage("Uploading...");
        prgDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "uploadPresImage";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context, "Picture added", Toast.LENGTH_LONG).show();
                    prgDialog.dismiss();
                    finish();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Some error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("image", image_name);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
