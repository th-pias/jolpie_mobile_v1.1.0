package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.ConditionAddAdapter;
import com.theuserhub.jolpie.Models.ConditionModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyConditionAdd extends AppCompatActivity {

    private Context context = this;
    private ListView addConditionList;
    private String user_id;
    ConditionAddAdapter conditionAddAdapter;
    List<ConditionModel> conditionModelsArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_condition_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Conditions");

        addConditionList = (ListView) findViewById(R.id.condition_add);

        user_id = getIntent().getStringExtra("user_id");

        conditionModelsArrayList = new ArrayList<ConditionModel>();

        conditionAddAdapter = new ConditionAddAdapter(context,conditionModelsArrayList, user_id);
        addConditionList.setAdapter(conditionAddAdapter);

        getOtherConditions();


    }

    private void getOtherConditions() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "otherConditions";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("ADDCondition", "ADDCondition: " + response.toString());
                try {
                    JSONArray allCond = new JSONArray(response);
                    for (int i=0;i<allCond.length();++i) {
                        JSONObject singleCond = (JSONObject) allCond.get(i);

                        String cond_id = singleCond.getString("condition_id");
                        String condName_en = singleCond.getString("condition_name");
                        String condName_bn = singleCond.getString("bangla_condition_name");

                        conditionModelsArrayList.add(new ConditionModel(cond_id,condName_en,condName_bn));
                    }
                    conditionAddAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d("ADDCondition", "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "tag_string_req");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
