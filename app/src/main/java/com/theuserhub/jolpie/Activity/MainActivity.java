package com.theuserhub.jolpie.Activity;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.theuserhub.jolpie.AndroidRTC.ReceiveCallActivity;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Fragments.Main_Fragment;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Services.NotificationService;
import com.theuserhub.jolpie.Services.ReceiveCallService;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

    private Context context = this;
    private String TAG = "MainActivity", tag_string_req = "main_str_req";

    private TextView notifyTextView,notifyBellTextView, messageTextView,searchTextView;
    private ImageView notifyImageView;
    private Timer timer, callTimer;
    private GoogleApiClient mGoogleApiClient;
    private MenuItem callDocMenuItem, appointMenuItem, savedDocMenuItem, patientsLikeMenuItem,
            myGroupMenuItem, myArticleMenuItem, myReviewMenuItem,
            myPrescribeMenuItem, buySubsMenuItem, consultMenuItem;
    private long yrInMill = 31536000000l;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_main_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setTitle("Jolpie");
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        userDatabaseHandler = new UserDatabaseHandler(MainActivity.this);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Typeface notifyBellFont = Typeface.createFromAsset( getAssets(), "fontawesome-webfont.ttf" );
        //notifyImageView = (ImageView) findViewById(R.id.notify_bell_iv);
        searchTextView = (TextView) findViewById(R.id.search_tv);
        messageTextView = (TextView) findViewById(R.id.message_tv);
        notifyBellTextView = (TextView) findViewById(R.id.notify_bell_tv);
        notifyTextView = (TextView) findViewById(R.id.notify_count_tv);

        searchTextView.setTypeface(notifyBellFont);
        messageTextView.setTypeface(notifyBellFont);
        notifyBellTextView.setTypeface(notifyBellFont);

        /*notifyImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyTextView.setVisibility(View.GONE);
                Intent intent = new Intent(context, NotificationActivity.class);
                startActivity(intent);
            }
        });*/

        searchTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SearchActivity.class);
                startActivity(intent);
            }
        });

        messageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessageActivity.class);
                startActivity(intent);
            }
        });

        notifyBellTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyTextView.setVisibility(View.GONE);
                Intent intent = new Intent(context, NotificationActivity.class);
                startActivity(intent);
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().requestProfile().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Fragment mFragment = null;
        FragmentManager mFragmentManager = getSupportFragmentManager();
        mFragment = new Main_Fragment();
        if (mFragment != null) {
            mFragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
        }

        callDocMenuItem = navigationView.getMenu().getItem(0);
        appointMenuItem = navigationView.getMenu().getItem(1);
        savedDocMenuItem = navigationView.getMenu().getItem(2);
        patientsLikeMenuItem = navigationView.getMenu().getItem(3);
        myGroupMenuItem = navigationView.getMenu().getItem(4);
        myArticleMenuItem = navigationView.getMenu().getItem(5);
        myReviewMenuItem = navigationView.getMenu().getItem(6);
        myPrescribeMenuItem = navigationView.getMenu().getItem(7);
        buySubsMenuItem  = navigationView.getMenu().getItem(8);
        consultMenuItem = navigationView.getMenu().getItem(9);
        //Log.e("MenuItem", menuItem.getTitle().toString());

        //consultMenuItem.setVisible(false);
        if (appUserGroupId.equals("1")) {
            if (!appUserEmail.equals("admin@jolpie.com")) {
                myArticleMenuItem.setVisible(false);
            }
            buySubsMenuItem.setVisible(false);
        }
        else if (appUserGroupId.equals("2")) {
            myArticleMenuItem.setVisible(false);
            patientsLikeMenuItem.setVisible(false);
        }
        else if (appUserGroupId.equals("3")) {
            myArticleMenuItem.setVisible(false);
            buySubsMenuItem.setVisible(false);
        }
        else if (appUserGroupId.equals("4")) {
            myArticleMenuItem.setVisible(false);
            appointMenuItem.setVisible(false);
            patientsLikeMenuItem.setVisible(false);
            myGroupMenuItem.setVisible(false);
            myPrescribeMenuItem.setVisible(false);
            buySubsMenuItem.setVisible(false);
            consultMenuItem.setVisible(false);
        }
        else if (appUserGroupId.equals("5")) {
            myArticleMenuItem.setVisible(false);
            appointMenuItem.setVisible(false);
            patientsLikeMenuItem.setVisible(false);
            myGroupMenuItem.setVisible(false);
            myPrescribeMenuItem.setVisible(false);
            buySubsMenuItem.setVisible(false);
            consultMenuItem.setVisible(false);
        }
        else if (appUserGroupId.equals("6")) {
            appointMenuItem.setVisible(false);
            patientsLikeMenuItem.setVisible(false);
            myGroupMenuItem.setVisible(false);
            myArticleMenuItem.setVisible(false);
            myReviewMenuItem.setVisible(false);
            myPrescribeMenuItem.setVisible(false);
            buySubsMenuItem.setVisible(false);
            consultMenuItem.setVisible(false);
        }

        if (appUserGroupId.equals("2")) {
            isSubscribedRequest();
        }

        stopService(new Intent(context,NotificationService.class));
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) context.getSystemService(ns);
        nMgr.cancelAll();

        startService(new Intent(context, ReceiveCallService.class));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_patients_like_me) {
            // Handle the camera action
            Intent intent = new Intent(context, PatientsLikeMeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_my_profile) {
            /*Intent intent = new Intent(context, ProfileDetailActivity.class);
            intent.putExtra("user_id", appUserId);
            intent.putExtra("user_fname", appUserFName);
            intent.putExtra("user_lname", appUserLName);
            intent.putExtra("user_email", appUserEmail);
            intent.putExtra("group_id", appUserGroupId);
            intent.putExtra("user_image_url", appUserImgUrl);
            startActivity(intent);*/
            Intent intent = new Intent(context, ProfileDetailNewActivity.class);
            intent.putExtra("user_id", appUserId);
            intent.putExtra("user_fname", appUserFName);
            intent.putExtra("user_lname", appUserLName);
            intent.putExtra("user_email", appUserEmail);
            intent.putExtra("group_id", appUserGroupId);
            intent.putExtra("user_image_url", appUserImgUrl);
            startActivity(intent);

        } /*else if (id == R.id.nav_log_out) {
            SessionManager sessionManager = new SessionManager(context);
            sessionManager.setLogin(false);
            userDatabaseHandler.deleteUsers();

            LoginManager.getInstance().logOut();

            if (mGoogleApiClient.isConnected()) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                // [START_EXCLUDE]
                                //updateUI(false);
                                // [END_EXCLUDE]
                            }
                        });
            }
            *//*if (mGoogleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                mGoogleApiClient.disconnect();
                mGoogleApiClient.connect();
                // updateUI(false);
                System.err.println("LOG OUT ^^^^^^^^^^^^^^^^^^^^ SUCESS");
            }*//*

            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
            finish();
        }*/
        else if (id == R.id.nav_account_settings) {
            Intent intent = new Intent(context, AccountSettingsActivity.class);
            startActivity(intent);

            /*Intent intent = new Intent(context,CompareActivity.class);
            startActivity(intent);*/
        }
        else if (id == R.id.nav_saved_doctors) {
            Intent intent = new Intent(context, SavedDoctorsActivity.class);
            startActivity(intent);

            /*Intent intent = new Intent(context,CompareActivity.class);
            startActivity(intent);*/
        }
        else if (id == R.id.nav_my_appoint) {
            Intent intent = new Intent(context, AppointmentActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_my_reviews) {
            Intent intent = new Intent(context, MyReviewsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_my_articles) {
            Intent intent = new Intent(context, MyArticlesActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_my_groups) {
            Intent intent = new Intent(context, MyGroupsActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_my_prescriptions) {
            Intent intent = new Intent(context, MyPrescriptionActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_buy_subscription) {
            Intent intent = new Intent(context, BuySubscriptionActivity.class);
            startActivity(intent);
        }

        else if (id == R.id.nav_online_consult) {
            Intent intent = new Intent(context, OnlineConsultLogActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_call_doctor) {
            Intent intent = new Intent(context, CallaDoctorActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getNotificationCount() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getNotificationCount";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                response = response.toString().trim();
                if (response.equals("0")) {
                    notifyTextView.setVisibility(View.GONE);
                } else {
                    notifyTextView.setText(response);
                    notifyTextView.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void isAnyCallRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "isAnyCall";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (!response.equals("null")) {
                    Toast.makeText(context,"Calling",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context,ReceiveCallActivity.class);
                    intent.putExtra("response",response);
                    startActivity(intent);
                    callTimer.cancel();
                    finish();
                }
                else {
                    //Toast.makeText(context,"No call",Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void isSubscribedRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getIsSubscribed";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String isSubscribed = jsonObject.getString("isSubscribed");
                    String sub_msg = "";
                    if (isSubscribed.equalsIgnoreCase("0")) {
                        sub_msg = "You are not a subcribed user. Please buy subscription.";
                        showAlart(sub_msg, 999, 0);
                    }
                    else {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("subscription");
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String subs_date = jsonObject1.getString("tran_date");
                        String durationString = jsonObject1.getString("duration");
                        int duration = 0;
                        try {
                            duration = Integer.parseInt(durationString);
                        }
                        catch (NumberFormatException e) {

                        }

                        Date transDate = dateFormat.parse(subs_date);
                        Calendar calendar = Calendar.getInstance();
                        Date todayDate = calendar.getTime();
                        String todayString = dateFormat.format(todayDate);

                        todayDate = dateFormat.parse(todayString);
                        Log.e("date",""+todayString +" : "+subs_date);
                        //double difference = (double)((double)todayDate.getTime() - (double)transDate.getTime())/((double)(365*24*60*60*1000));
                        long diff = todayDate.getTime() - transDate.getTime();
                        long dayDiff = TimeUnit.MILLISECONDS.toDays(diff);

                        if (dayDiff > duration) {
                            sub_msg = "Your subscription is expired. Please renew.";
                        }
                        else {
                            sub_msg = ""+(duration-dayDiff)+" days remaining.";
                        }
                        showAlart(sub_msg, dayDiff, duration);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showAlart(String msg, final long dayDiff, final int duration) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Subscription");
        alertDialogBuilder.setMessage(msg);

        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                if (dayDiff > duration) {
                    Intent intent = new Intent(context, BuySubscriptionActivity.class);
                    startActivity(intent);
                }
            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ////////////
            }
        });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        *//*if (id == R.id.action_prescription) {
            Intent intent = new Intent(context,PrescriptionActivity.class);
            intent.putExtra("patient_id", "141");
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_patients_likeme) {
            Intent intent = new Intent(context,PatientsLikeMeActivity.class);
            startActivity(intent);
            return true;
        }
        //noinspection SimplifiableIfStatement
        else if (id == R.id.action_settings) {
            Intent intent = new Intent(context,ProfileDetailActivity.class);
            intent.putExtra("user_id",appUserId);
            intent.putExtra("user_fname",appUserFName);
            intent.putExtra("user_lname",appUserLName);
            intent.putExtra("user_email",appUserEmail);
            intent.putExtra("group_id",appUserGroupId);
            intent.putExtra("user_image_url",appUserImgUrl);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_logout) {
            SessionManager sessionManager = new SessionManager(context);
            sessionManager.setLogin(false);
            userDatabaseHandler.deleteUsers();
            Intent intent = new Intent(context,LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }*//*
        if (id == R.id.action_search) {
            Intent intent = new Intent(context, SearchActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_message) {
            Intent intent = new Intent(context, MessageActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "getNotificationCount called");
                getNotificationCount();
            }
        }, 0, 20000);

        callTimer = new Timer();
        /*callTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "Is call??");
                isAnyCallRequest();
            }
        }, 10000, 20000);*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("pause", "onPause");
        timer.cancel();
    }

    @Override
    protected void onDestroy() {
        startService(new Intent(context, NotificationService.class));
        super.onDestroy();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
