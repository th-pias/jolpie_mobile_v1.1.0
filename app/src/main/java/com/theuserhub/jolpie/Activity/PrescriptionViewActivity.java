package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.PescribedDrugListAdapterTwo;
import com.theuserhub.jolpie.Adapters.PrescribedTestListAdapter;
import com.theuserhub.jolpie.Adapters.PrescribedTestListAdapterTwo;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Fragments.PrescriptionDrugAddDialogFragment;
import com.theuserhub.jolpie.Models.PrescribedDrugModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrescriptionViewActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "PrescriptionViewActivity", tag_string_req = "prescription_str_req";

    private ProgressDialog pDialog;
    private EditText testEditText;
    private TextView sendTextView, conditionTextView, drugTextView, testTextView, adviceTextView, testAddTextView, drugAddTextView,
            nameTextView, ageTextView, genderTextView, nameTV, conditionEditText, adviceEditText, doctorsNameTextView;
    private ListView drugListView, testListView;
    private LinearLayout conditionLayout, drugLayout, testLayout, adviceLayout;

    private String conditionString, adviceString, testString, prescription_id, patient_id, appointment_id, flag,
            patient_name, patient_age, patient_gender;
    private List<PrescribedDrugModel> eachDrugs;
    private List<String> eachTests;
    private PescribedDrugListAdapterTwo drugListAdapter;
    private PrescribedTestListAdapterTwo testListAdapter;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Prescription");

        conditionString = "";
        adviceString = "";

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent prescriptionIntent = getIntent();

        patient_id = prescriptionIntent.getStringExtra("getPatient_id");
        appointment_id = prescriptionIntent.getStringExtra("getAppointment_id");
        flag = prescriptionIntent.getStringExtra("flag");//patient doctor

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        //sendTextView = (TextView) findViewById(R.id.prescription_send_tv);

        doctorsNameTextView = (TextView) findViewById(R.id.doctors_name_tv);
        nameTextView = (TextView) findViewById(R.id.patient_name_tv);
        ageTextView = (TextView) findViewById(R.id.patient_age_tv);
        genderTextView = (TextView) findViewById(R.id.patient_gender_tv);

        conditionTextView = (TextView) findViewById(R.id.condition_tv);
        drugTextView = (TextView) findViewById(R.id.drug_tv);
        testTextView = (TextView) findViewById(R.id.test_tv);
        adviceTextView = (TextView) findViewById(R.id.advice_tv);
        drugAddTextView = (TextView) findViewById(R.id.prescribed_drug_add_tv);
        testAddTextView = (TextView) findViewById(R.id.prescribed_tests_add_tv);
        nameTV = (TextView) findViewById(R.id.name);

        conditionLayout = (LinearLayout) findViewById(R.id.prescribed_condition_layout);
        drugLayout = (LinearLayout) findViewById(R.id.prescribed_drug_layout);
        testLayout = (LinearLayout) findViewById(R.id.prescribed_tests_layout);
        adviceLayout = (LinearLayout) findViewById(R.id.prescribed_advice_layout);

        conditionEditText = (TextView) findViewById(R.id.prescribed_condition_et);
        testEditText = (EditText) findViewById(R.id.prescribed_tests_et);
        adviceEditText = (TextView) findViewById(R.id.prescribed_advice_et);

        drugListView = (ListView) findViewById(R.id.prescribed_drug_lv);
        testListView = (ListView) findViewById(R.id.prescribed_tests_lv);

        if (flag.equalsIgnoreCase("doctor")) {
            //nameTV.setText("Doctors Name:");
            doctorsNameTextView.setVisibility(View.VISIBLE);
            getUserGenderAge();
        }
        if (flag.equalsIgnoreCase("patient")) {
            //nameTV.setText("Patient Name:");
            doctorsNameTextView.setVisibility(View.GONE);
        }

        /*sendTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPrescriptionRequest();
            }
        });*/

        conditionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conditionVisible();
                drugGone();
                testGone();
                adviceGone();
            }
        });

        drugTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conditionGone();
                drugVisible();
                testGone();
                adviceGone();
            }
        });

        testTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conditionGone();
                drugGone();
                testVisible();
                adviceGone();
            }
        });

        adviceTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conditionGone();
                drugGone();
                testGone();
                adviceVisible();
            }
        });

        conditionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                conditionString = String.valueOf(s);
            }
        });

        eachDrugs = new ArrayList<PrescribedDrugModel>();
        drugListAdapter = new PescribedDrugListAdapterTwo(context, eachDrugs, getSupportFragmentManager());
        drugListView.setAdapter(drugListAdapter);

        drugAddTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("isEdit", "no");
                bundle.putInt("position", 0);
                bundle.putString("drug_name", "");
                bundle.putString("power", "");
                bundle.putString("dosage", "");
                bundle.putString("noofdays", "");
                PrescriptionDrugAddDialogFragment drugAddDialog = new PrescriptionDrugAddDialogFragment();
                drugAddDialog.setArguments(bundle);
                drugAddDialog.show(getSupportFragmentManager(), "Add Drug");
            }
        });
        eachTests = new ArrayList<String>();
        testListAdapter = new PrescribedTestListAdapterTwo(context, eachTests, getSupportFragmentManager());
        testListView.setAdapter(testListAdapter);

        testAddTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testString = testEditText.getText().toString();
                if (testString.trim().length() > 0) {
                    eachTests.add(testString);
                    testListAdapter.notifyDataSetChanged();
                    testEditText.setText("");
                } else {
                    Toast.makeText(context, "Please enter test name", Toast.LENGTH_SHORT).show();
                }
            }
        });

        adviceEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adviceString = String.valueOf(s);
            }
        });

        getPrescriptionRequest();
    }

    private void conditionVisible() {
        conditionLayout.setVisibility(View.VISIBLE);
        conditionTextView.setTextColor(Color.parseColor("#ffffff"));
        conditionTextView.setBackgroundColor(Color.parseColor("#3a773a"));
        //conditionEditText.setText(conditionString);
    }

    private void conditionGone() {
        conditionLayout.setVisibility(View.GONE);
        conditionTextView.setTextColor(Color.parseColor("#000000"));
        conditionTextView.setBackgroundColor(Color.parseColor("#ffffff"));
    }

    private void drugVisible() {
        drugLayout.setVisibility(View.VISIBLE);
        drugTextView.setTextColor(Color.parseColor("#ffffff"));
        drugTextView.setBackgroundColor(Color.parseColor("#3a773a"));
    }

    private void drugGone() {
        drugLayout.setVisibility(View.GONE);
        drugTextView.setTextColor(Color.parseColor("#000000"));
        drugTextView.setBackgroundColor(Color.parseColor("#ffffff"));
    }

    private void testVisible() {
        testLayout.setVisibility(View.VISIBLE);
        testTextView.setTextColor(Color.parseColor("#ffffff"));
        testTextView.setBackgroundColor(Color.parseColor("#3a773a"));
    }

    private void testGone() {
        testLayout.setVisibility(View.GONE);
        testTextView.setTextColor(Color.parseColor("#000000"));
        testTextView.setBackgroundColor(Color.parseColor("#ffffff"));
    }

    private void adviceVisible() {
        adviceLayout.setVisibility(View.VISIBLE);
        adviceTextView.setTextColor(Color.parseColor("#ffffff"));
        adviceTextView.setBackgroundColor(Color.parseColor("#3a773a"));
        //adviceEditText.setText(adviceString);
    }

    private void adviceGone() {
        adviceLayout.setVisibility(View.GONE);
        adviceTextView.setTextColor(Color.parseColor("#000000"));
        adviceTextView.setBackgroundColor(Color.parseColor("#ffffff"));
    }

    public void addDrugToList(PrescribedDrugModel drugModel) {
        eachDrugs.add(drugModel);
        Log.d("drugList", eachDrugs.get(eachDrugs.size() - 1).getDrug_name());
        drugListAdapter.notifyDataSetChanged();
    }

    public void editDrugFromList(int position, PrescribedDrugModel drugModel) {
        eachDrugs.set(position, drugModel);
        Log.d("drugList", eachDrugs.get(position).getDrug_name());
        drugListAdapter.notifyDataSetChanged();
    }

    private void getPrescriptionRequest() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "getPrescriptionData";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "prescription: " + response.toString());
                pDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject patientJsonObject = jsonObject.getJSONObject("patient");
                    //if (flag.equalsIgnoreCase("patient")) {
                        nameTextView.setText(patientJsonObject.getString("first_name") + " " + patientJsonObject.getString("last_name"));
                        String pat_gender = patientJsonObject.getString("gender").trim();
                        if (!pat_gender.equals("0")) {
                            if (pat_gender.equals("1")) {
                                genderTextView.setText("Male");
                            } else {
                                genderTextView.setText("Female");
                            }
                        }

                        String pat_birth_year = patientJsonObject.getString("birth_year").trim();
                        if (!pat_birth_year.equals("0")) {
                            int birthYear = Integer.parseInt(pat_birth_year);
                            Calendar today = Calendar.getInstance();
                            int currentYear = today.get(Calendar.YEAR);
                            int age = currentYear - birthYear;

                            ageTextView.setText(String.valueOf(age) + " year");
                        }
                    //}
                    //else {
                        //doctorsNameTextView.setText("Doctors Name:  Dr. "+patientJsonObject.getString("first_name") + " " + patientJsonObject.getString("last_name"));
                    //}

                    prescription_id = jsonObject.getJSONObject("prescription").getString("prescription_id");
                    conditionEditText.setText(jsonObject.getJSONObject("prescription").getString("conditions"));
                    ;
                    adviceEditText.setText(jsonObject.getJSONObject("prescription").getString("advice"));

                    JSONArray drugJsonArray = jsonObject.getJSONArray("medicines");
                    eachDrugs.clear();
                    for (int i = 0; i < drugJsonArray.length(); ++i) {
                        JSONObject drugJsonObject = (JSONObject) drugJsonArray.get(i);
                        String med_name = drugJsonObject.getString("medicine_name");
                        String med_dosage = drugJsonObject.getString("dosage");
                        String med_power = drugJsonObject.getString("power");
                        String med_days = drugJsonObject.getString("days");

                        eachDrugs.add(new PrescribedDrugModel(med_name, med_dosage, med_power, med_days));
                    }
                    drugListAdapter.notifyDataSetChanged();

                    JSONArray testJsonArray = jsonObject.getJSONArray("tests");
                    eachTests.clear();
                    for (int i = 0; i < testJsonArray.length(); ++i) {
                        JSONObject testJsonObject = (JSONObject) testJsonArray.get(i);
                        String tes_name = testJsonObject.getString("test_name");
                        eachTests.add(tes_name);
                    }
                    testListAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*prescription_id = response.toString().trim();

                for (int i=0;i<eachDrugs.size();++i) {
                    savePrescribedDrugs(eachDrugs.get(i));
                }

                for (int i=0;i<eachTests.size();++i) {
                    savePrescribedTests(eachTests.get(i));
                }

                Toast.makeText(context,"Prescription saved",Toast.LENGTH_SHORT).show();*/
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: Try Again." + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("appointment_id", appointment_id);
                if (flag.equals("patient")) {
                    params.put("patient_id", patient_id);
                    params.put("doctor_id", appUserId);
                } else {
                    params.put("patient_id", appUserId);
                    params.put("doctor_id", patient_id);
                }

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getUserGenderAge() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getUserGenderDob";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "prescription: " + response.toString());
                //pDialog.dismiss();
                try {
                    JSONObject patientJsonObject = new JSONObject(response);
                    doctorsNameTextView.setText("Doctors Name:  Dr. "+patientJsonObject.getString("first_name") + " " + patientJsonObject.getString("last_name"));
                    /*String pat_gender = patientJsonObject.getString("gender").trim();
                    if (!pat_gender.equals("0")) {
                        if (pat_gender.equals("1")) {
                            genderTextView.setText("Male");
                        } else {
                            genderTextView.setText("Female");
                        }
                    }

                    String pat_birth_year = patientJsonObject.getString("birth_year").trim();
                    if (!pat_birth_year.equals("0")) {
                        int birthYear = Integer.parseInt(pat_birth_year);
                        Calendar today = Calendar.getInstance();
                        int currentYear = today.get(Calendar.YEAR);
                        int age = currentYear - birthYear;

                        ageTextView.setText(String.valueOf(age) + " year");
                    }*/


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: Try Again." + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", patient_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
