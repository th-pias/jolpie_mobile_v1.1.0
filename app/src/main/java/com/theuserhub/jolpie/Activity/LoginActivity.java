package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.theuserhub.jolpie.Databases.SessionManager;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppConstant;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;
import com.theuserhub.jolpie.Volley.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//SinchService.StartFailedListener,
public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private String tag_string_req = "string_req";
    private Context context = this;
    private UserDatabaseHandler db;
    private SessionManager session;
    private ProgressDialog pDialog;

    private TextView signup_tv, forgot_passTextView, signInJolpieTextView, login_btn,facebook,google;
    private EditText email_et, password_et;
    private CheckBox show_password;
    private LinearLayout signInLayout;

    private boolean isJolpieAcc;
    private CallbackManager callbackManager;
    private ProgressDialog mProgressDialog;
    private String TAG_CANCEL = "tag cancel", TAG_ERROR = "tag error";
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 900;
    private static final String TAG = "SignInActivity";

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setTitle("Jolpie");
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        db = new UserDatabaseHandler(context);
        // Session manager
        session = new SessionManager(context);

        isJolpieAcc = false;

        signInLayout = (LinearLayout) findViewById(R.id.sign_in_layout);
        signInJolpieTextView = (TextView) findViewById(R.id.sign_in_jolpie_acc_tv);

        ////////////////////////////////FB/////////////////////////////////
        facebook = (TextView) findViewById(R.id.facebook);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFaceBookLogin();
            }
        });

        ////////////////////////////////////////google////////////////////////////////////////
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().requestProfile().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        google = (TextView) findViewById(R.id.google);

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        login_btn = (TextView) findViewById(R.id.login_btn);
        //login_btn.setEnabled(false);
        signup_tv = (TextView) findViewById(R.id.login_signup_tv);
        email_et = (EditText) findViewById(R.id.login_email_tv);
        password_et = (EditText) findViewById(R.id.login_password_tv);
        show_password = (CheckBox) findViewById(R.id.login_showpassword_cb);
        forgot_passTextView = (TextView) findViewById(R.id.forgot_password_tv);
        forgot_passTextView.setText(Html.fromHtml("<u>Forgot Password?</u>"));

        show_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!show_password.isChecked()) {
                    //password_et.setInputType(InputType.TYPE_CLASS_TEXT);
                    password_et.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    password_et.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                password_et.setSelection(password_et.getText().length());
            }
        });

        signInJolpieTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isJolpieAcc) {
                    isJolpieAcc = false;
                    signInLayout.setVisibility(View.GONE);
                }
                else {
                    isJolpieAcc = true;
                    signInLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        //login button pressed
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppFunctions.isNetworkConnected(LoginActivity.this)) {
                    Const.showNoInternetAlert(LoginActivity.this);
                } else {
                    final String emailAddress = email_et.getText().toString();
                    final String passwordString = password_et.getText().toString();

                    if (emailAddress.trim().length() > 0 && passwordString.trim().length() > 0) {

                        pDialog.setMessage("Signing in...");
                        showDialog();

                        String url = context.getResources().getString(R.string.MAIN_URL) + "login";

                        StringRequest strReq = new StringRequest(Request.Method.POST,
                                url, new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                Log.d("Test1", response.toString());
                                hideDialog();
                                if (!response.equals("wrong_credential")) {
                                    try {
                                        // user successfully logged in
                                        // Create login session
                                        session.setLogin(true);

                                        JSONObject jsonObject = new JSONObject(response);
                                        String user_id = jsonObject.getString("id");
                                        String user_firstname = jsonObject.getString("first_name");
                                        String user_lastname = jsonObject.getString("last_name");
                                        String user_email = jsonObject.getString("email_address");
                                        String user_group = jsonObject.getString("user_group_id");
                                        String user_image_url = jsonObject.getString("photo");
                                        //Toast.makeText(context, user_firstname + " " + user_lastname, Toast.LENGTH_LONG).show();
                                        // Inserting row in users table
                                        db.addUser(user_firstname, user_lastname, user_email, user_id, user_image_url, user_group);
                                        AppConstant.userID = user_id;

                                        //sinchAddUser(user_id);
                                        openMainActivity();
                                        //pDialog.setMessage("Wait...");
                                        //pDialog.show();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(context, "Error: Try again", Toast.LENGTH_LONG).show();
                                    }


                                } else {
                                    Toast.makeText(context, "Wrong Username/Password", Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d("Test", "Error: " + error.getMessage());
                                hideDialog();
                                Toast.makeText(context, "Some error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {
                                // Posting parameters to login url
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("email_address", emailAddress);//john@gmail.com
                                params.put("password", passwordString);//123456
                                //params.put("app_code",app_code);

                                return params;
                            }

                        };
                        // Adding request to request queue
                        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
                    } else {
                        Toast.makeText(context, "Please enter the credentials!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        signup_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        forgot_passTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });

        //pDialog.setMessage("Wait...");
        //pDialog.show();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /*private void sinchAddUser(String userName) {

        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(userName);
        } else {
            //openMainActivity();
        }
    }*/

    private void openMainActivity() {
        Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /*@Override
    protected void onServiceConnected() {
        Log.e("conn", "conn");
        login_btn.setEnabled(true);
        pDialog.dismiss();
        getSinchServiceInterface().setStartListener(this);
        if (session.isLoggedIn()) {
            userDatabaseHandler = new UserDatabaseHandler(context);
            HashMap<String, String> user = userDatabaseHandler.getUserDetails();

            appUserId = user.get("user_id");

            sinchAddUser(appUserId);
        }

    }

    @Override
    public void onStartFailed(SinchError error) {
        //Log.e("onStartFailed", error.toString());
        pDialog.dismiss();
        Toast.makeText(this, "Please press login again or later", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {
        Toast.makeText(this, "Success", Toast.LENGTH_LONG).show();
        pDialog.dismiss();
        openMainActivity();
    }*/

    ///////////////////FB//////////////////////////////
    private void onFaceBookLogin() {
        callbackManager = CallbackManager.Factory.create();

        // Set permissions
        final Bundle params = new Bundle();
        params.putString("user_friends", "public_profile, email, user_birthday");
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_friends", "public_profile, email, user_birthday"));//, "user_photos", "public_profile"

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity1", response.toString());
                                        Log.v("LoginActivity2", object.toString());
                                        // Application code
                                        try {
                                            String email = object.getString("email");
                                            String gender = object.getString("gender");
                                            String name = object.getString("name");
                                            signUpWithFacebook(name,email,gender);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG_CANCEL, "On cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d(TAG_ERROR, error.toString());
                    }
                });
    }

    ////////////////////google//////////////////////////////
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        //updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String name = acct.getDisplayName().toString();
            // acct.get
            String email = acct.getEmail().toString();
            String id = acct.getId().toString();

            signUpWithGoogle(name,email,"male");

            Log.d("success", name + email + id);
            Log.d("result", acct.toString());
            //updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading ...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    private void signUpWithFacebook(final String name, final String email, final String gender) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "facebookLogin";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String user_id = jsonObject.getString("id");
                    String user_fname = jsonObject.getString("first_name");
                    String user_lname = jsonObject.getString("last_name");
                    String user_email = jsonObject.getString("email_address");
                    String user_photo = jsonObject.getString("photo");
                    String user_group_id = jsonObject.getString("user_group_id");
                    String isExist = jsonObject.getString("isExist");


                    //sinchAddUser(user_id);

                    if (isExist.equals("0")) {
                        Intent intent = new Intent(context,SocialLoginGroupActivity.class);
                        intent.putExtra("user_id",user_id);
                        intent.putExtra("first_name",user_fname);
                        intent.putExtra("last_name",user_lname);
                        intent.putExtra("email_address",user_email);
                        intent.putExtra("photo",user_photo);
                        intent.putExtra("user_group_id",user_group_id);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        db = new UserDatabaseHandler(context);
                        // Session manager
                        session = new SessionManager(context);
                        session.setLogin(true);
                        db.addUser(user_fname, user_lname, user_email, user_id, user_photo, user_group_id);

                        //Log.e("userFb",name+":"+email+":"+user_id);
                        openMainActivity();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Some error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name", name);
                params.put("last_name", "");
                params.put("email_address", email);
                params.put("photo", "");
                params.put("gender", gender);
                //params.put("app_code",app_code);

                //Log.d(TAG,first_name+" : "+last_name+" : "+email_address+" : "+password+" : "+group_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void signUpWithGoogle(final String name, final String email, final String gender) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "googleLogin";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String user_id = jsonObject.getString("id");
                    String user_fname = jsonObject.getString("first_name");
                    String user_lname = jsonObject.getString("last_name");
                    String user_email = jsonObject.getString("email_address");
                    String user_photo = jsonObject.getString("photo");
                    String user_group_id = jsonObject.getString("user_group_id");
                    String isExist = jsonObject.getString("isExist");

                    if (isExist.equals("0")) {
                        Intent intent = new Intent(context,SocialLoginGroupActivity.class);
                        intent.putExtra("user_id",user_id);
                        intent.putExtra("first_name",user_fname);
                        intent.putExtra("last_name",user_lname);
                        intent.putExtra("email_address",user_email);
                        intent.putExtra("photo",user_photo);
                        intent.putExtra("user_group_id",user_group_id);
                        startActivity(intent);
                        finish();
                    }
                    else {

                        db = new UserDatabaseHandler(context);
                        // Session manager
                        session = new SessionManager(context);
                        session.setLogin(true);
                        db.addUser(user_fname, user_lname, user_email, user_id, user_photo, user_group_id);

                        //Log.e("userFb",name+":"+email+":"+user_id);
                        openMainActivity();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: Try Again.", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name", name);
                params.put("last_name", "");
                params.put("email_address", email);
                params.put("photo", "");
                params.put("gender", gender);
                //params.put("app_code",app_code);

                //Log.d(TAG,first_name+" : "+last_name+" : "+email_address+" : "+password+" : "+group_id);
                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
