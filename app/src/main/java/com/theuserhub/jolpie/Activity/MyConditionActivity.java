package com.theuserhub.jolpie.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.MyConditionListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.MyConditionsModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyConditionActivity extends AppCompatActivity {
    private Context context = this;

    private FloatingActionButton fab;
    private List<MyConditionsModel> myConditionsList;
    private MyConditionListAdapter myConditionListAdapter;
    private String tag_string_req = "my_condition_string_req";
    private String user_id;
    private TextView emptyTextView;
    private ListView conditionListView;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_condition);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Conditions");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();
        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        user_id = getIntent().getStringExtra("user_id");

        emptyTextView = (TextView) findViewById(R.id.empty_tv);

        if(user_id.equals(appUserId)){
            fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, MyConditionAdd.class);
                    i.putExtra("user_id", user_id);
                    startActivity(i);
                }
            });
        }


        myConditionsList = new ArrayList<MyConditionsModel>();
        conditionListView = (ListView) findViewById(R.id.condition_list);
        myConditionListAdapter =  new MyConditionListAdapter(context,myConditionsList);
        conditionListView.setAdapter(myConditionListAdapter);

        getMyConditions();
    }

    private void getMyConditions()
    {
        String url = context.getResources().getString(R.string.MAIN_URL) + "myConditions";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("MyCondition", "mycondition: " + response.toString());
                myConditionsList.clear();
                try {
                    JSONArray allCond = new JSONArray(response);
                    if (allCond.length() > 0 ) {
                        emptyTextView.setVisibility(View.GONE);
                        conditionListView.setVisibility(View.VISIBLE);
                    }
                    else {
                        emptyTextView.setVisibility(View.VISIBLE);
                        conditionListView.setVisibility(View.GONE);
                    }
                    for (int i=0;i<allCond.length();++i) {
                        JSONObject singleCond = (JSONObject) allCond.get(i);
                        String myCond_id = singleCond.getString("my_condition_id");
                        String cond_id = singleCond.getString("condition_id");
                        String condName_en = singleCond.getString("condition_name");
                        String condName_bn = singleCond.getString("bangla_condition_name");
                        // myConditionsSpinnerList.add(condName_en +"("+condName_bn+")");
                        myConditionsList.add(new MyConditionsModel(myCond_id,cond_id,condName_en,condName_bn));
                    }
                    myConditionListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d("MyCondition", "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyConditions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
