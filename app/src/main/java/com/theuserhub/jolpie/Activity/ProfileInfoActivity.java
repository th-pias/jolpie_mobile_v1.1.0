package com.theuserhub.jolpie.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.EducationModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.Models.WorkModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 4/10/2016.
 */
public class ProfileInfoActivity extends AppCompatActivity {
    private Context context = this;
    private String TAG = "ProfileInfoActivity", tag_string_request = "info_str_req";

    private ProgressDialog pDialog;
    private String user_id, user_fname, user_lname, group_id, affiliation,contact_no_1,street_address,
            address,registration_number,degree,type;
    private List<SpecialityModel>specialityList;
    private List<WorkModel>workList;
    private List<EducationModel>eduactionList;
    private LinearLayout experienceViewLinlay,aboutEditLinlay,aboutViewLinlay,specialityViewLinlay,
            eduAndTrainViewLinlay,othersViewLinlay,degreeLayout,
            info_registration_linlay,mobile_linlay,address_linlay,experienceAddLinlay,
            specialityAddLinlay,eduAndTrainAddLinlay,registrationEditLinlay,othersEditLinlay;
    private TextView degreeTextView,info_designation,info_mobile,info_address,noData,
            info_reg_num,info_affiliation, expEmptyTextView, eduEmptyTextView;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName,appUserLName,appUserEmail,appUserId,appUserGroupId,appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Info");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent infointent = getIntent();
        user_id = infointent.getStringExtra("user_id");
        user_fname = infointent.getStringExtra("user_fname");
        user_lname = infointent.getStringExtra("user_lname");
        group_id = infointent.getStringExtra("group_id");

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        specialityList = new ArrayList<SpecialityModel>();
        workList = new ArrayList<WorkModel>();
        eduactionList = new ArrayList<EducationModel>();

        aboutViewLinlay = (LinearLayout) findViewById(R.id.info_about_linlay);
        aboutEditLinlay = (LinearLayout) findViewById(R.id.info_about_edit_linlay);
        degreeLayout = (LinearLayout) findViewById(R.id.degree_layout);
        degreeTextView = (TextView) findViewById(R.id.degree_tv);
        info_designation = (TextView) findViewById(R.id.info_about_designation_tv);
        mobile_linlay = (LinearLayout) findViewById(R.id.mobile_linlay);
        info_mobile = (TextView) findViewById(R.id.info_about_mobile_tv);
        address_linlay = (LinearLayout) findViewById(R.id.address_linlay);
        info_address = (TextView) findViewById(R.id.info_about_address_tv);

        experienceViewLinlay = (LinearLayout) findViewById(R.id.info_experience_linlay);
        experienceAddLinlay = (LinearLayout) findViewById(R.id.info_experience_add_linlay);
        expEmptyTextView = (TextView) findViewById(R.id.experience_empty_tv);

        specialityViewLinlay = (LinearLayout) findViewById(R.id.info_specialization_linlay);
        specialityAddLinlay = (LinearLayout) findViewById(R.id.info_specialization_add_linlay);
        noData = (TextView) findViewById(R.id.no_data);

        eduAndTrainViewLinlay = (LinearLayout) findViewById(R.id.info_eduandtrain_linlay);
        eduAndTrainAddLinlay = (LinearLayout) findViewById(R.id.info_eduandtrain_add_linlay);
        eduEmptyTextView = (TextView) findViewById(R.id.education_empty_tv);

        othersViewLinlay = (LinearLayout) findViewById(R.id.info_others_linlay);
        othersEditLinlay = (LinearLayout) findViewById(R.id.info_others_edit_linlay);
        info_affiliation = (TextView) findViewById(R.id.info_others_affiliation_tv);

        info_registration_linlay = (LinearLayout) findViewById(R.id.info_registration_linlay);
        info_reg_num = (TextView) findViewById(R.id.info_registrations_regnum_tv);
        registrationEditLinlay = (LinearLayout) findViewById(R.id.info_registration_edit_linlay);

        getUserInfo();
    }

    private void updatingUi() {

        if (appUserId.equals(user_id)) {
            aboutEditLinlay.setVisibility(View.VISIBLE);
            experienceAddLinlay.setVisibility(View.VISIBLE);
            specialityAddLinlay.setVisibility(View.VISIBLE);
            eduAndTrainAddLinlay.setVisibility(View.VISIBLE);
            othersEditLinlay.setVisibility(View.VISIBLE);
            registrationEditLinlay.setVisibility(View.VISIBLE);
        }
        else {
            aboutEditLinlay.setVisibility(View.GONE);
            experienceAddLinlay.setVisibility(View.GONE);
            specialityAddLinlay.setVisibility(View.GONE);
            eduAndTrainAddLinlay.setVisibility(View.GONE);
            othersEditLinlay.setVisibility(View.GONE);
            registrationEditLinlay.setVisibility(View.GONE);
        }

        aboutEditLinlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getResources().getString(R.string.HOME_URL)));
                startActivity(browserIntent);
            }
        });
        if (group_id.equals("2")) {
            degreeTextView.setText("Degree");
            info_designation.setText(degree);
        }
        else if (group_id.equals("4")) {
            degreeTextView.setText("Type");
            if (type.equals("1")) {
                info_designation.setText("General Hospital");
            }
            else if (type.equals("2")) {
                info_designation.setText("Specialized Hospital");
            }
            else if (type.equals("3")) {
                info_designation.setText("Clinic");
            }
            else {
                info_designation.setText("");
            }
        }
        else {
            degreeLayout.setVisibility(View.GONE);
        }

        if (group_id.equals("1") || group_id.equals("2") || group_id.equals("3")) {
            mobile_linlay.setVisibility(View.GONE);
        }
        else {
            info_mobile.setText(contact_no_1);
        }

        if (group_id.equals("1") || group_id.equals("3")) {
            if (appUserId.equals(user_id)) {
                if (street_address.length()>0) {
                    info_address.setText(street_address + " " +address);
                }
                else {
                    info_address.setText(address);
                }
            }
            else {
                address_linlay.setVisibility(View.GONE);
            }
        }
        else if (group_id.equals("2")) {
            address_linlay.setVisibility(View.GONE);
        }
        else {
            if (street_address.length()>0) {
                info_address.setText(street_address + " " +address);
            }
            else {
                info_address.setText(address);
            }
        }

        experienceAddLinlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getResources().getString(R.string.HOME_URL)));
                startActivity(browserIntent);
            }
        });

        eduAndTrainAddLinlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getResources().getString(R.string.HOME_URL)));
                startActivity(browserIntent);
            }
        });

        if (group_id.equals("1") || group_id.equals("2") || group_id.equals("3")) {
            if (workList.size()>0) {
                ViewGroup lazzy_experience_linearlayout = (ViewGroup) findViewById(R.id.experience_linearlayout);
                for (int i=0;i<workList.size();++i) {
                    LayoutInflater inflater_child = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                    View child_view = inflater_child.inflate(R.layout.experience_content, lazzy_experience_linearlayout, false);

                    TextView exp_name = (TextView) child_view.findViewById(R.id.exp_name);
                    TextView exp_position = (TextView) child_view.findViewById(R.id.exp_position);
                    TextView exp_start = (TextView) child_view.findViewById(R.id.exp_start);
                    TextView exp_end = (TextView) child_view.findViewById(R.id.exp_end);

                    exp_name.setText(workList.get(i).getOrg_name());
                    exp_position.setText(workList.get(i).getPosition());
                    exp_start.setText(workList.get(i).getFrom());
                    exp_end.setText(workList.get(i).getTill());
                    lazzy_experience_linearlayout.addView(child_view);
                }
            }
            else {
                expEmptyTextView.setVisibility(View.VISIBLE);
            }

            if (eduactionList.size()>0) {
                ViewGroup lazzy_education_linear_layout = (ViewGroup) findViewById(R.id.education_linear_layout);
                for (int i=0;i<eduactionList.size();++i) {
                    LayoutInflater inflater_child = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                    View child_view = inflater_child.inflate(R.layout.education_content, lazzy_education_linear_layout, false);
                    ////////
                    TextView edu_title = (TextView) child_view.findViewById(R.id.edu_title);
                    TextView edu_institute = (TextView) child_view.findViewById(R.id.edu_institute);
                    TextView edu_year = (TextView) child_view.findViewById(R.id.edu_year);

                    edu_title.setText(eduactionList.get(i).getDegree());
                    edu_institute.setText(eduactionList.get(i).getIns_name());
                    edu_year.setText(eduactionList.get(i).getYear());

                    lazzy_education_linear_layout.addView(child_view);
                }
            }
            else {
                eduEmptyTextView.setVisibility(View.VISIBLE);
            }
        }
        else {
            experienceViewLinlay.setVisibility(View.GONE);
            eduAndTrainViewLinlay.setVisibility(View.GONE);
        }

        specialityAddLinlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddSpecialityActivity.class);
                intent.putExtra("user_id", user_id);
                startActivity(intent);
            }
        });

        if (group_id.equals("2")) {
            if (specialityList.size()>0) {
                for (int i=0;i<specialityList.size();++i) {
                    if (i == 0) {
                        noData.setText(specialityList.get(i).getName());
                    }
                    else {
                        noData.setText(noData.getText().toString() + "," + specialityList.get(i).getName());
                    }
                }
            }
            else {
                noData.setText("No Data Added.");
            }
        }
        else {
            specialityViewLinlay.setVisibility(View.GONE);
        }

        registrationEditLinlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getResources().getString(R.string.HOME_URL)));
                startActivity(browserIntent);
            }
        });
        if (group_id.equals("4") || group_id.equals("5") || group_id.equals("6")) {
            info_reg_num.setText(registration_number);
        }
        else {
            info_registration_linlay.setVisibility(View.GONE);
        }

        othersEditLinlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getResources().getString(R.string.HOME_URL)));
                startActivity(browserIntent);
            }
        });
        if (group_id.equals("2")) {
            if (affiliation.length()>0) {
                info_affiliation.setText(affiliation);
            }
            else {
                info_affiliation.setText("No Data Added.");
            }
        }
        else {
            othersViewLinlay.setVisibility(View.GONE);
        }
    }

    private void getUserInfo() {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "getUserInfo";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "work: " + response.toString());
                pDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    user_id = jsonObject.getString("id");
                    group_id = jsonObject.getString("user_group_id");
                    affiliation = jsonObject.getString("affiliation");
                    contact_no_1 = jsonObject.getString("contact_no_1");
                    street_address = jsonObject.getString("street_address");
                    address = jsonObject.getString("address");
                    registration_number = jsonObject.getString("registration_number");
                    degree = jsonObject.getString("degree");
                    type = jsonObject.getString("type");

                    if (group_id.equals("2")) {
                        JSONArray jsonArray1 = jsonObject.getJSONArray("speciality");
                        for (int i=0;i<jsonArray1.length();++i) {
                            JSONObject jsonObject1 = (JSONObject) jsonArray1.get(i);
                            String spe_id = jsonObject1.getString("specialization_id");
                            String spe_name = jsonObject1.getString("name");

                            specialityList.add(new SpecialityModel(spe_id,spe_name));
                        }
                    }

                    if (group_id.equals("1") || group_id.equals("2") || group_id.equals("3")) {
                        JSONArray jsonArray2 = jsonObject.getJSONArray("work");
                        for (int i = 0; i < jsonArray2.length(); ++i) {
                            JSONObject jsonObject2 = (JSONObject) jsonArray2.get(i);
                            String work_id = jsonObject2.getString("id");
                            String com_name = jsonObject2.getString("company_name");
                            String work_pos = jsonObject2.getString("position");
                            String work_start = jsonObject2.getString("start_date");
                            String work_end = jsonObject2.getString("end_date");

                            workList.add(new WorkModel(work_id, com_name, work_pos, work_start, work_end));
                        }

                        JSONArray jsonArray3 = jsonObject.getJSONArray("edu");
                        for (int i = 0; i < jsonArray3.length(); ++i) {
                            JSONObject jsonObject3 = (JSONObject) jsonArray3.get(i);
                            String edu_id = jsonObject3.getString("id");
                            String deg_name = jsonObject3.getString("title");
                            String ins_name = jsonObject3.getString("institute");
                            String edu_year = jsonObject3.getString("year");

                            eduactionList.add(new EducationModel(edu_id, deg_name, ins_name, edu_year));
                        }
                    }

                    updatingUi();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Error: try again.", Toast.LENGTH_LONG).show();
                    finish();
                }

                /*try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray workJsonArray = jsonObject.getJSONArray("work");

                    ViewGroup lazzy_experience_linearlayout = (ViewGroup) findViewById(R.id.experience_linearlayout);

                    for (int i=0;i<workJsonArray.length();++i) {
                        JSONObject workJsonObject = workJsonArray.getJSONObject(i);
                        String com_name = workJsonObject.getString("company_name");
                        String com_pos = workJsonObject.getString("position");
                        String com_start = workJsonObject.getString("start_date");
                        String com_end = workJsonObject.getString("end_date");

                        LayoutInflater inflater_child = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                        View child_view = inflater_child.inflate(R.layout.experience_content, lazzy_experience_linearlayout, false);
                        ////////
                        TextView exp_name = (TextView) child_view.findViewById(R.id.exp_name);
                        TextView exp_position = (TextView) child_view.findViewById(R.id.exp_position);
                        TextView exp_start = (TextView) child_view.findViewById(R.id.exp_start);
                        TextView exp_end = (TextView) child_view.findViewById(R.id.exp_end);

                        exp_name.setText(com_name);
                        exp_position.setText(com_pos);
                        exp_start.setText(com_start);
                        exp_end.setText(com_end);
                        lazzy_experience_linearlayout.addView(child_view);
                    }

                    JSONArray eduJsonArray = jsonObject.getJSONArray("education");

                    ViewGroup lazzy_education_linear_layout = (ViewGroup) findViewById(R.id.education_linear_layout);

                    for (int i=0;i<eduJsonArray.length();++i) {
                        JSONObject eduJsonObject = eduJsonArray.getJSONObject(i);
                        String edu_name = eduJsonObject.getString("title");
                        String edu_ins = eduJsonObject.getString("institute");
                        String edu_yr = eduJsonObject.getString("year");

                        LayoutInflater inflater_child = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                        View child_view = inflater_child.inflate(R.layout.education_content, lazzy_education_linear_layout, false);
                        ////////
                        TextView edu_title = (TextView) child_view.findViewById(R.id.edu_title);
                        TextView edu_institute = (TextView) child_view.findViewById(R.id.edu_institute);
                        TextView edu_year = (TextView) child_view.findViewById(R.id.edu_year);

                        edu_title.setText(edu_name);
                        edu_institute.setText(edu_ins);
                        edu_year.setText(edu_yr);

                        lazzy_education_linear_layout.addView(child_view);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: try again.", Toast.LENGTH_LONG).show();
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
