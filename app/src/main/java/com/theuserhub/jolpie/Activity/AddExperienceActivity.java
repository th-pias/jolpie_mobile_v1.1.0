package com.theuserhub.jolpie.Activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.theuserhub.jolpie.R;

import java.util.Calendar;

public class AddExperienceActivity extends AppCompatActivity {

    private Context context = this;

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView fromdateView, todateView;
    private CheckBox currentWorkBox;
    private int cur_year, cur_month, cur_day, from_year, from_month, from_day, to_year, to_month, to_day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_experience);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fromdateView = (TextView) findViewById(R.id.add_exp_fromdate_tv);
        todateView = (TextView) findViewById(R.id.add_exp_todate_tv);
        currentWorkBox = (CheckBox) findViewById(R.id.add_exp_curwork_cb);

        calendar = Calendar.getInstance();
        cur_year = calendar.get(Calendar.YEAR);
        cur_month = calendar.get(Calendar.MONTH) + 1;
        cur_day = calendar.get(Calendar.DAY_OF_MONTH);

        fromdateView.setText(new StringBuilder().append(cur_day).append("/").append(cur_month).append("/").append(cur_year));
        todateView.setText(new StringBuilder().append(cur_day).append("/").append(cur_month).append("/").append(cur_year));

        fromdateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
            }
        });

        todateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentWorkBox.isChecked()) {
                    Toast.makeText(context,"yes",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(context,"no",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
