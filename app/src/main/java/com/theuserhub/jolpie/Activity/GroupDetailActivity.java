package com.theuserhub.jolpie.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.GroupFeedListAdapter;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.SinglePostModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupDetailActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "GroupDetailActivity", tag_string_req = "grp_detail_str_req";

    private ListView groupPostsListView;
    private GroupFeedListAdapter grpFeedListAdapter;
    private List<SinglePostModel> feedList;
    private ImageView grpImageView;
    private NetworkImageView grpNImageView;
    private TextView grpNameTextView, grpDesTextView, grpMembrTextView, grpDiscussTextView, grpJoinTextView, grpAddMembrTextView;
    private LinearLayout grpDetailActivityLayout;
    private String group_id, group_name, group_description, group_image, group_membr_count, group_discuss_count, group_created_by;
    private boolean isJoined, isDefault;
    private FloatingActionButton fab;
    private boolean isEnd = false;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Group");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        Intent groupDetailIntent = getIntent();
        group_id = groupDetailIntent.getStringExtra("group_id");
        group_name = groupDetailIntent.getStringExtra("group_name");
        group_description = groupDetailIntent.getStringExtra("group_des");
        group_image = groupDetailIntent.getStringExtra("group_img");
        group_membr_count = groupDetailIntent.getStringExtra("group_membr");
        group_discuss_count = groupDetailIntent.getStringExtra("group_discuss");
        group_created_by = groupDetailIntent.getStringExtra("group_created_by");
        Log.d("NAme", group_name);
        //Toast.makeText(context, group_id, Toast.LENGTH_LONG).show();

        isJoined = false;
        isDefault = false;

        fab = (FloatingActionButton) findViewById(R.id.fab);
        if (appUserGroupId.equals("4") || appUserGroupId.equals("5") || appUserGroupId.equals("6")) {
            fab.setVisibility(View.GONE);
        }
        else {
            fab.setVisibility(View.VISIBLE);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PostActivity.class);
                intent.putExtra("group_id",group_id);
                startActivity(intent);
            }
        });

        groupPostsListView = (ListView) findViewById(R.id.grpDetail_posts_lv);
        feedList = new ArrayList<SinglePostModel>();
        grpFeedListAdapter = new GroupFeedListAdapter(context, feedList, appUserId, group_id,isEnd,appUserGroupId);
        groupPostsListView.setAdapter(grpFeedListAdapter);

        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup detailViewGroup = (ViewGroup) inflater.inflate(R.layout.header_group_detail, null);

        grpImageView = (ImageView) detailViewGroup.findViewById(R.id.grpDetail_img_iv);
        grpNImageView = (NetworkImageView) detailViewGroup.findViewById(R.id.grpDetail_img_niv);

        grpNameTextView = (TextView) detailViewGroup.findViewById(R.id.grpDetail_title_tv);
        grpDesTextView = (TextView) detailViewGroup.findViewById(R.id.grpDetail_des_tv);
        grpDetailActivityLayout = (LinearLayout) detailViewGroup.findViewById(R.id.grpDetail_activity_linlay);
        grpAddMembrTextView = (TextView) detailViewGroup.findViewById(R.id.grpDetail_member_add_tv);
        grpMembrTextView = (TextView) detailViewGroup.findViewById(R.id.grpDetail_member_count_tv);
        grpDiscussTextView = (TextView) detailViewGroup.findViewById(R.id.grpDetail_discuss_count_tv);
        grpJoinTextView = (TextView) detailViewGroup.findViewById(R.id.grpDetail_join_leave_delete_tv);

        groupPostsListView.addHeaderView(detailViewGroup);

        grpNameTextView.setText(group_name);
        grpDesTextView.setText(group_description);
        //grpMembrTextView.setText(group_membr_count + "\nMembers");
        //grpDiscussTextView.setText(group_discuss_count + "\nDisscuss");

        //getGroupDetail();

        /*if (appUserId.equals(group_created_by)) {
            grpAddMembrTextView.setVisibility(View.VISIBLE);
            grpDetailActivityLayout.setWeightSum(4.0f);
            grpJoinTextView.setText("Delete\nGroup");
            fab.setVisibility(View.VISIBLE);
        } else {
            grpAddMembrTextView.setVisibility(View.GONE);
            grpDetailActivityLayout.setWeightSum(3.0f);
        }*/

        /*if (AppFunctions.isNetworkConnected(context)) {
            getGroupPostsRequest();
        } else {
            Toast.makeText(context, "Connect your Internet", Toast.LENGTH_LONG).show();
        }*/

        grpAddMembrTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupDetailActivity.this, GroupMemberAddActivity.class);
                intent.putExtra("group_id", group_id);
                intent.putExtra("group_name", group_name);
                startActivity(intent);
            }
        });

        viewMemberAndJoinFeature();
    }

    private void viewMemberAndJoinFeature() {
        grpMembrTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupDetailActivity.this, GroupMemberListActivity.class);
                intent.putExtra("group_id", group_id);
                intent.putExtra("group_name", group_name);
                intent.putExtra("created_by", group_created_by);
                startActivity(intent);
            }
        });
        grpJoinTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (appUserId.equals(group_created_by)) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setTitle("Are you sure?");
                    alertDialogBuilder.setMessage("You wanted to delete this group");

                    alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Toast.makeText(context, "Delete", Toast.LENGTH_SHORT).show();
                            //deleteReviewRequest(eachItems.get(position).getReview_id(),position);
                            arg0.dismiss();
                        }
                    });

                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //alertDialog.show();
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    if (isJoined) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Are you sure?");
                        alertDialogBuilder.setMessage("You wanted to leave this group");

                        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                //Toast.makeText(context, "Leave", Toast.LENGTH_SHORT).show();
                                groupLeaveRequest();
                                arg0.dismiss();
                            }
                        });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //alertDialog.show();
                                dialog.dismiss();
                            }
                        });
                        final AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        joinGroupRequest();
                    }
                }
            }
        });
    }

    //is appuser already joined the detailed group...............................
    /*private void isJoinedRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "isJoinedGroup";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("0")) {
                    grpJoinTextView.setText("Join\nGroup");
                    isJoined = false;
                    fab.setVisibility(View.GONE);
                } else if (response.toString().trim().equals("1")) {
                    grpJoinTextView.setText("Leave\nGroup");
                    isJoined = true;
                    fab.setVisibility(View.VISIBLE);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("group_id", group_id);
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }*/

    //appuser wants to join the detailed group....................................
    private void joinGroupRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "joinGroup";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context, "Joined", Toast.LENGTH_SHORT).show();
                    grpJoinTextView.setText("Leave\nGroup");
                    isJoined = true;
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error: try again later", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("group_id", group_id);
                params.put("user_id", appUserId);
                params.put("status", "approved");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //appuser want leave the detailed group....................................
    private void groupLeaveRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "leaveGroup";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("isLeaved", response.toString());
                if (response.toString().trim().equals("1")) {
                    finish();
                } else {
                    Toast.makeText(context, "Error occured", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d("isLeaved", "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("group_id", group_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //always get the updated summary of the group.......................
    private void getGroupDetail() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getGroupDetail";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getGroupDetail: " + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String default_group = jsonObject.getString("default_group");
                    group_name = jsonObject.getString("name");
                    group_description = jsonObject.getString("description");
                    group_image = jsonObject.getString("image");
                    group_membr_count = jsonObject.getString("total_member");
                    group_discuss_count = jsonObject.getString("total_discussion");
                    group_created_by = jsonObject.getString("created_by");
                    String join_by_me = jsonObject.getString("isJoined");
                    if (join_by_me.trim().equalsIgnoreCase("0")) {
                        isJoined = false;
                    }
                    else {
                        isJoined = true;
                    }

                    if (group_image.length()>1) {
                        if (imageLoader == null)
                            imageLoader = AppController.getInstance().getImageLoader();

                        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + group_image;
                        grpNImageView.setImageUrl(meta_url, imageLoader);
                    }

                    grpNameTextView.setText(group_name);
                    grpDesTextView.setText(group_description);
                    grpDiscussTextView.setText(group_discuss_count + "\nDiscussion");

                    if (default_group.equals("1")) {
                        //grpJoinTextView.setText("");
                        //grpMembrTextView.setText("");
                        grpJoinTextView.setVisibility(View.GONE);
                        grpMembrTextView.setVisibility(View.GONE);

                        isDefault = true;
                        if (appUserGroupId.equals("4") || appUserGroupId.equals("5") || appUserGroupId.equals("6")) {
                            fab.setVisibility(View.GONE);
                        }
                        else {
                            fab.setVisibility(View.VISIBLE);
                        }
                        Log.e("default2", "" + default_group+isDefault);
                    }
                    else {
                        isDefault = false;
                        grpJoinTextView.setVisibility(View.VISIBLE);
                        grpMembrTextView.setVisibility(View.VISIBLE);
                        grpMembrTextView.setText(group_membr_count + "\nMembers");
                        if (appUserId.equals(group_created_by)) {
                            grpAddMembrTextView.setVisibility(View.VISIBLE);
                            grpDetailActivityLayout.setWeightSum(4.0f);
                            grpJoinTextView.setText("Delete\nGroup");
                            if (appUserGroupId.equals("4") || appUserGroupId.equals("5") || appUserGroupId.equals("6"))
                                fab.setVisibility(View.GONE);
                            else {
                                fab.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            grpAddMembrTextView.setVisibility(View.GONE);
                            grpDetailActivityLayout.setWeightSum(3.0f);
                            if (isJoined) {
                                grpJoinTextView.setText("Leave\nGroup");
                                if (appUserGroupId.equals("4") || appUserGroupId.equals("5") || appUserGroupId.equals("6"))
                                fab.setVisibility(View.GONE);
                                else {
                                    fab.setVisibility(View.VISIBLE);
                                }
                            }
                            else {
                                grpJoinTextView.setText("Join\nGroup");
                                fab.setVisibility(View.GONE);
                            }
                        }
                    }
                    //get posts of this group......................................
                    getGroupPostsRequest();
                    //get posts of this group......................................
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("group_id", group_id);
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //get detailed group posts.........................................................
    private void getGroupPostsRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getGroupPosts";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "GroupPost : "+response.toString());
                feedList.clear();
                String post_id,post_title,post_description,post_feeling,post_likes,post_answers,post_follows,post_userId,post_user_fname,post_user_lname,post_user_photo,
                        post_userGroupId,post_created,post_updated,post_imgUrl,post_vdoUrl,post_liked;
                try {
                    JSONArray allPosts = new JSONArray(response);
                    if(allPosts.length()<8){
                        isEnd = true;
                    }
                    for (int i = 0; i < allPosts.length(); ++i) {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        post_id = singlePost.getString("id");
                        post_title = singlePost.getString("title");
                        post_description = singlePost.getString("description");
                        post_feeling = singlePost.getString("feeling");
                        post_likes = singlePost.getString("good_count");
                        post_answers = singlePost.getString("answer_count");
                        post_follows = singlePost.getString("follow_count");
                        post_userId = singlePost.getString("user_id");
                        post_user_fname = singlePost.getString("first_name");
                        post_user_lname = singlePost.getString("last_name");
                        post_user_photo = singlePost.getString("user_photo");
                        post_userGroupId = singlePost.getString("user_group_id");
                        post_created = singlePost.getString("created_at");
                        post_updated = singlePost.getString("updated_at");
                        post_imgUrl = singlePost.getString("photo");
                        post_vdoUrl = singlePost.getString("video");
                        post_liked = singlePost.getString("liked_by_me");
                        //Toast.makeText(context,post_id+"\n"+post_title+"\n"+post_des,Toast.LENGTH_LONG).show();
                        SinglePostModel pst = new SinglePostModel(post_id,post_title,post_description,post_feeling,post_likes,post_answers,post_follows,post_userId,post_user_fname,post_user_lname,post_user_photo,
                                post_userGroupId,post_created,post_updated,post_imgUrl,post_vdoUrl,post_liked);
                        feedList.add(pst);
                    }

                    //refresh adapter
                    //grpFeedListAdapter.notifyDataSetChanged();
                    grpFeedListAdapter = new GroupFeedListAdapter(context, feedList, appUserId, group_id,isEnd,appUserGroupId);
                    groupPostsListView.setAdapter(grpFeedListAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("group_id", group_id);
                params.put("user_id", appUserId);
                params.put("start_index", "0");
                params.put("take_total", "8");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getGroupDetail();
    }

    /*@Override
    public void onPause() {
        super.onPause();
        //cancelling all request when fragment in pause....
        AppController.getInstance().cancelPendingRequests(tag_string_req);
    }*/
}
