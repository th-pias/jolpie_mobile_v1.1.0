package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;

import java.util.List;

/**
 * Created by tumpa on 3/3/2016.
 */
public class SpecialityDeleteAdapter extends BaseAdapter {
    private Context context;
    private List<SpecialityModel> specialityModelArrayList;

    private static LayoutInflater inflater=null;

    public SpecialityDeleteAdapter(Context context, List<SpecialityModel> specialityModelArrayList){

        this.context = context;
        this.specialityModelArrayList = specialityModelArrayList;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return specialityModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class Holder
    {
        TextView tv;
        Button btn;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_view_info_activity_speciality,null);
        holder.tv = (TextView) rowView.findViewById(R.id.current_speciality);
        holder.btn = (Button) rowView.findViewById(R.id.delete);
        SpecialityModel specialityModel = specialityModelArrayList.get(position);
        holder.tv.setText(specialityModel.getName().toString());
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return rowView;
    }
}
