package com.theuserhub.jolpie.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.GroupDetailActivity;
import com.theuserhub.jolpie.Activity.MagazineActivity;
import com.theuserhub.jolpie.Activity.MagazineDetailActivity;
import com.theuserhub.jolpie.Activity.PeopleWhoLiked;
import com.theuserhub.jolpie.Activity.PostDetailActivity;
import com.theuserhub.jolpie.Activity.PostUpdateActivity;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Models.NewsfeedModel;
import com.theuserhub.jolpie.Models.PatientsLikeMeModel;
import com.theuserhub.jolpie.Models.PostInfoModel;
import com.theuserhub.jolpie.Models.SavedDoctorsModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 4/10/2016.
 */
public class SavedDoctorsUpdatesAdapter extends BaseAdapter {
    private String TAG = "FeedListAdapter";
    private String tag_string_req = "feed_str_req";
    private LayoutInflater inflater;
    //private List<SinglePostModel> feedList;
    private List<NewsfeedModel> eachNewsfeed;
    private List<SavedDoctorsModel>eachPatients;
    private Context context;
    private String user_id, user_group_id;
    private boolean isEnd;
    private int limit_start = 5, limit_end = 5;

    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public SavedDoctorsUpdatesAdapter(Context context, List<NewsfeedModel> eachNewsfeed, String user_id, String user_group_id, boolean isEnd, List<SavedDoctorsModel>eachPatients) {
        this.context = context;
        this.eachNewsfeed = eachNewsfeed;
        this.user_id = user_id;
        this.user_group_id = user_group_id;
        this.isEnd = isEnd;
        this.eachPatients = eachPatients;
    }

    @Override
    public int getCount() {
        return eachNewsfeed.size();
    }

    @Override
    public Object getItem(int location) {
        return eachNewsfeed.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Boolean[] liked = {false};

        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_feed, null);

        Typeface editFont = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");

        final PostInfoModel postInfoModel = eachNewsfeed.get(position).getPostInfoModel();
        final String activity_by_user_name, post_by_user_name, post_to_user_name;
        if (eachNewsfeed.get(position).getBy_user_group_id().equals("2")) {
            activity_by_user_name = "Dr. " + eachNewsfeed.get(position).getBy_user_fname() + " " + eachNewsfeed.get(position).getBy_user_lname();
        } else {
            activity_by_user_name = eachNewsfeed.get(position).getBy_user_fname() + " " + eachNewsfeed.get(position).getBy_user_lname();
        }

        if (postInfoModel.getBy_user_group_id().equals("2")) {
            post_by_user_name = "Dr. " + postInfoModel.getBy_fname() + " " + postInfoModel.getBy_lname();
        } else {
            post_by_user_name = postInfoModel.getBy_fname() + " " + postInfoModel.getBy_lname();
        }

        if (postInfoModel.getTo_user_group_id().equals("2")) {
            post_to_user_name = "Dr. " + postInfoModel.getTo_fname() + " " + postInfoModel.getTo_lname();
        } else {
            post_to_user_name = postInfoModel.getTo_fname() + " " + postInfoModel.getTo_lname();
        }

        RelativeLayout activityLayout = (RelativeLayout) convertView.findViewById(R.id.feedActivity_layout);
        View viewUnderLayout = convertView.findViewById(R.id.view_under_feedActivity);
        TextView activityTextView = (TextView) convertView.findViewById(R.id.feedActivity_text);

        ImageView activityByImageView = (ImageView) convertView.findViewById(R.id.feedActivity_user_image);
        NetworkImageView activityByNImageView = (NetworkImageView) convertView.findViewById(R.id.feedActivity_user_nimage);

        ImageView postByImageView = (ImageView) convertView.findViewById(R.id.userimageoffeed);
        NetworkImageView postByNImageView = (NetworkImageView) convertView.findViewById(R.id.userNimageoffeed);

        TextView userNameOfFeed = (TextView) convertView.findViewById(R.id.usernameoffeed);
        RatingBar reviewRatingBar = (RatingBar) convertView.findViewById(R.id.feedratingbar);


        LinearLayout singleFeedBodyLinearLayout = (LinearLayout) convertView.findViewById(R.id.singlefeedbodylinlay);
        LinearLayout likeCommShareLayout = (LinearLayout) convertView.findViewById(R.id.like_comm_share_layout);
        LinearLayout likeCommShareActLayout = (LinearLayout) convertView.findViewById(R.id.like_comment_share_activity_layout);
        LinearLayout answerLinearLayout = (LinearLayout) convertView.findViewById(R.id.answerlinlay);
        LinearLayout answercountLinLay = (LinearLayout) convertView.findViewById(R.id.answercount_linlay);
        View underDesView = convertView.findViewById(R.id.view_under_des);
        View underLikeCount = convertView.findViewById(R.id.view_under_like_count);

        final Button editButton = (Button) convertView.findViewById(R.id.post_edit_delete_button);

        editButton.setTypeface(editFont);

        if (eachNewsfeed.get(position).getSection().equals("post")) {
            if (eachNewsfeed.get(position).getActivity().equals("post")) {
                viewUnderLayout.setVisibility(View.GONE);
            } else if (eachNewsfeed.get(position).getActivity().equals("liked")) {
                activityLayout.setVisibility(View.VISIBLE);

                String likedString = activity_by_user_name + " liked a Post";

                StyleSpan textBoldStyle = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                Spannable wordtoSpan = new SpannableString(likedString);
                wordtoSpan.setSpan(textBoldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);
                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());

            } else if (eachNewsfeed.get(position).getActivity().equals("commented")) {
                activityLayout.setVisibility(View.VISIBLE);

                String commentedString = activity_by_user_name + " commented a Post";

                StyleSpan textBoldStyle = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                Spannable wordtoSpan = new SpannableString(commentedString);
                wordtoSpan.setSpan(textBoldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);
                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (eachNewsfeed.get(position).getActivity().equals("shared")) {
                activityLayout.setVisibility(View.VISIBLE);

                String sharedString = activity_by_user_name + " shared a Post";

                StyleSpan textBoldStyle = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);
                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (eachNewsfeed.get(position).getActivity().equals("taged")) {
                activityLayout.setVisibility(View.VISIBLE);
            }
            String temp = "";
            SpannableStringBuilder builder = new SpannableStringBuilder();

            StyleSpan boldStyle = new StyleSpan(android.graphics.Typeface.BOLD);
            Spannable wordtoSpan = new SpannableString(post_by_user_name + " ");
            wordtoSpan.setSpan(boldStyle, 0, (post_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            if (postInfoModel.getFeeling().length() > 0) {
                temp = post_by_user_name + " feeling " + postInfoModel.getFeeling();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    builder.append(wordtoSpan).append(" feeling ");

                    if (postInfoModel.getFeeling().equalsIgnoreCase("Very Good")) {
                        builder.append(" ", new ImageSpan(context, R.drawable.feeling_very_good), 0);
                    } else if (postInfoModel.getFeeling().equalsIgnoreCase("Good")) {
                        builder.append(" ", new ImageSpan(context, R.drawable.feeling_good), 0);
                    } else if (postInfoModel.getFeeling().equalsIgnoreCase("Neutral")) {
                        builder.append(" ", new ImageSpan(context, R.drawable.feeling_neutral), 0);
                    } else if (postInfoModel.getFeeling().equalsIgnoreCase("Bad")) {
                        builder.append(" ", new ImageSpan(context, R.drawable.feeling_bad), 0);
                    } else if (postInfoModel.getFeeling().equalsIgnoreCase("Very Bad")) {
                        builder.append(" ", new ImageSpan(context, R.drawable.feeling_very_bad), 0);
                    }

                    builder.append(" " + postInfoModel.getFeeling());

                } else {
                    // do something for phones running an SDK before lollipop
                    builder.append(wordtoSpan).append(" feeling ").append(" ");

                    if (postInfoModel.getFeeling().equalsIgnoreCase("Very Good")) {
                        builder.setSpan(new ImageSpan(context, R.drawable.feeling_very_good),
                                builder.length() - 1, builder.length(), 0);
                    } else if (postInfoModel.getFeeling().equalsIgnoreCase("Good")) {
                        builder.setSpan(new ImageSpan(context, R.drawable.feeling_good),
                                builder.length() - 1, builder.length(), 0);
                    } else if (postInfoModel.getFeeling().equalsIgnoreCase("Neutral")) {
                        builder.setSpan(new ImageSpan(context, R.drawable.feeling_neutral),
                                builder.length() - 1, builder.length(), 0);
                    } else if (postInfoModel.getFeeling().equalsIgnoreCase("Bad")) {
                        builder.setSpan(new ImageSpan(context, R.drawable.feeling_bad),
                                builder.length() - 1, builder.length(), 0);
                    } else if (postInfoModel.getFeeling().equalsIgnoreCase("Very Bad")) {
                        builder.setSpan(new ImageSpan(context, R.drawable.feeling_very_bad),
                                builder.length() - 1, builder.length(), 0);
                    }

                    builder.append(" " + postInfoModel.getFeeling());
                }

                userNameOfFeed.setText(builder);
            } else {
                //temp = post_by_user_name;
                userNameOfFeed.setText(wordtoSpan);
            }

            //StyleSpan boldStyle = new StyleSpan(android.graphics.Typeface.BOLD);
            //Spannable wordtoSpan = new SpannableString(userNameOfFeed.getText().toString());
            //wordtoSpan.setSpan(boldStyle, 0, (post_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            //userNameOfFeed.setText(wordtoSpan);

            singleFeedBodyLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoCommentActivity(postInfoModel);
                }
            });

            answercountLinLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoCommentActivity(postInfoModel);
                }
            });

            answerLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoCommentActivity(postInfoModel);
                }
            });

            if (!eachNewsfeed.get(position).getActivity().equals("post")) {
                if (eachNewsfeed.get(position).getBy_user_photo().length() > 1) {
                    if (imageLoader == null)
                        imageLoader = AppController.getInstance().getImageLoader();

                    String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachNewsfeed.get(position).getBy_user_photo();
                    activityByNImageView.setImageUrl(meta_url, imageLoader);
                    //activityByImageView.setVisibility(View.GONE);
                }
            }
        } else if (eachNewsfeed.get(position).getSection().equals("group")) {
            if (eachNewsfeed.get(position).getActivity().equals("post")) {
                //Log.e("Check","checked");
                activityLayout.setVisibility(View.VISIBLE);
                String sharedString = activity_by_user_name + " started a discussion in group " + post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(Typeface.BOLD);
                StyleSpan textBoldStyle2 = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 1",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 2",Toast.LENGTH_SHORT).show();
                        gotoGroupDetail(postInfoModel);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(textBoldStyle2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan1, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(clickableSpan2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);

                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (eachNewsfeed.get(position).getActivity().equals("liked")) {
                activityLayout.setVisibility(View.VISIBLE);

                String sharedString = activity_by_user_name + " liked a discussion in group " + post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(Typeface.BOLD);
                StyleSpan textBoldStyle2 = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 1",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 2",Toast.LENGTH_SHORT).show();
                        gotoGroupDetail(postInfoModel);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(textBoldStyle2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan1, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(clickableSpan2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);

                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (eachNewsfeed.get(position).getActivity().equals("commented")) {
                activityLayout.setVisibility(View.VISIBLE);
                String sharedString = activity_by_user_name + " commented to a discussion in group " + post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(Typeface.BOLD);
                StyleSpan textBoldStyle2 = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 1",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 2",Toast.LENGTH_SHORT).show();
                        gotoGroupDetail(postInfoModel);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(textBoldStyle2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan1, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(clickableSpan2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);

                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (eachNewsfeed.get(position).getActivity().equals("shared")) {
                activityLayout.setVisibility(View.VISIBLE);

                String sharedString = activity_by_user_name + " shared a discussion of group " + post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(Typeface.BOLD);
                StyleSpan textBoldStyle2 = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 1",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 2",Toast.LENGTH_SHORT).show();
                        gotoGroupDetail(postInfoModel);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(textBoldStyle2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan1, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(clickableSpan2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);

                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (eachNewsfeed.get(position).getActivity().equals("taged")) {
                activityLayout.setVisibility(View.VISIBLE);
            }

            String temp = post_by_user_name;

            StyleSpan boldStyle = new StyleSpan(Typeface.BOLD);
            Spannable wordtoSpan = new SpannableString(temp);
            wordtoSpan.setSpan(boldStyle, 0, (post_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            userNameOfFeed.setText(wordtoSpan);

            singleFeedBodyLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoCommentActivity(postInfoModel);
                }
            });

            answercountLinLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoCommentActivity(postInfoModel);
                }
            });

            answerLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoCommentActivity(postInfoModel);
                }
            });

            if (eachNewsfeed.get(position).getBy_user_photo().length() > 1) {
                if (imageLoader == null)
                    imageLoader = AppController.getInstance().getImageLoader();

                String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachNewsfeed.get(position).getBy_user_photo();
                activityByNImageView.setImageUrl(meta_url, imageLoader);
                //activityByImageView.setVisibility(View.GONE);
            }
        } else if (eachNewsfeed.get(position).getSection().equals("magazine")) {
            likeCommShareActLayout.setVisibility(View.GONE);
            underLikeCount.setVisibility(View.GONE);
            if (eachNewsfeed.get(position).getActivity().equals("post")) {
                activityLayout.setVisibility(View.VISIBLE);
                String postedString = activity_by_user_name + " posted an article in Magazine on " + post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(Typeface.BOLD);
                StyleSpan textBoldStyle2 = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 1",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 2",Toast.LENGTH_SHORT).show();
                        gotoMagazineList(postInfoModel.getTo_id(),post_to_user_name);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                Spannable wordtoSpan = new SpannableString(postedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(textBoldStyle2, (postedString.length() - post_to_user_name.length()), postedString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan1, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(clickableSpan2, (postedString.length() - post_to_user_name.length()), postedString.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);

                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());

            } else if (eachNewsfeed.get(position).getActivity().equals("liked")) {
                activityLayout.setVisibility(View.VISIBLE);

                String likedString = activity_by_user_name + " liked on an article in Magazine on " + post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(Typeface.BOLD);
                StyleSpan textBoldStyle2 = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 1",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 2",Toast.LENGTH_SHORT).show();
                        gotoMagazineList(postInfoModel.getTo_id(),post_to_user_name);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                Spannable wordtoSpan = new SpannableString(likedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(textBoldStyle2, (likedString.length() - post_to_user_name.length()), likedString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan1, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(clickableSpan2, (likedString.length() - post_to_user_name.length()), likedString.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);

                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (eachNewsfeed.get(position).getActivity().equals("commented")) {
                activityLayout.setVisibility(View.VISIBLE);

                String commentedString = activity_by_user_name + " commented on an article in Magazine on " + post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(Typeface.BOLD);
                StyleSpan textBoldStyle2 = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 1",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 2",Toast.LENGTH_SHORT).show();
                        gotoMagazineList(postInfoModel.getTo_id(),post_to_user_name);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                Spannable wordtoSpan = new SpannableString(commentedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(textBoldStyle2, (commentedString.length() - post_to_user_name.length()), commentedString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan1, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(clickableSpan2, (commentedString.length() - post_to_user_name.length()), commentedString.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);

                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (eachNewsfeed.get(position).getActivity().equals("shared")) {
                activityLayout.setVisibility(View.VISIBLE);
                String sharedString = activity_by_user_name + " shared on an article in Magazine on " + post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(Typeface.BOLD);
                StyleSpan textBoldStyle2 = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 1",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 2",Toast.LENGTH_SHORT).show();
                        gotoMagazineList(postInfoModel.getTo_id(),post_to_user_name);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(textBoldStyle2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan1, 0, (activity_by_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(clickableSpan2, (sharedString.length() - post_to_user_name.length()), sharedString.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);

                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (eachNewsfeed.get(position).getActivity().equals("taged")) {
                activityLayout.setVisibility(View.VISIBLE);
            }

            String temp = post_by_user_name;

            StyleSpan boldStyle = new StyleSpan(Typeface.BOLD);
            Spannable wordtoSpan = new SpannableString(temp);
            wordtoSpan.setSpan(boldStyle, 0, (post_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            //userNameOfFeed.setText(wordtoSpan);
            userNameOfFeed.setText("Jolpie Magazine");

            singleFeedBodyLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoMagazineDetail(postInfoModel);
                }
            });

            answercountLinLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoMagazineDetail(postInfoModel);
                }
            });

            answerLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoMagazineDetail(postInfoModel);
                }
            });

            if (eachNewsfeed.get(position).getBy_user_photo().length() > 1) {
                if (imageLoader == null)
                    imageLoader = AppController.getInstance().getImageLoader();

                String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachNewsfeed.get(position).getBy_user_photo();
                activityByNImageView.setImageUrl(meta_url, imageLoader);
                //activityByImageView.setVisibility(View.GONE);
            }
        } else if (eachNewsfeed.get(position).getSection().equals("review")) {
            if (eachNewsfeed.get(position).getActivity().equals("reviewed")) {
                activityLayout.setVisibility(View.VISIBLE);
                likeCommShareLayout.setVisibility(View.GONE);
                underDesView.setVisibility(View.GONE);
                String sharedString = post_to_user_name + " was reviewed by " + activity_by_user_name + " ";

                StyleSpan textBoldStyle1 = new StyleSpan(Typeface.BOLD);
                StyleSpan textBoldStyle2 = new StyleSpan(Typeface.BOLD);
                ClickableSpan clickableSpan1 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 1",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, ProfileDetailNewActivity.class);
                        intent.putExtra("user_id", postInfoModel.getTo_id());
                        intent.putExtra("user_fname", postInfoModel.getTo_fname());
                        intent.putExtra("user_lname", postInfoModel.getTo_lname());
                        intent.putExtra("user_email", "");
                        intent.putExtra("group_id", postInfoModel.getTo_user_group_id());
                        intent.putExtra("user_image_url", postInfoModel.getTo_photo());
                        context.startActivity(intent);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ClickableSpan clickableSpan2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        //Toast.makeText(context,"clicked 2",Toast.LENGTH_SHORT).show();
                        gotoActivityByProfile(position);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (post_to_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(textBoldStyle2, (sharedString.length() - activity_by_user_name.length()), sharedString.length() - 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                wordtoSpan.setSpan(clickableSpan1, 0, (post_to_user_name.length() - 1), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(clickableSpan2, (sharedString.length() - activity_by_user_name.length()), sharedString.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                activityTextView.setText(wordtoSpan);

                activityTextView.setMovementMethod(LinkMovementMethod.getInstance());


            }

            String temp = activity_by_user_name;

            StyleSpan boldStyle = new StyleSpan(Typeface.BOLD);
            Spannable wordtoSpan = new SpannableString(temp);
            wordtoSpan.setSpan(boldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            userNameOfFeed.setText(wordtoSpan);

            reviewRatingBar.setVisibility(View.VISIBLE);
            reviewRatingBar.setRating(Float.parseFloat(postInfoModel.getStar_count()));


            singleFeedBodyLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            if (postInfoModel.getTo_photo().length() > 1) {
                if (imageLoader == null)
                    imageLoader = AppController.getInstance().getImageLoader();

                String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + postInfoModel.getTo_photo();
                activityByNImageView.setImageUrl(meta_url, imageLoader);
                //activityByImageView.setVisibility(View.GONE);
            } else {
                if (postInfoModel.getTo_user_group_id().equals("1") || postInfoModel.getTo_user_group_id().equals("3")) {
                    activityByImageView.setImageResource(R.drawable.patient_student_default);
                } else if (postInfoModel.getTo_user_group_id().equals("2")) {
                    activityByImageView.setImageResource(R.drawable.doctor_default);
                } else if (postInfoModel.getTo_user_group_id().equals("4")) {
                    activityByImageView.setImageResource(R.drawable.hospital_default);
                } else if (postInfoModel.getTo_user_group_id().equals("5")) {
                    activityByImageView.setImageResource(R.drawable.labs_default);
                } else if (postInfoModel.getTo_user_group_id().equals("6")) {
                    activityByImageView.setImageResource(R.drawable.pharmaceutical_default);
                }
            }


            if (eachNewsfeed.get(position).getBy_user_photo().length() > 1) {
                if (imageLoader == null)
                    imageLoader = AppController.getInstance().getImageLoader();

                String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachNewsfeed.get(position).getBy_user_photo();
                postByNImageView.setImageUrl(meta_url, imageLoader);
            } else {
                if (eachNewsfeed.get(position).getBy_user_group_id().equals("1") || eachNewsfeed.get(position).getBy_user_group_id().equals("3")) {
                    postByImageView.setImageResource(R.drawable.patient_student_default);
                } else if (eachNewsfeed.get(position).getBy_user_group_id().equals("2")) {
                    postByImageView.setImageResource(R.drawable.doctor_default);
                } else if (eachNewsfeed.get(position).getBy_user_group_id().equals("4")) {
                    postByImageView.setImageResource(R.drawable.hospital_default);
                } else if (eachNewsfeed.get(position).getBy_user_group_id().equals("5")) {
                    postByImageView.setImageResource(R.drawable.labs_default);
                } else if (eachNewsfeed.get(position).getBy_user_group_id().equals("6")) {
                    postByImageView.setImageResource(R.drawable.pharmaceutical_default);
                }

            }
        }


        if (!eachNewsfeed.get(position).getSection().equals("review")) {
            if (eachNewsfeed.get(position).getSection().equalsIgnoreCase("magazine")) {
                postByImageView.setImageResource(R.drawable.jolpie_emblem);
                postByImageView.setScaleType(ImageView.ScaleType.FIT_XY);
                //postByNImageView.setVisibility(View.GONE);
            }
            else {
                if (postInfoModel.getBy_photo().length() > 1) {
                    if (imageLoader == null)
                        imageLoader = AppController.getInstance().getImageLoader();

                    String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + postInfoModel.getBy_photo();
                    postByNImageView.setImageUrl(meta_url, imageLoader);
                } else {
                    if (postInfoModel.getBy_user_group_id().equals("1") || postInfoModel.getBy_user_group_id().equals("3")) {
                        postByImageView.setImageResource(R.drawable.patient_student_default);
                    } else if (postInfoModel.getBy_user_group_id().equals("2")) {
                        postByImageView.setImageResource(R.drawable.doctor_default);
                    } else if (postInfoModel.getBy_user_group_id().equals("4")) {
                        postByImageView.setImageResource(R.drawable.hospital_default);
                    } else if (postInfoModel.getBy_user_group_id().equals("5")) {
                        postByImageView.setImageResource(R.drawable.labs_default);
                    } else if (postInfoModel.getBy_user_group_id().equals("6")) {
                        postByImageView.setImageResource(R.drawable.pharmaceutical_default);
                    }
                }
            }
        }

        TextView feedTitle = (TextView) convertView.findViewById(R.id.feedtitle);
        if (postInfoModel.getTitle().length() > 0) {
            feedTitle.setVisibility(View.VISIBLE);
            feedTitle.setText(postInfoModel.getTitle());
        }
        TextView dateTime = (TextView) convertView.findViewById(R.id.datetime);
        dateTime.setText(postInfoModel.getCreated_at());

        final VideoView feedVideo = (VideoView) convertView.findViewById(R.id.feedvideo_vv);
        feedVideo.setVisibility(View.GONE);
        NetworkImageView feedImageNetwork = (NetworkImageView) convertView.findViewById(R.id.feedimage_network);
        feedImageNetwork.setVisibility(View.GONE);

        if (postInfoModel.getPhoto().trim().length() > 0) {
            if (imageLoader == null) {
                imageLoader = AppController.getInstance().getImageLoader();
            }
            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + postInfoModel.getPhoto();
            feedImageNetwork.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //thumbNail.setLayoutParams(new RelativeLayout.LayoutParams(imageWidth, imageWidth));
            feedImageNetwork.setImageUrl(meta_url, imageLoader);
            feedImageNetwork.setVisibility(View.VISIBLE);
        } else if (postInfoModel.getVideo().trim().length() > 0) {
            feedVideo.setVisibility(View.VISIBLE);
            //feedVideo.setMediaController(new MediaController(context));
            feedVideo.setVideoPath(context.getResources().getString(R.string.UPLOADS_URL) + "video/" + postInfoModel.getVideo());
            feedVideo.requestFocus();
            feedVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    feedVideo.start();
                    feedVideo.pause();
                }
            });
        }

        TextView description = (TextView) convertView.findViewById(R.id.description);
        if (postInfoModel.getDescription().length() > 100) {
            String tempString = postInfoModel.getDescription().substring(0, 99) + "...Continue Reading";

            Spannable wordtoSpan = new SpannableString(tempString);

            wordtoSpan.setSpan(new ForegroundColorSpan(Color.GRAY), 102, 118, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            /*final SpannableStringBuilder sb = new SpannableStringBuilder(tempString);

            // Span to set text color to some RGB value
            final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(224, 224, 224));

            // Span to make text bold
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

            // Set the text color for first 4 characters
            sb.setSpan(fcs, 99, 118, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            // make them also bold
            //sb.setSpan(bss, 0, 4, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            //yourTextView.setText(sb);*/
            description.setText(wordtoSpan);
        } else {
            description.setText(postInfoModel.getDescription());
        }

        final TextView goodCountTV = (TextView) convertView.findViewById(R.id.goodcount);
        goodCountTV.setText(postInfoModel.getLike_count());

        final TextView followCountTV = (TextView) convertView.findViewById(R.id.followcount);
        followCountTV.setText(postInfoModel.getShare_count());

        TextView answerCountTV = (TextView) convertView.findViewById(R.id.answercount);
        answerCountTV.setText(postInfoModel.getAnswer_count());

        //click on number of likes
        final LinearLayout goodcountLinLay = (LinearLayout) convertView.findViewById(R.id.goodcount_linlay);
        goodcountLinLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PeopleWhoLiked.class);
                intent.putExtra("peopleWhoFlag", "-1");
                //intent.putExtra("isPost","1");
                intent.putExtra("post_id", postInfoModel.getId());
                context.startActivity(intent);
            }
        });

        //click on number of likes
        LinearLayout followcountLinLay = (LinearLayout) convertView.findViewById(R.id.followcount_linlay);
        followcountLinLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PeopleWhoLiked.class);
                intent.putExtra("peopleWhoFlag", "1");
                //intent.putExtra("isPost","1");
                intent.putExtra("post_id", postInfoModel.getId());
                context.startActivity(intent);

            }
        });

        //click to show the user detail/profile..................
        LinearLayout feedPersonsDetail = (LinearLayout) convertView.findViewById(R.id.feedlist_user_layout);
        feedPersonsDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //gotoActivityByProfile(position);
                if (eachNewsfeed.get(position).getSection().equalsIgnoreCase("review")){
                    gotoActivityByProfile(position);
                }
                else if (eachNewsfeed.get(position).getSection().equalsIgnoreCase("magazine")) {
                    gotoMagazineDetail(postInfoModel);
                }
                else {
                    Intent intent = new Intent(context, ProfileDetailNewActivity.class);
                    intent.putExtra("user_id", postInfoModel.getBy_id());
                    intent.putExtra("user_fname", postInfoModel.getBy_fname());
                    intent.putExtra("user_lname", postInfoModel.getBy_lname());
                    intent.putExtra("user_email", "");
                    intent.putExtra("group_id", postInfoModel.getBy_user_group_id());
                    intent.putExtra("user_image_url", postInfoModel.getBy_photo());
                    context.startActivity(intent);
                }
            }
        });

        //click on like button for like / unlike
        final LinearLayout goodLinearLayout = (LinearLayout) convertView.findViewById(R.id.goodlinlay);
        final TextView goodImage = (TextView) convertView.findViewById(R.id.goodimageview);
        //Typeface goodFont = Typeface.createFromAsset( context.getAssets(), "fontawesome-webfont.ttf" );
        goodImage.setTypeface(editFont);
        final TextView goodTextView = (TextView) convertView.findViewById(R.id.goodTextView);

        /*String url = context.getResources().getString(R.string.MAIN_URL)+"isLiked";

        StringRequest strReq = new StringRequest(Request.Method.POST,
              url, new Response.Listener<String>() {

        @Override
        public void onResponse(String response) {
        Log.d("isliked", response.length() + " : " + response.toString());*/
        if (postInfoModel.getLiked_by_me().equals("0")) {
            liked[0] = false;
            //goodLinearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
            goodImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
            goodTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));

        } else {
            liked[0] = true;
            //goodLinearLayout.setBackgroundColor(Color.parseColor("#e3f2fd"));
            goodImage.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
            goodTextView.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
        }
            /*}
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("isliked", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("post_id", postInfoModel.getId());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);*/

        goodLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liked[0]) {
                    liked[0] = false;
                    postInfoModel.setLiked_by_me("0");
                    //goodLinearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    int numliked = Integer.parseInt(goodCountTV.getText().toString());
                    --numliked;
                    ////////////////////////////////////////////////////////////////////feedList.get(position).setPost_likes(String.valueOf(numliked));
                    goodCountTV.setText(String.valueOf(numliked));

                    AppFunctions.likeUnlikeRequest(context, postInfoModel.getId(), "0", user_id);
                } else {
                    liked[0] = true;
                    postInfoModel.setLiked_by_me("1");
                    //goodLinearLayout.setBackgroundColor(Color.parseColor("#e3f2fd"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                    int numliked = Integer.parseInt(goodCountTV.getText().toString());
                    ++numliked;
                    ///////////////////////////////////////////////////////////////////////feedList.get(position).setPost_likes(String.valueOf(numliked));
                    goodCountTV.setText(String.valueOf(numliked));

                    AppFunctions.likeUnlikeRequest(context, postInfoModel.getId(), "1", user_id);
                }
            }
        });

        TextView answerImage = (TextView) convertView.findViewById(R.id.answerimageview);
        //answerImage.setImageResource(R.mipmap.ic_comment);
        answerImage.setTypeface(editFont);

        //click on like button for like / unlike
        LinearLayout followLinearLayout = (LinearLayout) convertView.findViewById(R.id.followlinlay);
        followLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are you sure?");
                alertDialogBuilder.setMessage("You wanted to share this post?");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        try {
                            int numShared = Integer.parseInt(followCountTV.getText().toString());
                            ++numShared;
                            followCountTV.setText(String.valueOf(numShared));
                        } catch (NumberFormatException e) {

                        }

                        postShareRequest(postInfoModel.getId());
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ////////////
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        TextView followImage = (TextView) convertView.findViewById(R.id.followimageview);
        //followImage.setImageResource(R.mipmap.ic_share);
        followImage.setTypeface(editFont);

        if (user_id.equals(postInfoModel.getBy_id()) && (eachNewsfeed.get(position).getSection().equals("post") || eachNewsfeed.get(position).getSection().equals("group")) && !(eachNewsfeed.get(position).getActivity().equals("shared"))) {
            editButton.setVisibility(View.VISIBLE);
        } else {
            editButton.setVisibility(View.GONE);
        }
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu filterMenu = new PopupMenu(context, editButton);
                filterMenu.getMenuInflater().inflate(R.menu.menu_edit_delete, filterMenu.getMenu());
                filterMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context, "You Clicked : "+item.getTitle() , Toast.LENGTH_SHORT).show();
                        if (item.getItemId() == R.id.edit_item) {

                            Intent intent = new Intent(context, PostUpdateActivity.class);
                            if (eachNewsfeed.get(position).getSection().equals("post")) {
                                intent.putExtra("group_id", "0");
                            } else if (eachNewsfeed.get(position).getSection().equals("group")) {
                                intent.putExtra("group_id", postInfoModel.getTo_id());
                            }

                            intent.putExtra("post_id", postInfoModel.getId());
                            context.startActivity(intent);

                        } else if (item.getItemId() == R.id.delete_item) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                            alertDialogBuilder.setTitle("Are you sure?");
                            alertDialogBuilder.setMessage("You wanted to delete this?");

                            alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    postDeleteRequset(postInfoModel.getId());
                                }
                            });

                            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ////////////
                                }
                            });
                            final AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                        return true;
                    }
                });
                filterMenu.show();//showing popup menu
            }
        });

        if (position == eachNewsfeed.size() - 1 && !isEnd) {
            ViewGroup lazzyAttachlinlay = (ViewGroup) convertView.findViewById(R.id.lazzyloader_attach_linlay);
            LayoutInflater inflater_child = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View child_view = inflater_child.inflate(R.layout.lazzy_loader_layout, lazzyAttachlinlay, false);
            lazzyAttachlinlay.addView(child_view);
            getNewsfeeds(limit_start, limit_end);
            limit_start = limit_start + limit_end;
        }

        return convertView;
    }

    private void getNewsfeeds(final int start, final int take) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getNewsfeeds";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                String id, section, section_id, activity, activity_at, by_user_id, by_user_fname, by_user_lname, by_user_photo, by_user_group_id;
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    //limit_start = limit_start + limit_end;
                    if (jsonArray.length() < 5) {
                        isEnd = true;
                    }

                    for (int i = 0; i < jsonArray.length(); ++i) {
                        //eac
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        id = jsonObject.getString("newsfeed_id");
                        section = jsonObject.getString("section");
                        section_id = jsonObject.getString("section_id");
                        activity = jsonObject.getString("activity");
                        activity_at = jsonObject.getString("activity_at");
                        by_user_id = jsonObject.getString("by_user_id");
                        by_user_fname = jsonObject.getString("by_user_fname");
                        by_user_lname = jsonObject.getString("by_user_lname");
                        by_user_photo = jsonObject.getString("by_user_photo");
                        by_user_group_id = jsonObject.getString("by_user_group_id");

                        JSONObject infoJsonObject = jsonObject.getJSONObject("info");

                        String post_id, title, description, feeling, photo, video, created_at,
                                updated_at, by_id, by_fname, by_lname, by_photo, by_group_id,
                                to_id, to_fname, to_lname, to_photo, to_user_group_id,
                                like_count, answer_count, share_count, star_count, liked_by_me, shared_by_me;
                        post_id = infoJsonObject.getString("section_post_id");
                        title = infoJsonObject.getString("section_title");
                        description = infoJsonObject.getString("section_description");
                        feeling = infoJsonObject.getString("section_feeling").trim();
                        photo = infoJsonObject.getString("section_photo");
                        video = infoJsonObject.getString("section_video");
                        created_at = infoJsonObject.getString("section_at");
                        updated_at = infoJsonObject.getString("section_updated_at");
                        by_id = infoJsonObject.getString("section_by");
                        by_fname = infoJsonObject.getString("section_by_fname");
                        by_lname = infoJsonObject.getString("section_by_lname");
                        by_photo = infoJsonObject.getString("section_by_photo");
                        by_group_id = infoJsonObject.getString("section_by_user_group_id");
                        to_id = infoJsonObject.getString("section_to");
                        to_fname = infoJsonObject.getString("section_to_fname");
                        to_lname = infoJsonObject.getString("section_to_lname");
                        to_photo = infoJsonObject.getString("section_to_photo");
                        to_user_group_id = infoJsonObject.getString("section_to_user_group_id");
                        like_count = infoJsonObject.getString("good_count");
                        answer_count = infoJsonObject.getString("answer_count");
                        share_count = infoJsonObject.getString("share_count");
                        star_count = infoJsonObject.getString("star_count");
                        liked_by_me = infoJsonObject.getString("liked_by_me");
                        shared_by_me = infoJsonObject.getString("shared_by_me");


                        PostInfoModel infoModel = new PostInfoModel(post_id, title, description, feeling, photo, video, created_at,
                                updated_at, by_id, by_fname, by_lname, by_photo, by_group_id,
                                to_id, to_fname, to_lname, to_photo, to_user_group_id,
                                like_count, answer_count, share_count, star_count, liked_by_me, shared_by_me);

                        int patFlag = 0;
                        for (int k=0;k<eachPatients.size();++k) {
                            if (eachPatients.get(k).getUser_id().equals(by_id)) {
                                patFlag = 1;
                                break;
                            }
                        }
                        if (patFlag == 1) {
                            NewsfeedModel newsfeedModel = new NewsfeedModel(id, section, section_id, activity, activity_at, by_user_id, by_user_fname, by_user_lname, by_user_photo, by_user_group_id, infoModel);
                            eachNewsfeed.add(newsfeedModel);
                        }
                        /*NewsfeedModel newsfeedModel = new NewsfeedModel(id, section, section_id, activity, activity_at, by_user_id, by_user_fname, by_user_lname, by_user_photo, by_user_group_id, infoModel);

                        eachNewsfeed.add(newsfeedModel);*/
                    }
                    notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                isEnd = true;
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("user_group_id", user_group_id);
                params.put("start_index", String.valueOf(start));
                params.put("take_total", String.valueOf(take));
                //Log.d(TAG,+appUserId+magazine_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void postShareRequest(final String post_id) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "postShare";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context, "Post Shared Successfully", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("post_id", post_id);
                params.put("increase_share", "1");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void postDeleteRequset(final String post_id) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "deletePost";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context, "Post Deleted Successfully", Toast.LENGTH_SHORT).show();
                    eachNewsfeed.clear();
                    getNewsfeeds(0, 8);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("post_id", post_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void gotoActivityByProfile(int position) {
        Intent intent = new Intent(context, ProfileDetailNewActivity.class);
        intent.putExtra("user_id", eachNewsfeed.get(position).getBy_user_id());
        intent.putExtra("user_fname", eachNewsfeed.get(position).getBy_user_fname());
        intent.putExtra("user_lname", eachNewsfeed.get(position).getBy_user_lname());
        intent.putExtra("user_email", "");
        intent.putExtra("group_id", eachNewsfeed.get(position).getBy_user_group_id());
        intent.putExtra("user_image_url", eachNewsfeed.get(position).getBy_user_photo());
        context.startActivity(intent);
    }

    private void gotoCommentActivity(PostInfoModel postInfoModel) {
        //Intent intent = new Intent(context, CommentActivity.class);
        Intent intent = new Intent(context, PostDetailActivity.class);
        intent.putExtra("id", postInfoModel.getId());
        intent.putExtra("title", postInfoModel.getTitle());
        intent.putExtra("description", postInfoModel.getDescription());
        intent.putExtra("feeling", postInfoModel.getFeeling());
        intent.putExtra("photo", postInfoModel.getPhoto());
        intent.putExtra("video", postInfoModel.getVideo());
        intent.putExtra("created_at", postInfoModel.getCreated_at());
        intent.putExtra("updated_at", postInfoModel.getUpdated_at());
        intent.putExtra("user_id", postInfoModel.getBy_id());
        intent.putExtra("user_fname", postInfoModel.getBy_fname());
        intent.putExtra("user_lname", postInfoModel.getBy_lname());
        intent.putExtra("user_photo", postInfoModel.getBy_photo());
        intent.putExtra("user_group_id", postInfoModel.getBy_user_group_id());
        intent.putExtra("good_count", postInfoModel.getLike_count());
        intent.putExtra("answer_count", postInfoModel.getAnswer_count());
        intent.putExtra("share_count", postInfoModel.getShare_count());
        intent.putExtra("liked_by_me", postInfoModel.getLiked_by_me());
        intent.putExtra("shared_by_me", postInfoModel.getShared_by_me());
        context.startActivity(intent);
    }

    private void gotoMagazineDetail(PostInfoModel postInfoModel) {
        Intent intent = new Intent(context, MagazineDetailActivity.class);
        intent.putExtra("magazine_id", postInfoModel.getId());
        intent.putExtra("title", postInfoModel.getTitle());
        intent.putExtra("description", postInfoModel.getDescription());
        intent.putExtra("author_id", postInfoModel.getBy_id());
        intent.putExtra("created_at", postInfoModel.getCreated_at());
        intent.putExtra("picture", postInfoModel.getPhoto());
        intent.putExtra("like_count", postInfoModel.getLike_count());
        intent.putExtra("comment_count", postInfoModel.getAnswer_count());
        intent.putExtra("share_count", postInfoModel.getShare_count());
        intent.putExtra("category_id", postInfoModel.getTo_id());
        intent.putExtra("first_name", postInfoModel.getBy_fname());
        intent.putExtra("last_name", postInfoModel.getBy_lname());
        intent.putExtra("photo", postInfoModel.getBy_photo());
        intent.putExtra("user_group_id", postInfoModel.getBy_user_group_id());
        context.startActivity(intent);
    }

    private void gotoMagazineList(String category_id, String category_name) {
        Intent intent = new Intent(context, MagazineActivity.class);
        intent.putExtra("category_id", category_id);
        intent.putExtra("category_name", category_name);
        context.startActivity(intent);
    }

    private void gotoGroupDetail(PostInfoModel postInfoModel) {
        Intent intent = new Intent(context, GroupDetailActivity.class);
        intent.putExtra("group_id", postInfoModel.getTo_id());
        intent.putExtra("group_name", postInfoModel.getTo_fname());
        intent.putExtra("group_des", "");
        intent.putExtra("group_img", "");
        intent.putExtra("group_membr", "");
        intent.putExtra("group_discuss", "");
        intent.putExtra("group_created_by", "");
        context.startActivity(intent);
    }
}
