package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Models.MyConditionsModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tumpa on 3/2/2016.
 */
public class MyConditionListAdapter extends BaseAdapter {
    List<MyConditionsModel> myConditionsList;
    Context context;
    private static LayoutInflater inflater=null;
    public MyConditionListAdapter(Context context,List<MyConditionsModel> myConditionsList){
        this.context = context;
        this.myConditionsList = myConditionsList;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return myConditionsList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class Holder
    {
        TextView tv;
        Button btn;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_my_condition,null);
        holder.tv = (TextView) rowView.findViewById(R.id.current_condition);
        holder.btn = (Button) rowView.findViewById(R.id.delete);
        MyConditionsModel myConditionsModel = myConditionsList.get(position);

        holder.tv.setText(myConditionsModel.getConditionName_en()+"\n("+myConditionsModel.getConditionName_bn()+")");
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCondition(position);
            }
        });
        return rowView;
    }

    private void deleteCondition(final int position) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "deleteMyCondition";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("MyCondition", "mycondition: " + response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context, "Deleted successfully", Toast.LENGTH_LONG).show();
                    myConditionsList.remove(position);
                    notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d("MyCondition", "Error: " + error.getMessage());
                Toast.makeText(context, "Error: Try again later", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("my_condition_id", myConditionsList.get(position).getMyId());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "tag_string_req");
    }
}
