package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Models.MessagesModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 1/4/2016.
 */
public class MessagesListAdapter extends BaseAdapter{

    private Context context;
    private List<MessagesModel> eachItems;
    private String appUserId;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public MessagesListAdapter(Context context, List<MessagesModel> eachItems, String appUserId) {
        this.context = context;
        this.eachItems = eachItems;
        this.appUserId = appUserId;
    }

    @Override
    public int getCount() {
        return eachItems.size();
    }

    @Override
    public Object getItem(int position) {
        return eachItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null)
        {
            convertView = inflater.inflate(R.layout.list_row_messages, null);
        }

        NetworkImageView fromNetworkImageView = (NetworkImageView) convertView.findViewById(R.id.list_messages_receiver_niv);
        if (eachItems.get(position).getTo_photo().length()>1) {

                if (imageLoader == null)
                    imageLoader = AppController.getInstance().getImageLoader();
                String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachItems.get(position).getTo_photo();
            fromNetworkImageView.setImageUrl(meta_url, imageLoader);

        }
        TextView msg_from = (TextView) convertView.findViewById(R.id.list_messages_receiver_name);
        msg_from.setText(eachItems.get(position).getTo_first_name()+" "+eachItems.get(position).getTo_last_name());

        TextView msg_text = (TextView) convertView.findViewById(R.id.list_messages_last_text);
        if (appUserId.equals(eachItems.get(position).getFrom_user_id())) {
            msg_text.setText("You: "+eachItems.get(position).getMsg_text());
        }
        else{
            msg_text.setText(eachItems.get(position).getMsg_text());
        }

        TextView msg_date = (TextView) convertView.findViewById(R.id.list_messages_last_time);
        msg_date.setText(eachItems.get(position).getMsg_created_at());

        return convertView;
    }
}
