package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.ChamberActivity;
import com.theuserhub.jolpie.Activity.DrugProfileActivity;
import com.theuserhub.jolpie.Activity.OnlineConsultancyActivity;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Models.CategoryItem;
import com.theuserhub.jolpie.Models.SearchUserModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 11/8/2015.
 */
public class CategorizedAdapter extends BaseAdapter {
    private String TAG = "CategorizedAdapter", tag_string_req = "cat_adapter_str_req";

    private Context context;
    private List<SearchUserModel> eachItems;
    private List<String> compareIds;
    private Button compareTextView;
    private String appUserId, user_group_id, appUserGroupId, speciality_type,search_item,
            latitude_str, longitude_str;
    private boolean isEnd;
    private int limit_start = 8, limit_end = 5;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public CategorizedAdapter(Context context, List<SearchUserModel> eachItems,
                              List<String> compareIds, Button compareTextView,
                              String appUserId, String appUserGroupId,
                              String user_group_id, boolean isEnd, String speciality_type, String search_item,
                              String latitude_str, String longitude_str) {
        this.context = context;
        this.eachItems = eachItems;
        this.compareIds = compareIds;
        this.compareTextView = compareTextView;
        this.appUserId = appUserId;
        this.appUserGroupId = appUserGroupId;
        this.user_group_id = user_group_id;
        this.isEnd = isEnd;
        this.speciality_type = speciality_type;
        this.search_item = search_item;
        this.latitude_str = latitude_str;
        this.longitude_str = longitude_str;
    }

    @Override
    public int getCount() {
        return eachItems.size();
    }

    @Override
    public Object getItem(int position) {
        return eachItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_categoryitem, null);
        }

        Typeface userFont = Typeface.createFromAsset( context.getAssets(), "fontawesome-webfont.ttf" );

        ImageView doctors_item_user_iv = (ImageView) convertView.findViewById(R.id.doctors_item_user_iv);
        NetworkImageView itemNImageView = (NetworkImageView) convertView.findViewById(R.id.doctors_item_user_niv);
        if (eachItems.get(position).getPhoto().length() > 1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();
            String meta_url;
            if (eachItems.get(position).getUser_group_id().equals("6")) {
                meta_url = context.getResources().getString(R.string.UPLOADS_URL_DRUG) + eachItems.get(position).getPhoto();
            }
            else {
                meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachItems.get(position).getPhoto();
            }

            itemNImageView.setImageUrl(meta_url, imageLoader);
        } else {
            if (eachItems.get(position).getUser_group_id().equals("1") || eachItems.get(position).getUser_group_id().equals("3")) {
                doctors_item_user_iv.setImageResource(R.drawable.patient_student_default);
            } else if (eachItems.get(position).getUser_group_id().equals("2")) {
                doctors_item_user_iv.setImageResource(R.drawable.doctor_default);
            } else if (eachItems.get(position).getUser_group_id().equals("4")) {
                doctors_item_user_iv.setImageResource(R.drawable.hospital_default);
            } else if (eachItems.get(position).getUser_group_id().equals("5")) {
                doctors_item_user_iv.setImageResource(R.drawable.labs_default);
            } else if (eachItems.get(position).getUser_group_id().equals("6")) {
                doctors_item_user_iv.setImageResource(R.drawable.drug_default);
            } else if (eachItems.get(position).getUser_group_id().equals("7")) {
                doctors_item_user_iv.setImageResource(R.drawable.group_default_new);
            }
        }

        TextView itemName = (TextView) convertView.findViewById(R.id.doctors_item_ttl_tv);
        if (eachItems.get(position).getUser_group_id().equals("2")) {
            itemName.setText("Dr. " + eachItems.get(position).getFirstName() + " " + eachItems.get(position).getLastName());
        } else if (eachItems.get(position).getUser_group_id().equals("6") || eachItems.get(position).getUser_group_id().equals("7")) {
            itemName.setText(eachItems.get(position).getFirstName());
        } else {
            itemName.setText(eachItems.get(position).getFirstName() + " " + eachItems.get(position).getLastName());
        }

        TextView itemDegreeType = (TextView) convertView.findViewById(R.id.doctors_item_degree_tv);
        if (eachItems.get(position).getUser_group_id().equals("2")) {
            itemDegreeType.setText(eachItems.get(position).getDegree());
        } else if (eachItems.get(position).getUser_group_id().equals("4")) {
            if (eachItems.get(position).getType().equals("1")) {
                itemDegreeType.setText("General Hospital");
            } else if (eachItems.get(position).getType().equals("2")) {
                itemDegreeType.setText("Specialized Hospital");
            } else if (eachItems.get(position).getType().equals("3")) {
                itemDegreeType.setText("Clinic");
            } else {
                itemDegreeType.setText("");
            }
        } else if (eachItems.get(position).getUser_group_id().equals("5")) {
            itemDegreeType.setText("");
        } else if (eachItems.get(position).getUser_group_id().equals("6")) {
            itemDegreeType.setText(eachItems.get(position).getLastName());
        }

        TextView itemCompanyName = (TextView) convertView.findViewById(R.id.doctors_item_company_name_tv);

        if (eachItems.get(position).getUser_group_id().equals("6")) {
            itemCompanyName.setText(eachItems.get(position).getDegree());
        } else {
            List<SpecialityModel> specialityList = eachItems.get(position).getSpecialityModels();

            String specialities = "";
            for (int c = 0; c < specialityList.size(); ++c) {
                if (c != 0) {
                    specialities = specialities + ", ";
                }
                specialities = specialities + specialityList.get(c).getName();
            }
            itemCompanyName.setText(specialities);
        }

        //TextView itemCompanyPosition = (TextView) convertView.findViewById(R.id.doctors_item_company_position_tv);
        //itemCompanyPosition.setText(eachItems.get(position).getCompany_position());

        ImageView locationPointer = (ImageView) convertView.findViewById(R.id.doctors_item_locationpointer_iv);
        locationPointer.setImageResource(R.mipmap.ic_location_pointer);

        TextView itemLocation = (TextView) convertView.findViewById(R.id.doctors_item_location_tv);
        itemLocation.setText(eachItems.get(position).getDistrict());

        RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.doctors_item_rating_iv);
        TextView reviewCount = (TextView) convertView.findViewById(R.id.doctors_item_reviews_tv);

        if (eachItems.get(position).getUser_group_id().equals("6")) {
            locationPointer.setVisibility(View.GONE);
            itemLocation.setVisibility(View.GONE);
            ratingBar.setVisibility(View.GONE);
            reviewCount.setVisibility(View.GONE);
        }
        else {
            if (eachItems.get(position).getReview_count().equals("0")) {
                reviewCount.setText("0 Review");
                ratingBar.setRating(0.0f);
            } else if (eachItems.get(position).getReview_count().equals("1")) {
                reviewCount.setText("1 Review");
                ratingBar.setRating(Float.parseFloat(eachItems.get(position).getReview_star()));
            } else {
                reviewCount.setText(eachItems.get(position).getReview_count() + " Reviews");
                float sum = Float.parseFloat(eachItems.get(position).getReview_star());
                float count = Float.parseFloat(eachItems.get(position).getReview_count());
                float temp = sum / count;
                ratingBar.setRating(temp);
            }
        }

        TextView itemRateTextView = (TextView) convertView.findViewById(R.id.doctors_item_rate_tv);
        if (eachItems.get(position).getItemPrice().trim().length()>0) {
            itemRateTextView.setText("BDT  "+eachItems.get(position).getItemPrice());
        }

        View betweenView = convertView.findViewById(R.id.view_between_detail_button);
        RelativeLayout lowerActivityLayout = (RelativeLayout) convertView.findViewById(R.id.doctors_item_lower_activity_layout);
        LinearLayout compareLayout = (LinearLayout) convertView.findViewById(R.id.doctors_item_compare_layout);
        final CheckBox compareCheckBox = (CheckBox) convertView.findViewById(R.id.compare_checkBox);

        if (eachItems.get(position).getUser_group_id().equals("2")) {
            lowerActivityLayout.setVisibility(View.VISIBLE);
            betweenView.setVisibility(View.VISIBLE);
        } else {
            lowerActivityLayout.setVisibility(View.GONE);
            betweenView.setVisibility(View.GONE);
        }

        compareCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (compareCheckBox.isChecked()) {
                    compareIds.add(eachItems.get(position).getId());
                    //Toast.makeText(context,"Add: "+eachItems.get(position).getId(),Toast.LENGTH_SHORT).show();
                    if (compareIds.size() > 1) {
                        if (compareIds.size() > 4) {
                            compareTextView.setVisibility(View.GONE);
                            Toast.makeText(context, "You cannot add more than 4 items to the compare list", Toast.LENGTH_LONG).show();
                        } else {
                            compareTextView.setVisibility(View.VISIBLE);
                            compareTextView.bringToFront();
                        }
                    } else {
                        compareTextView.setVisibility(View.GONE);
                    }
                } else {
                    compareIds.remove(eachItems.get(position).getId());
                    //Toast.makeText(context,"Delete: "+eachItems.get(position).getId(),Toast.LENGTH_SHORT).show();
                    if (compareIds.size() < 2) {
                        compareTextView.setVisibility(View.GONE);
                    } else {
                        if (compareIds.size() <= 4) {
                            compareTextView.setVisibility(View.VISIBLE);
                            compareTextView.bringToFront();
                        }
                    }
                }
                /*if (compareIds.size()==5) {
                }*/
                notifyDataSetChanged();
            }
        });

        final int indexOfCompare = compareIds.indexOf(eachItems.get(position).getId());
        if (indexOfCompare >= 0) {
            compareCheckBox.setChecked(true);
        } else {
            compareCheckBox.setChecked(false);
        }

        LinearLayout saveLayout = (LinearLayout) convertView.findViewById(R.id.doctors_item_save_layout);
        final TextView saveTextView = (TextView) convertView.findViewById(R.id.doctors_item_save_tv);
        final TextView saveImage = (TextView) convertView.findViewById(R.id.save_image);
        saveImage.setTypeface(userFont);
        if (eachItems.get(position).getIsSaved().equals("0")) {
            saveTextView.setText("Save");
            //saveImage.setImageResource(R.drawable.heart_black);
            saveImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
        } else {
            saveTextView.setText("Unsave");
            //saveImage.setImageResource(R.drawable.heart_red);
            saveImage.setTextColor(ContextCompat.getColor(context, R.color.red_900));
        }

        saveLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eachItems.get(position).getIsSaved().equals("0")) {
                    saveDoctorRequest(position);
                    saveTextView.setText("Unsave");
                    //saveImage.setImageResource(R.drawable.heart_red);
                    saveImage.setTextColor(ContextCompat.getColor(context, R.color.red_900));
                    eachItems.get(position).setIsSaved("1");
                } else {
                    unsaveDoctorRequest(position);
                    saveTextView.setText("Save");
                    //saveImage.setImageResource(R.drawable.heart_black);
                    saveImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    eachItems.get(position).setIsSaved("0");
                }
            }
        });

        RelativeLayout consultLayout = (RelativeLayout) convertView.findViewById(R.id.consult_layout);
        TextView cunsultTextView = (TextView) convertView.findViewById(R.id.doctors_consult_tv);

        RelativeLayout appointmentLayout = (RelativeLayout) convertView.findViewById(R.id.appointment_r_l);
        TextView appointmentTextView = (TextView) convertView.findViewById(R.id.doctors_item_appointment_tv);

        Log.e("isconsult", eachItems.get(position).getIsConsult());

        if (eachItems.get(position).getUser_group_id().equals("2")) {
            if (eachItems.get(position).getIsSubscribed().length() > 0) {
                if (eachItems.get(position).getIsSubscribed().equals("0")) {
                    consultLayout.setVisibility(View.GONE);
                    appointmentLayout.setVisibility(View.GONE);
                }
                else {
                    if (eachItems.get(position).getIsConsult().length() > 0) {
                        if (eachItems.get(position).getIsConsult().trim().equals("0")) {
                            consultLayout.setVisibility(View.GONE);
                            Log.e("Gone", eachItems.get(position).getIsConsult());
                        } else {
                            Log.e("View", eachItems.get(position).getIsConsult());
                            if (user_group_id.equals("4") || user_group_id.equals("5") ||
                                    user_group_id.equals("6") || appUserGroupId.equals("4") ||
                                    appUserGroupId.equals("5") || appUserGroupId.equals("6")) {
                                consultLayout.setVisibility(View.GONE);
                            } else {
                                consultLayout.setVisibility(View.VISIBLE);
                            }

                            consultLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, OnlineConsultancyActivity.class);
                                    intent.putExtra("user_id", eachItems.get(position).getId());
                                    context.startActivity(intent);
                                }
                            });
                        }
                    } else {
                        consultLayout.setVisibility(View.GONE);
                    }

                    if (eachItems.get(position).getIsAppointment().length() > 0) {
                        if (eachItems.get(position).getIsAppointment().trim().equals("0")) {
                            appointmentLayout.setVisibility(View.GONE);
                            Log.e("Gone", eachItems.get(position).getIsAppointment());

                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)consultLayout.getLayoutParams();
                            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                            //params.addRule(RelativeLayout.LEFT_OF, R.id.id_to_be_left_of);
                            consultLayout.setLayoutParams(params);
                        } else {
                            Log.e("View", eachItems.get(position).getIsAppointment());
                            if (user_group_id.equals("4") || user_group_id.equals("5") ||
                                    user_group_id.equals("6") || appUserGroupId.equals("4") ||
                                    appUserGroupId.equals("5") || appUserGroupId.equals("6")) {
                                appointmentTextView.setVisibility(View.GONE);
                                appointmentLayout.setVisibility(View.GONE);

                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)consultLayout.getLayoutParams();
                                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                                //params.addRule(RelativeLayout.LEFT_OF, R.id.id_to_be_left_of);
                                consultLayout.setLayoutParams(params);
                            } else {
                                appointmentTextView.setVisibility(View.VISIBLE);
                                appointmentLayout.setVisibility(View.VISIBLE);
                            }

                            appointmentTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, ChamberActivity.class);
                                    intent.putExtra("user_id", eachItems.get(position).getId());
                                    intent.putExtra("name", eachItems.get(position).getFirstName() + " " + eachItems.get(position).getLastName());
                                    intent.putExtra("degree", eachItems.get(position).getDegree());
                                    intent.putExtra("image_url", eachItems.get(position).getPhoto());
                                    context.startActivity(intent);
                                }
                            });
                        }
                    } else {
                        appointmentLayout.setVisibility(View.GONE);
                    }
                }
            }
        }


       /* if () {
            appointmentTextView.setVisibility(View.GONE);
        }
        else {
            appointmentTextView.setVisibility(View.VISIBLE);
        }*/

        LinearLayout itemDetailLinearLayout = (LinearLayout) convertView.findViewById(R.id.doctors_item_linlayout);
        itemDetailLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (eachItems.get(position).getUser_group_id().trim().equals("6")) {
                    //Log.e("group_id",eachItems.get(position).getUser_group_id());
                    Intent intent = new Intent(context, DrugProfileActivity.class);
                    intent.putExtra("drug_id", eachItems.get(position).getId());
                    intent.putExtra("brand_name", eachItems.get(position).getFirstName());
                    intent.putExtra("description", "");
                    intent.putExtra("packet_image", "");
                    intent.putExtra("packet_title", "");
                    intent.putExtra("packet_description", "");
                    intent.putExtra("drug_image", eachItems.get(position).getPhoto());
                    intent.putExtra("overview", "");
                    intent.putExtra("side_effect", "");
                    intent.putExtra("dosage", "");
                    intent.putExtra("drugs_profile", "");
                    intent.putExtra("generic_name", eachItems.get(position).getLastName());
                    intent.putExtra("first_name", eachItems.get(position).getDegree());
                    intent.putExtra("user_id", "");
                    context.startActivity(intent);
                }
                else {
                    Log.e("group_id",eachItems.get(position).getUser_group_id());
                    Intent intent = new Intent(context, ProfileDetailNewActivity.class);
                    intent.putExtra("user_id", eachItems.get(position).getId());
                    intent.putExtra("user_fname", eachItems.get(position).getFirstName());
                    intent.putExtra("user_lname", eachItems.get(position).getLastName());
                    intent.putExtra("user_email", "");
                    intent.putExtra("group_id", eachItems.get(position).getUser_group_id());
                    intent.putExtra("user_image_url", eachItems.get(position).getPhoto());
                    context.startActivity(intent);
                }
            }
        });

        /*if (position == eachItems.size() - 1 && !isEnd) {
            ViewGroup lazzyAttachlinlay = (ViewGroup) convertView.findViewById(R.id.lazzyloader_attach_linlay);
            LayoutInflater inflater_child = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View child_view = inflater_child.inflate(R.layout.lazzy_loader_layout, lazzyAttachlinlay, false);
            ////////
            lazzyAttachlinlay.addView(child_view);
            getUsersRequest(limit_start, limit_end);
        }*/
        return convertView;
    }

    private void saveDoctorRequest(final int position) {

        String url = context.getResources().getString(R.string.MAIN_URL) + "saveDoctor";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("doctor_id", eachItems.get(position).getId());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void unsaveDoctorRequest(final int position) {

        String url = context.getResources().getString(R.string.MAIN_URL) + "unsaveDoctor";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("doctor_id", eachItems.get(position).getId());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getUsersRequest(final int start, final int take) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "searchGetUsers";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getUsers: " + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    limit_start = limit_start + limit_end;
                    if (jsonArray.length() < 5) {
                        isEnd = true;
                    }

                    for (int i = 0; i < jsonArray.length(); ++i) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                        List<SpecialityModel>specialityList = new ArrayList<SpecialityModel>();
                        String id, firstName,lastName,photo, user_group_id, degree, type,
                                district, review_count, review_star, isSaved, isConsult, item_price, isSubscribed, isAppointment;
                        id = jsonObject.getString("id");
                        firstName = jsonObject.getString("first_name");
                        lastName = jsonObject.getString("last_name");
                        photo = jsonObject.getString("photo");
                        user_group_id = jsonObject.getString("user_group_id");
                        degree = jsonObject.getString("degree");
                        type = jsonObject.getString("type");//////
                        district = jsonObject.getString("district");
                        isConsult = jsonObject.getString("isConsult");
                        isSubscribed = jsonObject.getString("isSubscribed");
                        isAppointment = jsonObject.getString("isAppointment");
                        item_price = jsonObject.getString("item_price");

                        if (user_group_id.equals("2")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("speciality");
                            for (int j=0; j<jsonArray1.length();++j) {
                                JSONObject jsonObject1 = (JSONObject) jsonArray1.get(j);
                                String speciality_id = jsonObject1.getString("specialization_id");
                                String speciality_name = jsonObject1.getString("name");

                                SpecialityModel specialityModel = new SpecialityModel(speciality_id,speciality_name);
                                specialityList.add(specialityModel);
                            }
                        }
                        else {

                        }

                        if (!user_group_id.equals("6")) {
                            JSONArray jsonArray2 = jsonObject.getJSONArray("review");
                            JSONObject jsonObject2 = (JSONObject) jsonArray2.get(0);

                            review_count = jsonObject2.getString("count");
                            review_star = jsonObject2.getString("rating");
                        }
                        else {
                            review_count = "0";
                            review_star = "0";
                        }

                        isSaved = jsonObject.getString("isSaved");

                        CategoryItem categoryItem = new CategoryItem(id, firstName,lastName,photo, user_group_id, degree, type, district, review_count, review_star,specialityList,isSaved,isConsult, item_price, isSubscribed, isAppointment);

                        //eachItems.add(categoryItem);
                    }
                    notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                isEnd = true;
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_group_id", user_group_id);
                params.put("user_id", appUserId);
                params.put("start_index", String.valueOf(start));
                params.put("take_total", String.valueOf(take));
                params.put("speciality_type", speciality_type);
                params.put("search_item", search_item);
                params.put("latitude", latitude_str);
                params.put("longitude", longitude_str);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}
