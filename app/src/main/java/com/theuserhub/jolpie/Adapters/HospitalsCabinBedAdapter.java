package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.theuserhub.jolpie.Models.CabinBedModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 4/11/2016.
 */
public class HospitalsCabinBedAdapter extends BaseAdapter{
    private Context context;
    private List<CabinBedModel> eachCabin;

    private LayoutInflater inflater;

    public HospitalsCabinBedAdapter(Context context, List<CabinBedModel> eachCabin) {
        this.context = context;
        this.eachCabin = eachCabin;
    }

    @Override
    public int getCount() {
        return eachCabin.size();
    }

    @Override
    public Object getItem(int position) {
        return eachCabin.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_cabin_bed, null);
        }

        TextView nameTextView = (TextView) convertView.findViewById(R.id.service_name_tv);
        TextView desTextView = (TextView) convertView.findViewById(R.id.service_des_tv);
        TextView priceTextView = (TextView) convertView.findViewById(R.id.service_price_tv);

        nameTextView.setText(eachCabin.get(position).getService_name());
        desTextView.setText(eachCabin.get(position).getService_des());
        priceTextView.setText(eachCabin.get(position).getService_price());

        return convertView;
    }
}
