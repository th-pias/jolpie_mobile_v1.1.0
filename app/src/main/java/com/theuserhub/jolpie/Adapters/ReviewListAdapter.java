package com.theuserhub.jolpie.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Activity.ReviewListActivity;
import com.theuserhub.jolpie.Fragments.ReviewEditDialogFragment;
import com.theuserhub.jolpie.Models.ReviewModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 1/11/2016.
 */
public class ReviewListAdapter extends BaseAdapter{
    private Context context;
    private List<ReviewModel>eachItems;
    private String appUserId,tag_string_req="review_dlt_str_req";
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private LayoutInflater inflater;
    private FragmentManager fragmentManager;

    public ReviewListAdapter(Context context, List<ReviewModel> eachItems, String appUserId, FragmentManager fragmentManager) {
        this.context = context;
        this.eachItems = eachItems;
        this.appUserId = appUserId;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return eachItems.size();
    }

    @Override
    public Object getItem(int position) {
        return eachItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_review_list, null);
        }
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        RelativeLayout review_img_r_l = (RelativeLayout) convertView.findViewById(R.id.review_img_r_l);////
        NetworkImageView image_img_view = (NetworkImageView) convertView.findViewById(R.id.image_img_view);/////

        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachItems.get(position).getReview_by_photo();
        image_img_view.setImageUrl(meta_url,imageLoader);

        RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.reviews_rating_star);
        ratingBar.setRating(Float.parseFloat(eachItems.get(position).getReview_star_count()));

        TextView review_date = (TextView) convertView.findViewById(R.id.reviews_date_tv);
        review_date.setText(eachItems.get(position).getReview_created_at());

        TextView reviewed_name = (TextView) convertView.findViewById(R.id.reviews_by_name_tv);////

        if(eachItems.get(position).getReview_group_id().equals("2")){
            reviewed_name.setText("Dr. "+eachItems.get(position).getReview_fname()+" "+eachItems.get(position).getReview_lname());
        }else{
            reviewed_name.setText(eachItems.get(position).getReview_fname()+" "+eachItems.get(position).getReview_lname());
        }
        reviewed_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProfileDetailNewActivity.class);
                i.putExtra("user_id",eachItems.get(position).getReview_by());
                i.putExtra("user_fname",eachItems.get(position).getReview_fname());
                i.putExtra("user_lname",eachItems.get(position).getReview_lname());
                i.putExtra("user_email","");
                i.putExtra("group_id",eachItems.get(position).getReview_group_id());
                i.putExtra("user_image_url",eachItems.get(position).getReview_by_photo());
                context.startActivity(i);
            }
        });
        review_img_r_l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProfileDetailNewActivity.class);
                i.putExtra("user_id",eachItems.get(position).getReview_by());
                i.putExtra("user_fname",eachItems.get(position).getReview_fname());
                i.putExtra("user_lname",eachItems.get(position).getReview_lname());
                i.putExtra("user_email","");
                i.putExtra("group_id",eachItems.get(position).getReview_group_id());
                i.putExtra("user_image_url",eachItems.get(position).getReview_by_photo());
                context.startActivity(i);
            }
        });

        TextView review_title = (TextView) convertView.findViewById(R.id.reviews_title_tv);
        review_title.setText(eachItems.get(position).getReview_title());

        TextView review_des = (TextView) convertView.findViewById(R.id.reviews_description_tv);
        review_des.setText(eachItems.get(position).getReview_description());

        RelativeLayout editDeleteLayout = (RelativeLayout) convertView.findViewById(R.id.reviews_edit_delete_rl);
        ImageView editView = (ImageView) convertView.findViewById(R.id.reviews_edit_iv);
        ImageView deleteView = (ImageView) convertView.findViewById(R.id.reviews_delete_iv);
        if (appUserId.equals(eachItems.get(position).getReview_by())) {
            editDeleteLayout.setVisibility(View.VISIBLE);
        }
        else {
            editDeleteLayout.setVisibility(View.GONE);
        }
        editView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("position", String.valueOf(position));
                bundle.putString("user_id", appUserId);
                bundle.putString("review_id", eachItems.get(position).getReview_id());
                bundle.putString("reviewer_id", eachItems.get(position).getReview_by());
                bundle.putString("review_title", eachItems.get(position).getReview_title());
                bundle.putString("review_des", eachItems.get(position).getReview_description());
                bundle.putString("review_star", eachItems.get(position).getReview_star_count());
                ReviewEditDialogFragment reviewEditDialogFragment = new ReviewEditDialogFragment();
                reviewEditDialogFragment.setArguments(bundle);
                reviewEditDialogFragment.show(fragmentManager,"My Review");
            }
        });

        deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are you sure?");
                alertDialogBuilder.setMessage("You wanted to delete review");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //Toast.makeText(MainActivity.this, "You clicked yes button", Toast.LENGTH_LONG).show();
                        deleteReviewRequest(eachItems.get(position).getReview_id(),position);
                        arg0.dismiss();
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //alertDialog.show();
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        return convertView;
    }

    private void deleteReviewRequest(final String review_id, final int position) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "reviewDelete";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("ReviewDelete", response.toString());
                ((ReviewListActivity)context).enableReviewListFAB();
                eachItems.remove(position);
                notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("ReviewDelete", "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("review_id", review_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req );
    }
}
