package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Models.MyConditionsModel;
import com.theuserhub.jolpie.Models.PatientsLikeMeModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 1/27/2016.
 */
public class PatientsLikeMeFragmentAdapter extends BaseAdapter {
    private Context context;
    private List<PatientsLikeMeModel> eachPatients;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private LayoutInflater inflater;

    public PatientsLikeMeFragmentAdapter(Context context, List<PatientsLikeMeModel> eachPatients) {
        this.context = context;
        this.eachPatients = eachPatients;
    }

    @Override
    public int getCount() {
        return eachPatients.size();
    }

    @Override
    public Object getItem(int position) {
        return eachPatients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_patients_like_me_fragment, null);
        }

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();


        TextView nameTextView = (TextView) convertView.findViewById(R.id.patients_like_name_tv);
        TextView conditionsTextView = (TextView) convertView.findViewById(R.id.patients_like_conditions_tv);

        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.patient_like_me_r_l);

        ImageView patients_like_iv = (ImageView) convertView.findViewById(R.id.patients_like_iv);
        NetworkImageView networkImageView = (NetworkImageView) convertView.findViewById(R.id.image_img_view);


        if(eachPatients.get(position).getUser_group_id().equals("2")){
            nameTextView.setText("Dr. "+eachPatients.get(position).getFirst_name() + " " + eachPatients.get(position).getLast_name());
        }else{
            nameTextView.setText(eachPatients.get(position).getFirst_name() + " " + eachPatients.get(position).getLast_name());
        }
        List<MyConditionsModel> conditionsModels = eachPatients.get(position).getMyConditionsModels();
        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachPatients.get(position).getImage();
        if(eachPatients.get(position).getImage().length()>0){
            networkImageView.setImageUrl(meta_url,imageLoader);
        }else{
            if(eachPatients.get(position).getUser_group_id().equals("1")||eachPatients.get(position).getUser_group_id().equals("3")){
                patients_like_iv.setImageResource(R.drawable.patient_student_default);
            }else if(eachPatients.get(position).getUser_group_id().equals("2")){
                patients_like_iv.setImageResource(R.drawable.doctor_default);
            }else if(eachPatients.get(position).getUser_group_id().equals("4")){
                patients_like_iv.setImageResource(R.drawable.hospital_default);
            }else if(eachPatients.get(position).getUser_group_id().equals("5")){
                patients_like_iv.setImageResource(R.drawable.labs_default);
            }else if(eachPatients.get(position).getUser_group_id().equals("6")){
                patients_like_iv.setImageResource(R.drawable.pharmaceutical_default);
            }
        }


        String conditions = "";
        for (int c = 0; c < conditionsModels.size(); ++c) {
            if (c != 0) {
                conditions = conditions + ", ";
            }
            conditions = conditions + conditionsModels.get(c).getConditionName_en() + "(" + conditionsModels.get(c).getConditionName_bn() + ")";
        }
        conditionsTextView.setText(conditions);

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProfileDetailNewActivity.class);

                i.putExtra("user_id",eachPatients.get(position).getUser_id());
                i.putExtra("user_fname",eachPatients.get(position).getFirst_name());
                i.putExtra("user_lname",eachPatients.get(position).getLast_name());
                i.putExtra("user_email","");
                i.putExtra("group_id",eachPatients.get(position).getUser_group_id());
                i.putExtra("user_image_url",eachPatients.get(position).getImage());
                context.startActivity(i);
            }
        });

        return convertView;
    }
}
