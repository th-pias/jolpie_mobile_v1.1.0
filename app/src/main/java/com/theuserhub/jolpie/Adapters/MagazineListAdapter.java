package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Activity.MagazineActivity;
import com.theuserhub.jolpie.Activity.MagazineDetailActivity;
import com.theuserhub.jolpie.Models.ArticlesModel;
import com.theuserhub.jolpie.Models.MagazinesModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 1/28/2016.
 */
public class MagazineListAdapter extends BaseAdapter{
    private Context context;
    private List<MagazinesModel>magazineList;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public MagazineListAdapter(Context context, List<MagazinesModel> magazineList) {
        this.context = context;
        this.magazineList = magazineList;
    }

    @Override
    public int getCount() {
        return magazineList.size();
    }

    @Override
    public Object getItem(int position) {
        return magazineList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_magazine, null);
        }

        Typeface articleFont = Typeface.createFromAsset( context.getAssets(), "fontawesome-webfont.ttf" );

        RelativeLayout catLayout = (RelativeLayout) convertView.findViewById(R.id.magazine_category_name_layout);
        TextView catTextView = (TextView) convertView.findViewById(R.id.magazine_cat_name_tv);
        TextView rightTextView = (TextView) convertView.findViewById(R.id.magazine_cat_name_iv);
        rightTextView.setTypeface(articleFont);

        LinearLayout firstItemLayout = (LinearLayout) convertView.findViewById(R.id.article_item_one_layout);
        ImageView firstItemImageView = (ImageView) convertView.findViewById(R.id.article_item_one_iv);
        NetworkImageView firstItemNImageView = (NetworkImageView) convertView.findViewById(R.id.article_item_one_niv);
        TextView firstItemTextView = (TextView) convertView.findViewById(R.id.article_item_one_tv);

        LinearLayout sectItemLayout = (LinearLayout) convertView.findViewById(R.id.article_item_two_layout);
        ImageView secItemImageView = (ImageView) convertView.findViewById(R.id.article_item_two_iv);
        NetworkImageView secItemNImageView = (NetworkImageView) convertView.findViewById(R.id.article_item_two_niv);
        TextView secItemTextView = (TextView) convertView.findViewById(R.id.article_item_two_tv);

        LinearLayout thirdItemLayout = (LinearLayout) convertView.findViewById(R.id.article_item_three_layout);
        ImageView thirdItemImageView = (ImageView) convertView.findViewById(R.id.article_item_three_iv);
        NetworkImageView thirdItemNImageView = (NetworkImageView) convertView.findViewById(R.id.article_item_three_niv);
        TextView thirdItemTextView = (TextView) convertView.findViewById(R.id.article_item_three_tv);

        catTextView.setText(magazineList.get(position).getCategory());

        final List<ArticlesModel> eachArticles = magazineList.get(position).getArticlesModels();
        for (int m=0;m<eachArticles.size();++m) {
            if (m == 0) {
                firstItemLayout.setVisibility(View.VISIBLE);
                firstItemTextView.setText(eachArticles.get(m).getTitle());
                if (eachArticles.get(m).getPicture().length()>1) {
                    if (imageLoader == null)
                        imageLoader = AppController.getInstance().getImageLoader();

                    String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachArticles.get(m).getPicture();
                    firstItemNImageView.setImageUrl(meta_url, imageLoader);
                }
                else {
                    firstItemNImageView.setVisibility(View.GONE);
                    firstItemImageView.setVisibility(View.GONE);
                }
            }
            else if (m == 1) {
                sectItemLayout.setVisibility(View.VISIBLE);
                secItemTextView.setText(eachArticles.get(m).getTitle());
                if (eachArticles.get(m).getPicture().length()>1) {
                    if (imageLoader == null)
                        imageLoader = AppController.getInstance().getImageLoader();

                    String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachArticles.get(m).getPicture();
                    secItemNImageView.setImageUrl(meta_url, imageLoader);
                }
                else {
                    secItemNImageView.setVisibility(View.GONE);
                    secItemImageView.setVisibility(View.GONE);
                }
            }
            else {
                thirdItemLayout.setVisibility(View.VISIBLE);
                thirdItemTextView.setText(eachArticles.get(m).getTitle());
                if (eachArticles.get(m).getPicture().length()>1) {
                    if (imageLoader == null)
                        imageLoader = AppController.getInstance().getImageLoader();

                    String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachArticles.get(m).getPicture();
                    thirdItemNImageView.setImageUrl(meta_url, imageLoader);
                }
                else {
                    thirdItemNImageView.setVisibility(View.GONE);
                    thirdItemImageView.setVisibility(View.GONE);
                }
            }
        }

        catLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MagazineActivity.class);
                intent.putExtra("category_id",magazineList.get(position).getCategory_id());
                intent.putExtra("category_name",magazineList.get(position).getCategory());
                context.startActivity(intent);
            }
        });

        firstItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getArticleDetail(0,eachArticles);
            }
        });

        sectItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getArticleDetail(1,eachArticles);
            }
        });

        thirdItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getArticleDetail(2,eachArticles);
            }
        });

        return convertView;
    }

    private void getArticleDetail(int pos, List<ArticlesModel>eachArticles) {
        Intent intent = new Intent(context, MagazineDetailActivity.class);
        intent.putExtra("magazine_id",eachArticles.get(pos).getId());
        intent.putExtra("title",eachArticles.get(pos).getTitle());
        intent.putExtra("description",eachArticles.get(pos).getDescription());
        intent.putExtra("author_id",eachArticles.get(pos).getAuthor_id());
        intent.putExtra("created_at",eachArticles.get(pos).getCreated_at());
        intent.putExtra("picture",eachArticles.get(pos).getPicture());
        intent.putExtra("like_count",eachArticles.get(pos).getLike_count());
        intent.putExtra("comment_count",eachArticles.get(pos).getComment_count());
        intent.putExtra("share_count",eachArticles.get(pos).getShare_count());
        intent.putExtra("category_id",eachArticles.get(pos).getCategory_id());
        intent.putExtra("first_name",eachArticles.get(pos).getAuthor_fname());
        intent.putExtra("last_name",eachArticles.get(pos).getAuthor_lname());
        intent.putExtra("photo",eachArticles.get(pos).getAuthor_photo());
        intent.putExtra("user_group_id",eachArticles.get(pos).getAuthor_group_id());
        context.startActivity(intent);
    }
}
