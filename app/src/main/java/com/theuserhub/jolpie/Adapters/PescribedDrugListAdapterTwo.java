package com.theuserhub.jolpie.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.theuserhub.jolpie.Fragments.PrescriptionDrugAddDialogFragment;
import com.theuserhub.jolpie.Models.PrescribedDrugModel;
import com.theuserhub.jolpie.R;

import java.util.List;

/**
 * Created by Admin on 1/25/2016.
 */
/**
 * Created by tumpa on 3/13/2016.
 */
public class PescribedDrugListAdapterTwo extends BaseAdapter{

    private Context context;
    private List<PrescribedDrugModel> eachDrugs;
    private FragmentManager fragmentManager;

    private LayoutInflater inflater;

    public PescribedDrugListAdapterTwo(Context context, List<PrescribedDrugModel> eachDrugs, FragmentManager fragmentManager) {
        this.context = context;
        this.eachDrugs = eachDrugs;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return eachDrugs.size();
    }

    @Override
    public Object getItem(int position) {
        return eachDrugs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_prescribed_drugs, null);
        }
        TextView drugNameTextView = (TextView) convertView.findViewById(R.id.pres_drug_name_tv);
        TextView powerTextView = (TextView) convertView.findViewById(R.id.pres_drug_power_tv);
        TextView dosageTextView = (TextView) convertView.findViewById(R.id.pres_drug_dosage_tv);
        TextView daysTextView = (TextView) convertView.findViewById(R.id.pres_drug_days_tv);

        TextView editTextView = (TextView) convertView.findViewById(R.id.pres_drug_edit_tv);
        TextView deletTextView = (TextView) convertView.findViewById(R.id.pres_drug_delete_tv);

        editTextView.setVisibility(View.GONE);
        deletTextView.setVisibility(View.GONE);

        drugNameTextView.setText(eachDrugs.get(position).getDrug_name());
        powerTextView.setText(eachDrugs.get(position).getPower());
        dosageTextView.setText(eachDrugs.get(position).getDosage());
        daysTextView.setText(eachDrugs.get(position).getNoOfDays()+" Days");

        editTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("isEdit","yes");
                bundle.putInt("position", position);
                bundle.putString("drug_name",eachDrugs.get(position).getDrug_name());
                bundle.putString("power", eachDrugs.get(position).getPower());
                bundle.putString("dosage", eachDrugs.get(position).getDosage());
                bundle.putString("noofdays",eachDrugs.get(position).getNoOfDays());
                PrescriptionDrugAddDialogFragment drugAddDialog = new PrescriptionDrugAddDialogFragment();
                drugAddDialog.setArguments(bundle);
                drugAddDialog.show(fragmentManager, "Edit Drug");
                //drugAddDialog.setCancelable(false);
            }
        });

        deletTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are you sure?");
                alertDialogBuilder.setMessage("You wanted to delete this medicine");

                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        eachDrugs.remove(position);
                        notifyDataSetChanged();
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        return convertView;
    }
}
