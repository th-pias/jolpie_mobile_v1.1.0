package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Activity.GalleryImageFullActivity;
import com.theuserhub.jolpie.Models.UploadPrescriptionsModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by tumpa on 3/13/2016.
 */

public class UploadPrescriptionAdapter extends BaseAdapter {

    private List<UploadPrescriptionsModel> uploadPrescriptionsList;
    private Context context;
    private static LayoutInflater inflater=null;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private UploadPrescriptionsModel uploadPrescriptionsModel;

    public UploadPrescriptionAdapter(Context context,List<UploadPrescriptionsModel> uploadPrescriptionsList){
        this.context = context;
        this.uploadPrescriptionsList = uploadPrescriptionsList;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return uploadPrescriptionsList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class Holder
    {
        TextView imgName;
        NetworkImageView Img;
        LinearLayout linearLayout;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View contentView = null;

        uploadPrescriptionsModel = uploadPrescriptionsList.get(position);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + uploadPrescriptionsModel.getPrescription_image();

        //contentView = inflater.inflate(R.layout.list_row_prescription,null);

        contentView = inflater.inflate(R.layout.list_row_image_gallary,null);
        holder.imgName = (TextView) contentView.findViewById(R.id.image_name_tv);

        holder.Img = (NetworkImageView) contentView.findViewById(R.id.image_img_view);

        holder.linearLayout = (LinearLayout) contentView.findViewById(R.id.gallery_linear_layout);

        holder.imgName.setText("");//uploadPrescriptionsModel.getPrescription_image());


        holder.Img.setImageUrl(meta_url, imageLoader);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, GalleryImageFullActivity.class);
                i.putExtra("image",uploadPrescriptionsModel.getPrescription_image().toString());
                i.putExtra("flag","2");
                context.startActivity(i);
            }
        });

        return contentView;
    }
}
