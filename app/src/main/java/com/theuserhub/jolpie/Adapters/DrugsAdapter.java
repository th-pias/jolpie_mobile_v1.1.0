package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Models.DrugItem;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 11/18/2015.
 */
public class DrugsAdapter extends BaseAdapter {

    private Context context;
    private List<DrugItem> eachItems;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private LayoutInflater inflater;

    public DrugsAdapter(Context context, List<DrugItem> eachItems) {
        this.context = context;
        this.eachItems = eachItems;
    }

    @Override
    public int getCount() {
        return eachItems.size();
    }

    @Override
    public Object getItem(int position) {
        return eachItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_drugitem, null);
        }

        TextView itemName = (TextView) convertView.findViewById(R.id.drugs_item_ttl_tv);
        itemName.setText(eachItems.get(position).getGeneric_name());

        TextView itemBrandName = (TextView) convertView.findViewById(R.id.drugs_item_type_tv);
        itemBrandName.setText(eachItems.get(position).getBrand_name());

        TextView companyName = (TextView) convertView.findViewById(R.id.drugs_item_chember_tv);
        companyName.setText(eachItems.get(position).getFirst_name());

        ImageView locationPointer = (ImageView) convertView.findViewById(R.id.drugs_item_locationpointer_iv);
        locationPointer.setImageResource(R.mipmap.ic_location_pointer);

        NetworkImageView item_user_niv = (NetworkImageView) convertView.findViewById(R.id.item_user_niv);
        if (eachItems.get(position).getDrug_image().length() > 0) {
            if (imageLoader == null) {
                imageLoader = AppController.getInstance().getImageLoader();
            }
            String meta_url = context.getResources().getString(R.string.UPLOADS_URL_DRUG) + eachItems.get(position).getDrug_image();

            item_user_niv.setImageUrl(meta_url, imageLoader);
            item_user_niv.setVisibility(View.VISIBLE);
        }

        /*LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.drag_linear_layout);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DrugProfileActivity.class);
                i.putExtra("brand_name", eachItems.get(position).getBrand_name().toString());
                i.putExtra("description",eachItems.get(position).getDescription().toString());
                i.putExtra("packet_image",eachItems.get(position).getPacket_image().toString());
                i.putExtra("packet_title",eachItems.get(position).getPacket_title().toString());
                i.putExtra("packet_description",eachItems.get(position).getPacket_description().toString());
                i.putExtra("drug_image",eachItems.get(position).getDrug_image().toString());
                i.putExtra("overview",eachItems.get(position).getOverview().toString());
                i.putExtra("side_effect",eachItems.get(position).getSide_effect().toString());
                i.putExtra("dosage",eachItems.get(position).getDosage().toString());
                i.putExtra("drugs_profile",eachItems.get(position).getDrugs_profile().toString());
                i.putExtra("generic_name",eachItems.get(position).getGeneric_name().toString());
                i.putExtra("first_name",eachItems.get(position).getFirst_name().toString());
                i.putExtra("user_id", eachItems.get(position).getUser_id().toString());
                context.startActivity(i);
            }
        });*/


        return convertView;
    }
}
