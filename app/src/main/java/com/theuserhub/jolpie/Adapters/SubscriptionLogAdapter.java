package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.theuserhub.jolpie.Models.SubscriptionLogModel;
import com.theuserhub.jolpie.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Admin on 4/3/2016.
 */
public class SubscriptionLogAdapter extends BaseAdapter{

    private Context context;
    private List<SubscriptionLogModel>eachSubscript;

    private LayoutInflater inflater;
    private SimpleDateFormat dateFormat;
    private TextView buySubsTextView;

    public SubscriptionLogAdapter(Context context, List<SubscriptionLogModel> eachSubscript, TextView buySubsTextView) {
        this.context = context;
        this.eachSubscript = eachSubscript;
        this.buySubsTextView = buySubsTextView;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    @Override
    public int getCount() {
        return eachSubscript.size();
    }

    @Override
    public Object getItem(int position) {
        return eachSubscript.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null)
        {
            convertView = inflater.inflate(R.layout.list_row_subscription_log, null);
        }

        TextView dateTextView = (TextView) convertView.findViewById(R.id.date_tv);
        TextView nameTextView = (TextView) convertView.findViewById(R.id.name_tv);
        TextView isActiveTextView = (TextView) convertView.findViewById(R.id.is_active_tv);

        dateTextView.setText(eachSubscript.get(position).getTran_date());
        nameTextView.setText("PRIME PLAN");

        if (position==0){
            Date transDate = null;
            try {
                transDate = dateFormat.parse(eachSubscript.get(position).getTran_date());

                Calendar calendar = Calendar.getInstance();
                Date todayDate = calendar.getTime();
                String todayString = dateFormat.format(todayDate);

                todayDate = dateFormat.parse(todayString);
                Log.e("date", "" + todayString + " : " + eachSubscript.get(position).getTran_date());
                //double difference = (double)((double)todayDate.getTime() - (double)transDate.getTime())/((double)(365*24*60*60*1000));
                long diff = todayDate.getTime() - transDate.getTime();
                long dayDiff = TimeUnit.MILLISECONDS.toDays(diff);
                int durationInt = 0;
                try {
                    durationInt = Integer.parseInt(eachSubscript.get(position).getDuration());
                }
                catch (NumberFormatException e) {

                }

                if (dayDiff > durationInt) {
                    buySubsTextView.setVisibility(View.VISIBLE);
                    isActiveTextView.setText("InActive");
                    isActiveTextView.setTextColor(ContextCompat.getColor(context, R.color.red_500));
                }
                else {
                    buySubsTextView.setVisibility(View.GONE);
                    isActiveTextView.setText("Active");
                    isActiveTextView.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else {
            isActiveTextView.setText("InActive");
            isActiveTextView.setTextColor(ContextCompat.getColor(context, R.color.red_500));
        }
        return convertView;
    }
}
