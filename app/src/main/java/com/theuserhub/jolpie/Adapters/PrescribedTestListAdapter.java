package com.theuserhub.jolpie.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.theuserhub.jolpie.R;

import java.util.List;

/**
 * Created by Admin on 1/25/2016.
 */
public class PrescribedTestListAdapter extends BaseAdapter{

    private Context context;
    private List<String>eachTests;
    private FragmentManager fragmentManager;

    private LayoutInflater inflater;

    public PrescribedTestListAdapter(Context context, List<String> eachTests, FragmentManager fragmentManager) {
        this.context = context;
        this.eachTests = eachTests;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return eachTests.size();
    }

    @Override
    public Object getItem(int position) {
        return eachTests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_prescribed_tests, null);
        }
        TextView testNameTextView = (TextView) convertView.findViewById(R.id.pres_test_name_tv);
        TextView deleteTextView = (TextView) convertView.findViewById(R.id.pres_test_delete_tv);

        testNameTextView.setText(eachTests.get(position));

        deleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are you sure?");
                alertDialogBuilder.setMessage("You wanted to delete this test");

                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        eachTests.remove(position);
                        notifyDataSetChanged();
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        return convertView;
    }
}
