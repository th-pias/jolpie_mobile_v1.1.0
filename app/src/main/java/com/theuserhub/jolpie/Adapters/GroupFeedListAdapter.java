package com.theuserhub.jolpie.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.PeopleWhoLiked;
import com.theuserhub.jolpie.Activity.PostDetailActivity;
import com.theuserhub.jolpie.Activity.PostUpdateActivity;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Models.SinglePostModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 8/16/2015.
 */
public class GroupFeedListAdapter extends BaseAdapter {
    private String TAG = "GroupFeedListAdapter";
    private String tag_string_req = "feed_str_req";
    private LayoutInflater inflater;
    private List<SinglePostModel> feedList;
    private Context context;
    private String user_id, group_id, appUserGroupId;
    private boolean isEnd;
    private int limit_start = 8, limit_end = 5;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public GroupFeedListAdapter(Context context, List<SinglePostModel> feedList, String user_id, String group_id, boolean isEnd, String appUserGroupId) {
        this.context = context;
        this.feedList = feedList;
        this.user_id = user_id;
        this.group_id = group_id;
        this.isEnd = isEnd;
        this.appUserGroupId = appUserGroupId;
    }

    @Override
    public int getCount() {
        return feedList.size();
    }

    @Override
    public Object getItem(int location) {
        return feedList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Boolean[] liked = {false};

        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_group_feed, null);

        Typeface editFont = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");

        if (position == feedList.size()-1 && !isEnd) {
            ViewGroup lazzyAttachlinlay = (ViewGroup) convertView.findViewById(R.id.lazzyloader_attach_linlay);
            LayoutInflater inflater_child = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            View child_view = inflater_child.inflate(R.layout.lazzy_loader_layout, lazzyAttachlinlay, false);
            lazzyAttachlinlay.addView(child_view);
            getGroupPostsRequest(limit_start, limit_end);
        }

        final Button editButton = (Button) convertView.findViewById(R.id.post_edit_delete_button);
        editButton.setTypeface(editFont);

        ImageView userimageoffeed = (ImageView) convertView.findViewById(R.id.userimageoffeed);
        NetworkImageView networkImageView = (NetworkImageView) convertView.findViewById(R.id.item_user_niv);

        if (feedList.get(position).getPost_user_photo().length() > 0) {
            if (imageLoader == null) {
                imageLoader = AppController.getInstance().getImageLoader();
            }
            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + feedList.get(position).getPost_user_photo();

            networkImageView.setImageUrl(meta_url, imageLoader);
            //networkImageView.setVisibility(View.VISIBLE);
            userimageoffeed.setVisibility(View.INVISIBLE);
        }else{
            if(feedList.get(position).getPost_userGroupId().equals("1")||feedList.get(position).getPost_userGroupId().equals("3")){
                userimageoffeed.setImageResource(R.drawable.patient_student_default);
            }else if(feedList.get(position).getPost_userGroupId().equals("2")){
                userimageoffeed.setImageResource(R.drawable.doctor_default);
            }else if(feedList.get(position).getPost_userGroupId().equals("4")){
                userimageoffeed.setImageResource(R.drawable.hospital_default);
            }else if(feedList.get(position).getPost_userGroupId().equals("5")){
                userimageoffeed.setImageResource(R.drawable.labs_default);
            }else if(feedList.get(position).getPost_userGroupId().equals("6")){
                userimageoffeed.setImageResource(R.drawable.pharmaceutical_default);
            }
        }



        TextView userNameOfFeed = (TextView) convertView.findViewById(R.id.usernameoffeed);
        if (feedList.get(position).getPost_userGroupId().equals("2")) {
            userNameOfFeed.setText("Dr. "+feedList.get(position).getPost_user_fname() + " " + feedList.get(position).getPost_user_lname());
        }
        else {
            userNameOfFeed.setText(feedList.get(position).getPost_user_fname() + " " + feedList.get(position).getPost_user_lname());
        }

        TextView feedTitle = (TextView) convertView.findViewById(R.id.feedtitle);
        if (feedList.get(position).getPost_title().length() > 0) {
            feedTitle.setVisibility(View.VISIBLE);
            feedTitle.setText(feedList.get(position).getPost_title());
        }
        TextView dateTime = (TextView) convertView.findViewById(R.id.datetime);
        dateTime.setText(feedList.get(position).getPost_created());

        final VideoView feedVideo = (VideoView) convertView.findViewById(R.id.feedvideo_vv);
        feedVideo.setVisibility(View.GONE);
        NetworkImageView feedImageNetwork = (NetworkImageView) convertView.findViewById(R.id.feedimage_network);
        feedImageNetwork.setVisibility(View.GONE);
        if (feedList.get(position).getPost_imgUrl().trim().length() > 0) {
            if (imageLoader == null) {
                imageLoader = AppController.getInstance().getImageLoader();
            }
            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + feedList.get(position).getPost_imgUrl();
            //feedImageNetwork.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //thumbNail.setLayoutParams(new RelativeLayout.LayoutParams(imageWidth, imageWidth));
            feedImageNetwork.setImageUrl(meta_url, imageLoader);
            feedImageNetwork.setVisibility(View.VISIBLE);
        }

        else if (feedList.get(position).getPost_vdoUrl().trim().length() > 0) {
            feedVideo.setVisibility(View.VISIBLE);
            //feedVideo.setMediaController(new MediaController(context));
            feedVideo.setVideoPath(context.getResources().getString(R.string.UPLOADS_URL) + "video/" + feedList.get(position).getPost_vdoUrl());
            feedVideo.requestFocus();
            feedVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    feedVideo.start();
                }
            });
        }

        TextView description = (TextView) convertView.findViewById(R.id.description);
        if (feedList.get(position).getPost_description().length() > 100) {
            String tempString = feedList.get(position).getPost_description().substring(0, 99) + "...Continue Reading";

            Spannable wordtoSpan = new SpannableString(tempString);

            wordtoSpan.setSpan(new ForegroundColorSpan(Color.GRAY), 102, 118, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            /*final SpannableStringBuilder sb = new SpannableStringBuilder(tempString);

            // Span to set text color to some RGB value
            final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(224, 224, 224));

            // Span to make text bold
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

            // Set the text color for first 4 characters
            sb.setSpan(fcs, 99, 118, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            // make them also bold
            //sb.setSpan(bss, 0, 4, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            //yourTextView.setText(sb);*/
            description.setText(wordtoSpan);
        } else {
            description.setText(feedList.get(position).getPost_description());
        }

        final TextView goodCountTV = (TextView) convertView.findViewById(R.id.goodcount);
        goodCountTV.setText(feedList.get(position).getPost_likes());

        final TextView followCountTV = (TextView) convertView.findViewById(R.id.followcount);
        followCountTV.setText(feedList.get(position).getPost_follows());

        TextView answerCountTV = (TextView) convertView.findViewById(R.id.answercount);
        answerCountTV.setText(feedList.get(position).getPost_answers());

        //click on number of likes
        final LinearLayout goodcountLinLay = (LinearLayout) convertView.findViewById(R.id.goodcount_linlay);
        goodcountLinLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PeopleWhoLiked.class);
                intent.putExtra("peopleWhoFlag", "-1");
                intent.putExtra("post_id", feedList.get(position).getPost_id());
                context.startActivity(intent);
            }
        });

        //click on number of comments
        LinearLayout answercountLinLay = (LinearLayout) convertView.findViewById(R.id.answercount_linlay);
        answercountLinLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liked[0]) {
                    gotoCommentActivity(position,"1");
                }
                else {
                    gotoCommentActivity(position,"0");
                }
            }
        });

        //click on number of likes
        LinearLayout followcountLinLay = (LinearLayout) convertView.findViewById(R.id.followcount_linlay);
        followcountLinLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PeopleWhoLiked.class);
                intent.putExtra("peopleWhoFlag", "1");
                intent.putExtra("post_id", feedList.get(position).getPost_id());
                context.startActivity(intent);

            }
        });

        //click to show the user detail/profile..................
        LinearLayout feedPersonsDetail = (LinearLayout) convertView.findViewById(R.id.feedlist_user_layout);
        feedPersonsDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileDetailNewActivity.class);
                //intent.putExtra("group_id",feedList.get(position).getPost_userGroupId());
                intent.putExtra("user_id",feedList.get(position).getPost_userId());
                intent.putExtra("user_fname",feedList.get(position).getPost_user_fname());
                intent.putExtra("user_lname",feedList.get(position).getPost_user_lname());
                intent.putExtra("user_email","");
                intent.putExtra("group_id",feedList.get(position).getPost_userGroupId());
                intent.putExtra("user_image_url",feedList.get(position).getPost_imgUrl());
                context.startActivity(intent);
            }
        });

        //click on a feed for detail view..............................
        LinearLayout singleFeedBodyLinearLayout = (LinearLayout) convertView.findViewById(R.id.singlefeedbodylinlay);
        singleFeedBodyLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liked[0]) {
                    gotoCommentActivity(position, "1");
                } else {
                    gotoCommentActivity(position, "0");
                }
            }
        });

        //click on like button for like / unlike
        LinearLayout likeCommentShareLayout = (LinearLayout) convertView.findViewById(R.id.like_comment_share_layout);
        if (appUserGroupId.equals("4") || appUserGroupId.equals("5") || appUserGroupId.equals("6")) {
            if (feedList.get(position).getPost_userGroupId().equals("4") || feedList.get(position).getPost_userGroupId().equals("5") || feedList.get(position).getPost_userGroupId().equals("6")) {
                likeCommentShareLayout.setVisibility(View.VISIBLE);
            }
            else {
                likeCommentShareLayout.setVisibility(View.GONE);
            }
        }
        final LinearLayout goodLinearLayout = (LinearLayout) convertView.findViewById(R.id.goodlinlay);
        final TextView goodImage = (TextView) convertView.findViewById(R.id.goodimageview);
        goodImage.setTypeface(editFont);
        final TextView goodTextView = (TextView) convertView.findViewById(R.id.goodTextView);

        if (feedList.get(position).getPost_liked().equalsIgnoreCase("0")) {
            liked[0] = false;
            //goodLinearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
            goodImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
            goodTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
        }
        else {
            liked[0] = true;
            //goodLinearLayout.setBackgroundColor(Color.parseColor("#e3f2fd"));
            goodImage.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
            goodTextView.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
        }

        goodLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liked[0]) {
                    liked[0] = false;
                    //goodLinearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.grey_500));
                    int numliked = Integer.parseInt(goodCountTV.getText().toString());
                    --numliked;
                    feedList.get(position).setPost_likes(String.valueOf(numliked));
                    goodCountTV.setText(String.valueOf(numliked));

                    AppFunctions.likeUnlikeRequest(context, feedList.get(position).getPost_id(), "0", user_id);
                } else {
                    liked[0] = true;
                    //goodLinearLayout.setBackgroundColor(Color.parseColor("#e3f2fd"));
                    goodImage.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                    goodTextView.setTextColor(ContextCompat.getColor(context, R.color.jolpie_text));
                    int numliked = Integer.parseInt(goodCountTV.getText().toString());
                    ++numliked;
                    feedList.get(position).setPost_likes(String.valueOf(numliked));
                    goodCountTV.setText(String.valueOf(numliked));

                    AppFunctions.likeUnlikeRequest(context, feedList.get(position).getPost_id(), "1", user_id);
                }
            }
        });


        //click on comment button
        LinearLayout answerLinearLayout = (LinearLayout) convertView.findViewById(R.id.answerlinlay);
        answerLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liked[0]) {
                    gotoCommentActivity(position,"1");
                }
                else {
                    gotoCommentActivity(position,"0");
                }
            }
        });
        TextView answerImage = (TextView) convertView.findViewById(R.id.answerimageview);
        answerImage.setTypeface(editFont);

        //click on like button for like / unlike
        LinearLayout followLinearLayout = (LinearLayout) convertView.findViewById(R.id.followlinlay);
        followLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are you sure?");
                alertDialogBuilder.setMessage("You wanted to share this discussion?");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        int numShared = Integer.parseInt(followCountTV.getText().toString());
                        ++numShared;
                        followCountTV.setText(String.valueOf(numShared));

                        postShareRequest(feedList.get(position).getPost_id());
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ////////////
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        TextView followImage = (TextView) convertView.findViewById(R.id.followimageview);
        followImage.setTypeface(editFont);

        if (user_id.equals(feedList.get(position).getPost_userId())) {
            editButton.setVisibility(View.VISIBLE);
        }
        else {
            editButton.setVisibility(View.GONE);
        }
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu filterMenu = new PopupMenu(context, editButton);
                filterMenu.getMenuInflater().inflate(R.menu.menu_edit_delete, filterMenu.getMenu());
                filterMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(context, "You Clicked : "+item.getTitle() , Toast.LENGTH_SHORT).show();
                        if (item.getItemId() == R.id.edit_item) {

                            Intent intent = new Intent(context, PostUpdateActivity.class);
                            intent.putExtra("group_id", group_id);
                            intent.putExtra("post_id", feedList.get(position).getPost_id());
                            context.startActivity(intent);

                        } else if (item.getItemId() == R.id.delete_item) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                            alertDialogBuilder.setTitle("Are you sure?");
                            alertDialogBuilder.setMessage("You wanted to delete this?");

                            alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    postDeleteRequset(feedList.get(position).getPost_id(),position);
                                }
                            });

                            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ////////////
                                }
                            });
                            final AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                        return true;
                    }
                });
                filterMenu.show();//showing popup menu
            }
        });



        return convertView;
    }

    private void postDeleteRequset(final String post_id, final int position) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "deletePost";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context,"Post Deleted Successfully",Toast.LENGTH_SHORT).show();
                    feedList.remove(position);
                    notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("post_id", post_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void postShareRequest(final String post_id) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "postShare";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context, "Discussion Shared Successfully", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("post_id", post_id);
                params.put("increase_share", "1");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void gotoCommentActivity(int position, String liked_by_me) {

        Intent intent = new Intent(context, PostDetailActivity.class);
        intent.putExtra("id", feedList.get(position).getPost_id());
        intent.putExtra("title", feedList.get(position).getPost_title());
        intent.putExtra("description", feedList.get(position).getPost_description());
        intent.putExtra("feeling", feedList.get(position).getPost_feeling());
        intent.putExtra("photo", feedList.get(position).getPost_imgUrl());
        intent.putExtra("video", feedList.get(position).getPost_vdoUrl());
        intent.putExtra("created_at", feedList.get(position).getPost_created());
        intent.putExtra("updated_at", feedList.get(position).getPost_updated());
        intent.putExtra("user_id", feedList.get(position).getPost_userId());
        intent.putExtra("user_fname", feedList.get(position).getPost_user_fname());
        intent.putExtra("user_lname", feedList.get(position).getPost_user_lname());
        intent.putExtra("user_photo", feedList.get(position).getPost_user_photo());
        intent.putExtra("user_group_id", feedList.get(position).getPost_userGroupId());
        intent.putExtra("good_count", feedList.get(position).getPost_likes());
        intent.putExtra("answer_count", feedList.get(position).getPost_answers());
        intent.putExtra("share_count", feedList.get(position).getPost_follows());
        intent.putExtra("liked_by_me", liked_by_me);
        intent.putExtra("shared_by_me", "0");
        context.startActivity(intent);
    }

    private void getGroupPostsRequest(final int start, final int take) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getGroupPosts";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "GroupPost : " + response.toString());
                /*feedList.clear(); //xvxcvcx*/
                String post_id,post_title,post_description,post_feeling,post_likes,post_answers,post_follows,post_userId,post_user_fname,post_user_lname,post_user_photo,
                        post_userGroupId,post_created,post_updated,post_imgUrl,post_vdoUrl,post_liked;
                try {
                    JSONArray allPosts = new JSONArray(response);
                    limit_start = limit_start+limit_end;
                    if (allPosts.length()<5){
                        isEnd = true;
                    }
                    //Log.d("Test2", allPosts.toString());
                    for (int i = 0; i < allPosts.length(); ++i) {
                        JSONObject singlePost = (JSONObject) allPosts.get(i);
                        post_id = singlePost.getString("id");
                        post_title = singlePost.getString("title");
                        post_description = singlePost.getString("description");
                        post_feeling = singlePost.getString("feeling");
                        post_likes = singlePost.getString("good_count");
                        post_answers = singlePost.getString("answer_count");
                        post_follows = singlePost.getString("follow_count");
                        post_userId = singlePost.getString("user_id");
                        post_user_fname = singlePost.getString("first_name");
                        post_user_lname = singlePost.getString("last_name");
                        post_user_photo = singlePost.getString("user_photo");
                        post_userGroupId = singlePost.getString("user_group_id");
                        post_created = singlePost.getString("created_at");
                        post_updated = singlePost.getString("updated_at");
                        post_imgUrl = singlePost.getString("photo");
                        post_vdoUrl = singlePost.getString("video");
                        post_liked = singlePost.getString("liked_by_me");
                        //Toast.makeText(context,post_id+"\n"+post_title+"\n"+post_des,Toast.LENGTH_LONG).show();
                        SinglePostModel pst = new SinglePostModel(post_id,post_title,post_description,post_feeling,post_likes,post_answers,post_follows,post_userId,post_user_fname,post_user_lname,post_user_photo,
                                post_userGroupId,post_created,post_updated,post_imgUrl,post_vdoUrl,post_liked);
                        feedList.add(pst);
                    }

                    //refresh adapter
                    notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                isEnd = true;
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(context, "Error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("group_id", group_id);
                params.put("group_id", user_id);
                params.put("start_index", String.valueOf(start));
                params.put("take_total", String.valueOf(take));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
