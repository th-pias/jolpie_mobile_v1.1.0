package com.theuserhub.jolpie.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Models.GroupMemberModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 1/19/2016.
 */
public class GroupMemberAddAdapter extends BaseAdapter{
    private String TAG = "GroupMemberAddActivity", tag_string_req = "grp_memebr_add_str_req";
    private Context context;
    private List<GroupMemberModel> eachItems;
    private String group_id;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private ProgressDialog pDialog;

    public GroupMemberAddAdapter(Context context, List<GroupMemberModel> eachItems, String group_id) {
        this.context = context;
        this.eachItems = eachItems;
        this.group_id = group_id;
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
    }

    @Override
    public int getCount() {
        return eachItems.size();
    }

    @Override
    public Object getItem(int position) {
        return eachItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_grp_member_add_list, null);
        }

        ImageView autoImageView = (ImageView) convertView.findViewById(R.id.grp_membr_iv);
        NetworkImageView autoNImageView = (NetworkImageView) convertView.findViewById(R.id.grp_membr_niv);

        if (eachItems.get(position).getImage().length() > 1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachItems.get(position).getImage();
            autoNImageView.setImageUrl(meta_url, imageLoader);
        } else {
            if (eachItems.get(position).getUser_group_id().equals("1") || eachItems.get(position).getUser_group_id().equals("3")) {
                autoImageView.setImageResource(R.drawable.patient_student_default);
            } else if (eachItems.get(position).getUser_group_id().equals("2")) {
                autoImageView.setImageResource(R.drawable.doctor_default);
            } else if (eachItems.get(position).getUser_group_id().equals("4")) {
                autoImageView.setImageResource(R.drawable.hospital_default);
            } else if (eachItems.get(position).getUser_group_id().equals("5")) {
                autoImageView.setImageResource(R.drawable.labs_default);
            } else if (eachItems.get(position).getUser_group_id().equals("6")) {
                autoImageView.setImageResource(R.drawable.drug_default);
            } else if (eachItems.get(position).getUser_group_id().equals("7")) {
                autoImageView.setImageResource(R.drawable.group_default_new);
            }
        }

        TextView autoTitle = (TextView) convertView.findViewById(R.id.grp_membr_name_tv);
        if (eachItems.get(position).getUser_group_id().equals("2")) {
            autoTitle.setText("Dr. "+eachItems.get(position).getFirst_name()+" "+eachItems.get(position).getLast_name());
        }
        else {
            autoTitle.setText(eachItems.get(position).getFirst_name()+" "+eachItems.get(position).getLast_name());
        }


        TextView addTextView = (TextView) convertView.findViewById(R.id.grp_membr_add_tv);
        addTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                joinGroupRequest(position);
            }
        });
        return convertView;
    }

    private void joinGroupRequest(final int position) {
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL) + "joinGroup";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                pDialog.dismiss();
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context, "Member added", Toast.LENGTH_LONG).show();
                    eachItems.remove(position);
                    notifyDataSetChanged();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("group_id", group_id);
                params.put("user_id", eachItems.get(position).getUser_id());
                params.put("status", "approved");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
