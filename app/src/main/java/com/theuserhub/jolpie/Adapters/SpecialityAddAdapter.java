package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tumpa on 3/3/2016.
 */
public class SpecialityAddAdapter extends BaseAdapter{
    private Context context;
    private List<SpecialityModel> specialityModelsArrayList;
    String user_id;
    private static LayoutInflater inflater=null;
    public SpecialityAddAdapter(Context context,List<SpecialityModel> specialityModelsArrayList,String user_id){
        this.context = context;
        this.specialityModelsArrayList = specialityModelsArrayList;
        this.user_id = user_id;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return specialityModelsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView tv;
        Button btn;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_view_add_speciality,null);
        holder.tv = (TextView) rowView.findViewById(R.id.speciality_txt);
        holder.btn = (Button) rowView.findViewById(R.id.add_btn);

        holder.tv.setText(specialityModelsArrayList.get(position).getName().toString());
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMySpecialityRequest(position);
                Log.d("on click","Speciality add adapter");
            }
        });
        return rowView;
    }





    private void addMySpecialityRequest(final int position) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "addSpeciality";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response.toString().trim().equals("ok")) {
                    Toast.makeText(context, "Added Successfully", Toast.LENGTH_LONG).show();
                    specialityModelsArrayList.remove(position);
                    notifyDataSetChanged();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d("Add speciality", "Error: " + error.getMessage());
                Toast.makeText(context, "Error: Try again later", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
               params.put("user_id", user_id);
               params.put("specialization_id", specialityModelsArrayList.get(position).getId());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "tag_string_req");
    }
}
