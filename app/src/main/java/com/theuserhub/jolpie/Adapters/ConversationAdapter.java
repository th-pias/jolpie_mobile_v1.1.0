package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Models.MessagesModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 1/5/2016.
 */
public class ConversationAdapter extends BaseAdapter{

    private Context context;
    private String tag_string_req = "conv_str_req";
    private List<MessagesModel> eachItems;
    private String appUserId,appUserImgUrl,toUserImgUrl;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public ConversationAdapter(Context context, List<MessagesModel> eachItems, String appUserId, String appUserImgUrl, String toUserImgUrl) {
        this.context = context;
        this.eachItems = eachItems;
        this.appUserId = appUserId;
        this.appUserImgUrl = appUserImgUrl;
        this.toUserImgUrl = toUserImgUrl;
    }

    @Override
    public int getCount() {
        return eachItems.size();
    }

    @Override
    public Object getItem(int position) {
        return eachItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.list_row_conversation, null);
        }

        ImageView myImageView = (ImageView) convertView.findViewById(R.id.my_iv);
        ImageView fromImageView = (ImageView) convertView.findViewById(R.id.from_iv);
        NetworkImageView myNImageView = (NetworkImageView) convertView.findViewById(R.id.my_niv);
        NetworkImageView fromNImageView = (NetworkImageView) convertView.findViewById(R.id.from_niv);

        TextView msg_received = (TextView) convertView.findViewById(R.id.list_conv_received_text_tv);
        TextView msg_received_date = (TextView) convertView.findViewById(R.id.list_conv_received_date_tv);
        TextView msg_sent = (TextView) convertView.findViewById(R.id.list_conv_sent_text_tv);
        TextView msg_sent_date = (TextView) convertView.findViewById(R.id.list_conv_sent_date_tv);

        if (eachItems.get(position).getFrom_user_id().equals(appUserId)) {
            fromImageView.setVisibility(View.GONE);
            msg_received.setVisibility(View.GONE);
            msg_received_date.setVisibility(View.GONE);
            msg_sent.setVisibility(View.VISIBLE);
            msg_sent_date.setVisibility(View.VISIBLE);
            msg_sent.setText(eachItems.get(position).getMsg_text());
            msg_sent_date.setText(eachItems.get(position).getMsg_created_at());
            if (appUserImgUrl.length()>1) {
                if (imageLoader == null)
                    imageLoader = AppController.getInstance().getImageLoader();
                String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + appUserImgUrl;
                myNImageView.setImageUrl(meta_url, imageLoader);
            }
            /*else {

            }*/
        }
        else {
            msg_received.setVisibility(View.VISIBLE);
            msg_received_date.setVisibility(View.VISIBLE);
            msg_received.setText(eachItems.get(position).getMsg_text());
            msg_received_date.setText(eachItems.get(position).getMsg_created_at());
            myImageView.setVisibility(View.GONE);
            msg_sent.setVisibility(View.GONE);
            msg_sent_date.setVisibility(View.GONE);

            if (toUserImgUrl.length()>1) {
                if (imageLoader == null)
                    imageLoader = AppController.getInstance().getImageLoader();
                String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + toUserImgUrl;
                fromNImageView.setImageUrl(meta_url, imageLoader);
            }
        }

        return convertView;
    }
}
