package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.theuserhub.jolpie.Activity.ChamberScheduleActivity;
import com.theuserhub.jolpie.Models.ChamberExModel;
import com.theuserhub.jolpie.Models.ChamberModel;
import com.theuserhub.jolpie.Models.ScheduleModel;
import com.theuserhub.jolpie.R;

import java.util.List;

/**
 * Created by Admin on 2/9/2016.
 */
public class ChamberListAdapter extends BaseAdapter {
    private Context context;
    private List<ChamberExModel> eachChambers;
    private String appUserId, user_id, appUserGroupId;
    private String name;
    private String degree;
    private String image;
    private LayoutInflater inflater;

    public ChamberListAdapter(Context context, List<ChamberExModel> eachChambers, String appUserId, String appUserGroupId, String user_id,String name,String degree,String image) {
        this.context = context;
        this.eachChambers = eachChambers;
        this.appUserId = appUserId;
        this.appUserGroupId = appUserGroupId;
        this.user_id = user_id;
        this.name = name;
        this.image = image;
        this.degree = degree;
    }

    @Override
    public int getCount() {
        return eachChambers.size();
    }

    @Override
    public Object getItem(int position) {
        return eachChambers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null)
        {
            convertView = inflater.inflate(R.layout.list_row_chamber, null);
        }

        boolean isSun = false;
        boolean isMon = false;
        boolean isTue = false;
        boolean isWed = false;
        boolean isThu = false;
        boolean isFri = false;
        boolean isSat = false;

        TextView nameTextView = (TextView) convertView.findViewById(R.id.chamber_name_tv);
        TextView addressTextView = (TextView) convertView.findViewById(R.id.chamber_address_tv);
        TextView feeTextView = (TextView) convertView.findViewById(R.id.chamber_fee_tv);
        TextView mobileTextView = (TextView) convertView.findViewById(R.id.chamber_contact_number_tv);

        LinearLayout sunLayout = (LinearLayout) convertView.findViewById(R.id.sun_layout);
        TextView sunTextView = (TextView) convertView.findViewById(R.id.sun_time);
        LinearLayout monLayout = (LinearLayout) convertView.findViewById(R.id.mon_layout);
        TextView monTextView = (TextView) convertView.findViewById(R.id.mon_time);
        LinearLayout tueLayout = (LinearLayout) convertView.findViewById(R.id.tue_layout);
        TextView tueTextView = (TextView) convertView.findViewById(R.id.tue_time);
        LinearLayout wedLayout = (LinearLayout) convertView.findViewById(R.id.wed_layout);
        TextView wedTextView = (TextView) convertView.findViewById(R.id.wed_time);
        LinearLayout thuLayout = (LinearLayout) convertView.findViewById(R.id.thu_layout);
        TextView thuTextView = (TextView) convertView.findViewById(R.id.thu_time);
        LinearLayout friLayout = (LinearLayout) convertView.findViewById(R.id.fri_layout);
        TextView friTextView = (TextView) convertView.findViewById(R.id.fri_time);
        LinearLayout satLayout = (LinearLayout) convertView.findViewById(R.id.sat_layout);
        TextView satTextView = (TextView) convertView.findViewById(R.id.sat_time);

        TextView appointButton = (TextView) convertView.findViewById(R.id.chamber_appoint_button);

        if (eachChambers.get(position).getChamber_first_name().length()>1) {
            nameTextView.setVisibility(View.VISIBLE);
            nameTextView.setText(eachChambers.get(position).getChamber_first_name()+" "+eachChambers.get(position).getChamber_last_name());
        }
        else {
            nameTextView.setVisibility(View.GONE);
        }

        if (eachChambers.get(position).getChamber_street_address().length()>0) {
            addressTextView.setText(eachChambers.get(position).getChamber_street_address()+ ", "+eachChambers.get(position).getChamber_address());
        }
        else {
            addressTextView.setText(eachChambers.get(position).getChamber_address());
        }

        feeTextView.setText("BDT "+eachChambers.get(position).getChamber_fee());

        if (eachChambers.get(position).getChamber_mobile().length()>1) {
            mobileTextView.setVisibility(View.VISIBLE);
            mobileTextView.setText(eachChambers.get(position).getChamber_mobile());
        }
        else {
            mobileTextView.setVisibility(View.GONE);
        }

        List<ScheduleModel> scheduleModelList = eachChambers.get(position).getScheduleModelList();

        for (int i=0; i<scheduleModelList.size();++i) {
            Log.e("day",scheduleModelList.get(i).getDay());
            if (scheduleModelList.get(i).getDay().equalsIgnoreCase("sun")) {
                if (isSun) {
                    sunTextView.setText(sunTextView.getText().toString()+"\n"+scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
                else {
                    isSun = true;
                    sunLayout.setVisibility(View.VISIBLE);
                    sunTextView.setText(scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
            }

            else if (scheduleModelList.get(i).getDay().equalsIgnoreCase("mon")) {
                if (isMon) {
                    monTextView.setText(monTextView.getText().toString()+"\n"+scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
                else {
                    isMon = true;
                    monLayout.setVisibility(View.VISIBLE);
                    monTextView.setText(scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
            }
            else if (scheduleModelList.get(i).getDay().equalsIgnoreCase("tue")) {
                if (isTue) {
                    tueTextView.setText(tueTextView.getText().toString()+"\n"+scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
                else {
                    isTue = true;
                    tueLayout.setVisibility(View.VISIBLE);
                    tueTextView.setText(scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
            }
            else if (scheduleModelList.get(i).getDay().equalsIgnoreCase("wed")) {
                if (isWed) {
                    wedTextView.setText(wedTextView.getText().toString()+"\n"+scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
                else {
                    isWed = true;
                    wedLayout.setVisibility(View.VISIBLE);
                    wedTextView.setText(scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
            }
            else if (scheduleModelList.get(i).getDay().equalsIgnoreCase("thu")) {
                if (isThu) {
                    thuTextView.setText(thuTextView.getText().toString()+"\n"+scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
                else {
                    isThu = true;
                    thuLayout.setVisibility(View.VISIBLE);
                    thuTextView.setText(scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
            }
            else if (scheduleModelList.get(i).getDay().equalsIgnoreCase("fri")) {
                if (isFri) {
                    friTextView.setText(friTextView.getText().toString()+"\n"+scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
                else {
                    isFri = true;
                    friLayout.setVisibility(View.VISIBLE);
                    friTextView.setText(scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
            }
            else if (scheduleModelList.get(i).getDay().equalsIgnoreCase("sat")) {
                if (isSat) {
                    satTextView.setText(satTextView.getText().toString()+"\n"+scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
                else {
                    isSat = true;
                    satLayout.setVisibility(View.VISIBLE);
                    satTextView.setText(scheduleModelList.get(i).getStart_time() + " - " + scheduleModelList.get(i).getEnd_time());
                }
            }
        }

        if (appUserGroupId.equals("4") || appUserGroupId.equals("5") || appUserGroupId.equals("6")) {
            appointButton.setVisibility(View.GONE);
        }
        else {
            if (appUserId.equals(user_id)) {
                appointButton.setVisibility(View.GONE);
            }
            else {
                appointButton.setVisibility(View.VISIBLE);
            }
        }
        appointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChamberScheduleActivity.class);
                intent.putExtra("chamber_id",eachChambers.get(position).getChamber_id());
                intent.putExtra("patient_time",eachChambers.get(position).getEach_patient_time());
                intent.putExtra("chamber_id",eachChambers.get(position).getChamber_id());
                intent.putExtra("organisation_id",eachChambers.get(position).getOrganisation_id());
                intent.putExtra("chamber_first_name",eachChambers.get(position).getChamber_first_name());
                intent.putExtra("chamber_last_name",eachChambers.get(position).getChamber_last_name());
                intent.putExtra("chamber_photo",eachChambers.get(position).getChamber_photo());
                intent.putExtra("chamber_user_group_id",eachChambers.get(position).getChamber_user_group_id());
                intent.putExtra("chamber_street_address",eachChambers.get(position).getChamber_street_address());
                intent.putExtra("chamber_address",eachChambers.get(position).getChamber_address());
                intent.putExtra("cut_of_time",eachChambers.get(position).getCut_of_time());
                intent.putExtra("chamber_fee",eachChambers.get(position).getChamber_fee());
                intent.putExtra("each_patient_time",eachChambers.get(position).getEach_patient_time());
                intent.putExtra("name",name);
                intent.putExtra("degree",degree);
                intent.putExtra("image",image);
                context.startActivity(intent);
            }
        });
        return convertView;
    }
}
