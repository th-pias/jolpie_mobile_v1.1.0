package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.theuserhub.jolpie.Fragments.CategorizedFragment;
import com.theuserhub.jolpie.Fragments.DrugsFragment;
import com.theuserhub.jolpie.Fragments.GroupsFragment;
import com.theuserhub.jolpie.Fragments.HomeFragment;

import java.util.List;

/**
 * Created by Admin on 12/15/2015.
 */
public class SearchScrolledTabAdapter extends FragmentStatePagerAdapter
{
    private List<String> tabTitlesList;
    private Context context;
    public SearchScrolledTabAdapter(Context context, FragmentManager fm, List<String> tabTitlesList)
    {
        super(fm);
        this.context = context;
        this.tabTitlesList = tabTitlesList;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        return tabTitlesList.get(position);
    }

    @Override
    public int getCount()
    {
        return tabTitlesList.size();
    }

    @Override
    public Fragment getItem(int position)
    {
        //Toast.makeText(context,""+position,Toast.LENGTH_LONG).show();
        Fragment mfragment=null;
        if (position == 3){
            mfragment = new DrugsFragment();
        }
        else {
            mfragment = new CategorizedFragment();
            Bundle bundle=new Bundle();
            bundle.putInt("position",position);
            bundle.putString("tabname",tabTitlesList.get(position));
            mfragment.setArguments(bundle);
        }
        return mfragment;
    }
}
