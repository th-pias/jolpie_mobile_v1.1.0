package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Models.SimpleUserModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 11/19/2015.
 */
public class PeopleWhoLikedAdapter extends BaseAdapter {

    private Context context;
    private List<SimpleUserModel> eachItems;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public PeopleWhoLikedAdapter(Context context, List<SimpleUserModel> eachItems) {
        this.context = context;
        this.eachItems = eachItems;
    }

    @Override
    public int getCount() {
        return eachItems.size();
    }

    @Override
    public Object getItem(int position) {
        return eachItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_like_share, null);
        }

        TextView itemName = (TextView) convertView.findViewById(R.id.peoplewho_title_tv);
        if (eachItems.get(position).getUser_group_id().equals("2")) {
            itemName.setText("Dr. "+eachItems.get(position).getFirst_name() + " " + eachItems.get(position).getLast_name());
        }
        else {
            itemName.setText(eachItems.get(position).getFirst_name() + " " + eachItems.get(position).getLast_name());
        }

        ImageView peoplewho_iv = (ImageView) convertView.findViewById(R.id.peoplewho_iv);
        NetworkImageView networkImageView = (NetworkImageView) convertView.findViewById(R.id.peoplewho_niv);
        if (eachItems.get(position).getPhoto().length()>1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachItems.get(position).getPhoto();
            networkImageView.setImageUrl(meta_url, imageLoader);
        }else{
            if(eachItems.get(position).getUser_group_id().equals("1")||eachItems.get(position).getUser_group_id().equals("3")){
                peoplewho_iv.setImageResource(R.drawable.patient_student_default);
            }else if(eachItems.get(position).getUser_group_id().equals("2")){
                peoplewho_iv.setImageResource(R.drawable.doctor_default);
            }else if(eachItems.get(position).getUser_group_id().equals("4")){
                peoplewho_iv.setImageResource(R.drawable.hospital_default);
            }else if(eachItems.get(position).getUser_group_id().equals("5")){
                peoplewho_iv.setImageResource(R.drawable.labs_default);
            }else if(eachItems.get(position).getUser_group_id().equals("6")){
                peoplewho_iv.setImageResource(R.drawable.pharmaceutical_default);
            }
        }


        return convertView;
    }
}
