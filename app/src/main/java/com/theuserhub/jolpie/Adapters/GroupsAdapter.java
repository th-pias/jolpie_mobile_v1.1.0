package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Models.GroupItem;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 11/8/2015.
 */
public class GroupsAdapter extends BaseAdapter {

    private String TAG = "GroupsAdapter", tag_string_req = "create_grp_str_req";

    private Context context;
    private List<GroupItem> eachItems;
    private int flag;
    private String appUserId;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public GroupsAdapter(Context context, List<GroupItem> eachItems, int flag, String appUserId) {
        this.context = context;
        this.eachItems = eachItems;
        this.flag = flag;
        this.appUserId = appUserId;
    }

    @Override
    public int getCount() {
        return eachItems.size();
    }

    @Override
    public Object getItem(int position) {
        return eachItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null)
        {
            convertView = inflater.inflate(R.layout.list_row_newgroup, null);
        }

        NetworkImageView networkImageView = (NetworkImageView) convertView.findViewById(R.id.grp_item_niv);
        if (eachItems.get(position).getPostgroup_image().length()>1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachItems.get(position).getPostgroup_image();
            networkImageView.setImageUrl(meta_url, imageLoader);
        }

        //Typeface font = Typeface.createFromAsset( context.getAssets(), "fontawesome-webfont.ttf" );
        TextView postgroup_title = (TextView) convertView.findViewById(R.id.group_item_title_tv);
        postgroup_title.setText(eachItems.get(position).getPostgroup_name());
        //postgroup_title.setTypeface(font);

        TextView postgroup_member_count = (TextView) convertView.findViewById(R.id.number_of_member_txt);
        postgroup_member_count.setText(eachItems.get(position).getPostgroup_member_count());

        Button joinGroup = (Button) convertView.findViewById(R.id.group_join_btn);
        if (flag==3) {
            joinGroup.setVisibility(View.VISIBLE);
            postgroup_member_count.setVisibility(View.VISIBLE);
        }
        if (flag == 2){
            postgroup_member_count.setVisibility(View.VISIBLE);
        }
        joinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();
                joinGroupRequest(position);
            }
        });
        return convertView;
    }

    private void joinGroupRequest(final int position) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "joinGroup";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (response.toString().trim().equals("ok")) {
                    eachItems.remove(position);
                    notifyDataSetChanged();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("group_id", eachItems.get(position).getPostgroup_id());
                params.put("user_id", appUserId);
                params.put("status", "pending");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}