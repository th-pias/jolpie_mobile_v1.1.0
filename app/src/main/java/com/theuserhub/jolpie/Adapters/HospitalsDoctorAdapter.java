package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Models.DoctorsModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 4/11/2016.
 */
public class HospitalsDoctorAdapter extends BaseAdapter{
    private Context context;
    private List<DoctorsModel>eachDoctors;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public HospitalsDoctorAdapter(Context context, List<DoctorsModel> eachDoctors) {
        this.context = context;
        this.eachDoctors = eachDoctors;
    }

    @Override
    public int getCount() {
        return eachDoctors.size();
    }

    @Override
    public Object getItem(int position) {
        return eachDoctors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_doctors, null);
        }

        ImageView doctors_item_user_iv = (ImageView) convertView.findViewById(R.id.doctors_item_user_iv);
        NetworkImageView itemNImageView = (NetworkImageView) convertView.findViewById(R.id.doctors_item_user_niv);
        if (eachDoctors.get(position).getImage().length() > 1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachDoctors.get(position).getImage();
            itemNImageView.setImageUrl(meta_url, imageLoader);
        } else {
            if (eachDoctors.get(position).getUser_group_id().equals("1") || eachDoctors.get(position).getUser_group_id().equals("3")) {
                doctors_item_user_iv.setImageResource(R.drawable.patient_student_default);
            } else if (eachDoctors.get(position).getUser_group_id().equals("2")) {
                doctors_item_user_iv.setImageResource(R.drawable.doctor_default);
            } else if (eachDoctors.get(position).getUser_group_id().equals("4")) {
                doctors_item_user_iv.setImageResource(R.drawable.hospital_default);
            } else if (eachDoctors.get(position).getUser_group_id().equals("5")) {
                doctors_item_user_iv.setImageResource(R.drawable.labs_default);
            } else if (eachDoctors.get(position).getUser_group_id().equals("6")) {
                doctors_item_user_iv.setImageResource(R.drawable.drug_default);
            } else if (eachDoctors.get(position).getUser_group_id().equals("7")) {
                doctors_item_user_iv.setImageResource(R.drawable.group_default_new);
            }
        }

        TextView itemName = (TextView) convertView.findViewById(R.id.doctors_item_ttl_tv);
        if (eachDoctors.get(position).getUser_group_id().equals("2")) {
            itemName.setText("Dr. " + eachDoctors.get(position).getFirst_name() + " " + eachDoctors.get(position).getLast_name());
        } else if (eachDoctors.get(position).getUser_group_id().equals("6")) {
            itemName.setText(eachDoctors.get(position).getFirst_name());
        } else {
            itemName.setText(eachDoctors.get(position).getFirst_name() + " " + eachDoctors.get(position).getLast_name());
        }

        TextView itemDegreeType = (TextView) convertView.findViewById(R.id.doctors_item_degree_tv);
        if (eachDoctors.get(position).getUser_group_id().equals("2")) {
            itemDegreeType.setText(eachDoctors.get(position).getDegree());
        }

        TextView itemCompanyName = (TextView) convertView.findViewById(R.id.doctors_item_speciality_tv);

        if (eachDoctors.get(position).getUser_group_id().equals("6")) {
            itemCompanyName.setText(eachDoctors.get(position).getDegree());
        } else {
            List<SpecialityModel> specialityList = eachDoctors.get(position).getSpecialityModels();

            String specialities = "";
            for (int c = 0; c < specialityList.size(); ++c) {
                if (c != 0) {
                    specialities = specialities + ", ";
                }
                specialities = specialities + specialityList.get(c).getName();
            }
            itemCompanyName.setText(specialities);
        }
        return convertView;
    }
}
