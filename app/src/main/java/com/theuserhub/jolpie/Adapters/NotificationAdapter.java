package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Models.NotificationModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 2/24/2016.
 */
public class NotificationAdapter extends BaseAdapter{
    private Context context;
    private List<NotificationModel> eachNotify;
    private String appUserName, appUserId;
    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public NotificationAdapter(Context context, List<NotificationModel> eachNotify, String appUserId, String appUserName) {
        this.context = context;
        this.eachNotify = eachNotify;
        this.appUserId = appUserId;
        this.appUserName = appUserName;
    }

    @Override
    public int getCount() {
        return eachNotify.size();
    }

    @Override
    public Object getItem(int position) {
        return eachNotify.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null)
        {
            convertView = inflater.inflate(R.layout.list_row_notification, null);
        }

        ImageView itemImageView = (ImageView) convertView.findViewById(R.id.notify_item_iv);
        NetworkImageView itemNImageView = (NetworkImageView) convertView.findViewById(R.id.notify_item_niv);

        if (eachNotify.get(position).getActivity_by_photo().length()>1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();
            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachNotify.get(position).getActivity_by_photo();
            itemNImageView.setImageUrl(meta_url, imageLoader);
        }
        else{
            if(eachNotify.get(position).getActivity_by_group_id().equals("1")||eachNotify.get(position).getActivity_by_group_id().equals("3")){
                itemImageView.setImageResource(R.drawable.patient_student_default);
            }else if(eachNotify.get(position).getActivity_by_group_id().equals("2")){
                itemImageView.setImageResource(R.drawable.doctor_default);
            }else if(eachNotify.get(position).getActivity_by_group_id().equals("4")){
                itemImageView.setImageResource(R.drawable.hospital_default);
            }else if(eachNotify.get(position).getActivity_by_group_id().equals("5")){
                itemImageView.setImageResource(R.drawable.labs_default);
            }else if(eachNotify.get(position).getActivity_by_group_id().equals("6")){
                itemImageView.setImageResource(R.drawable.pharmaceutical_default);
            }
        }
        String activity_by_user_name = "";
        if (eachNotify.get(position).getActivity_by_group_id().equals("2")) {
            activity_by_user_name = "Dr. "+ eachNotify.get(position).getActivity_by_fname() + " " + eachNotify.get(position).getActivity_by_lname();
        }
        else {
            activity_by_user_name = eachNotify.get(position).getActivity_by_fname() + " " + eachNotify.get(position).getActivity_by_lname();
        }
        TextView activityTextView = (TextView) convertView.findViewById(R.id.nitify_tv);

        if (eachNotify.get(position).getSection().equals("post")) {
            if (eachNotify.get(position).getActivity().equals("post")) {

            }
            else if (eachNotify.get(position).getActivity().equals("liked")) {

                String likedString = activity_by_user_name+" liked your Post";

                StyleSpan textBoldStyle = new StyleSpan(android.graphics.Typeface.BOLD);

                Spannable wordtoSpan = new SpannableString(likedString);
                wordtoSpan.setSpan(textBoldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                activityTextView.setText(wordtoSpan);

            }
            else if (eachNotify.get(position).getActivity().equals("commented")) {

                String commentedString = activity_by_user_name+" commented on your Post";

                StyleSpan textBoldStyle = new StyleSpan(android.graphics.Typeface.BOLD);

                Spannable wordtoSpan = new SpannableString(commentedString);
                wordtoSpan.setSpan(textBoldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                activityTextView.setText(wordtoSpan);

            }
            else if (eachNotify.get(position).getActivity().equals("shared")) {

                String sharedString = activity_by_user_name+" shared your Post";

                StyleSpan textBoldStyle = new StyleSpan(android.graphics.Typeface.BOLD);

                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                activityTextView.setText(wordtoSpan);

            }
            else if (eachNotify.get(position).getActivity().equals("taged")) {
            }
        }
        else if (eachNotify.get(position).getSection().equals("group")) {
            if (eachNotify.get(position).getActivity().equals("post")) {
                String sharedString = activity_by_user_name+" started a discussion";//+post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);

                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                activityTextView.setText(wordtoSpan);
            }
            else if (eachNotify.get(position).getActivity().equals("liked")) {
                String sharedString = activity_by_user_name+" liked a discussion";//+post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);

                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                activityTextView.setText(wordtoSpan);
            }
            else if (eachNotify.get(position).getActivity().equals("commented")) {
                String sharedString = activity_by_user_name+" commented to a discussion";//+post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                activityTextView.setText(wordtoSpan);
            }
            else if (eachNotify.get(position).getActivity().equals("shared")) {
                String sharedString = activity_by_user_name+" shared a discussion";//+post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                activityTextView.setText(wordtoSpan);
            }
            else if (eachNotify.get(position).getActivity().equals("taged")) {
            }
        }
        else if (eachNotify.get(position).getSection().equals("magazine")) {
            if (eachNotify.get(position).getActivity().equals("post")) {
                String postedString = activity_by_user_name+" posted an article";//+post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                Spannable wordtoSpan = new SpannableString(postedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                activityTextView.setText(wordtoSpan);
            }
            else if (eachNotify.get(position).getActivity().equals("liked")) {
                String likedString = activity_by_user_name+" liked on an article";//+post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                Spannable wordtoSpan = new SpannableString(likedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                activityTextView.setText(wordtoSpan);
            }
            else if (eachNotify.get(position).getActivity().equals("commented")) {
                String commentedString = activity_by_user_name+" commented on an article";//+post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                Spannable wordtoSpan = new SpannableString(commentedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                activityTextView.setText(wordtoSpan);
            }
            else if (eachNotify.get(position).getActivity().equals("shared")) {
                String sharedString = activity_by_user_name+" shared on an article";//+post_to_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                activityTextView.setText(wordtoSpan);
            }
            else if (eachNotify.get(position).getActivity().equals("taged")) {
            }
        }
        /*else if (eachNotify.get(position).getSection().equals("review")) {
            if (eachNotify.get(position).getActivity().equals("reviewed")) {
                String sharedString = post_to_user_name+" was reviewed by "+activity_by_user_name;

                StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                Spannable wordtoSpan = new SpannableString(sharedString);
                wordtoSpan.setSpan(textBoldStyle1, 0, (post_to_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                activityTextView.setText(wordtoSpan);
            }
        }*/

        TextView itemDate = (TextView) convertView.findViewById(R.id.nitify_at_tv);
        itemDate.setText(eachNotify.get(position).getCreated_at());

        return convertView;
    }
}
