package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Models.CommentModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 11/17/2015.
 */
public class CommentListAdapter extends BaseAdapter{
    private Context context;
    private List<CommentModel>commentList;
    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public CommentListAdapter(Context context, List<CommentModel> commentList) {
        this.context = context;
        this.commentList = commentList;
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null)
        {
            convertView = inflater.inflate(R.layout.list_row_comment, null);
        }

        NetworkImageView item_user_niv = (NetworkImageView) convertView.findViewById(R.id.item_user_niv);
        ImageView userimageofcomment = (ImageView) convertView.findViewById(R.id.userimageofcomment);
        Log.e("image",commentList.get(position).getUserFname()+" : "+commentList.get(position).getUserPhoto());
        if (commentList.get(position).getUserPhoto().length()>1){
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + commentList.get(position).getUserPhoto();
            item_user_niv.setImageUrl(meta_url, imageLoader);

        }else{

            if(commentList.get(position).getUserGroupId().equals("1")||commentList.get(position).getUserGroupId().equals("3")){
                userimageofcomment.setImageResource(R.drawable.patient_student_default);
            }else if(commentList.get(position).getUserGroupId().equals("2")){
                userimageofcomment.setImageResource(R.drawable.doctor_default);
            }else if(commentList.get(position).getUserGroupId().equals("4")){
                userimageofcomment.setImageResource(R.drawable.hospital_default);
            }else if(commentList.get(position).getUserGroupId().equals("5")){
                userimageofcomment.setImageResource(R.drawable.labs_default);
            }else if(commentList.get(position).getUserGroupId().equals("6")){
                userimageofcomment.setImageResource(R.drawable.pharmaceutical_default);
            }
        }

        TextView userNameOfComment= (TextView) convertView.findViewById(R.id.usernameofcomment);
        if (commentList.get(position).getUserGroupId().equals("2")) {
            userNameOfComment.setText("Dr. "+commentList.get(position).getUserFname() + " " + commentList.get(position).getUserLname());
        }
        else {
            userNameOfComment.setText(commentList.get(position).getUserFname() + " " + commentList.get(position).getUserLname());
        }
        userNameOfComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ProfileDetailNewActivity.class);
                intent.putExtra("user_id",commentList.get(position).getUserIdOfComment());
                intent.putExtra("user_fname",commentList.get(position).getUserFname());
                intent.putExtra("user_lname",commentList.get(position).getUserLname());
                intent.putExtra("user_email","");
                intent.putExtra("group_id",commentList.get(position).getUserGroupId());
                intent.putExtra("user_image_url",commentList.get(position).getUserPhoto());
                context.startActivity(intent);
            }
        });

        TextView commentDetails=(TextView) convertView.findViewById(R.id.comment_tv);
        commentDetails.setText(commentList.get(position).getCommentdetails());

        TextView commentDate=(TextView) convertView.findViewById(R.id.commentdate);
        commentDate.setText(commentList.get(position).getCommentDate());

        TextView commentLikeCountTV=(TextView) convertView.findViewById(R.id.comment_like_count_textview);

        ImageView commentLikeImageview= (ImageView) convertView.findViewById(R.id.comment_like_imageview);
        //commentLikeImageview.setImageResource(R.mipmap.ic_like);
        return convertView;
    }
}
