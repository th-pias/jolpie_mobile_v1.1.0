package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.widget.Toast;

import com.theuserhub.jolpie.Fragments.CategorizedFragment;
import com.theuserhub.jolpie.Fragments.DoctorsFragment;
import com.theuserhub.jolpie.Fragments.GroupsFragment;
import com.theuserhub.jolpie.Fragments.HomeFragment;
import com.theuserhub.jolpie.Fragments.InfoFragment;

import java.util.List;

/**
 * Created by Admin on 11/9/2015.
 */
public class ProfileScrolledTab_Adapter extends FragmentPagerAdapter
{
    List<String> tabTitlesList;
    Context context;
    public ProfileScrolledTab_Adapter(Context context, FragmentManager fm, List<String> tabTitlesList)
    {
        super(fm);
        this.context = context;
        this.tabTitlesList = tabTitlesList;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        return tabTitlesList.get(position);
    }

    @Override
    public int getCount()
    {
        return tabTitlesList.size();
    }

    @Override
    public Fragment getItem(int position)
    {
        Toast.makeText(context, "" + position, Toast.LENGTH_LONG).show();
        Fragment mfragment;
            mfragment = new InfoFragment();
        return mfragment;
    }
}
