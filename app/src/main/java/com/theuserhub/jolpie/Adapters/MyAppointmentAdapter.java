package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Activity.MyAppointmentCancelActivity;
import com.theuserhub.jolpie.Activity.PrescriptionActivity;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Models.MyAppointmentModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 2/20/2016.
 */
public class MyAppointmentAdapter extends BaseAdapter{

    private Context context;
    private List<MyAppointmentModel>eachAppointments;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public MyAppointmentAdapter(Context context, List<MyAppointmentModel> eachAppointments) {
        this.context = context;
        this.eachAppointments = eachAppointments;
    }

    @Override
    public int getCount() {
        return eachAppointments.size();
    }

    @Override
    public Object getItem(int position) {
        return eachAppointments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null)
        {
            convertView = inflater.inflate(R.layout.list_row_my_appointment, null);
        }

        Typeface appotFont = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");

        TextView prescribeButton = (TextView) convertView.findViewById(R.id.appointment_prescribe_btn);
        prescribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PrescriptionActivity.class);
                intent.putExtra("patient_id",eachAppointments.get(position).getUser_id());
                intent.putExtra("appointment_id",eachAppointments.get(position).getId());
                context.startActivity(intent);
            }
        });

        TextView cancelButton = (TextView) convertView.findViewById(R.id.appointment_cancel_btn);
        cancelButton.setTypeface(appotFont);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, MyAppointmentCancelActivity.class);
                i.putExtra("appointment_id",eachAppointments.get(position).getId());
                context.startActivity(i);
                /*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Are you sure?");
                alertDialogBuilder.setMessage("You wanted to cancel your appointment");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });

                alertDialogBuilder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();*/
            }
        });

        NetworkImageView networkImageView = (NetworkImageView) convertView.findViewById(R.id.my_appot_item_niv);
        ImageView my_appot_item_iv = (ImageView) convertView.findViewById(R.id.my_appot_item_iv);

        if (eachAppointments.get(position).getPhoto().length()>1){
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachAppointments.get(position).getPhoto();
            networkImageView.setImageUrl(meta_url, imageLoader);
        }else{
            if(eachAppointments.get(position).getUser_group_id().equals("1")||eachAppointments.get(position).getUser_group_id().equals("3")){
                my_appot_item_iv.setImageResource(R.drawable.patient_student_default);
            }else if(eachAppointments.get(position).getUser_group_id().equals("2")){
                my_appot_item_iv.setImageResource(R.drawable.doctor_default);
            }else if(eachAppointments.get(position).getUser_group_id().equals("4")){
                my_appot_item_iv.setImageResource(R.drawable.hospital_default);
            }else if(eachAppointments.get(position).getUser_group_id().equals("5")){
                my_appot_item_iv.setImageResource(R.drawable.labs_default);
            }else if(eachAppointments.get(position).getUser_group_id().equals("6")){
                my_appot_item_iv.setImageResource(R.drawable.pharmaceutical_default);
            }
        }

        TextView nameTextView = (TextView) convertView.findViewById(R.id.name_tv);
        TextView dateTextView = (TextView) convertView.findViewById(R.id.date_tv);

        if (eachAppointments.get(position).getUser_group_id().equals("2")) {
            nameTextView.setText("Dr. "+eachAppointments.get(position).getFirst_name() + " " + eachAppointments.get(position).getLast_name());
        }
        else {
            nameTextView.setText(eachAppointments.get(position).getFirst_name() + " " + eachAppointments.get(position).getLast_name());
        }
        nameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileDetailNewActivity.class);
                intent.putExtra("user_id", eachAppointments.get(position).getUser_id());
                intent.putExtra("user_fname", eachAppointments.get(position).getFirst_name());
                intent.putExtra("user_lname", eachAppointments.get(position).getLast_name());
                intent.putExtra("user_email", "");
                intent.putExtra("group_id", eachAppointments.get(position).getUser_group_id());
                intent.putExtra("user_image_url", eachAppointments.get(position).getPhoto());
                context.startActivity(intent);
            }
        });

        dateTextView.setText(eachAppointments.get(position).getAppointment_time());

        return convertView;
    }
}
