package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Activity.GalleryActivity;
import com.theuserhub.jolpie.Models.Album;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by tumpa on 2/24/2016.
 */
public class GalleryAdapter extends BaseAdapter {
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private Context context;
    private List<Album> albumList;
    private static LayoutInflater inflater=null;

    public GalleryAdapter(GalleryActivity context,List<Album> albumList){
        this.context = context;
        this.albumList = albumList;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return albumList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class Holder
    {
        TextView name;
        NetworkImageView galleryFolderImg;
        ImageView img_default;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View contentView = null;
        Album album = albumList.get(position);
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + album.getPicture();

        /*holder.img_default = (ImageView) contentView.findViewById(R.id.img_default);
        if(album.getPicture().length()>0){
            holder.img_default.setVisibility(View.INVISIBLE);
        }*/

        contentView = inflater.inflate(R.layout.list_row_gallery,null);
        holder.name = (TextView) contentView.findViewById(R.id.image_folder_name_tv);
        holder.galleryFolderImg = (NetworkImageView) contentView.findViewById(R.id.image_folder_img_view);

        holder.name.setText(album.getAlbum_name());
        holder.galleryFolderImg.setImageUrl(meta_url,imageLoader);


        return contentView;
    }
}
