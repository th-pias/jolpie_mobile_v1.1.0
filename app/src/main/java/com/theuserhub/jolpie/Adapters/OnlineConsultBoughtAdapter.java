package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Models.OnlineConsultLogModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 4/4/2016.
 */
public class OnlineConsultBoughtAdapter extends BaseAdapter{
    private Context context;
    private List<OnlineConsultLogModel>eachLog;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public OnlineConsultBoughtAdapter(Context context, List<OnlineConsultLogModel> eachLog) {
        this.context = context;
        this.eachLog = eachLog;
    }

    @Override
    public int getCount() {
        return eachLog.size();
    }

    @Override
    public Object getItem(int position) {
        return eachLog.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null)
        {
            convertView = inflater.inflate(R.layout.list_row_online_consult_log, null);
        }

        ImageView itemImageView = (ImageView) convertView.findViewById(R.id.imageView2);
        NetworkImageView itemNImageView = (NetworkImageView) convertView.findViewById(R.id.nimageView2);

        TextView nameTextView = (TextView) convertView.findViewById(R.id.textView51);
        TextView degreeTextView = (TextView) convertView.findViewById(R.id.textView52);
        TextView packageTextView = (TextView) convertView.findViewById(R.id.textView53);
        TextView priceTextView = (TextView) convertView.findViewById(R.id.price_tv);
        TextView textMsgTextView = (TextView) convertView.findViewById(R.id.textView54);
        TextView audioTextView = (TextView) convertView.findViewById(R.id.textView55);
        TextView videoTextView = (TextView) convertView.findViewById(R.id.textView56);
        TextView validityTextView = (TextView) convertView.findViewById(R.id.textView57);
        TextView purchaseTextView = (TextView) convertView.findViewById(R.id.textView58);
        TextView expiresTextView = (TextView) convertView.findViewById(R.id.textView59);
        TextView statusTextView = (TextView) convertView.findViewById(R.id.textView60);

        if (eachLog.get(position).getPhoto().length() > 1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachLog.get(position).getPhoto();
            itemNImageView.setImageUrl(meta_url, imageLoader);
        } else {
            if (eachLog.get(position).getUser_group_id().equals("1") || eachLog.get(position).getUser_group_id().equals("3")) {
                itemImageView.setImageResource(R.drawable.patient_student_default);
            } else if (eachLog.get(position).getUser_group_id().equals("2")) {
                itemImageView.setImageResource(R.drawable.doctor_default);
            } else if (eachLog.get(position).getUser_group_id().equals("4")) {
                itemImageView.setImageResource(R.drawable.hospital_default);
            } else if (eachLog.get(position).getUser_group_id().equals("5")) {
                itemImageView.setImageResource(R.drawable.labs_default);
            } else if (eachLog.get(position).getUser_group_id().equals("6")) {
                itemImageView.setImageResource(R.drawable.drug_default);
            } else if (eachLog.get(position).getUser_group_id().equals("7")) {
                itemImageView.setImageResource(R.drawable.group_default_new);
            }
        }

        if (eachLog.get(position).getUser_group_id().equals("2")) {
            nameTextView.setText("Dr. " + eachLog.get(position).getFirst_name() + " " + eachLog.get(position).getLast_name());
        }
        else {
            nameTextView.setText(eachLog.get(position).getFirst_name()+" "+eachLog.get(position).getLast_name());
        }

        if (eachLog.get(position).getDegree().length()>1) {
            degreeTextView.setText(eachLog.get(position).getDegree());
        }
        else {
            degreeTextView.setVisibility(View.GONE);
        }
        packageTextView.setText(eachLog.get(position).getPackage_name());
        priceTextView.setText("BDT "+eachLog.get(position).getPrice());
        textMsgTextView.setText(eachLog.get(position).getText());
        audioTextView.setText(eachLog.get(position).getAudio());
        videoTextView.setText(eachLog.get(position).getVideo());
        validityTextView.setText(eachLog.get(position).getDuration()+" Days");
        purchaseTextView.setText(eachLog.get(position).getTran_date());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date purchaseDate = new Date();
        try {
            purchaseDate = dateFormat.parse(eachLog.get(position).getTran_date());

            Calendar calendar = Calendar.getInstance();
            Date todayDate = calendar.getTime();
            String todayString = dateFormat.format(todayDate);
            todayDate = dateFormat.parse(todayString);

            calendar.setTime(purchaseDate);
            calendar.add(Calendar.DAY_OF_YEAR, Integer.parseInt(eachLog.get(position).getDuration()));
            Date expireDate = calendar.getTime();
            String expireString = dateFormat.format(expireDate);
            expireDate = dateFormat.parse(expireString);

            expiresTextView.setText(expireString);

            if (todayDate.after(expireDate)) {
                statusTextView.setText("Expired");
            }
            else {
                statusTextView.setText("Active");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
