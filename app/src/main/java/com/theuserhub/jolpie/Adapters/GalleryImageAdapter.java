package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Activity.GalleryImageActivity;
import com.theuserhub.jolpie.Activity.GalleryImageFullActivity;
import com.theuserhub.jolpie.Models.AlbumImage;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by tumpa on 2/24/2016.
 */
public class GalleryImageAdapter extends BaseAdapter {

    private String [] prgmNameList;
    private Context context;
    private static LayoutInflater inflater=null;
    private List<AlbumImage> albumImagesList;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public GalleryImageAdapter(GalleryImageActivity context,List<AlbumImage> albumImagesList){
        this.context = context;
        this.albumImagesList = albumImagesList;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return albumImagesList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView imgName;
        NetworkImageView Img;
        LinearLayout linearLayout;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View contentView = null;
        AlbumImage albumImage;
        albumImage = albumImagesList.get(position);
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + albumImage.getPicture();

        contentView = inflater.inflate(R.layout.list_row_image_gallary,null);
        holder.imgName = (TextView) contentView.findViewById(R.id.image_name_tv);

        holder.Img = (NetworkImageView) contentView.findViewById(R.id.image_img_view);

        holder.linearLayout = (LinearLayout) contentView.findViewById(R.id.gallery_linear_layout);

        holder.imgName.setText(albumImage.getPicture_caption());


        holder.Img.setImageUrl(meta_url, imageLoader);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, GalleryImageFullActivity.class);
                i.putExtra("image",albumImagesList.get(position).getPicture().toString());
                i.putExtra("flag","2");
                context.startActivity(i);
            }
        });

        return contentView;
    }
}
