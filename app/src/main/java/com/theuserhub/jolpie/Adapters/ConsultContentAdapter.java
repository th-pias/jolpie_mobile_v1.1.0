package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.theuserhub.jolpie.Activity.ConsultPackagesActivity;
import com.theuserhub.jolpie.Models.ConsultContentData;
import com.theuserhub.jolpie.R;

import java.util.List;

/**
 * Created by tumpa on 2/23/2016.
 */


public class ConsultContentAdapter extends BaseAdapter {
    private List <ConsultContentData> consultContentDatas;
    private static LayoutInflater inflater=null;
    private Context context;
    String [] prgmNameList;
    private String appUserId;

    public ConsultContentAdapter(Context context,List<ConsultContentData> consultContentDatas, String appUserId){
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.prgmNameList = prgmNameList;
        this.consultContentDatas = consultContentDatas;
        this.appUserId = appUserId;
    }
    @Override
    public int getCount() {
        return consultContentDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView available,name,price,duration,audioCount,videoCount,textMgsCount,addCart;
        LinearLayout textMsgLayout, audioCallLayout, videoCallLayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View contentView;

        contentView = inflater.inflate(R.layout.list_row_consult_packages, null);
        holder.available = (TextView) contentView.findViewById(R.id.available_tv);
        holder.name=(TextView) contentView.findViewById(R.id.name);
        holder.price = (TextView) contentView.findViewById(R.id.price);
        holder.textMsgLayout = (LinearLayout) contentView.findViewById(R.id.text_msg_layout);
        holder.textMgsCount = (TextView) contentView.findViewById(R.id.text_msg_count_tv);
        holder.audioCallLayout = (LinearLayout) contentView.findViewById(R.id.audio_call_layout);
        holder.audioCount = (TextView) contentView.findViewById(R.id.audio_count_tv);
        holder.videoCallLayout = (LinearLayout) contentView.findViewById(R.id.video_call_layout);
        holder.videoCount = (TextView) contentView.findViewById(R.id.video_count_tv);
        holder.duration=(TextView) contentView.findViewById(R.id.duration);

        holder.addCart = (TextView) contentView.findViewById(R.id.add_cart_img_btn);

        ConsultContentData ccData = consultContentDatas.get(position);

        if (position == 0) {
            holder.available.setVisibility(View.VISIBLE);
            if (appUserId.equals(ccData.getUser_id())) {
                holder.available.setText("My Packages");
            }
            else {
                holder.available.setText("Available Packages");
            }
        }
        else {
            holder.available.setVisibility(View.GONE);
        }

        holder.name.setText(ccData.getPackage_name());
        //holder.audioVideo.setText("Audio:" + ccData.getAudio() + "\nVideo:" + ccData.getVideo() + "\nText" + ccData.getText());
        if (ccData.getText().trim().equalsIgnoreCase("0")) {
            holder.textMsgLayout.setVisibility(View.GONE);
        }
        else {
            holder.textMgsCount.setText(ccData.getText());
        }
        if (ccData.getAudio().trim().equalsIgnoreCase("0")) {
            holder.audioCallLayout.setVisibility(View.GONE);
        }
        else {
            holder.audioCount.setText(ccData.getAudio());
        }
        if (ccData.getVideo().trim().equalsIgnoreCase("0")) {
            holder.videoCallLayout.setVisibility(View.GONE);
        }
        else {
            holder.videoCount.setText(ccData.getVideo());
        }
        holder.duration.setText(ccData.getDuration()+ " Days");
        holder.price.setText("BDT "+ccData.getPrice());

        if (appUserId.equals(ccData.getUser_id())) {
            holder.addCart.setVisibility(View.GONE);
        }
        else {
            holder.addCart.setVisibility(View.VISIBLE);
        }
        holder.addCart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Toast.makeText(context, "You Clicked addToCart", Toast.LENGTH_LONG).show();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getResources().getString(R.string.HOME_URL)));
                context.startActivity(browserIntent);
            }
        });




        return contentView;
    }
}
