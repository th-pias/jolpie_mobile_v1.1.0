package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.theuserhub.jolpie.Fragments.CareAtHomeFragment;
import com.theuserhub.jolpie.Fragments.CategorizedFragment;
import com.theuserhub.jolpie.Fragments.DoctorsFragment;
import com.theuserhub.jolpie.Fragments.DrugsFragment;
import com.theuserhub.jolpie.Fragments.GroupsFragment;
import com.theuserhub.jolpie.Fragments.HomeFragment;
import com.theuserhub.jolpie.Fragments.MagazineFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 10/5/2015.
 */
public class ScrolledTab_Adapter extends FragmentStatePagerAdapter {
    List<String> tabTitlesList;
    Context context;

    public ScrolledTab_Adapter(Context context, FragmentManager fm, List<String> tabTitlesList) {
        super(fm);
        this.context = context;
        this.tabTitlesList = tabTitlesList;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitlesList.get(position);
    }

    @Override
    public int getCount() {
        return tabTitlesList.size();
    }

    @Override
    public Fragment getItem(int position) {
        //Toast.makeText(context,""+position,Toast.LENGTH_LONG).show();
        Fragment mfragment = null;
        if (position == 0) {
            mfragment = new HomeFragment();
        } else if (position == 1) {
            mfragment = new GroupsFragment();
        } else if (position == 2) {
            mfragment = new MagazineFragment();
        } else if (position == 3) {
            mfragment = new CareAtHomeFragment();
        }
        /*else if (position == 1) {
            mfragment = new DrugsFragment();
        }else {
            mfragment = new CategorizedFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            bundle.putString("tabname", tabTitlesList.get(position));
            mfragment.setArguments(bundle);
        }*/
        return mfragment;
    }
}
