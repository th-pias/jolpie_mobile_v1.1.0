package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Models.ArticlesModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 1/30/2016.
 */
public class MagazineListActivityAdapter extends BaseAdapter{

    private Context context;
    private List<ArticlesModel> eachArticles;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public MagazineListActivityAdapter(Context context, List<ArticlesModel> eachArticles) {
        this.context = context;
        this.eachArticles = eachArticles;
    }

    @Override
    public int getCount() {
        return eachArticles.size();
    }

    @Override
    public Object getItem(int position) {
        return eachArticles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_magazine_activity, null);
        }

        RelativeLayout imageLayout = (RelativeLayout) convertView.findViewById(R.id.magazine_iv_layout);
        ImageView articleImageView = (ImageView) convertView.findViewById(R.id.magazine_iv);
        NetworkImageView articleNImageView = (NetworkImageView) convertView.findViewById(R.id.magazine_niv);
        TextView titleTextView = (TextView) convertView.findViewById(R.id.magazine_title_tv);
        TextView authorTextView = (TextView) convertView.findViewById(R.id.magazine_author_tv);

        titleTextView.setText(eachArticles.get(position).getTitle());
        authorTextView.setText("By: "+eachArticles.get(position).getAuthor_fname()+" "+eachArticles.get(position).getAuthor_lname());

        if (eachArticles.get(position).getPicture().length()>1) {
            imageLayout.setVisibility(View.VISIBLE);
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachArticles.get(position).getPicture();
            articleNImageView.setImageUrl(meta_url, imageLoader);
        }
        else {
            imageLayout.setVisibility(View.GONE);
        }
        return convertView;
    }
}
