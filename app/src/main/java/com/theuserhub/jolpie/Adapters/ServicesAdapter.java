package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.theuserhub.jolpie.Models.ServiceModel;
import com.theuserhub.jolpie.R;

import java.util.List;

/**
 * Created by tumpa on 3/2/2016.
 */
public class ServicesAdapter extends BaseAdapter {

    List<ServiceModel> serviceArrayList;
    private Context context;
    private static LayoutInflater inflater=null;


    public ServicesAdapter(Context context,List<ServiceModel> serviceArrayList){
        this.context = context;
        this.serviceArrayList = serviceArrayList;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return serviceArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class Holder
    {
        TextView category_name;
        TextView service_name;
        TextView service_price;
        Button edit;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_view_services,null);
        holder.category_name = (TextView) rowView.findViewById(R.id.category_name_tv);
        holder.service_name = (TextView) rowView.findViewById(R.id.service_name_tv); ;
        holder.service_price = (TextView) rowView.findViewById(R.id.service_price_tv); ;
        holder.edit = (Button) rowView.findViewById(R.id.edit);

        ServiceModel serviceModel = serviceArrayList.get(position);
        if (serviceModel.getCategory_id().equals("0")) {
            holder.category_name.setText("Package name: "+serviceModel.getCategory_name().toString());
            holder.service_name.setText("Services name: "+serviceModel.getService_name().toString());
        }
        else {
            holder.category_name.setText("Category name: "+serviceModel.getCategory_name().toString());
            holder.service_name.setText("Service name: "+serviceModel.getService_name().toString());
        }
        holder.service_price.setText(serviceModel.getService_price().toString()+" Tk");

        return rowView;
    }
}
