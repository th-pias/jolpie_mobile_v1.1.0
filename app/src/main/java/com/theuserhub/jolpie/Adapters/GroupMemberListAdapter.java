package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.ProfileDetailNewActivity;
import com.theuserhub.jolpie.Models.GroupMemberModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 1/17/2016.
 */
public class GroupMemberListAdapter extends BaseAdapter{

    private String TAG = "GroupMemberListActivity",tag_string_req = "grp_membr_str_req";
    private Context context;
    private List<GroupMemberModel>eachItems;
    private String appUserId,created_by,group_id;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private LayoutInflater inflater;

    public GroupMemberListAdapter(Context context, List<GroupMemberModel> eachItems, String appUserId, String created_by, String group_id) {
        this.context = context;
        this.eachItems = eachItems;
        this.appUserId = appUserId;
        this.created_by = created_by;
        this.group_id = group_id;
    }

    @Override
    public int getCount() {
        return eachItems.size();
    }

    @Override
    public Object getItem(int position) {
        return eachItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_grp_membr_list, null);
        }
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        TextView nameTextView = (TextView) convertView.findViewById(R.id.grp_membr_name_tv);

        if(eachItems.get(position).getUser_group_id().equals("2")){
            nameTextView.setText("Dr. "+eachItems.get(position).getFirst_name()+" "+eachItems.get(position).getLast_name());
        }else {
            nameTextView.setText(eachItems.get(position).getFirst_name()+" "+eachItems.get(position).getLast_name());
        }

        Typeface memberFont = Typeface.createFromAsset( context.getAssets(), "fontawesome-webfont.ttf" );
        TextView dltTextView = (TextView) convertView.findViewById(R.id.grp_membr_delete_tv);
        dltTextView.setTypeface(memberFont);
        RelativeLayout groupMemberListRL = (RelativeLayout) convertView.findViewById(R.id.grp_member_list_r_l);

        NetworkImageView image_img_view = (NetworkImageView) convertView.findViewById(R.id.image_img_view);
        ImageView grp_membr_iv = (ImageView) convertView.findViewById(R.id.grp_membr_iv);

        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachItems.get(position).getImage();

        if(eachItems.get(position).getImage().length()>0){
            image_img_view.setImageUrl(meta_url,imageLoader);
        }else{
            if(eachItems.get(position).getUser_group_id().equals("1")||eachItems.get(position).getUser_group_id().equals("3")){
                grp_membr_iv.setImageResource(R.drawable.patient_student_default);
            }else if(eachItems.get(position).getUser_group_id().equals("2")){
                grp_membr_iv.setImageResource(R.drawable.doctor_default);
            }else if(eachItems.get(position).getUser_group_id().equals("4")){
                grp_membr_iv.setImageResource(R.drawable.hospital_default);
            }else if(eachItems.get(position).getUser_group_id().equals("5")){
                grp_membr_iv.setImageResource(R.drawable.labs_default);
            }else if(eachItems.get(position).getUser_group_id().equals("6")){
                grp_membr_iv.setImageResource(R.drawable.pharmaceutical_default);
            }
        }


        if (appUserId.equals(created_by)) {
            dltTextView.setVisibility(View.VISIBLE);
        }
        else {
            dltTextView.setVisibility(View.GONE);
        }
        dltTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                groupLeaveRequest(position);
            }
        });
        groupMemberListRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProfileDetailNewActivity.class);
                i.putExtra("user_id",eachItems.get(position).getUser_id());
                i.putExtra("user_fname",eachItems.get(position).getFirst_name());
                i.putExtra("user_lname",eachItems.get(position).getUser_id());
                i.putExtra("user_email","");
                i.putExtra("group_id",eachItems.get(position).getUser_group_id());
                i.putExtra("user_image_url",eachItems.get(position).getImage());
                context.startActivity(i);
            }
        });
        return convertView;
    }

    private void groupLeaveRequest(final int position) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "leaveGroup";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("isLeaved", response.toString());
                if (response.toString().trim().equals("1")) {
                    Toast.makeText(context, "Member removed successfully", Toast.LENGTH_SHORT).show();
                    eachItems.remove(position);
                    notifyDataSetChanged();
                }
                else {
                    Toast.makeText(context, "Not deleted", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d("isLeaved", "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", eachItems.get(position).getUser_id());
                params.put("group_id", group_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
