package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Models.GivenPrescriptionModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by tumpa on 3/13/2016.
 */
public class PrescriptionAdapter extends BaseAdapter {
    private Context context;
    private static LayoutInflater inflater=null;
    private List<GivenPrescriptionModel> PrescriptionList;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public PrescriptionAdapter(Context context,List<GivenPrescriptionModel> PrescriptionList){
        this.context = context;
        this.PrescriptionList = PrescriptionList;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return PrescriptionList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class Holder
    {
        TextView Name;
        NetworkImageView Img;
        ImageView imgView;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View contentView = null;
        GivenPrescriptionModel givenPrescriptionModel;
        givenPrescriptionModel = PrescriptionList.get(position);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + givenPrescriptionModel.getPhoto();

        contentView = inflater.inflate(R.layout.list_row_prescription,null);

        holder.Name = (TextView) contentView.findViewById(R.id.name);
        holder.Img = (NetworkImageView) contentView.findViewById(R.id.image_img_view);
        holder.imgView = (ImageView) contentView.findViewById(R.id.image_view_patient);

        if (givenPrescriptionModel.getUser_group_id().equals("2")) {
            holder.Name.setText("Dr. " + givenPrescriptionModel.getFirst_name() + " " + givenPrescriptionModel.getLast_name());
        }
        else {
            holder.Name.setText(givenPrescriptionModel.getFirst_name() + " " + givenPrescriptionModel.getLast_name());
        }

        holder.Img.setImageUrl(meta_url, imageLoader);
        return contentView;
    }
}
