package com.theuserhub.jolpie.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.theuserhub.jolpie.Models.MyConditionsModel;
import com.theuserhub.jolpie.Models.SavedDoctorsModel;
import com.theuserhub.jolpie.Models.SpecialityModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.List;

/**
 * Created by Admin on 2/1/2016.
 */
public class SavedDoctorsFragmentAdapter extends BaseAdapter{

    private Context context;
    private List<SavedDoctorsModel> eachDoctors;

    private LayoutInflater inflater;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public SavedDoctorsFragmentAdapter(Context context, List<SavedDoctorsModel> eachDoctors) {
        this.context = context;
        this.eachDoctors = eachDoctors;
    }

    @Override
    public int getCount() {
        return eachDoctors.size();
    }

    @Override
    public Object getItem(int position) {
        return eachDoctors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_saved_doctors_fragment, null);
        }

        NetworkImageView itemNImageView = (NetworkImageView) convertView.findViewById(R.id.saved_doctors_niv);
        if (eachDoctors.get(position).getImage().length()>1) {
            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + eachDoctors.get(position).getImage();
            itemNImageView.setImageUrl(meta_url, imageLoader);
        }

        TextView nameTextView = (TextView) convertView.findViewById(R.id.saved_doctors_name_tv);
        TextView degreeTextView = (TextView) convertView.findViewById(R.id.saved_doctors_degree_tv);
        TextView specialityTextView = (TextView) convertView.findViewById(R.id.saved_doctors_speciality_tv);

        if (eachDoctors.get(position).getUser_group_id().equals("2")) {
            nameTextView.setText("Dr. "+eachDoctors.get(position).getFirst_name() + " " + eachDoctors.get(position).getLast_name());
        }
        else {
            nameTextView.setText(eachDoctors.get(position).getFirst_name() + " " + eachDoctors.get(position).getLast_name());
        }

        degreeTextView.setText(eachDoctors.get(position).getDegree());

        List<SpecialityModel> specialityModels = eachDoctors.get(position).getSpecialityModels();

        String specialities = "";
        for (int c = 0; c < specialityModels.size(); ++c) {
            if (c != 0) {
                specialities = specialities + ", ";
            }
            specialities = specialities + specialityModels.get(c).getName();
        }
        specialityTextView.setText(specialities);

        return convertView;
    }
}
