package com.theuserhub.jolpie.AndroidRTC;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Adapters.ConversationAdapter;
import com.theuserhub.jolpie.Databases.ChattingSession;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.MessagesModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ChattingActivity extends AppCompatActivity {

    private Context context = this;
    private String TAG = "ChattingActivity", tag_string_req = "chatting_str_req";

    private EditText msgEditText;
    private ImageView msgSendImageView;
    private ListView conversationListView;
    private List<MessagesModel> eachItems;
    private ConversationAdapter conversationAdapter;
    private String to_user_id,to_user_fname,to_user_lname,to_user_photo,to_user_group_id;
    private Timer timer;
    private ProgressDialog pDialog;
    private ChattingSession chattingSession;

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Messages");

        Intent intent = getIntent();
        to_user_id = intent.getStringExtra("to_user_id");
        to_user_fname = intent.getStringExtra("first_name");
        to_user_lname = intent.getStringExtra("last_name");
        to_user_photo = intent.getStringExtra("photo");
        to_user_group_id = intent.getStringExtra("user_group_id");

        if (to_user_group_id.equals("2")) {
            getSupportActionBar().setTitle("Dr. "+to_user_fname+" "+to_user_lname);
        }
        else {
            getSupportActionBar().setTitle(to_user_fname + " " + to_user_lname);
        }

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        chattingSession = new ChattingSession(context);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        conversationListView = (ListView) findViewById(R.id.conversations_lv);
        eachItems = new ArrayList<MessagesModel>();
        conversationAdapter = new ConversationAdapter(context,eachItems,appUserId,appUserImgUrl,to_user_photo);
        conversationListView.setAdapter(conversationAdapter);

        timer = new Timer();

        sendCallrequest();
        //getAllMessages(to_user_id);

        //conversationListView.setSelection(eachItems.size()-1);
        // listview.setSelection(listview.getAdapter().getCount()-1);

        msgEditText = (EditText) findViewById(R.id.conversation_et);
        msgSendImageView = (ImageView) findViewById(R.id.conversation_send_iv);

        msgSendImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg_text = msgEditText.getText().toString();
                if (msg_text.length()>0) {
                    AppController.getInstance().cancelPendingRequests(tag_string_req);
                    conversationListView.setSelection(eachItems.size()-1);
                    sendMsgRequest(msg_text);
                    msgEditText.setText("");
                }
            }
        });
    }

    private void getAllMessages(final String to_user_id){
        pDialog.setMessage("Updating...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL)+"getAllMessages";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                eachItems.clear();
                Log.d("getConversations", response.toString());
                try {
                    JSONArray allMsg = new JSONArray(response);
                    for (int i=0;i<allMsg.length();++i) {
                        JSONObject singleMsg = (JSONObject) allMsg.get(i);
                        String msg_id = singleMsg.getString("id");
                        String msg_from_id = singleMsg.getString("from_user_id");
                        String msg_to_id = singleMsg.getString("to_user_id");
                        String msg_text = singleMsg.getString("message");
                        String msg_ceated_at = singleMsg.getString("created_at");

                        eachItems.add(new MessagesModel(msg_id,msg_from_id,msg_to_id,msg_text,msg_ceated_at));

                        if (i == allMsg.length()-1) {
                            chattingSession.setMaxChatId(msg_id);
                        }
                    }
                    //changed adapter
                    conversationAdapter.notifyDataSetChanged();
                    //conversationListView.setSelection(eachItems.size()-1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        Log.d(TAG, "getConversation called");
                        getNewMessages();
                    }
                }, 10000, 5000);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("getConversations", "Error: " + error.getMessage());
                pDialog.dismiss();
                Toast.makeText(context,"Error: Try again.",Toast.LENGTH_LONG).show();
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_user_id", appUserId);
                params.put("to_user_id", to_user_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void sendMsgRequest(final String msg_text) {
        //pDialog.setMessage("Sending...");
        //pDialog.show();
        AppController.getInstance().cancelPendingRequests(tag_string_req);
        String url = context.getResources().getString(R.string.MAIN_URL)+"saveNewMessage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //pDialog.dismiss();
                /*msgEditText.setText("");
                Log.d("saveMsg", response.toString());
                if (response.toString().trim().equals("ok")) {
                    getConversation(to_user_id);
                }*/
                try {
                    JSONArray allMsg = new JSONArray(response);
                    for (int i=0;i<allMsg.length();++i) {
                        JSONObject singleMsg = (JSONObject) allMsg.get(i);
                        String msg_id = singleMsg.getString("id");
                        String msg_from_id = singleMsg.getString("from_user_id");
                        String msg_to_id = singleMsg.getString("to_user_id");
                        String msg_text = singleMsg.getString("message");
                        String msg_ceated_at = singleMsg.getString("created_at");

                        eachItems.add(new MessagesModel(msg_id,msg_from_id,msg_to_id,msg_text,msg_ceated_at));

                        if (i == allMsg.length()-1) {
                            chattingSession.setMaxChatId(msg_id);
                        }
                    }
                    //changed adapter
                    conversationAdapter.notifyDataSetChanged();
                    //conversationListView.setSelection(eachItems.size()-1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //pDialog.dismiss();
                VolleyLog.d("saveMsg", "Error: " + error.getMessage());
                Toast.makeText(context, "Message not sent. Try again later.", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_user_id", appUserId);
                params.put("to_user_id", to_user_id);
                params.put("message", msg_text);
                params.put("conversation_id", "0");
                params.put("max_id", chattingSession.getMaxChatId());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getNewMessages() {
        String url = context.getResources().getString(R.string.MAIN_URL)+"getNewMessage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //pDialog.dismiss();
                /*msgEditText.setText("");
                Log.d("saveMsg", response.toString());
                if (response.toString().trim().equals("ok")) {
                    getConversation(to_user_id);
                }*/
                try {
                    JSONArray allMsg = new JSONArray(response);
                    if (allMsg.length()>0) {
                        for (int i = 0; i < allMsg.length(); ++i) {
                            JSONObject singleMsg = (JSONObject) allMsg.get(i);
                            String msg_id = singleMsg.getString("id");
                            String msg_from_id = singleMsg.getString("from_user_id");
                            String msg_to_id = singleMsg.getString("to_user_id");
                            String msg_text = singleMsg.getString("message");
                            String msg_ceated_at = singleMsg.getString("created_at");

                            eachItems.add(new MessagesModel(msg_id, msg_from_id, msg_to_id, msg_text, msg_ceated_at));

                            if (i == allMsg.length() - 1) {
                                chattingSession.setMaxChatId(msg_id);
                            }
                        }
                        //changed adapter
                        conversationAdapter.notifyDataSetChanged();
                    }
                    //conversationListView.setSelection(eachItems.size()-1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //pDialog.dismiss();
                VolleyLog.d("saveMsg", "Error: " + error.getMessage());
                Toast.makeText(context, "Not Updated.", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_user_id", appUserId);
                params.put("to_user_id", to_user_id);
                params.put("max_id", chattingSession.getMaxChatId());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void sendCallrequest(){
        pDialog.setMessage("Wait...");
        pDialog.show();
        String url = context.getResources().getString(R.string.MAIN_URL)+"callRealTime";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "call: " + response.toString());
                pDialog.dismiss();
                getAllMessages(to_user_id);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_user_id", appUserId);
                params.put("to_user_id", to_user_id);
                params.put("type", "text");
                params.put("conversation_room", "");
                params.put("purpose", "Text Chat");
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }
}
