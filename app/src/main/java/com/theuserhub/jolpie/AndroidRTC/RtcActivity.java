package com.theuserhub.jolpie.AndroidRTC;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.MediaStream;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoRendererGui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RtcActivity extends Activity implements WebRtcClient.RtcListener {

    private Context context = this;
    private String TAG = "RtcActivity", tag_string_req = "rtc_string_req";

    private final static int VIDEO_CALL_SENT = 666;
    private static final String VIDEO_CODEC_VP9 = "VP9";
    private static final String AUDIO_CODEC_OPUS = "opus";
    // Local preview screen position before call is connected.
    private static final int LOCAL_X_CONNECTING = 0;
    private static final int LOCAL_Y_CONNECTING = 0;
    private static final int LOCAL_WIDTH_CONNECTING = 100;
    private static final int LOCAL_HEIGHT_CONNECTING = 100;
    // Local preview screen position after call is connected.
    private static final int LOCAL_X_CONNECTED = 72;
    private static final int LOCAL_Y_CONNECTED = 72;
    private static final int LOCAL_WIDTH_CONNECTED = 25;
    private static final int LOCAL_HEIGHT_CONNECTED = 25;
    // Remote video screen position
    private static final int REMOTE_X = 0;
    private static final int REMOTE_Y = 0;
    private static final int REMOTE_WIDTH = 100;
    private static final int REMOTE_HEIGHT = 100;
    private VideoRendererGui.ScalingType scalingType = VideoRendererGui.ScalingType.SCALE_ASPECT_FILL;
    private GLSurfaceView vsv;
    private VideoRenderer.Callbacks localRender;
    private VideoRenderer.Callbacks remoteRender;
    private WebRtcClient client;
    private String mSocketAddress;
    private String callerId; //= "asdlkfjsldkiwer654";
    private String user_id,flag, callType, purposeString, user_fname,user_lname, user_photo, user_group_id;
    private TextView cancelCallTextView, speakerTextView, userNameTextView, userNameVideoTextView;
    private AudioManager audioManager;
    private LinearLayout audioCallLayout;
    private NetworkImageView userNetworkImageView;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(
                LayoutParams.FLAG_FULLSCREEN
                        | LayoutParams.FLAG_KEEP_SCREEN_ON
                        | LayoutParams.FLAG_DISMISS_KEYGUARD
                        | LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(R.layout.activity_video_call);

        mSocketAddress = "https://" + getResources().getString(R.string.host);
        mSocketAddress += (":" + getResources().getString(R.string.port) + "/");

        userDatabaseHandler = new UserDatabaseHandler(context);
        HashMap<String, String> user = userDatabaseHandler.getUserDetails();

        appUserFName = user.get("first_name");
        appUserLName = user.get("last_name");
        appUserEmail = user.get("email");
        appUserId = user.get("user_id");
        appUserImgUrl = user.get("user_image_url");
        appUserGroupId = user.get("user_group_id");

        user_id = getIntent().getStringExtra("user_id");
        flag = getIntent().getStringExtra("flag");
        callType = getIntent().getStringExtra("type");

        if (flag.equals("1")) {
            callerId = getIntent().getStringExtra("conversation_room");
        }
        else {
            purposeString = getIntent().getStringExtra("purpose");
        }

        Log.d("CallType", callType);
        //Toast.makeText(context, ""+callType, Toast.LENGTH_SHORT).show();

        Typeface onlineFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");

        audioCallLayout = (LinearLayout) findViewById(R.id.audio_call_layout);
        userNetworkImageView = (NetworkImageView) findViewById(R.id.user_niv);
        userNameTextView = (TextView) findViewById(R.id.user_name_tv);
        userNameVideoTextView = (TextView) findViewById(R.id.user_name_video_tv);
        speakerTextView = (TextView) findViewById(R.id.speaker_tv);
        speakerTextView.setTypeface(onlineFont);
        cancelCallTextView = (TextView) findViewById(R.id.cancel_call_tv);
        cancelCallTextView.setTypeface(onlineFont);

        vsv = (GLSurfaceView) findViewById(R.id.glview_call);
        vsv.setPreserveEGLContextOnPause(true);
        vsv.setKeepScreenOn(true);

        if (callType.equals("audio") || callType.equals("service")) {
            audioCallLayout.setVisibility(View.VISIBLE);
        }
        else {
            audioCallLayout.setVisibility(View.GONE);
            userNameVideoTextView.setVisibility(View.VISIBLE);
        }

        getUserInfo();

        VideoRendererGui.setView(vsv, new Runnable() {
            @Override
            public void run() {
                init();
            }
        });

        // local and remote render
        remoteRender = VideoRendererGui.create(
                REMOTE_X, REMOTE_Y,
                REMOTE_WIDTH, REMOTE_HEIGHT, scalingType, false);
        localRender = VideoRendererGui.create(
                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING,
                LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING, scalingType, true);

        final Intent intent = getIntent();
        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            callerId = segments.get(0);
        }

        audioManager = (AudioManager)getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_IN_CALL);

        cancelCallTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(client != null) {
                    client.onDestroy();
                    //client = null;
                }*/
                finish();
            }
        });
        audioManager.setMode(AudioManager.MODE_IN_CALL);

        speakerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (audioManager.isSpeakerphoneOn()) {
                    audioManager.setSpeakerphoneOn(false);
                    speakerTextView.setText(R.string.icon_audio);
                }
                else {
                    audioManager.setSpeakerphoneOn(true);
                    speakerTextView.setText(R.string.icon_audio_cancel);
                }
            }
        });
    }

    private void init() {
        Point displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);
        PeerConnectionParameters params;
        if (callType.equals("audio") || callType.equals("service")) {
            params = new PeerConnectionParameters(
                    false, false, displaySize.x, displaySize.y, 30, 1, VIDEO_CODEC_VP9, true, 1, AUDIO_CODEC_OPUS, true);
        }
        else {
            params = new PeerConnectionParameters(
                    true, false, displaySize.x, displaySize.y, 30, 1, VIDEO_CODEC_VP9, true, 1, AUDIO_CODEC_OPUS, true);
        }
        /*PeerConnectionParameters params = new PeerConnectionParameters(
                true, false, displaySize.x, displaySize.y, 30, 1, VIDEO_CODEC_VP9, true, 1, AUDIO_CODEC_OPUS, true);*/

        client = new WebRtcClient(context, this, mSocketAddress, params, VideoRendererGui.getEGLContext(),callType);
    }

    @Override
    public void onPause() {
        super.onPause();
        vsv.onPause();
        if(client != null) {
            client.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        vsv.onResume();
        if(client != null) {
            client.onResume();
        }
    }

    @Override
    public void onDestroy() {

        if(client != null) {
            //client = null;
            client.onDestroy();
            try {
                android.os.Process.killProcess(android.os.Process.myPid());
            }catch (Exception e) {

            }

            //android.os.Process.killProcess(android.os.Process.myPid());
        }
        super.onDestroy();
    }

    @Override
    public void onCallReady(String callId) {
        if (callerId != null) {
            try {
                answer(callerId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            call(callId);
        }

        //Toast.makeText(context,"sdfsdfsdfsdfs",Toast.LENGTH_LONG).show();
    }

    public void answer(String callerId) throws JSONException {
        client.sendMessage(callerId, "init", null);
        startCam();
    }

    public void call(String callId) {
        //Intent msg = new Intent(Intent.ACTION_SEND);
       // msg.putExtra(Intent.EXTRA_TEXT, mSocketAddress + callId);
        //msg.setType("text/plain");
        //startActivityForResult(Intent.createChooser(msg, "Call someone :"), VIDEO_CALL_SENT);
        //if (flag.equals("0")) {
        sendCallRequest(callId);
        startCam();
        //}

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIDEO_CALL_SENT) {
            startCam();
        }
    }

    public void startCam() {

        audioManager.setSpeakerphoneOn(true);
        // Camera settings
        client.start("android_test");
    }

    //public void start

    @Override
    public void onStatusChanged(final String newStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), newStatus, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onLocalStream(MediaStream localStream) {
        /*if (!callType.equals("audio") && !callType.equals("service")) {
            localStream.videoTracks.get(0).addRenderer(new VideoRenderer(localRender));
        }*/
        if (callType.equalsIgnoreCase("video")) {
            localStream.videoTracks.get(0).addRenderer(new VideoRenderer(localRender));
        }
        VideoRendererGui.update(localRender,
                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING,
                LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING,
                scalingType,false);

        //VideoRendererGui.update(localRender,LOCAL_X_CONNECTING,LOCAL_Y_CONNECTING);
    }

    @Override
    public void onAddRemoteStream(MediaStream remoteStream, int endPoint) {
        /*if (!callType.equals("audio") && !callType.equals("service")) {
            remoteStream.videoTracks.get(0).addRenderer(new VideoRenderer(remoteRender));
        }*/
        if (callType.equalsIgnoreCase("video")) {
            remoteStream.videoTracks.get(0).addRenderer(new VideoRenderer(remoteRender));
        }
        VideoRendererGui.update(remoteRender,
                REMOTE_X, REMOTE_Y,
                REMOTE_WIDTH, REMOTE_HEIGHT, scalingType,false);
        VideoRendererGui.update(localRender,
                LOCAL_X_CONNECTED, LOCAL_Y_CONNECTED,
                LOCAL_WIDTH_CONNECTED, LOCAL_HEIGHT_CONNECTED,
                scalingType,false);
    }

    @Override
    public void onRemoveRemoteStream(int endPoint) {
        VideoRendererGui.update(localRender,
                LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING,
                LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING,
                scalingType,false);
    }

    private void sendCallRequest(final String callId) {
        Log.e("Service",callType);
        //Toast.makeText(context, "send call", Toast.LENGTH_SHORT).show();
        String url = context.getResources().getString(R.string.MAIN_URL)+"callRealTime";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "call: " + response.toString());
                //Toast.makeText(context, "send call"+response.toString(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "send call err"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_user_id", appUserId);
                params.put("to_user_id", user_id);
                params.put("type", callType);
                params.put("conversation_room", callId);
                params.put("purpose", purposeString);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getUserInfo() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getUserGeneralInfo";

        final StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "receive: " + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    user_fname = jsonObject.getString("first_name");
                    user_lname = jsonObject.getString("last_name");
                    user_photo = jsonObject.getString("photo");
                    user_group_id = jsonObject.getString("user_group_id");

                    if (callType.equals("video")) {
                        if (user_group_id.equals("2")) {
                            userNameVideoTextView.setText("Dr. "+user_fname+" "+user_lname);
                        }
                        else {
                            userNameVideoTextView.setText(user_fname+" "+user_lname);
                        }
                    }
                    else {
                        if (imageLoader == null) {
                            imageLoader = AppController.getInstance().getImageLoader();
                        }
                        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + user_photo;
                        userNetworkImageView.setImageUrl(meta_url, imageLoader);

                        if (user_group_id.equals("2")) {
                            userNameTextView.setText("Dr. "+user_fname+" "+user_lname);
                        }
                        else {
                            userNameTextView.setText(user_fname+" "+user_lname);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //pDialog.dismiss();
                //finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}