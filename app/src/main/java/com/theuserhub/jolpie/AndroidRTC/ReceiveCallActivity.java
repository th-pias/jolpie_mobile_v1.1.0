package com.theuserhub.jolpie.AndroidRTC;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.RequiresPermission;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.ConversationActivity;
import com.theuserhub.jolpie.Activity.MainActivity;
import com.theuserhub.jolpie.AndroidRTC.RtcActivity;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Services.ReceiveCallService;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ReceiveCallActivity extends AppCompatActivity {
    private Context context = this;
    private String TAG = "ReceiveCallActivity", tag_string_req = "receive_str_request";

    private TextView answerTextView, cancelTextView, userNameTextView, callingTextView;
    private String response, id, type, from_user_id, to_user_id, to_user_group_id, conversation_room, status, duration, time, created_at, first_name, photo, gender, call_status, call_time;
    private NetworkImageView userNetworkImageView;
    private ImageView userImageView;
    private MediaPlayer mediaPlayer;
    private Timer callHoldTimer;

    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_receive_call);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        response = getIntent().getStringExtra("response");
        try {
            JSONObject jsonObject = new JSONObject(response);
            id = jsonObject.getString("id");
            type = jsonObject.getString("type");
            from_user_id = jsonObject.getString("from_user_id");
            to_user_id = jsonObject.getString("to_user_id");
            to_user_group_id = jsonObject.getString("user_group_id");
            conversation_room = jsonObject.getString("conversation_room");
            status = jsonObject.getString("status");
            duration = jsonObject.getString("duration");
            time = jsonObject.getString("time");
            created_at = jsonObject.getString("created_at");
            first_name = jsonObject.getString("first_name");
            photo = jsonObject.getString("photo");
            gender = jsonObject.getString("gender");

        } catch (JSONException e) {
            e.printStackTrace();
            finish();
        }

        Log.e("CallType", type);

        stopService(new Intent(context, ReceiveCallService.class));

        callingTextView = (TextView) findViewById(R.id.calling_tv);
        userNameTextView = (TextView) findViewById(R.id.user_name_tv);
        userNetworkImageView = (NetworkImageView) findViewById(R.id.user_niv);
        answerTextView = (TextView) findViewById(R.id.answer_call_tv);
        cancelTextView = (TextView) findViewById(R.id.cancel_call_tv);

        final Typeface callReceiveFont = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        answerTextView.setTypeface(callReceiveFont);
        cancelTextView.setTypeface(callReceiveFont);


        if (type.equals("text")) {
            answerTextView.setText(R.string.icon_comments);
            cancelTextView.setText(R.string.icon_comments);
            callingTextView.setText("Text calling... ");
        } else if (type.equals("audio")) {
            answerTextView.setText(R.string.icon_phone);
            cancelTextView.setText(R.string.icon_phone);
            callingTextView.setText("Audio calling... ");
        } else if (type.equals("service")) {
            answerTextView.setText(R.string.icon_phone);
            cancelTextView.setText(R.string.icon_phone);
            callingTextView.setText("24/7 calling... ");
        }else {
            answerTextView.setText(R.string.icon_video_camera);
            cancelTextView.setText(R.string.icon_video_camera);
            callingTextView.setText("Video calling... ");
        }

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        String meta_url = context.getResources().getString(R.string.UPLOADS_URL) + photo;
        userNetworkImageView.setImageUrl(meta_url, imageLoader);

        userNameTextView.setText(first_name);

        answerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callReceiveRequest();
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(context,MainActivity.class);
                //startActivity(intent);
                //finish();
                cancelCallRequest();
            }
        });

        mediaPlayer = MediaPlayer.create(this, R.raw.ring);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

            }
        });
        mediaPlayer.start();


    }

    private void callReceiveRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "receiveCall";

        final StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "receive: " + response.toString());
                conversation_room = response.toString().trim();
                Log.e("CallType2",type);
                Log.e("room",conversation_room);
                callHoldTimer.cancel();
                AppController.getInstance().cancelPendingRequests(tag_string_req);
                //mediaPlayer.stop();
                if (!type.equals("text")) {
                    Intent intent = new Intent(context, RtcActivity.class);
                    intent.putExtra("user_id", from_user_id);
                    intent.putExtra("flag", "1");
                    intent.putExtra("conversation_room", conversation_room);
                    intent.putExtra("type", type);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(context, ChattingActivity.class);
                    intent.putExtra("to_user_id", from_user_id);
                    intent.putExtra("first_name", first_name);
                    intent.putExtra("last_name", "");
                    intent.putExtra("photo", photo);
                    intent.putExtra("user_group_id", to_user_group_id);//user_group_id);
                    startActivity(intent);
                }
                answerTextView.setVisibility(View.GONE);
                cancelTextView.setVisibility(View.GONE);
                callingTextView.setText("Call from");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //pDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("conversation_id", id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getCallStatus() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getCallStatus";

        final StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "receive: " + response.toString());
                call_status = response.toString();
                if (call_status.equals("")) {
                    finish();
                } else {
                    if (call_status.equalsIgnoreCase("calling")) {
                        Toast.makeText(context,"on call",Toast.LENGTH_SHORT).show();
                    } else if (call_status.equals("receive")) {
                        Toast.makeText(context,"on receive",Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (call_status.equals("cancel")) {
                        Toast.makeText(context,"on cancel",Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (call_status.equals("expired")) {
                        Toast.makeText(context,"on expired",Toast.LENGTH_SHORT).show();
                        disconnectCallRequest();
                    } else {
                        Toast.makeText(context,"on disconnect",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //pDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("conversation_id", id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void cancelCallRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "cancelCall";

        final StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "receive: " + response.toString());
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //pDialog.dismiss();
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("conversation_id", id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void disconnectCallRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "disconnectCall";

        final StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "receive: " + response.toString());
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //pDialog.dismiss();
                finish();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("conversation_id", id);
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //mediaPlayer.stop();
        callHoldTimer.cancel();
        AppController.getInstance().cancelPendingRequests(tag_string_req);
        mediaPlayer.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        callHoldTimer = new Timer();
        callHoldTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "getNotificationCount called");
                getCallStatus();
            }
        }, 7000, 7000);
    }

    @Override
    protected void onDestroy() {
        startService(new Intent(context, ReceiveCallService.class));
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancelCallRequest();
    }
}
