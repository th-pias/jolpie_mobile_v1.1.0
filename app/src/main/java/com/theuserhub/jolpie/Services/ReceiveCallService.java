package com.theuserhub.jolpie.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.AndroidRTC.ReceiveCallActivity;
import com.theuserhub.jolpie.Databases.NotificationSession;
import com.theuserhub.jolpie.Databases.SessionManager;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Volley.app.AppController;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Admin on 4/27/2016.
 */
public class ReceiveCallService extends Service {
    private Context context;
    private String TAG = "ReceiveCallService", tag_string_req = "rec_call_ser_str_req";
    private int uniqueNumber = 0;
    private SessionManager session;
    private NotificationSession notificationSession;
    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    private Timer timer;

    public ReceiveCallService() {
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Call Service","Called");
        context = getApplicationContext();
        session = new SessionManager(context);
        final Handler handler = new Handler();
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            if (session.isLoggedIn()) {
                                notificationSession = new NotificationSession(context);
                                userDatabaseHandler = new UserDatabaseHandler(context);
                                HashMap<String, String> user = userDatabaseHandler.getUserDetails();

                                appUserFName = user.get("first_name");
                                appUserLName = user.get("last_name");
                                appUserEmail = user.get("email");
                                appUserId = user.get("user_id");
                                appUserImgUrl = user.get("user_image_url");
                                appUserGroupId = user.get("user_group_id");

                                isAnyCallRequest();
                            }

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }

                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 10000, 10000); //execute in every 30000 ms
        // Let it continue running until it is stopped.
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("Service","Destroyed");
        timer.cancel();
    }

    private void isAnyCallRequest() {
        String url = context.getResources().getString(R.string.MAIN_URL) + "isAnyCall";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                if (!response.equals("null")) {
                    Toast.makeText(context, "Calling", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context,ReceiveCallActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("response",response);
                    startActivity(intent);
                } else {
                    //Toast.makeText(context,"No call",Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //prgDialog.hide();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
