package com.theuserhub.jolpie.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.theuserhub.jolpie.Activity.MainActivity;
import com.theuserhub.jolpie.Databases.NotificationSession;
import com.theuserhub.jolpie.Databases.SessionManager;
import com.theuserhub.jolpie.Databases.UserDatabaseHandler;
import com.theuserhub.jolpie.Models.NotificationModel;
import com.theuserhub.jolpie.R;
import com.theuserhub.jolpie.Utility.AppFunctions;
import com.theuserhub.jolpie.Volley.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Admin on 4/20/2016.
 */
public class NotificationService extends Service {
    private Context context;
    private String TAG = "NotificationService", tag_string_req = "not_ser_str_req";
    private int uniqueNumber = 0;
    private SessionManager session;
    private NotificationSession notificationSession;
    private UserDatabaseHandler userDatabaseHandler;
    private String appUserFName, appUserLName, appUserEmail, appUserId, appUserGroupId, appUserImgUrl;

    public NotificationService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = getApplicationContext();
        session = new SessionManager(context);
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            if (session.isLoggedIn()) {
                                notificationSession = new NotificationSession(context);
                                userDatabaseHandler = new UserDatabaseHandler(context);
                                HashMap<String, String> user = userDatabaseHandler.getUserDetails();

                                appUserFName = user.get("first_name");
                                appUserLName = user.get("last_name");
                                appUserEmail = user.get("email");
                                appUserId = user.get("user_id");
                                appUserImgUrl = user.get("user_image_url");
                                appUserGroupId = user.get("user_group_id");

                                getNotifyRequest(notificationSession.getNotificationId());
                            }

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }

                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 30000, 30000); //execute in every 30000 ms
        // Let it continue running until it is stopped.

        return START_STICKY;
    }

    private void getNotifyRequest(final String notification_id) {
        String url = context.getResources().getString(R.string.MAIN_URL) + "getUnseenNotification";

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Notify: " + response.toString());

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() <= 0) {
                    } else {

                        for (int i = 0; i < jsonArray.length(); ++i) {

                            JSONObject singleReview = (JSONObject) jsonArray.get(i);
                            String id, section, section_id, activity, activity_by_id, activity_by_fname, activity_by_lname, activity_by_photo, activity_by_group_id, seen, created_at;
                            id = singleReview.getString("id");
                            section = singleReview.getString("section");
                            section_id = singleReview.getString("section_id");
                            activity = singleReview.getString("activity");
                            activity_by_id = singleReview.getString("user_id");
                            activity_by_fname = singleReview.getString("first_name");
                            activity_by_lname = singleReview.getString("last_name");
                            activity_by_photo = singleReview.getString("photo");
                            activity_by_group_id = singleReview.getString("user_group_id");
                            seen = singleReview.getString("seen");
                            created_at = singleReview.getString("created_at");

                            if (i == 0) {
                                notificationSession.setNotificationId(id);
                            }

                            if (!section.equals("review")) {
                                NotificationModel notificationModel = new NotificationModel(id, section, section_id, activity, activity_by_id, activity_by_fname, activity_by_lname, activity_by_photo, activity_by_group_id, seen, created_at);

                                Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                NotificationCompat.Builder mBuilder = null;
                                /*mBuilder=
                                        (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                .setSmallIcon(R.mipmap.ic_launcher_border)
                                                //.setContentTitle("New post in " + AppFunctions.APP_NAME)
                                                .setContentText(activity)
                                                .setSound(soundUri);*/
                                String activity_by_user_name = "";
                                if (activity_by_group_id.equals("2")) {
                                    activity_by_user_name = "Dr. " + activity_by_fname + " " + activity_by_lname;
                                } else {
                                    activity_by_user_name = activity_by_fname + " " + activity_by_lname;
                                }

                                if (section.equals("post")) {
                                    if (activity.equals("post")) {

                                    } else if (activity.equals("liked")) {

                                        String likedString = activity_by_user_name + " liked your Post";

                                        StyleSpan textBoldStyle = new StyleSpan(android.graphics.Typeface.BOLD);

                                        Spannable wordtoSpan = new SpannableString(likedString);
                                        wordtoSpan.setSpan(textBoldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);

                                    } else if (activity.equals("commented")) {

                                        String commentedString = activity_by_user_name + " commented on your Post";

                                        StyleSpan textBoldStyle = new StyleSpan(android.graphics.Typeface.BOLD);

                                        Spannable wordtoSpan = new SpannableString(commentedString);
                                        wordtoSpan.setSpan(textBoldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);

                                    } else if (activity.equals("shared")) {

                                        String sharedString = activity_by_user_name + " shared your Post";

                                        StyleSpan textBoldStyle = new StyleSpan(android.graphics.Typeface.BOLD);

                                        Spannable wordtoSpan = new SpannableString(sharedString);
                                        wordtoSpan.setSpan(textBoldStyle, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);

                                    } else if (activity.equals("taged")) {
                                    }
                                } else if (section.equals("group")) {
                                    if (activity.equals("post")) {
                                        String sharedString = activity_by_user_name + " started a discussion";//+post_to_user_name;

                                        StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);

                                        Spannable wordtoSpan = new SpannableString(sharedString);
                                        wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);
                                    } else if (activity.equals("liked")) {
                                        String sharedString = activity_by_user_name + " liked a discussion";//+post_to_user_name;

                                        StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);

                                        Spannable wordtoSpan = new SpannableString(sharedString);
                                        wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);
                                    } else if (activity.equals("commented")) {
                                        String sharedString = activity_by_user_name + " commented to a discussion";//+post_to_user_name;

                                        StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                                        Spannable wordtoSpan = new SpannableString(sharedString);
                                        wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);
                                    } else if (activity.equals("shared")) {
                                        String sharedString = activity_by_user_name + " shared a discussion";//+post_to_user_name;

                                        StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                                        Spannable wordtoSpan = new SpannableString(sharedString);
                                        wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);
                                    } else if (activity.equals("taged")) {
                                    }
                                } else if (section.equals("magazine")) {
                                    if (activity.equals("post")) {
                                        String postedString = activity_by_user_name + " posted an article";//+post_to_user_name;

                                        StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                                        Spannable wordtoSpan = new SpannableString(postedString);
                                        wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);
                                    } else if (activity.equals("liked")) {
                                        String likedString = activity_by_user_name + " liked on an article";//+post_to_user_name;

                                        StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                                        Spannable wordtoSpan = new SpannableString(likedString);
                                        wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);
                                    } else if (activity.equals("commented")) {
                                        String commentedString = activity_by_user_name + " commented on an article";//+post_to_user_name;

                                        StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                                        Spannable wordtoSpan = new SpannableString(commentedString);
                                        wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(wordtoSpan)
                                                        .setSound(soundUri);
                                    } else if (activity.equals("shared")) {
                                        String sharedString = activity_by_user_name + " shared on an article";//+post_to_user_name;

                                        StyleSpan textBoldStyle1 = new StyleSpan(android.graphics.Typeface.BOLD);
                                        Spannable wordtoSpan = new SpannableString(sharedString);
                                        wordtoSpan.setSpan(textBoldStyle1, 0, (activity_by_user_name.length()), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                        mBuilder =
                                                (NotificationCompat.Builder) new NotificationCompat.Builder(NotificationService.this)
                                                        .setSmallIcon(R.drawable.small_logo_icon)
                                                        .setContentTitle("Jolpie")
                                                        .setContentText(activity)
                                                        .setSound(soundUri);
                                    } else if (activity.equals("taged")) {
                                    }
                                }

                                if (mBuilder != null) {
                                    mBuilder.setAutoCancel(true);

                                    Intent resultIntent = new Intent(NotificationService.this, MainActivity.class);

                                    //resultIntent.putExtra("post_id", post_id);
                                    //resultIntent.putExtra("cat_title", "New Post");
                                    //resultIntent.putExtra("post_id", pst_id);
                                    //resultIntent.putExtra("post_title", pst_ttl);
                                    //resultIntent.putExtra("post_content", pst_cntnt);
                                    //resultIntent.putExtra("post_date", pst_date);
                                    //resultIntent.putExtra("post_guid", pst_guid);

                                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                                    PendingIntent resultPendingIntent =
                                            PendingIntent.getActivity(
                                                    NotificationService.this,
                                                    uniqueNumber,
                                                    resultIntent,
                                                    0
                                            );


                                    mBuilder.setContentIntent(resultPendingIntent);
                                    //mBuilder.s

                                    NotificationManager mNotifyMgr =
                                            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                                    //mNotifyMgr.notify();
                                    mNotifyMgr.notify(uniqueNumber, mBuilder.build());
                                    ++uniqueNumber;
                                }
                            }
                        }
                        //makeSeen();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(context, "Posting error occured" + error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", appUserId);
                params.put("notification_id", notification_id);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
