package com.theuserhub.jolpie.Databases;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.theuserhub.jolpie.Models.SinglePostModel;

public class HomeDatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "jolpie";

	// Contacts table name
	private static final String TABLE_CONTACTS = "posts";

	// Contacts Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_POST_ID = "post_id";
	private static final String KEY_TITLE = "title";
	private static final String KEY_DESCRIPTION = "description";
	private static final String KEY_LIKES = "good_count";
	private static final String KEY_COMMENTS = "answer_count";
	private static final String KEY_SHARES = "follow_count";
	private static final String KEY_USER_ID = "user_id";
	private static final String KEY_GROUP_ID = "group_id";
	private static final String KEY_CREATED_AT = "created_at";
	private static final String KEY_UPDATED_AT = "updated_at";
	private static final String KEY_IS_DELETED = "is_deleted";
	private static final String KEY_FNAME = "first_name";
	private static final String KEY_LNAME = "last_name";
	private static final String KEY_IMG_URL = "photo";
	private static final String KEY_VDO_URL = "video";

	public HomeDatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_POST_ID + " TEXT," + KEY_TITLE + " TEXT,"
				+ KEY_DESCRIPTION + " TEXT," + KEY_LIKES + " TEXT,"
				+ KEY_COMMENTS + " TEXT," + KEY_SHARES + " TEXT,"
				+ KEY_USER_ID + " TEXT," + KEY_GROUP_ID + " TEXT,"
				+ KEY_CREATED_AT + " TEXT," + KEY_UPDATED_AT + " TEXT,"
				+ KEY_IS_DELETED + " TEXT," + KEY_FNAME + " TEXT,"
				+ KEY_LNAME + " TEXT," + KEY_IMG_URL + " TEXT" + KEY_VDO_URL + " TEXT" + ")";
		db.execSQL(CREATE_CONTACTS_TABLE);
		Log.d("Database", "database table created");
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new contact
	public void addContact(SinglePostModel post) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_POST_ID, post.getPost_id());
		values.put(KEY_TITLE, post.getPost_title());
		values.put(KEY_DESCRIPTION, post.getPost_description());
		values.put(KEY_LIKES, post.getPost_likes());
		values.put(KEY_COMMENTS, post.getPost_answers());
		values.put(KEY_SHARES, post.getPost_follows());
		values.put(KEY_USER_ID, post.getPost_userId());
		values.put(KEY_GROUP_ID, post.getPost_userGroupId());
		values.put(KEY_CREATED_AT, post.getPost_created());
		values.put(KEY_UPDATED_AT, post.getPost_updated());
		values.put(KEY_IS_DELETED, "0");
		values.put(KEY_FNAME, post.getPost_user_fname());
		values.put(KEY_LNAME, post.getPost_user_lname());
		values.put(KEY_IMG_URL, post.getPost_imgUrl());
		values.put(KEY_VDO_URL, post.getPost_vdoUrl());

		// Inserting Row
		db.insert(TABLE_CONTACTS, null, values);
		db.close(); // Closing database connection
	}

	public void deleteAll() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("delete from "+ TABLE_CONTACTS);
		db.close();
	}

	// Getting single contact
	SinglePostModel getContact(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID, KEY_POST_ID, KEY_TITLE, KEY_DESCRIPTION, KEY_LIKES,
						KEY_COMMENTS, KEY_SHARES, KEY_USER_ID, KEY_GROUP_ID, KEY_CREATED_AT, KEY_UPDATED_AT,
						KEY_IS_DELETED, KEY_FNAME, KEY_LNAME, KEY_IMG_URL, KEY_VDO_URL}, KEY_ID + "=?",
						new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		SinglePostModel post = new SinglePostModel(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2),
									cursor.getString(3), cursor.getString(4), cursor.getString(5),
									cursor.getString(6), cursor.getString(7), cursor.getString(8),
									cursor.getString(9), cursor.getString(10), cursor.getString(11),
									cursor.getString(12), cursor.getString(13), cursor.getString(14),
									cursor.getString(15), cursor.getString(16));
		// return contact
		return post;
	}
	
	// Getting All Contacts
	public List<SinglePostModel> getAllContacts() {
		List<SinglePostModel> contactList = new ArrayList<SinglePostModel>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				SinglePostModel post = new SinglePostModel(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2),
						cursor.getString(3), cursor.getString(4), cursor.getString(5),
						cursor.getString(6), cursor.getString(7), cursor.getString(8),
						cursor.getString(9), cursor.getString(10), cursor.getString(11),
						cursor.getString(12), cursor.getString(13), cursor.getString(14),
						cursor.getString(15), cursor.getString(16));
				// Adding contact to list
				contactList.add(post);
			} while (cursor.moveToNext());
		}

		// return contact list
		return contactList;
	}

	// Updating single contact
	public int updateContact(SinglePostModel post) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_POST_ID, post.getPost_id());
		values.put(KEY_TITLE, post.getPost_title());
		values.put(KEY_DESCRIPTION, post.getPost_description());
		values.put(KEY_LIKES, post.getPost_likes());
		values.put(KEY_COMMENTS, post.getPost_answers());
		values.put(KEY_SHARES, post.getPost_follows());
		values.put(KEY_USER_ID, post.getPost_userId());
		values.put(KEY_GROUP_ID, post.getPost_userGroupId());
		values.put(KEY_CREATED_AT, post.getPost_created());
		values.put(KEY_UPDATED_AT, post.getPost_updated());
		values.put(KEY_IS_DELETED, "0");
		values.put(KEY_FNAME, post.getPost_user_fname());
		values.put(KEY_LNAME, post.getPost_user_lname());
		values.put(KEY_IMG_URL, post.getPost_imgUrl());
		values.put(KEY_VDO_URL, post.getPost_vdoUrl());

		// updating row
		return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
				new String[] { String.valueOf(post.getPost_id()) });
	}

	// Deleting single contact
	public void deleteContact(SinglePostModel post) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
				new String[] { String.valueOf(post.getPost_id()) });
		db.close();
	}


	// Getting contacts Count
	public int getContactsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

}
