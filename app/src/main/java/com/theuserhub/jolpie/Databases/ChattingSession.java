package com.theuserhub.jolpie.Databases;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Admin on 4/26/2016.
 */
public class ChattingSession {
    // LogCat tag
    private static String TAG = "ChattingSession";

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "ChattingSession";

    private static final String KEY_CHAT_ID = "chatId";

    public ChattingSession(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setMaxChatId(String max_id) {

        editor.putString(KEY_CHAT_ID, max_id);

        // commit changes
        editor.commit();

        Log.d(TAG, "chatting id session modified!");
    }

    public String getMaxChatId(){
        return pref.getString(KEY_CHAT_ID, "0");
    }
}
